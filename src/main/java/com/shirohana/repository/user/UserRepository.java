package com.shirohana.repository.user;

import com.shirohana.entity.farm.Farm;
import com.shirohana.entity.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByUsernameAndDeletedFalse(String username);
    Optional<User> findByIdAndDeletedFalse(Long id);
    List<User> findAllByDeletedFalse();

    @Query("SELECT u.farms FROM User u WHERE u.username = :username AND u.deleted = false")
    List<Farm> findFarmsByUsername(@Param("username") String username);

}
