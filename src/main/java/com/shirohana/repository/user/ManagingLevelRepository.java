package com.shirohana.repository.user;

import com.shirohana.entity.user.ManagingLevel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ManagingLevelRepository extends JpaRepository<ManagingLevel,Long> {
    ManagingLevel findByLevel(int level);
}
