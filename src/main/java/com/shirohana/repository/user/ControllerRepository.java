package com.shirohana.repository.user;

import com.shirohana.entity.user.Controller;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ControllerRepository extends JpaRepository<Controller, Long> {
    Controller findByName(String name);
}
