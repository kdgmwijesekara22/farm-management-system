package com.shirohana.repository.user;

import com.shirohana.entity.user.RoleFarm;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RoleFarmRepository extends JpaRepository<RoleFarm, Long> {
    List<RoleFarm> findByRoleId(Long roleId);
    void deleteByRoleId(Long roleId);
}
