package com.shirohana.repository.user;

import com.shirohana.entity.user.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoleRepository extends JpaRepository<Role,Long> {
    Role findByName(String name);
    List<Role> findByNameIn(List<String> names);
    boolean existsByName(String name);

}
