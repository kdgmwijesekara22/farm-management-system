package com.shirohana.repository.farm;

import com.shirohana.entity.farm.Bay;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BayRepository extends JpaRepository<Bay, Long> {
    List<Bay> findByGreenhouseId(Long greenhouseId);

    Bay findByIdAndDeletedFalse(Long id);

    List<Bay> findByGreenhouseIdAndDeletedFalse(Long greenhouseId);

    Optional<Bay> findByNameAndGreenhouseIdAndDeletedFalse(String name, Long greenhouseId);
}
