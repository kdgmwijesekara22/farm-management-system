package com.shirohana.repository.farm;


import com.shirohana.entity.farm.Bed;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BedRepository extends JpaRepository<Bed, Long> {
    List<Bed> findByBayIdAndDeletedFalse(Long bayId);


    boolean existsByNameAndBayIdAndDeletedFalse(String name, Long bayId);

    @Query("SELECT b FROM Bed b WHERE b.id = :id AND b.deleted = false")
    Optional<Bed> findByIdAndDeletedFalse(@Param("id") Long id);
}
