package com.shirohana.repository.farm;

import com.shirohana.entity.farm.Farm;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface FarmRepository extends JpaRepository<Farm, Long> {
    List<Farm> findByDeletedFalse();
    Optional<Farm> findByNameAndDeletedFalse(String name);
    List<Farm> findByIdIn(List<Long> ids);
    Farm findByIdAndDeletedFalse(Long Id);
}
