package com.shirohana.repository.farm;


import com.shirohana.entity.farm.Farm;
import com.shirohana.entity.farm.Greenhouse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface GreenhouseRepository extends JpaRepository<Greenhouse, Long> {
    List<Greenhouse> findByFarmId(Long farmId);

    List<Greenhouse> findByFarmIdAndDeletedFalse(Long farmId);
    Greenhouse findByIdAndDeletedFalse(Long Id);

    Optional<Greenhouse> findByNameAndDeletedFalse(String name);

    boolean existsByFarmIdAndDeletedFalse(Long farmId);
}
