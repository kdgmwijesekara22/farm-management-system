
package com.shirohana.repository.planttasksetup;

import com.shirohana.entity.planttasksetup.HarvestingAndRetentionSetup;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface HarvestingAndRetentionSetupRepository extends JpaRepository<HarvestingAndRetentionSetup, Long> {

    Optional<HarvestingAndRetentionSetup> findByFamilyNameIdAndDeletedFalse(Long familyNameId);

    Optional<HarvestingAndRetentionSetup> findByIdAndDeletedFalse(Long id);

    List<HarvestingAndRetentionSetup> findAllByDeletedFalse();

}
