
package com.shirohana.repository.planttasksetup.maintenancetasksetup;


import com.shirohana.entity.planttasksetup.maintenancetasksetup.MaintenanceTaskSetup;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface MaintenanceTaskSetupRepository extends JpaRepository<MaintenanceTaskSetup, Long> {

    List<MaintenanceTaskSetup> findByDeletedFalse();
    Optional<MaintenanceTaskSetup> findByFamilyNameIdAndTaskNameIdAndDeletedFalse(Long familyNameId, Long taskNameId);

    List<MaintenanceTaskSetup> findByFamilyNameIdAndDeletedFalse(Long familyNameId);
}
