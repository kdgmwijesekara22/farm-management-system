package com.shirohana.repository.planttasksetup.maintenancetasksetup;

import com.shirohana.entity.planttasksetup.maintenancetasksetup.MaintenanceTSSubTask;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MaintenanceTSSubTaskRepository extends JpaRepository<MaintenanceTSSubTask, Long> {

    @Query("SELECT s FROM MaintenanceTSSubTask s WHERE s.productionId = :productionId")
    List<MaintenanceTSSubTask> findByProductionId(@Param("productionId") Long productionId);

    @Query("SELECT s FROM MaintenanceTSSubTask s WHERE s.nurseryId = :nurseryId")
    List<MaintenanceTSSubTask> findByNurseryId(@Param("nurseryId") Long nurseryId);
}
