package com.shirohana.repository.planttasksetup.maintenancetasksetup;

import com.shirohana.entity.planttasksetup.maintenancetasksetup.MaintenanceTSProd;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MaintenanceTSProdRepository extends JpaRepository<MaintenanceTSProd, Long> {
}
