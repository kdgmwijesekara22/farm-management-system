package com.shirohana.repository.planttasksetup.maintenancetasksetup;

import com.shirohana.entity.planttasksetup.maintenancetasksetup.MaintenanceTSNursery;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MaintenanceTSNurseryRepository extends JpaRepository<MaintenanceTSNursery, Long> {
}
