
package com.shirohana.repository.planttasksetup.monitoringtasksetup;

import com.shirohana.entity.planttasksetup.monitoringtasksetup.MonitoringTaskSetup;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface MonitoringTaskSetupRepository extends JpaRepository<MonitoringTaskSetup, Long> {

    List<MonitoringTaskSetup> findByDeletedFalse();

    Optional<MonitoringTaskSetup> findByFamilyNameIdAndTaskNameIdAndDeletedFalse(Long familyNameId, Long taskNameId);

    List<MonitoringTaskSetup> findByFamilyNameIdAndDeletedFalse(Long familyNameId);
}
