package com.shirohana.repository.planttasksetup.monitoringtasksetup;


import com.shirohana.entity.planttasksetup.monitoringtasksetup.MonitoringTSSubTask;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MonitoringTSSubTaskRepository extends JpaRepository<MonitoringTSSubTask,Long> {

    @Query("SELECT s FROM MonitoringTSSubTask s WHERE s.productionId = :productionId")
    List<MonitoringTSSubTask> findByProductionId(@Param("productionId") Long productionId);

    @Query("SELECT s FROM MonitoringTSSubTask s WHERE s.nurseryId = :nurseryId")
    List<MonitoringTSSubTask> findByNurseryId(@Param("nurseryId") Long nurseryId);
}
