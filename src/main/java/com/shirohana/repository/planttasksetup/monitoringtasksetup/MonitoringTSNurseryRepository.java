package com.shirohana.repository.planttasksetup.monitoringtasksetup;

import com.shirohana.entity.planttasksetup.maintenancetasksetup.MaintenanceTSNursery;
import com.shirohana.entity.planttasksetup.monitoringtasksetup.MonitoringTSNursery;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MonitoringTSNurseryRepository extends JpaRepository<MonitoringTSNursery, Long> {
}
