package com.shirohana.repository.planttasksetup.monitoringtasksetup;

import com.shirohana.entity.planttasksetup.monitoringtasksetup.MonitoringTSProd;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MonitoringTSProdRepository extends JpaRepository<MonitoringTSProd,Long> {
}
