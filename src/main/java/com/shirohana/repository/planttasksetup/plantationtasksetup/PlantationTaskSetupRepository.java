package com.shirohana.repository.planttasksetup.plantationtasksetup;

import com.shirohana.entity.planttasksetup.plantationtasksetup.PlantationTaskSetup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface PlantationTaskSetupRepository extends JpaRepository<PlantationTaskSetup, Long> {
    Optional<PlantationTaskSetup> findByIdAndDeletedFalse(Long id);
    List<PlantationTaskSetup> findByFamilyNameIdAndDeletedFalse(Long familyNameId);
    List<PlantationTaskSetup> findByDeletedFalse();
    Optional<PlantationTaskSetup> findByFamilyNameIdAndTaskNameAndDeletedFalse(Long familyNameId, String taskName);
}
