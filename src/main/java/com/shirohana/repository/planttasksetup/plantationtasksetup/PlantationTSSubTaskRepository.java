package com.shirohana.repository.planttasksetup.plantationtasksetup;


import com.shirohana.entity.planttasksetup.plantationtasksetup.PlantationTSSubTask;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PlantationTSSubTaskRepository extends JpaRepository<PlantationTSSubTask, Long> {
    List<PlantationTSSubTask> findByPlantationTaskSetupIdAndDeletedFalse(Long plantationTaskSetupId);


}
