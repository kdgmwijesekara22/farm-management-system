package com.shirohana.repository.planttasksetup.harvestingtasksetup;


import com.shirohana.entity.planttasksetup.harvestingtasksetup.HarvestingTaskSetup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface HarvestingTaskSetupRepository  extends JpaRepository<HarvestingTaskSetup, Long> {
    Optional<HarvestingTaskSetup> findByIdAndDeletedFalse(Long id);
    List<HarvestingTaskSetup> findByFamilyNameIdAndDeletedFalse(Long familyNameId);
    List<HarvestingTaskSetup> findByDeletedFalse();
    Optional<HarvestingTaskSetup> findByFamilyNameIdAndTaskNameAndDeletedFalse(Long familyNameId, String taskName);
}
