package com.shirohana.repository.planttasksetup.harvestingtasksetup;

import com.shirohana.entity.planttasksetup.harvestingtasksetup.HarvestingTSSubTask;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface HarvestingTSSubTaskRepository extends JpaRepository<HarvestingTSSubTask, Long> {
    List<HarvestingTSSubTask> findByHarvestingTaskSetupIdAndDeletedFalse(Long harvestingTaskSetupId);
}
