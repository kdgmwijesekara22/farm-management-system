package com.shirohana.repository.planttasksetup.qualitytasksetup;

import com.shirohana.entity.planttasksetup.qualitytasksetup.QualityTSSubTask;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QualityTSSubTaskRepository extends JpaRepository<QualityTSSubTask, Long> {
    List<QualityTSSubTask> findByQualityTaskSetupIdAndDeletedFalse(Long qualityTaskSetupId);
}
