package com.shirohana.repository.planttasksetup.qualitytasksetup;


import com.shirohana.entity.planttasksetup.qualitytasksetup.QualityTaskSetup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface QualityTaskSetupRepository extends JpaRepository<QualityTaskSetup, Long> {
    Optional<QualityTaskSetup> findByIdAndDeletedFalse(Long id);
    List<QualityTaskSetup> findByFamilyNameIdAndDeletedFalse(Long familyNameId);
    List<QualityTaskSetup> findByDeletedFalse();
    Optional<QualityTaskSetup> findByFamilyNameIdAndTaskNameAndDeletedFalse(Long familyNameId, String taskName);
}
