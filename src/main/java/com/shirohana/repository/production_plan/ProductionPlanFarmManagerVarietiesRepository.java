package com.shirohana.repository.production_plan;

import com.shirohana.entity.schedule.production_plan.ProductionPlanFarmManagerVarieties;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductionPlanFarmManagerVarietiesRepository extends JpaRepository<ProductionPlanFarmManagerVarieties, Long> {
    List<ProductionPlanFarmManagerVarieties> findByProdPlanFarmMgrIdAndDeletedFalse(Long productionPlanFarmManagerId);
}
