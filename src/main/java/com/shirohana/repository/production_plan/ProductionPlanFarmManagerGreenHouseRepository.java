package com.shirohana.repository.production_plan;

import com.shirohana.entity.schedule.production_plan.ProductionPlanFarmManagerGreenHouse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductionPlanFarmManagerGreenHouseRepository extends JpaRepository<ProductionPlanFarmManagerGreenHouse, Long> {
    List<ProductionPlanFarmManagerGreenHouse> findByProdPlanFarmMgrIdAndDeletedFalse(Long productionPlanFarmManagerId);

    List<ProductionPlanFarmManagerGreenHouse> findByGreenHouseIdAndDeletedFalse(Long greenhouseId);
}
