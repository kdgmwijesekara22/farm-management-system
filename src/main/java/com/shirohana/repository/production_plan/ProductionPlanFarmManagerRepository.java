package com.shirohana.repository.production_plan;

import com.shirohana.entity.schedule.production_plan.ProductionPlanFarmManager;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface ProductionPlanFarmManagerRepository extends JpaRepository<ProductionPlanFarmManager, Long> {

    @Query("SELECT ppm FROM ProductionPlanFarmManager ppm " +
            "JOIN PlantationSchedule ps ON ppm.plantationScheduleId = ps.id " +
            "WHERE ppm.deleted = false AND ps.expired = false")
    List<ProductionPlanFarmManager> findByDeletedFalse();

    @Query("SELECT CASE WHEN COUNT(ppm) > 0 THEN true ELSE false END FROM ProductionPlanFarmManager ppm " +
            "JOIN PlantationSchedule ps ON ppm.plantationScheduleId = ps.id " +
            "WHERE ppm.plantationPlanId = :id AND ppm.deleted = false AND ps.expired = false")
    boolean existsByPlantationPlanIdAndDeletedFalse(@Param("id") Long id);

    @Query("SELECT ppm FROM ProductionPlanFarmManager ppm " +
            "JOIN ProductionPlanFarmManagerGreenHouse ppgh ON ppm.id = ppgh.prodPlanFarmMgrId " +
            "JOIN PlantationSchedule ps ON ppm.plantationScheduleId = ps.id " +
            "WHERE ppm.deleted = false AND ps.expired = false AND ppgh.greenHouseId IN :farmIds")
    List<ProductionPlanFarmManager> findByFarmIdsAndDeletedFalse(@Param("farmIds") Set<Long> farmIds);

    @Query("SELECT ppm FROM ProductionPlanFarmManager ppm " +
            "JOIN PlantationSchedule ps ON ppm.plantationScheduleId = ps.id " +
            "WHERE ppm.farmId = :farmId AND ppm.deleted = false AND ps.expired = false")
    List<ProductionPlanFarmManager> findByFarmIdAndDeletedFalse(@Param("farmId") Long farmId);

    @Query("SELECT ppm FROM ProductionPlanFarmManager ppm " +
            "JOIN PlantationSchedule ps ON ppm.plantationScheduleId = ps.id " +
            "WHERE ppm.saved = true AND ppm.plantationPlanId IN :plantationPlanIds AND ppm.deleted = false AND ps.expired = false")
    List<ProductionPlanFarmManager> findBySavedTrueAndPlantationPlanIdIn(@Param("plantationPlanIds") List<Long> plantationPlanIds);

    @Query("SELECT ppm FROM ProductionPlanFarmManager ppm " +
            "JOIN ProductionPlanFarmManagerGreenHouse ppgh ON ppm.id = ppgh.prodPlanFarmMgrId " +
            "JOIN PlantationSchedule ps ON ppm.plantationScheduleId = ps.id " +
            "WHERE ppm.deleted = false AND ps.expired = false AND ppgh.greenHouseId = :greenhouseId AND ppm.id NOT IN " +
            "(SELECT pghm.productionPlanFarmManagerId FROM ProductionPlanGHManager pghm WHERE pghm.saved = true)")
    List<ProductionPlanFarmManager> findPendingByGreenhouseId(@Param("greenhouseId") Long greenhouseId);
}
