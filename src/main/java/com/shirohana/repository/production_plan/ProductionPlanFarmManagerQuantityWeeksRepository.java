package com.shirohana.repository.production_plan;

import com.shirohana.entity.schedule.production_plan.ProductionPlanFarmMangerQuantityWeeks;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductionPlanFarmManagerQuantityWeeksRepository extends JpaRepository<ProductionPlanFarmMangerQuantityWeeks, Long> {
    List<ProductionPlanFarmMangerQuantityWeeks> findByProdPlanFarmMgrGreenHouseTotalQtyVarietyIdAndDeletedFalse(Long productionPlanFarmManagerGreenHouseTotalQuantityVarietyId);
}
