package com.shirohana.repository.production_plan_green_house;

import com.shirohana.entity.schedule.production_plan_green_house.ProductionPlanGHMVarietyQuantity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductionPlanGreenHouseManagerVarietyQuantityRepository extends JpaRepository<ProductionPlanGHMVarietyQuantity, Long> {
    List<ProductionPlanGHMVarietyQuantity> findByProductionPlanGHMWeeksYearsId(Long productionPlanGHMWeeksYearsId);
}