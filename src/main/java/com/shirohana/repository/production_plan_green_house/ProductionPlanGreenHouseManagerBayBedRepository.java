package com.shirohana.repository.production_plan_green_house;

import com.shirohana.entity.schedule.production_plan_green_house.ProductionPlanGHMBayBed;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductionPlanGreenHouseManagerBayBedRepository extends JpaRepository<ProductionPlanGHMBayBed, Long> {
    List<ProductionPlanGHMBayBed> findByProductionPlanGHManagerIdAndDeletedFalse(Long productionPlanGHManagerId);
    List<ProductionPlanGHMBayBed> findByBedIdAndDeletedFalse(Long productionPlanGHManagerId);
}