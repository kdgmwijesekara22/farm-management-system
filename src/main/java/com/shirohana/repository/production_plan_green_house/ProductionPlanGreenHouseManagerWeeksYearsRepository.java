package com.shirohana.repository.production_plan_green_house;

import com.shirohana.entity.schedule.production_plan_green_house.ProductionPlanGHMWeeksYears;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductionPlanGreenHouseManagerWeeksYearsRepository extends JpaRepository<ProductionPlanGHMWeeksYears, Long> {
    List<ProductionPlanGHMWeeksYears> findByProductionPlanGHMBayBedId(Long productionPlanGHMBayBedId);
}