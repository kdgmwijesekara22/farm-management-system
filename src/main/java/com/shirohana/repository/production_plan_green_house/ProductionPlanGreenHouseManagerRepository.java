package com.shirohana.repository.production_plan_green_house;

import com.shirohana.entity.schedule.production_plan_green_house.ProductionPlanGHManager;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductionPlanGreenHouseManagerRepository extends JpaRepository<ProductionPlanGHManager, Long> {
    boolean existsByPlanName(String planName);
    boolean existsByGreenhouseIdAndProductionPlanFarmManagerId(Long greenhouseId, Long productionPlanFarmManagerId);
    List<ProductionPlanGHManager> findByGreenhouseIdAndDeletedFalse(Long greenhouseId);
    List<ProductionPlanGHManager> findByProductionPlanFarmManagerIdInAndSavedTrue(List<Long> productionPlanFarmManagerIds);
}
