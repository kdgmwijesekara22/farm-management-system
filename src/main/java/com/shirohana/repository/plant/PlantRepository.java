package com.shirohana.repository.plant;

import com.shirohana.entity.plant.Plant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PlantRepository extends JpaRepository<Plant, Long> {
    Optional<Plant> findByFamilyNameAndDeletedFalse(String familyName);
    List<Plant> findAllByDeletedFalse();
    boolean existsByIdAndDeletedFalse(Long id);
    Optional<Plant> findByIdAndDeletedFalse(Long id);

}
