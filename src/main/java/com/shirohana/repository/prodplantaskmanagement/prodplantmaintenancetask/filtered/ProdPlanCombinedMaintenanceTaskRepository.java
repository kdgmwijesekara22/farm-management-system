package com.shirohana.repository.prodplantaskmanagement.prodplantmaintenancetask.filtered;

import com.shirohana.entity.prodplantaskmanagement.prodplanmaintenancetask.filtered.ProdPlanCombinedMaintenanceTask;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProdPlanCombinedMaintenanceTaskRepository extends JpaRepository<ProdPlanCombinedMaintenanceTask, Long> {

    boolean existsByMasterDataIdAndSupervisorTaskIdAndProdPlanId(Long masterDataId, Long supervisorTaskId, Long prodPlanId);
}
