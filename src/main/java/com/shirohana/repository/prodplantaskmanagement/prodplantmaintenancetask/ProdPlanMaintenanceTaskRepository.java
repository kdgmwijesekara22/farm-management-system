package com.shirohana.repository.prodplantaskmanagement.prodplantmaintenancetask;

import com.shirohana.entity.prodplantaskmanagement.prodplanmaintenancetask.ProdPlanMaintenanceTask;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ProdPlanMaintenanceTaskRepository extends JpaRepository<ProdPlanMaintenanceTask, Long> {
    List<ProdPlanMaintenanceTask> findAllByDeletedFalse();
    Optional<ProdPlanMaintenanceTask> findByIdAndDeletedFalse(Long id);

    List<ProdPlanMaintenanceTask> findAllByProdPlanIdAndDeletedFalse(Long prodPlanId);
}
