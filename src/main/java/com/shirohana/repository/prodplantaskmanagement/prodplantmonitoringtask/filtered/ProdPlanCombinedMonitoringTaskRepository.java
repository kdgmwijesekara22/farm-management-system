package com.shirohana.repository.prodplantaskmanagement.prodplantmonitoringtask.filtered;

import com.shirohana.entity.prodplantaskmanagement.prodplanmonitoringtask.filtered.ProdPlanCombinedMonitoringTask;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProdPlanCombinedMonitoringTaskRepository extends JpaRepository<ProdPlanCombinedMonitoringTask, Long> {
    boolean existsByMasterDataIdAndSupervisorTaskIdAndProdPlanId(Long masterDataId, Long supervisorTaskId, Long prodPlanId);
}
