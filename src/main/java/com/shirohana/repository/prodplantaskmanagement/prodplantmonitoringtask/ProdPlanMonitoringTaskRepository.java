package com.shirohana.repository.prodplantaskmanagement.prodplantmonitoringtask;

import com.shirohana.entity.prodplantaskmanagement.prodplanmaintenancetask.ProdPlanMaintenanceTask;
import com.shirohana.entity.prodplantaskmanagement.prodplanmonitoringtask.ProdPlanMonitoringTask;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ProdPlanMonitoringTaskRepository extends JpaRepository<ProdPlanMonitoringTask, Long> {
    List<ProdPlanMonitoringTask> findAllByDeletedFalse();
    List<ProdPlanMonitoringTask> findAllByProdPlanIdAndDeletedFalse(Long prodPlanId);
    Optional<ProdPlanMonitoringTask> findByIdAndDeletedFalse(Long id);
}
