package com.shirohana.repository.shedule.pplan;

import com.shirohana.entity.schedule.plantation_plan.PlantationPlan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PlantationPlanRepository extends JpaRepository<PlantationPlan, Long> {

//    List<PlantationPlan> findByDeletedFalse();
//    List<PlantationPlan> findByPlantationScheduleIdAndDeletedFalse(Long plantationScheduleId);
//    List<PlantationPlan> findByIdInAndDeletedFalse(List<Long> ids);

    @Query("SELECT pp FROM PlantationPlan pp " +
            "JOIN PlantationSchedule ps ON pp.plantationScheduleId = ps.id " +
            "WHERE pp.deleted = false AND ps.expired = false")
    List<PlantationPlan> findByDeletedFalse();

    @Query("SELECT pp FROM PlantationPlan pp " +
            "JOIN PlantationSchedule ps ON pp.plantationScheduleId = ps.id " +
            "WHERE pp.plantationScheduleId = :plantationScheduleId " +
            "AND pp.deleted = false AND ps.expired = false")
    List<PlantationPlan> findByPlantationScheduleIdAndDeletedFalse(@Param("plantationScheduleId") Long plantationScheduleId);

    @Query("SELECT pp FROM PlantationPlan pp " +
            "JOIN PlantationSchedule ps ON pp.plantationScheduleId = ps.id " +
            "WHERE pp.id IN :ids " +
            "AND pp.deleted = false AND ps.expired = false")
    List<PlantationPlan> findByIdInAndDeletedFalse(@Param("ids") List<Long> ids);

    PlantationPlan findByIdAndDeletedFalse(Long id);
}
