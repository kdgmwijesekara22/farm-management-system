package com.shirohana.repository.shedule.pplan;

import com.shirohana.entity.schedule.plantation_plan.PlantationPlanQuantityWeeks;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PlantationPlanQuantityWeeksRepository extends JpaRepository<PlantationPlanQuantityWeeks, Long> {

    // Method to find all non-deleted PlantationPlanQuantityWeeks by PlantationPlanFarmTotalQuantityVarietyId
    List<PlantationPlanQuantityWeeks> findByPlantationPlanFarmTotalQuantityVarietyIdAndDeletedFalse(Long plantationPlanFarmTotalQuantityVarietyId);

    // Additional query methods can be defined here if needed
}
