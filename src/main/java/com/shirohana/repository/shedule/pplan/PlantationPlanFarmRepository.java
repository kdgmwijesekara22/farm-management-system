package com.shirohana.repository.shedule.pplan;

import com.shirohana.entity.schedule.plantation_plan.PlantationPlan;
import com.shirohana.entity.schedule.plantation_plan.PlantationPlanFarm;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface PlantationPlanFarmRepository extends JpaRepository<PlantationPlanFarm, Long> {
    List<PlantationPlanFarm> findByPlantationPlanIdAndDeletedFalse(Long plantationPlanId);

    List<PlantationPlanFarm> findByFarmIdInAndDeletedFalse(Set<Long> farmIds);

    List<PlantationPlanFarm> findByDeletedFalse();
    List<PlantationPlan> findByIdInAndDeletedFalse(List<Long> ids);
    List<PlantationPlanFarm> findByFarmIdAndDeletedFalse(Long farmId);
    boolean existsByFarmIdAndDeletedFalse(Long id);

}
