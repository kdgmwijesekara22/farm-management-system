package com.shirohana.repository.shedule.pplan;

import com.shirohana.entity.schedule.plantation_plan.PlantationPlanFarmTotalQuantityVariety;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PlantationPlanFarmQuantityVarietyRepository extends JpaRepository<PlantationPlanFarmTotalQuantityVariety, Long> {
    List<PlantationPlanFarmTotalQuantityVariety> findByPlantationPlanFarmIdAndDeletedFalse(Long plantationPlanFarmId);
    List<PlantationPlanFarmTotalQuantityVariety> findByPlantationPlanFarmIdInAndDeletedFalse(List<Long> plantationPlanFarmIds);
    List<PlantationPlanFarmTotalQuantityVariety> findByPlantationPlanFarmIdAndVarietyIdAndDeletedFalse(Long plantationPlanFarmId, Long varietyId);

}