package com.shirohana.repository.shedule.pschedule;

import com.shirohana.entity.schedule.plantation_schedule.PlantationSchedule;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface PlantationScheduleRepository extends JpaRepository<PlantationSchedule, Long> {

    List<PlantationSchedule> findByDeletedFalse();
    Optional<PlantationSchedule> findByNameAndDeletedFalse(String name);
    PlantationSchedule findByIdAndDeletedFalse(Long id);
    boolean existsByFamilyIdAndDeletedFalse(Long id);

}
