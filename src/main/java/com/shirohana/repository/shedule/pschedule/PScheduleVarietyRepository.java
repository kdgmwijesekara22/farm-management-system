package com.shirohana.repository.shedule.pschedule;

import com.shirohana.entity.schedule.plantation_schedule.PlantationScheduleVariety;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PScheduleVarietyRepository extends JpaRepository<PlantationScheduleVariety, Long> {
    List<PlantationScheduleVariety> findByPlantationScheduleIdAndDeletedFalse(Long plantationScheduleId);

    // Updated method to return a List instead of a single result
    List<PlantationScheduleVariety> findByVerityId(Long verityId);
    List<PlantationScheduleVariety> findByPlantationScheduleIdAndVerityId(Long plantationScheduleId, Long verityId);



}
