package com.shirohana.repository.shedule.pschedule;

import com.shirohana.entity.schedule.plantation_schedule.PlantationScheduleQuantityWeeks;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PScheduleQuantityWeeksRepository extends JpaRepository<PlantationScheduleQuantityWeeks, Long> {
    List<PlantationScheduleQuantityWeeks> findByPScheduleVarietyIdInAndDeletedFalse(List<Long> varietyIds);

}