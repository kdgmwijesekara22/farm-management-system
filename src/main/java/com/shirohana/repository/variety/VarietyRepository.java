package com.shirohana.repository.variety;

import com.shirohana.entity.employee.Employee;
import com.shirohana.entity.variety.Variety;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface VarietyRepository extends JpaRepository<Variety, Long> {
    Optional<Variety> findByFamilyNameIdAndVarietyNameAndDeletedFalse(Long familyNameId, String varietyName);
    List<Variety> findAllByDeletedFalse();

    Optional<Variety> findByIdAndDeletedFalse(Long id);

    List<Variety> findByFamilyNameIdAndDeletedFalse(Long familyNameId);

    boolean existsByIdAndDeletedFalse(Long id);

    boolean existsByFamilyNameIdAndDeletedFalse(Long id);
}
