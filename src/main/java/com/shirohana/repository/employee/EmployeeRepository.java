package com.shirohana.repository.employee;

import com.shirohana.entity.employee.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface EmployeeRepository extends JpaRepository<Employee,Long> {
    boolean existsByNicAndDeletedFalse(String nic);
    boolean existsByEmployeeGivenIdAndDeletedFalse(String employeeGivenId);
    Optional<Employee> findByIdAndDeletedFalse(Long id);
    Optional<Employee> findByEmployeeGivenIdAndDeletedFalse(String employeeGivenId);
    List<Employee> findAllByDeletedFalse();

}
