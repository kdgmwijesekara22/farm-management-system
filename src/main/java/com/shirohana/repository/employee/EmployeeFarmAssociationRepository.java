package com.shirohana.repository.employee;

import com.shirohana.entity.employee.EmployeeFarmAssociation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface EmployeeFarmAssociationRepository extends JpaRepository<EmployeeFarmAssociation, Long> {
    List<EmployeeFarmAssociation> findByEmployeeId(Long employeeId);
    void deleteByEmployeeId(Long employeeId);
}
