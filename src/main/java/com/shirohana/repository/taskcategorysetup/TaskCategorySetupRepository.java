package com.shirohana.repository.taskcategorysetup;

import com.shirohana.entity.task.core.TaskCategorySetup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface TaskCategorySetupRepository extends JpaRepository<TaskCategorySetup, Long> {

        Optional<TaskCategorySetup> findByTaskNameAndDeletedFalse(String taskName);

        List<TaskCategorySetup> findByTaskTypeAndDeletedFalse(String taskName);

        List<TaskCategorySetup> findAllByDeletedFalse();

        List<TaskCategorySetup> findAllByTaskTypeAndDeletedFalse(String taskType);

        Optional<TaskCategorySetup> findByIdAndDeletedFalse(Long id);

        Optional<TaskCategorySetup> findByTaskType(String taskType);

        @Query("SELECT tcs FROM TaskCategorySetup tcs " +
                "JOIN TaskMaintenance tm ON tcs.taskName = tm.taskName AND tcs.taskType = tm.taskType " +
                "JOIN TaskMainVariety tmv ON tm.id = tmv.taskMaintenanceId " +
                "JOIN TaskMonitoring tmg ON tcs.taskName = tmg.taskName AND tcs.taskType = tmg.taskType " +
                "JOIN TaskMoniVariety tmgv ON tmg.id = tmgv.taskMonitoringId " +
                "WHERE (tmv.varietyId = :varietyId OR tmgv.varietyId = :varietyId) " +
                "AND tcs.deleted = false")
        List<TaskCategorySetup> findByVarietyIdAndDeletedFalse(@Param("varietyId") Long varietyId);

        List<TaskCategorySetup> findByTaskNameAndTaskTypeAndDeletedFalse(String taskName, String taskType);


        @Query("SELECT tcs FROM TaskCategorySetup tcs " +
                "JOIN TaskCategoryTaskVarietySchedule tctvs ON tctvs.taskCategorySetup.id = tcs.id " +
                "JOIN TaskVarietySchedule tvs ON tvs.id = tctvs.taskVarietySchedule.id " +
                "WHERE tcs.taskType = :taskType " +
                "AND tvs.variety.id = :varietyId " +
                "AND tcs.deleted = false")
        List<TaskCategorySetup> findByTaskTypeAndVarietyIdAndDeletedFalse(@Param("taskType") String taskType,
                                                                          @Param("varietyId") Long varietyId);

}

