package com.shirohana.constants;

public class Purpose {
    public static final String MOTHER_PLANT = "MOTHER_PLANT";
    public static final String PRODUCTION_PLANT = "PRODUCTION_PLANT";
    public static final String MOTHER_PLANT_RETENTION = "MOTHER_PLANT_RETENTION";
    public static final String PRODUCTION_PLANT_RETENTION = "PRODUCTION_PLANT_RETENTION";
    public static final String MOTHER_PRODUCTION_PLANT = "MOTHER_PRODUCTION_PLANT";

    // Private constructor to prevent instantiation
    private Purpose() {
        throw new UnsupportedOperationException("This is a constants class and cannot be instantiated");
    }
}
