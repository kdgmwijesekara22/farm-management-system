package com.shirohana.constants;

public class TaskTypeConstants {
    public static final String MONITORING = "MONITORING";
    public static final String MAINTENANCE = "MAINTENANCE";

    public static final String QUALITY = "QUALITY";

    public static final String PLANTATION = "PLANTATION";

    public static final String HARVESTING = "HARVESTING";



    private TaskTypeConstants() {}
}
