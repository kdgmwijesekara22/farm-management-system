package com.shirohana.constants;

public class TimeUnitConstants {
    public static final String DAY = "DAY";
    public static final String WEEK = "WEEK";
    public static final String MONTH = "MONTH";
}
