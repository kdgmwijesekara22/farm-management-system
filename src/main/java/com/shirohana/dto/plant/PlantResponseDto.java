package com.shirohana.dto.plant;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PlantResponseDto {
    private Long id;
    private String familyName;
    private String scientificName;
    private String commonName;
    private boolean reproduced;
    private int recoveryTime;
    private boolean deleted;
    private String createdBy;
    private LocalDateTime createdAt;
    private String updatedBy;
    private LocalDateTime updatedAt;
    private LocalDateTime deletedAt;
    private String deletedBy;
}
