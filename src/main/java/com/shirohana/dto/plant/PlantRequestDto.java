package com.shirohana.dto.plant;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PlantRequestDto {

    @NotBlank(message = "Family name is mandatory")
    private String familyName;

    @NotBlank(message = "Scientific name is mandatory")
    private String scientificName;

    private String commonName;

    private boolean reproduced;

    @NotNull(message = "Recovery time is mandatory")
    private int recoveryTime;
}
