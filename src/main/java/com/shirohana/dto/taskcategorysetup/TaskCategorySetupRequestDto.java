package com.shirohana.dto.taskcategorysetup;

import jakarta.validation.constraints.NotBlank;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TaskCategorySetupRequestDto {

    @NotBlank(message = "Task name is mandatory")
    private String taskName;

    @NotBlank(message = "Task type is mandatory")
    private String taskType;

    @NotBlank(message = "Task description is mandatory")
    private String taskDescription;
}
