package com.shirohana.dto.variety;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class VarietyResponseDto {
    private Long id;
    private String varietyName;
    private String varietyColor;
    private String commonColor;
    private Long familyId;
    private String familyName;
    private boolean deleted;
    private String createdBy;
    private LocalDateTime createdAt;
    private String updatedBy;
    private LocalDateTime updatedAt;
    private LocalDateTime deletedAt;
    private String deletedBy;
}
