package com.shirohana.dto.variety;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class VarietyRequestDto {

    @NotBlank(message = "Variety name is mandatory")
    private String varietyName;

    @NotBlank(message = "Variety color is mandatory")
    private String varietyColor; // Expecting hex value

    @NotBlank(message = "Common color is mandatory")
    private String commonColor;

    @NotNull(message = "Family ID is mandatory")
    private Long familyNameId;
}
