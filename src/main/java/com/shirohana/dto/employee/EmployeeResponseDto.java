package com.shirohana.dto.employee;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class EmployeeResponseDto {
    private Long id;
    private String fullName;
    private List<String> roleName;
    private List<Long> roleId;
    private LocalDate dateOfBirth;
    private String mobile;
    private String nic;
    private String address;
    private List<String> farmName;
    private List<Long> farmId;
    private String employeeGivenId;
    private String createdBy;
    private LocalDateTime createdDate;
    private String lastModifiedBy;
    private LocalDateTime lastModifiedDate;
    private String deletedBy;
    private LocalDateTime deletedAt;
    private boolean deleted;
}
