package com.shirohana.dto.employee;


import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Past;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class EmployeeRequestDto {
    @NotBlank(message = "Full name is required")
    private String fullName;

    @NotNull(message = "Role ID is required")
    private List<Long> roleIds;

    @NotNull(message = "Date of birth is required")
    @Past(message = "Date of birth must be in the past")
    private LocalDate dateOfBirth;

    @NotBlank(message = "Mobile number is required")
    @Size(min = 10, max = 10, message = "Mobile number must be exactly 10 digits")
    private String mobile;

    @NotBlank(message = "NIC is required")
    private String nic;

    @NotBlank(message = "Address is required")
    private String address;

    @NotNull(message = "Farm IDs are required")
    private List<Long> farmIds;

    @NotBlank(message = "Employee ID is required")
    private String employeeGivenId;
}
