package com.shirohana.dto.farm;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class FarmResponseDto {
    private Long id;
    private String name;
    private String location;
    private double farmSize;
    private String createdBy;
    private String updatedBy;
    private String deletedBy;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
    private int availableGreenhouseCount;
    private int availableBayCount;
    private int availableBedCount;
}
