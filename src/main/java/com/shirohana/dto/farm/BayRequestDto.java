package com.shirohana.dto.farm;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BayRequestDto {
    private String name;
    private Long greenhouseId;
}
