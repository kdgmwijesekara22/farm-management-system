package com.shirohana.dto.farm;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class FarmRequestDto {

    @NotEmpty(message = "Name must not be empty")
    private String name;

    @NotEmpty(message = "Name must not be empty")
    private String location;
    private double farmSize;
}
