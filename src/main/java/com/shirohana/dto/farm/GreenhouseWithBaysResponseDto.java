package com.shirohana.dto.farm;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GreenhouseWithBaysResponseDto {
    private Long farmId;
    private List<GreenhouseDto> greenhouses;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class GreenhouseDto {
        private Long greenhouseId;
        private String greenhouseName;
        private List<BayDto> bays;

        @Data
        @NoArgsConstructor
        @AllArgsConstructor
        @Builder
        public static class BayDto {
            private Long bayId;
            private String bayName;
        }
    }
}
