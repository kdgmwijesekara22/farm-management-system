package com.shirohana.dto;

public class CustomResponse<T> {
    private int status;
    private T data;
    private String message;

    public CustomResponse(int status, T data) {
        this.status = status;
        this.data = data;
    }

    public CustomResponse(int status, T data, String message) {
        this.status = status;
        this.data = data;
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
