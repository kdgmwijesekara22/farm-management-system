package com.shirohana.dto.prodplantaskmanagement.prodplanmonitoringtask;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProdPlanMonitoringTaskRequestDto {

    private Long familyNameId;
    private Long farmId;

    private Long plantationScheduleId;

    private Long plantationPlanId;

    private Long prodPlanId;
    private Long userId;

    private String taskName;
    private String taskDescription;

    private boolean production;
    private Integer prodNoOfTerms;
    private String prodTimeUnit;

    private boolean prodUptoEffectiveStatus;
    private Integer prodUptoEffectiveWeek;

    private boolean nursery;
    private Integer nurseryNoOfTerms;
    private String nurseryTimeUnit;

    private boolean nurseryUptoEffectiveStatus;
    private Integer nurseryUptoEffectiveWeek;

}
