package com.shirohana.dto.prodplantaskmanagement.prodplanmaintenancetask.filtered;


import com.shirohana.dto.planttasksetup.maintenancetasksetup.SubTaskResponseDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProdPlanMaintenanceTaskFilterByPurposeCombinedResponseDto {
    private Long id;
    private Long masterDataId;
    private Long supervisorTaskId;
    private String taskName;
    private String taskDescription;
    private Long familyNameId;
    private boolean production; //production true means purpose is production.
    private boolean masterData; // this shows this task coming from master data
    private String taskType; // should give task type (Monitoring, Maintenance, Harvesting, Plantation, QA)
    private Integer noOfTerms;
    private String timeUnit;
    private boolean monitoring;
    private Double stdValueMin;
    private Double stdValueMax;
    private String stdUnit;
    private boolean uptoEffectiveStatus;
    private Integer uptoEffectiveWeek;
    private List<SubTaskResponseDto> subTasks;

    private Long prodPlanId;

    private String createdBy;
    private LocalDateTime createdDate;
    private String lastModifiedBy;
    private LocalDateTime lastModifiedDate;
    private LocalDateTime deletedAt;
    private String deletedBy;
}
