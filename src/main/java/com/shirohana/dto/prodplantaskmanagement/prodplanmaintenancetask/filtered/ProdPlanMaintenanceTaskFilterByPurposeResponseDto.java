package com.shirohana.dto.prodplantaskmanagement.prodplanmaintenancetask.filtered;

import com.shirohana.dto.planttasksetup.maintenancetasksetup.SubTaskResponseDto;
import com.shirohana.dto.planttasksetup.maintenancetasksetup.requestDtos.create.SubTaskRequestDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProdPlanMaintenanceTaskFilterByPurposeResponseDto {

    private Long id;
    private String taskName;
    private String taskDescription;

    private Long familyNameId;
    private boolean production;
    private boolean masterData; // this shows this task coming from master data
    private String taskType;
    private Integer noOfTerms;
    private String timeUnit;
    private boolean monitoring;
    private Double stdValueMin;
    private Double stdValueMax;
    private String stdUnit;
    private boolean uptoEffectiveStatus;
    private Integer uptoEffectiveWeek;
    private List<SubTaskResponseDto> subTasks;

    private Long prodPlanId;

    private String createdBy;
    private LocalDateTime createdDate;
    private String lastModifiedBy;
    private LocalDateTime lastModifiedDate;
    private LocalDateTime deletedAt;
    private String deletedBy;
}
