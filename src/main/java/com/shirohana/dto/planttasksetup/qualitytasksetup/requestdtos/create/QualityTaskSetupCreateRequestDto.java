package com.shirohana.dto.planttasksetup.qualitytasksetup.requestdtos.create;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class QualityTaskSetupCreateRequestDto {

    private Long familyNameId;
    private String taskName;
    private String description;
    private Integer noOfTerms;
    private String timeUnit;
    private boolean uptoEffectiveStatus;
    private Integer uptoEffectiveWeek;
    private List<SubTaskRequestDto> subTasks;
}
