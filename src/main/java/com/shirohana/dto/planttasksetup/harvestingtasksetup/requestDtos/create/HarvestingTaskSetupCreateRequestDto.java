package com.shirohana.dto.planttasksetup.harvestingtasksetup.requestDtos.create;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class HarvestingTaskSetupCreateRequestDto {
    private Long familyNameId;
    private String taskName;
    private String description;
    private Integer noOfTerms;
    private String timeUnit;
    private boolean uptoEffectiveStatus;
    private Integer uptoEffectiveWeek;
    private List<SubTaskRequestDto> subTasks;
}
