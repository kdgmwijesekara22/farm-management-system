package com.shirohana.dto.planttasksetup.harvestingtasksetup.requestDtos.update;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class HarvestingTaskSetupUpdateRequestDto {
    private String taskName;
    private String description;
    private Long familyNameId;
    private Integer noOfTerms;
    private String timeUnit;
    private boolean uptoEffectiveStatus;
    private Integer uptoEffectiveWeek;
    private List<SubTaskWithIdRequestDto> subTasks;
}
