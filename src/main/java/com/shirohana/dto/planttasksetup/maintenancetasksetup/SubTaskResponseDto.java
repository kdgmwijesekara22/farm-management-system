package com.shirohana.dto.planttasksetup.maintenancetasksetup;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SubTaskResponseDto {
    private Long id;
    private String subTaskName;
    private String description;
    private String createdBy;
    private LocalDateTime createdDate;
    private String lastModifiedBy;
    private LocalDateTime lastModifiedDate;
    private LocalDateTime deletedAt;
    private String deletedBy;
}
