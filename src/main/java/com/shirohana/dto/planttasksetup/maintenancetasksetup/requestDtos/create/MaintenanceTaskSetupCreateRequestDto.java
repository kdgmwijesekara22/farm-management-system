package com.shirohana.dto.planttasksetup.maintenancetasksetup.requestDtos.create;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MaintenanceTaskSetupCreateRequestDto {
    private Long taskNameId;
    private Long familyNameId;

    private boolean production;
    private Integer prodNoOfTerms;
    private String prodTimeUnit;
    private boolean prodMonitoring;
    private Double prodStdValueMin;
    private Double prodStdValueMax;
    private String prodUnit;
    private boolean prodUptoEffectiveStatus;
    private Integer prodUptoEffectiveWeek;
    private List<SubTaskRequestDto> prodSubTasks;

    private boolean nursery;
    private Integer nurseryNoOfTerms;
    private String nurseryTimeUnit;
    private boolean nurseryMonitoring;
    private Double nurseryStdValueMin;
    private Double nurseryStdValueMax;
    private String nurseryUnit;
    private boolean nurseryUptoEffectiveStatus;
    private Integer nurseryUptoEffectiveWeek;
    private List<SubTaskRequestDto> nurserySubTasks;
}
