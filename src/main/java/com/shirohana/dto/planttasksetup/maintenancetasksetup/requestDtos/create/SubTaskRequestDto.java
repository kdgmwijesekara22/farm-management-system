package com.shirohana.dto.planttasksetup.maintenancetasksetup.requestDtos.create;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SubTaskRequestDto {
    private String subTaskName;
    private String description;
}
