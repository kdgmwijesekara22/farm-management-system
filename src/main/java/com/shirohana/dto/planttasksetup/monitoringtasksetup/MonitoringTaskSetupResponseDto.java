package com.shirohana.dto.planttasksetup.monitoringtasksetup;



import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MonitoringTaskSetupResponseDto {

    private Long id;
    private String taskName;
    private String taskDescription;
    private Long familyNameId;
    private String taskType; //Monitoring
    private boolean production;
    private Integer prodNoOfTerms;
    private String prodTimeUnit;
    private boolean prodMonitoring;
    private Double prodStdValueMin;
    private Double prodStdValueMax;
    private String prodUnit;
    private boolean prodUptoEffectiveStatus;
    private Integer prodUptoEffectiveWeek;
    private List<SubTaskResponseDto> prodSubTasks;
    private boolean nursery;
    private Integer nurseryNoOfTerms;
    private String nurseryTimeUnit;
    private boolean nurseryMonitoring;
    private Double nurseryStdValueMin;
    private Double nurseryStdValueMax;
    private String nurseryUnit;
    private boolean nurseryUptoEffectiveStatus;
    private Integer nurseryUptoEffectiveWeek;
    private List<SubTaskResponseDto> nurserySubTasks;

    private Long prodPlanId;

    private String createdBy;
    private LocalDateTime createdDate;
    private String lastModifiedBy;
    private LocalDateTime lastModifiedDate;
    private LocalDateTime deletedAt;
    private String deletedBy;

}
