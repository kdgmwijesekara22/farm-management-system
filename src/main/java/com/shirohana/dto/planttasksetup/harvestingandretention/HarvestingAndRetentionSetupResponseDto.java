package com.shirohana.dto.planttasksetup.harvestingandretention;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class HarvestingAndRetentionSetupResponseDto {

    private Long id;
    private Long familyNameId;
    private int prodPlantHarvestDuration;
    private boolean retentionProdPlant;
    private int prodPlantRetentionHarvestDuration;
    private int prodPlantNumOfRetentions;
    private int mothPlantHarvestingDuration;
    private boolean retentionMothPlant;
    private int mothPlantRetentionHarvestDuration;
    private int mothPlantNumOfRetentions;
    private int preNurseryDuration;
    private int nurseryDuration;
    private String createdBy;
    private LocalDateTime createdAt;
    private String updatedBy;
    private LocalDateTime updatedAt;
    private String deletedBy;
    private LocalDateTime deletedAt;
}
