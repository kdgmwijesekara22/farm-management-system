package com.shirohana.dto.planttasksetup.harvestingandretention.validation;

import com.shirohana.dto.planttasksetup.harvestingandretention.HarvestingAndRetentionSetupRequestDto;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class HarvestingAndRetentionValidator implements ConstraintValidator<ValidHarvestingAndRetention, HarvestingAndRetentionSetupRequestDto> {

    @Override
    public boolean isValid(HarvestingAndRetentionSetupRequestDto dto, ConstraintValidatorContext context) {
        boolean isValid = true;

        if (dto.isRetentionProdPlant()) {
            if (dto.getProdPlantRetentionHarvestDuration() <= 0) {
                context.buildConstraintViolationWithTemplate("Production plant retention harvest duration must be greater than 0")
                        .addPropertyNode("prodPlantRetentionHarvestDuration")
                        .addConstraintViolation();
                isValid = false;
            }
            if (dto.getProdPlantNumOfRetentions() <= 0) {
                context.buildConstraintViolationWithTemplate("Number of production plant retentions must be greater than 0")
                        .addPropertyNode("prodPlantNumOfRetentions")
                        .addConstraintViolation();
                isValid = false;
            }
        }

        if (dto.isRetentionMothPlant()) {
            if (dto.getMothPlantRetentionHarvestDuration() <= 0) {
                context.buildConstraintViolationWithTemplate("Mother plant retention harvest duration must be greater than 0")
                        .addPropertyNode("mothPlantRetentionHarvestDuration")
                        .addConstraintViolation();
                isValid = false;
            }
            if (dto.getMothPlantNumOfRetentions() <= 0) {
                context.buildConstraintViolationWithTemplate("Number of mother plant retentions must be greater than 0")
                        .addPropertyNode("mothPlantNumOfRetentions")
                        .addConstraintViolation();
                isValid = false;
            }
        }

        return isValid;
    }
}
