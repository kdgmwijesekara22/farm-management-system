package com.shirohana.dto.planttasksetup.harvestingandretention.validation;


import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = HarvestingAndRetentionValidator.class)
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidHarvestingAndRetention {

    String message() default "Invalid Harvesting and Retention setup";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
