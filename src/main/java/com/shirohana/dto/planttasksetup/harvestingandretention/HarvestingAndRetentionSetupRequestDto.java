package com.shirohana.dto.planttasksetup.harvestingandretention;


import com.shirohana.dto.planttasksetup.harvestingandretention.validation.ValidHarvestingAndRetention;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ValidHarvestingAndRetention
public class HarvestingAndRetentionSetupRequestDto {

    @NotNull(message = "Family name ID is required")
    private Long familyNameId;

    @NotNull(message = "Production plant harvest duration is required")
    @Min(value = 1, message = "Production plant harvest duration must be at least 1")
    private Integer prodPlantHarvestDuration;

    private boolean retentionProdPlant;

    @Min(value = 0, message = "Production plant retention harvest duration must be at least 0")
    private Integer prodPlantRetentionHarvestDuration;

    @Min(value = 0, message = "Number of production plant retentions must be at least 0")
    private Integer prodPlantNumOfRetentions;

    @NotNull(message = "Mother plant harvesting duration is required")
    @Min(value = 1, message = "Mother plant harvesting duration must be at least 1")
    private Integer mothPlantHarvestingDuration;

    private boolean retentionMothPlant;

    @Min(value = 0, message = "Mother plant retention harvest duration must be at least 0")
    private Integer mothPlantRetentionHarvestDuration;

    @Min(value = 0, message = "Number of mother plant retentions must be at least 0")
    private Integer mothPlantNumOfRetentions;

    @Min(value = 1, message = "Pre-nursery duration must be at least 1")
    private Integer preNurseryDuration;

    @Min(value = 1, message = "Nursery duration must be at least 1")
    private Integer nurseryDuration;
}
