package com.shirohana.dto.planttasksetup.plantationtasksetup.requestdtos.update;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SubTaskWithIdRequestDto {
    private Long id;
    private String subTaskName;
    private String description;
}
