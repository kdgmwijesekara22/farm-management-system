package com.shirohana.dto.user;

import lombok.*;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class FarmAssignmentRequest {
    private List<Long> farmIds;
}
