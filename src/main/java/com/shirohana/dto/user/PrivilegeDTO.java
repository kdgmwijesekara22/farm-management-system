package com.shirohana.dto.user;

import lombok.Data;

@Data
public class PrivilegeDTO {
    private Long id;
    private String name;
}
