package com.shirohana.dto.user;

import com.shirohana.dto.farm.FarmDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserDTO {
    private String username;
    private String password;
    private boolean isEmployee;
    private Long employeeId;
    private String contactNumber;
    private String email;
    private List<Long> roleIds; // Changed to List<Long> for role IDs
    private int managingLevel;
    private List<FarmDTO> farms;
}
