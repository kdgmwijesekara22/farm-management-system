package com.shirohana.dto.user;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class UserDetailDTO {
    private Long id;  // Added user ID
    private String username;
    private int level;
    private Long employeeId;
    private String email;
    private String contactNumber;  // Added contact number
    private List<RoleDetailDTO> roles;
    private List<FarmDetailDTO> farms;
}
