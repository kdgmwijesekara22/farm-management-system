package com.shirohana.dto.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PermissionGroupDTO {
    private String name;
    private boolean assigned; // To indicate if any permission in this group is assigned
    private List<PermissionDTO> permissions;
}
