package com.shirohana.dto.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CurrentUserDTO {
    private String username;
    private List<String> roles;
    private int managingLevel;
    private List<PermissionGroupDTO> privileges;
}
