package com.shirohana.dto.schedule.production_plan;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProductionPlanFarmManagerQuantityWeeksResponseDto {
    private Long id;
    private Long productionPlanSupervisorGreenHouseId;
    private int transplantWeek;
    private int harvestWeek;
    private int quantity;
    private boolean deleted;
}
