package com.shirohana.dto.schedule.production_plan;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProductionPlanFarmManagerGreenHouseRequestDto {
    private Long greenHouseId;
    private String greenHouseName;
    private Long farmId; // Added farmId field
    private List<ProductionPlanFarmManagerGreenHouseTotalQuantityVarietyRequestDto> totalQuantities;
}
