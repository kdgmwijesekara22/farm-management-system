package com.shirohana.dto.schedule.production_plan;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProductionPlanFarmManagerGreenHouseTotalQuantityVarietyResponseDto {
    private Long id;
    private Long varietyId;
    private String varietyName;
    private int totalQuantity;
    private List<ProductionPlanFarmManagerQuantityWeeksResponseDto> quantityWeeks;
}
