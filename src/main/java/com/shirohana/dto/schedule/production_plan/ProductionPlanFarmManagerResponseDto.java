package com.shirohana.dto.schedule.production_plan;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProductionPlanFarmManagerResponseDto {
    private Long id;
    private Long farmId; // Added farmId field
    private Long plantationPlanId;
    private Long plantationScheduleId;
    private String orientation;
    private String planName;
    private String plantationStartDate;
    private String plantationEndDate;
    private int startWeek;
    private int endWeek;
    private boolean saved;
    private Long familyId;
    private List<ProductionPlanFarmManagerGreenHouseResponseDto> greenHouses;
}
