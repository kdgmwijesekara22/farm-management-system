package com.shirohana.dto.schedule.production_plan;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProductionPlanFarmManagerVarietiesResponseDto {
    private Long productionPlanSupervisorId;
    private Long varietyId;
    private String varietyName;
    private boolean deleted;
}
