package com.shirohana.dto.schedule.production_plan;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProductionPlanFarmManagerRequestDto {
    private Long plantationPlanId;
    private Long plantationScheduleId;
    private String orientation;
    private String planName;
    private String plantationStartDate;
    private String plantationEndDate;
    private int startWeek; // New field
    private int endWeek;   // New field
    private Long farmId; // Added farmId field
    private List<ProductionPlanFarmManagerGreenHouseRequestDto> greenHouses;
//    private List<ProductionPlanFarmManagerVarietiesRequestDto> varieties;
}
