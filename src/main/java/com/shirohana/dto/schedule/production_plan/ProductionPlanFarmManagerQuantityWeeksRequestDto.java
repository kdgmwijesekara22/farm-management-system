package com.shirohana.dto.schedule.production_plan;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProductionPlanFarmManagerQuantityWeeksRequestDto {
    private int transplantWeek;
    private int harvestWeek;
    private int quantity;
}
