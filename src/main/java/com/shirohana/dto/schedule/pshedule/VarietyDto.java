package com.shirohana.dto.schedule.pshedule;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class VarietyDto {
    private Long id;
    private String varietyName;
    private String varietyColor;
    private String commonColor;
    private Long familyNameId;
}