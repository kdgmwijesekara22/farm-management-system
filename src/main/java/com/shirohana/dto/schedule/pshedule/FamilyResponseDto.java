package com.shirohana.dto.schedule.pshedule;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class FamilyResponseDto {
    private PlantDto plant;
    private List<VarietyDto> varieties;
}