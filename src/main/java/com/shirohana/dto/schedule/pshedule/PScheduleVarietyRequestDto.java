package com.shirohana.dto.schedule.pshedule;

import lombok.*;

import java.util.List;

@AllArgsConstructor
@Builder
@Data
public class PScheduleVarietyRequestDto {
    private String varietyId;
    private String varietyName;
    private List<PScheduleQuantityWeeksRequestDto> pScheduleQuantityWeeksRequestDtoList;
}