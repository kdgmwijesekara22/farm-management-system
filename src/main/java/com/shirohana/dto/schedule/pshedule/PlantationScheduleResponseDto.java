package com.shirohana.dto.schedule.pshedule;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PlantationScheduleResponseDto {
    private Long id;
    private String name;
    private Long familyId;//foreign key
    private String familyName;
    private String purpose;
    private int hDuration;
    private String plantationStartDate;  // Date format: yyyy-MM-dd
    private String plantationEndDate;    // Date format: yyyy-MM-dd
    private int startWeek;
    private int endWeek;
    private boolean expired;              // New field to indicate if the schedule has expired
    private List<PScheduleVarietyResponseDto> pScheduleVarietyRequestDtoList;
    private String orientation;
}
