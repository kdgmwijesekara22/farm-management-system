package com.shirohana.dto.schedule.pshedule;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
@Builder
public class PlantationScheduleRequestDto {
    private String name;
    private Long familyId;
    private String purpose;
    private int hDuration;
    private String plantationStartDate;
    private String plantationEndDate;
    private int startWeek; // New field
    private int endWeek;   // New field
    private List<PScheduleVarietyRequestDto> pScheduleVarietyRequestDtoList;
    private String orientation;
}
