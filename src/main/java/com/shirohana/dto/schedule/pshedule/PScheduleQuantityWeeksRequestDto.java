package com.shirohana.dto.schedule.pshedule;

import lombok.*;

@AllArgsConstructor
@Builder
@Data
public class PScheduleQuantityWeeksRequestDto {
    private String transplantWeek;
    private String harvestWeek;
    private String quantity;
}