package com.shirohana.dto.schedule.pshedule;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PScheduleQuantityWeeksResponseDto {
    private Long id;
    private Long PScheduleVarietyId;
    private int transplantWeek;
    private int harvestWeek;
    private int quantity;
}
