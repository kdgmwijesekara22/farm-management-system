package com.shirohana.dto.schedule.pshedule;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PlantDto {
    private Long id;
    private String familyName;
    private String scientificName;
    private String commonName;
    private boolean reproduced;
    private int recoveryTime;
}