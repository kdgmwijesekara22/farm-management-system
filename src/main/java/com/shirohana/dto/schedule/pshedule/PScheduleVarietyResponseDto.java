package com.shirohana.dto.schedule.pshedule;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PScheduleVarietyResponseDto {
    private Long id;
    private Long plantationScheduleId;
    private Long varietyId;
    private String varietyName;
    private int totalQuantity;
    private List<PScheduleQuantityWeeksResponseDto> pScheduleQuantityWeeksRequestDtoList;
}
