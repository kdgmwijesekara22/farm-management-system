package com.shirohana.dto.schedule.production_plan_green_house;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProductionPlanGHMWeeksYearsResponseDto {
    private Long id;
    private int trayplantWeek;
    private int harvestWeek;
    private Long productionPlanGHMBayBedId;
    private int trayplantStartYear;
    private int transplantEndYear;
    private List<ProductionPlanGHMVarietyQuantityResponseDto> varietyQuantities; // Include list of VarietyQuantityResponseDto
}
