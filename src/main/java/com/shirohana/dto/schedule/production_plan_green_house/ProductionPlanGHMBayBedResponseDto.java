package com.shirohana.dto.schedule.production_plan_green_house;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProductionPlanGHMBayBedResponseDto {
    private Long id;
    private Long productionPlanGHManagerId;
    private Long bayId;
    private Long bedId;
    private List<ProductionPlanGHMWeeksYearsResponseDto> weeksYears; // Include list of WeeksYearsResponseDto
}
