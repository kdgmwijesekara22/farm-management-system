package com.shirohana.dto.schedule.production_plan_green_house;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProductionPlanGHManagerResponseDto {
    private Long id;
    private Long productionPlanFarmManagerId;
    private Long farmId;
    private Long greenhouseId;
    private Long plantationPlanId;
    private Long plantationScheduleId;
    private Long familyId;
    private String planName;
    private String plantationStartDate;
    private String plantationEndDate;
    private Integer startWeek;
    private Integer endWeek;
    private Integer startYear;
    private Integer endYear;
    private boolean deleted;
    private boolean saved;
    private String orientation;
    private boolean expired; // New field to indicate if the schedule is expired
    private List<ProductionPlanGHMBayBedResponseDto> bayBeds; // Include list of BayBedResponseDto
}
