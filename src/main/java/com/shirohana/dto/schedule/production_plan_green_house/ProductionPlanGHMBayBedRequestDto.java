package com.shirohana.dto.schedule.production_plan_green_house;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProductionPlanGHMBayBedRequestDto {
    private Long productionPlanGHManagerId;
    private Long bayId;
    private Long bedId;
    private List<ProductionPlanGHMWeeksYearsRequestDto> weeksYears; // Include list of WeeksYearsRequestDto
}
