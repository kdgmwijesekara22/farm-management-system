package com.shirohana.dto.schedule.production_plan_green_house;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProductionPlanGHMVarietyQuantityResponseDto {
    private Long id;
    private Long productionPlanGHMWeeksYearsId;
    private Long varietyId;
    private String varietyName;
    private int quantity;
}
