package com.shirohana.dto.schedule.psplan;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class PlantationPlanFarmQuantityVarietyRequestDto {
    private String varietyId;
    private String varietyName;
    private int quantity;
    private List<PlantationPlanQuantityWeeksRequestDto> quantityWeeks;
}
