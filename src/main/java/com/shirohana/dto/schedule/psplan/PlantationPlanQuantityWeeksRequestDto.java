package com.shirohana.dto.schedule.psplan;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class PlantationPlanQuantityWeeksRequestDto {
    private Long plantationPlanFarmTotalQuantityVarietyId;
    private int transplantWeek;
    private int harvestWeek;
    private int quantity;
}
