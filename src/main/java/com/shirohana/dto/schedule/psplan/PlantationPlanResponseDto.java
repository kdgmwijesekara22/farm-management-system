package com.shirohana.dto.schedule.psplan;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import java.util.List;

@Data
@AllArgsConstructor
@Builder
public class PlantationPlanResponseDto {
    private Long id;
    private String plantationStartDate;
    private String plantationEndDate;
    private int startWeek;
    private int endWeek;
    private String plantationPlanName;
    private String orientation;  //
    private Long plantationScheduleId;
    private String plantationScheduleName; // Add this field
    private List<PlantationPlanFarmResponseDto> farms;
}
