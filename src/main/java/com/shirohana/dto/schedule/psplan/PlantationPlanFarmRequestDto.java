package com.shirohana.dto.schedule.psplan;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class PlantationPlanFarmRequestDto {
    private Long farmId;
    private List<PlantationPlanFarmQuantityVarietyRequestDto> quantities;
}
