package com.shirohana.dto.schedule.psplan;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import java.util.List;

@Data
@AllArgsConstructor
@Builder
public class PlantationPlanFarmResponseDto {
    private Long id;
    private Long farmId;
    private List<PlantationPlanFarmQuantityVarietyResponseDto> quantities;
}
