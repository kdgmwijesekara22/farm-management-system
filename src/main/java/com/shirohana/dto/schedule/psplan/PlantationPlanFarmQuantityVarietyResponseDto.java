package com.shirohana.dto.schedule.psplan;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
@Builder
public class PlantationPlanFarmQuantityVarietyResponseDto {
    private Long id;
    private int quantity;
    private String varietyId;
    private String varietyName; // Add this field
    private List<PlantationPlanQuantityWeeksResponseDto> quantityWeeks;

}
