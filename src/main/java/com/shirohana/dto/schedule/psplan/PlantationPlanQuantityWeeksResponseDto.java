package com.shirohana.dto.schedule.psplan;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class PlantationPlanQuantityWeeksResponseDto {
    private Long id;
    private Long plantationPlanFarmTotalQuantityVarietyId;
    private int transplantWeek;
    private int harvestWeek;
    private int quantity;
    private boolean deleted;
}
