package com.shirohana.dto.schedule.psplan;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class PlantationPlanRequestDto {
    private String plantationStartDate;
    private String plantationEndDate;
    private String plantationPlanName;
    private Long plantationScheduleId;
    private int startWeek; // New field
    private int endWeek;   // New field
    private List<PlantationPlanFarmRequestDto> farms;
    private String orientation;  //

}
