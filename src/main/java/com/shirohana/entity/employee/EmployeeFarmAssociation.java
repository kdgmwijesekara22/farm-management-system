package com.shirohana.entity.employee;


import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "employee_farm_association")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class EmployeeFarmAssociation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "farm_id", nullable = false)
    private Long farmId;

    @Column(name = "employee_id", nullable = false)
    private Long employeeId;
}
