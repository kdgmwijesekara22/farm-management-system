package com.shirohana.entity.employee;

import com.shirohana.entity.audit.AuditEntity;
import com.shirohana.entity.user.Role;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.envers.Audited;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "employee")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Audited
public class Employee extends AuditEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "full_name", nullable = false)
    private String fullName;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "employee_roles",
            joinColumns = @JoinColumn(name = "employee_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles = new HashSet<>();  // Updated to support multiple roles

    @Column(name = "date_of_birth", nullable = false)
    private LocalDate dateOfBirth;

    @Column(name = "mobile", nullable = false)
    private String mobile;

    @Column(name = "nic", nullable = false)
    private String nic;

    @Column(name = "address", nullable = false)
    private String address;

    @Column(name = "employee_given_id", nullable = false)
    private String employeeGivenId;

    @Column(nullable = true)
    private String deletedBy;

    @Column(nullable = true)
    private LocalDateTime deletedAt;

    @Column(nullable = false)
    private boolean deleted = false;
}
