package com.shirohana.entity.variety;

import com.shirohana.entity.audit.AuditEntity;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Entity
@Table(name = "variety")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Variety extends AuditEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "variety_name", nullable = false)
    private String varietyName;

    @Column(name = "variety_color", nullable = false)
    private String varietyColor; // Expecting hex value

    @Column(name = "common_color", nullable = false)
    private String commonColor;

    @Column(name = "family_id", nullable = false)
    private Long familyNameId;//Plant Id //Foreign Key Plant

    @Column(name = "deleted", nullable = false)
    private boolean deleted = false;

    @Column(name = "deleted_at")
    private LocalDateTime deletedAt;

    @Column(name = "deleted_by")
    private String deletedBy;
}
