package com.shirohana.entity.taskcategorysetup;


import com.shirohana.entity.audit.AuditEntity;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.envers.Audited;

import java.time.LocalDateTime;

@Entity
@Table(name = "task_category_setup")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Audited
public class TaskCategorySetup extends AuditEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String taskName;

    private String taskType;

    private String taskDescription;

    private boolean deleted=false;

    private LocalDateTime deletedAt;

    private String deletedBy;
}
