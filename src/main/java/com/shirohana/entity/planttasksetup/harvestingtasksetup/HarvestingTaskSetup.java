package com.shirohana.entity.planttasksetup.harvestingtasksetup;

import com.shirohana.constants.TimeUnitConstants;
import com.shirohana.entity.audit.AuditEntity;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.envers.Audited;

import java.time.LocalDateTime;

@Entity
@Table(name = "harvesting_task_setup")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Audited
public class HarvestingTaskSetup extends AuditEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long familyNameId;
    private String taskName;
    private String taskDescription;
    private Integer noOfTerms = 1;
    private String timeUnit = TimeUnitConstants.WEEK;
    private boolean uptoEffectiveStatus = false;
    private Integer uptoEffectiveWeek = 14;
    private boolean deleted;
    private Long prodPlanId;
    private String deletedBy;
    private LocalDateTime deletedAt;
}
