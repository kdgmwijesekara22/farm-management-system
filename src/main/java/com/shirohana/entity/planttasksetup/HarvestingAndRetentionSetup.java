package com.shirohana.entity.planttasksetup;

import com.shirohana.entity.audit.AuditEntity;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "harvesting_and_retention_setup")
public class HarvestingAndRetentionSetup extends AuditEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long familyNameId;

    private int prodPlantHarvestDuration;
    private boolean retentionProdPlant;
    private int prodPlantRetentionHarvestDuration;
    private int prodPlantNumOfRetentions;
    private int mothPlantHarvestingDuration;
    private boolean retentionMothPlant;
    private int mothPlantRetentionHarvestDuration;
    private int mothPlantNumOfRetentions;
    private int preNurseryDuration;
    private int nurseryDuration;
    private boolean deleted = false;
    private String deletedBy;
    private LocalDateTime deletedAt;
}
