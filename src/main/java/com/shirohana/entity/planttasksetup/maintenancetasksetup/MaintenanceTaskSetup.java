package com.shirohana.entity.planttasksetup.maintenancetasksetup;

import com.shirohana.entity.audit.AuditEntity;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.envers.Audited;

import java.time.LocalDateTime;


@Entity
@Table(name = "maintenance_task_setup")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Audited
public class MaintenanceTaskSetup extends AuditEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long taskNameId;

    @Column(name = "family_name_id", nullable = false)
    private Long familyNameId;

    @Column(name = "deleted", nullable = false)
    private boolean deleted;

    @Column(name = "deleted_at")
    private LocalDateTime deletedAt;

    private Long prodPlanId;

    @Column(name = "deleted_by")
    private String deletedBy;
    // No relational mapping, handle via service layer
    private Long productionId;

    private Long nurseryId;

}
