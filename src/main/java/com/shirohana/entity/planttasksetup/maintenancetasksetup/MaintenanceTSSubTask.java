package com.shirohana.entity.planttasksetup.maintenancetasksetup;

import com.shirohana.entity.audit.AuditEntity;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.envers.Audited;

import java.time.LocalDateTime;


@Entity
@Table(name = "maintenance_ts_sub_task")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Audited
public class MaintenanceTSSubTask extends AuditEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String subTaskName;

    private String description;

    private Long productionId;

    private Long nurseryId;

    @Column(name = "deleted", nullable = false)
    private boolean deleted;

    @Column(name = "deleted_at")
    private LocalDateTime deletedAt;

    @Column(name = "deleted_by")
    private String deletedBy;
}
