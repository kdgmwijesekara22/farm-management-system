package com.shirohana.entity.planttasksetup.monitoringtasksetup;

import com.shirohana.entity.audit.AuditEntity;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;


@Entity
@Table(name = "monitoring_ts_nursery")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MonitoringTSNursery extends AuditEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Integer noOfTerms;
    private String timeUnit;
    private boolean monitoring;
    private Double stdValueMin;
    private Double stdValueMax;
    private String unit;
    private boolean uptoEffectiveStatus;
    private Integer uptoEffectiveWeek;

    @Column(name = "deleted", nullable = false)
    private boolean deleted;

    @Column(name = "deleted_at")
    private LocalDateTime deletedAt;

    @Column(name = "deleted_by")
    private String deletedBy;
}
