package com.shirohana.entity.planttasksetup.monitoringtasksetup;

import com.shirohana.entity.audit.AuditEntity;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;


@Entity
@Table(name = "monitoring_task_setup")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MonitoringTaskSetup extends AuditEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long taskNameId;

    @Column(name = "family_name_id", nullable = false)
    private Long familyNameId;

    @Column(name = "deleted", nullable = false)
    private boolean deleted;

    @Column(name = "deleted_at")
    private LocalDateTime deletedAt;

    private Long prodPlanId;

    @Column(name = "deleted_by")
    private String deletedBy;
    // No relational mapping, handle via service layer
    private Long productionId;

    private Long nurseryId;

}
