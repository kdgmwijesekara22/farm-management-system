package com.shirohana.entity.planttasksetup.plantationtasksetup;


import com.shirohana.entity.audit.AuditEntity;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.envers.Audited;

import java.time.LocalDateTime;

@Entity
@Table(name = "plantation_ts_sub_task")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Audited
public class PlantationTSSubTask extends AuditEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String subTaskName;
    private String description;

    private Long plantationTaskSetupId;

    @Column(name = "deleted", nullable = false)
    private boolean deleted;

    @Column(name = "deleted_at")
    private LocalDateTime deletedAt;

    @Column(name = "deleted_by")
    private String deletedBy;
}
