package com.shirohana.entity.planttasksetup.qualitytasksetup;


import com.shirohana.constants.TimeUnitConstants;
import com.shirohana.entity.audit.AuditEntity;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.envers.Audited;

import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "quality_task_setup")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Audited
public class QualityTaskSetup extends AuditEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long familyNameId;
    private String taskName;
    private String taskDescription;
    private Integer noOfTerms = 1;
    private String timeUnit = TimeUnitConstants.WEEK;
    private boolean uptoEffectiveStatus = true;
    private Integer uptoEffectiveWeek = 1;
    private boolean deleted;
    private Long prodPlanId;
    private String deletedBy;
    private LocalDateTime deletedAt;
}
