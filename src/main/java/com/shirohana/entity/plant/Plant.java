package com.shirohana.entity.plant;

import com.shirohana.entity.audit.AuditEntity;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Entity
@Table(name = "plant")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
//Plant = Family
public class Plant extends AuditEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;//familyId

    @Column(name = "family_name", nullable = false)
    private String familyName;

    @Column(name = "scientific_name", nullable = false)
    private String scientificName;

    @Column(name = "common_name")
    private String commonName;

    @Column(name = "reproduced", nullable = false)
    private boolean reproduced;

    @Column(name = "recovery_time")
    private int recoveryTime;

    @Column(name = "deleted", nullable = false)
    private boolean deleted = false;

    @Column(name = "deleted_at")
    private LocalDateTime deletedAt;

    @Column(name = "deleted_by")
    private String deletedBy;
}
