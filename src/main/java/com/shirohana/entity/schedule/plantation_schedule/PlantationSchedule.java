package com.shirohana.entity.schedule.plantation_schedule;

import com.shirohana.entity.audit.AuditEntity;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.envers.Audited;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Audited
public class PlantationSchedule extends AuditEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private Long familyId;//foreign key by plant//plant id
    private String purpose;
    private int hDuration;
    private String plantationStartDate;
    private String plantationEndDate;
    private boolean deleted = false;
    private int startWeek;
    private int endWeek;
    private boolean expired = false;
    private String orientation;

}
