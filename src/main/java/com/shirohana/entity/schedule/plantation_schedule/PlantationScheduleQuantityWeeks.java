package com.shirohana.entity.schedule.plantation_schedule;

import com.shirohana.entity.audit.AuditEntity;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.envers.Audited;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Audited
public class PlantationScheduleQuantityWeeks extends AuditEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long PScheduleVarietyId;
    private int transplantWeek;
    private int harvestWeek;
    private int quantity;
    private boolean deleted = false;
}
