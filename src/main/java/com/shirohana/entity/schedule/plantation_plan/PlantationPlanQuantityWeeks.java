package com.shirohana.entity.schedule.plantation_plan;

import com.shirohana.entity.audit.AuditEntity;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.envers.Audited;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Audited
public class PlantationPlanQuantityWeeks extends AuditEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long plantationPlanFarmTotalQuantityVarietyId;
    private int transplantWeek;
    private int harvestWeek;
    private int quantity;
    private boolean deleted = false;
}
