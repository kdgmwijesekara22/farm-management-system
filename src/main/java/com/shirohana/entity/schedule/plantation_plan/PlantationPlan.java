package com.shirohana.entity.schedule.plantation_plan;

import com.shirohana.entity.audit.AuditEntity;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.envers.Audited;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Audited
public class PlantationPlan extends AuditEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String plantationStartDate;

    private String plantationEndDate;

    private int startWeek; // New field

    private int endWeek;   // New field

    private String plantationPlanName;

    private Long plantationScheduleId;//foreign key plantation schedule

    private String orientation;  // <-- Add this line

    private boolean deleted = false;

    private boolean expired = false;
}
