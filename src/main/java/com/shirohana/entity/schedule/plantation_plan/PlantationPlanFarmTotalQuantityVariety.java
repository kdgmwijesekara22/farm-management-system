package com.shirohana.entity.schedule.plantation_plan;

import com.shirohana.entity.audit.AuditEntity;
import jakarta.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import jakarta.persistence.*;
import org.hibernate.envers.Audited;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Audited
public class PlantationPlanFarmTotalQuantityVariety extends AuditEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long plantationPlanFarmId;

    private Long varietyId;

    private String varietyName;

    private int quantity;//total quntity

    private boolean deleted = false;

}
