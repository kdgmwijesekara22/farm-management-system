package com.shirohana.entity.schedule.plantation_plan;

import com.shirohana.entity.audit.AuditEntity;
import jakarta.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.envers.Audited;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Audited
public class PlantationPlanFarm extends AuditEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long plantationPlanId;//foreign id

    private Long farmId;//foreign id farm

    private boolean deleted = false;
}
