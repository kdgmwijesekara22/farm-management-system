package com.shirohana.entity.schedule.production_plan;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProductionPlanFarmManager {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long farmId; //Foreign key PlantationPlan
    private Long plantationPlanId;//Foreign key by Farm
    private Long plantationScheduleId;//Foreign key by Plantation Schedule
    private String orientation;
    private String purpose;
    private String planName;
    private String plantationStartDate;
    private String plantationEndDate;
    private int startWeek;
    private int endWeek;
    private boolean deleted = false;
    private boolean expired = false;
    private boolean saved = false;

}
