package com.shirohana.entity.schedule.production_plan;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProductionPlanFarmMangerQuantityWeeks {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long prodPlanFarmMgrGreenHouseTotalQtyVarietyId;//foreign key
    private int transplantWeek;
    private int harvestWeek;
    private int quantity;
    private boolean deleted = false;
}
