package com.shirohana.entity.schedule.production_plan;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProductionPlanFarmManagerVarieties {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;  // Added id field
    private Long prodPlanFarmMgrId; //foreign key
    private Long varietyId;
    private String varietyName;
    private boolean deleted = false;
}
