package com.shirohana.entity.schedule.production_plan;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProductionPlanFarmManagerGreenHouse {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long farmId; // Added farmId field
    private Long greenHouseId;
    private String greenHouseName;
    private Long prodPlanFarmMgrId;//foreign key
    private boolean deleted = false;
}
