package com.shirohana.entity.schedule.production_plan_green_house;


import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProductionPlanGHMWeeksYears {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private int trayplantWeek;
    private int harvestWeek;
    private Long productionPlanGHMBayBedId;//foreign key
    private int trayplantStartYear;
    private int transplantEndYear;
}