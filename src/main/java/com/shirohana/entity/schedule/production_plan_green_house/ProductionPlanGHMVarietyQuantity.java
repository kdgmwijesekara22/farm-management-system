package com.shirohana.entity.schedule.production_plan_green_house;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProductionPlanGHMVarietyQuantity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long productionPlanGHMWeeksYearsId;//main foreign key
    private Long varietyId;//foreign key
    private String varietyName;
    private int quantity;//total quntity

}