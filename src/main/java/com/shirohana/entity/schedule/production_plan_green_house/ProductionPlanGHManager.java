package com.shirohana.entity.schedule.production_plan_green_house;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProductionPlanGHManager {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long productionPlanFarmManagerId;//main foreign key
    private Long farmId;//foreign key
    private Long greenhouseId;//foreign key
    private Long plantationPlanId;//foreign key
    private Long plantationScheduleId;//foreign key
    private Long familyId;//foreign key
    private String planName;
    private String plantationStartDate;
    private String plantationEndDate;
    private boolean deleted = false;
    private boolean saved = false;

    private Integer startWeek;
    private Integer endWeek;
    private Integer startYear;
    private Integer endYear;
    private String orientation;
}
