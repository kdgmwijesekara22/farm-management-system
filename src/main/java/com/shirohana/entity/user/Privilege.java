package com.shirohana.entity.user;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.envers.Audited;


@Entity
@Table(name = "privileges", uniqueConstraints = @UniqueConstraint(columnNames = {"name"}))
@Data
@NoArgsConstructor
@Audited
public class Privilege {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, unique = true)
    private String name;

    @ManyToOne
    @JoinColumn(name = "group_id")
    @JsonBackReference
    private PermissionGroup group;

    @ManyToOne
    @JoinColumn(name = "controller_id")
    private Controller controller;

    public Privilege(String name, PermissionGroup group, Controller controller) {
        this.name = name;
        this.group = group;
        this.controller = controller;
    }
}

