package com.shirohana.entity.user;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.shirohana.entity.audit.AuditEntity;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.envers.Audited;

import java.util.List;

@Entity
@Table(name = "permission_groups")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Audited
public class PermissionGroup extends AuditEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, unique = true)
    private String name;

    @OneToMany(mappedBy = "group", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JsonManagedReference
    private List<Privilege> permissions;

    public PermissionGroup(String name) {
        this.name = name;
    }
}

