package com.shirohana.entity.user;

import com.shirohana.entity.audit.AuditEntity;
import com.shirohana.entity.farm.Farm;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.envers.Audited;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Audited
public class RoleFarm extends AuditEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "role_id", nullable = false)
    private Role role;

    @ManyToOne
    @JoinColumn(name = "farm_id", nullable = false)
    private Farm farm;

    public RoleFarm(Role role, Farm farm) {
        this.role = role;
        this.farm = farm;
    }
}
