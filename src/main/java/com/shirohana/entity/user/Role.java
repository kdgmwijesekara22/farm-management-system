package com.shirohana.entity.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.shirohana.entity.audit.AuditEntity;
import com.shirohana.entity.employee.Employee;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.envers.Audited;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "role")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Audited
public class Role extends AuditEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;

    @ManyToMany(mappedBy = "roles")
    @JsonIgnore // Prevent infinite loop
    private Collection<User> users = new HashSet<>();

    @ManyToMany(mappedBy = "roles")
    @JsonIgnore
    private Collection<Employee> employees = new HashSet<>();  // Added relation to Employee

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "roles_privileges",
            joinColumns = @JoinColumn(name = "role_id"),
            inverseJoinColumns = @JoinColumn(name = "privilege_id"))
    private Set<Privilege> privileges;

    @OneToMany(mappedBy = "role")
    private Set<RoleFarm> roleFarms;
}
