package com.shirohana.entity.farm;

import com.shirohana.entity.audit.AuditEntity;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.envers.Audited;

import java.time.LocalDateTime;

@Entity
@Table(name = "bed")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Audited
public class Bed extends AuditEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(name = "bay_id", nullable = false)
    private Long bayId;//foreign key bay id

    @Column(nullable = false)
    private String createdBy;

    @Column(nullable = true)
    private String updatedBy;

    @Column(nullable = true)
    private String deletedBy;

    @Column(nullable = false)
    private LocalDateTime createdAt;

    @Column(nullable = true)
    private LocalDateTime updatedAt;

    @Column(nullable = true)
    private LocalDateTime deletedAt;

    @Column(nullable = false)
    private boolean deleted = false;

//    @Column(name = "bed_is_available", nullable = true)
//    private String bedIsAvailable; // New column with values 'AVAILABLE' or 'NOT_AVAILABLE'

    private Integer trayPlantWeek = 0;
    private Integer harvestWeek = 0;
    private Integer transplantStartYear = 0;
    private Integer transplantEndYear = 0;

}