package com.shirohana.entity.prodplantaskmanagement.prodplanmaintenancetask;


import com.shirohana.entity.audit.AuditEntity;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.envers.Audited;

import java.time.LocalDateTime;

@Entity
@Table(name = "prod_plan_maintenance_task")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Audited
public class ProdPlanMaintenanceTask extends AuditEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "family_name_id", nullable = false)
    private Long familyNameId;

    @Column(name = "farm_id", nullable = false)
    private Long farmId;

    @Column(name = "plantation_schedule_id", nullable = false)
    private Long plantationScheduleId;

    @Column(name = "plantation_plan_id", nullable = false)
    private Long plantationPlanId;

    @Column(name = "prod_plan_id", nullable = false)
    private Long prodPlanId;

    @Column(name = "user_id", nullable = false)
    private Long userId;

    @Column(name = "task_name", nullable = false)
    private String taskName;

    @Column(name = "task_description", nullable = false)
    private String taskDescription;

    @Column(name = "production", nullable = false)
    private boolean production = false;

    @Column(name = "prod_no_of_terms")
    private Integer prodNoOfTerms;

    @Column(name = "prod_time_unit")
    private String prodTimeUnit;

    @Column(name = "prod_upto_effective_status", nullable = false)
    private boolean prodUptoEffectiveStatus;

    @Column(name = "prod_upto_effective_week")
    private Integer prodUptoEffectiveWeek;

    @Column(name = "nursery", nullable = false)
    private boolean nursery;

    @Column(name = "nursery_no_of_terms")
    private Integer nurseryNoOfTerms;

    @Column(name = "nursery_time_unit")
    private String nurseryTimeUnit;

    @Column(name = "nursery_upto_effective_status", nullable = false)
    private boolean nurseryUptoEffectiveStatus;

    @Column(name = "nursery_upto_effective_week")
    private Integer nurseryUptoEffectiveWeek;

    @Column(name = "deleted", nullable = false)
    private boolean deleted;

    @Column(name = "deleted_at")
    private LocalDateTime deletedAt;

    @Column(name = "deleted_by")
    private String deletedBy;

}
