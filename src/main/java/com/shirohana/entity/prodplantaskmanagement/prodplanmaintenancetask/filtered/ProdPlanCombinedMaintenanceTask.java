package com.shirohana.entity.prodplantaskmanagement.prodplanmaintenancetask.filtered;


import com.shirohana.entity.audit.AuditEntity;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.envers.Audited;

import java.time.LocalDateTime;

@Entity
@Table(name = "prod_plan_combined_maintenance_task")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Audited
public class ProdPlanCombinedMaintenanceTask extends AuditEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long masterDataId;
    private Long supervisorTaskId;
    private String taskType;
    private Long prodPlanId;
    private String purpose;
    private boolean deleted=false;

    private Long farmId;

    private Long plantationScheduleId;

    private Long plantationPlanId;

    private Long userId;

    private String deletedBy;
    private LocalDateTime deletedAt;
}
