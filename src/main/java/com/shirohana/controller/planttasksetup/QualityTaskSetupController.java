package com.shirohana.controller.planttasksetup;


import com.shirohana.constants.Permissions;
import com.shirohana.dto.CustomResponse;
import com.shirohana.dto.planttasksetup.qualitytasksetup.requestdtos.create.QualityTaskSetupCreateRequestDto;
import com.shirohana.dto.planttasksetup.qualitytasksetup.requestdtos.update.QualityTaskSetupUpdateRequestDto;
import com.shirohana.dto.planttasksetup.qualitytasksetup.responsedtos.QualityTaskSetupResponseDto;
import com.shirohana.exception.ResourceNotFoundException;
import com.shirohana.service.interfaces.planttasksetup.QualityTaskSetupService;
import jakarta.validation.ValidationException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/quality-task-setup")
@RequiredArgsConstructor
public class QualityTaskSetupController {
    private final QualityTaskSetupService service;

    @PostMapping
    @PreAuthorize("hasAuthority('" + Permissions.QUALITY_TASK_SETUP_CREATE_PERMISSION + "')")
    public ResponseEntity<CustomResponse<QualityTaskSetupResponseDto>> createSetup(@Validated @RequestBody QualityTaskSetupCreateRequestDto requestDto) {
        try {
            QualityTaskSetupResponseDto responseDto = service.createTask(requestDto);
            return ResponseEntity.status(HttpStatus.CREATED).body(new CustomResponse<>(1001, responseDto));
        } catch (ValidationException ex) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new CustomResponse<>(1002, null, ex.getMessage()));
        } catch (ResourceNotFoundException ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new CustomResponse<>(1003, null, ex.getMessage()));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null, "An unexpected error occurred."));
        }
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('" + Permissions.QUALITY_TASK_SETUP_UPDATE_PERMISSION + "')")
    public ResponseEntity<CustomResponse<QualityTaskSetupResponseDto>> updateSetup(@PathVariable Long id, @Validated @RequestBody QualityTaskSetupUpdateRequestDto requestDto) {
        try {
            QualityTaskSetupResponseDto responseDto = service.updateTask(id, requestDto);
            return ResponseEntity.ok(new CustomResponse<>(1000, responseDto));
        } catch (ValidationException ex) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new CustomResponse<>(1002, null, ex.getMessage()));
        } catch (ResourceNotFoundException ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new CustomResponse<>(1003, null, ex.getMessage()));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null, "An unexpected error occurred."));
        }
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('" + Permissions.QUALITY_TASK_SETUP_DELETE_PERMISSION + "')")
    public ResponseEntity<CustomResponse<Void>> deleteSetup(@PathVariable Long id) {
        try {
            service.deleteTask(id);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body(new CustomResponse<>(1000, null));
        } catch (ResourceNotFoundException ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new CustomResponse<>(1003, null, ex.getMessage()));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null, "An unexpected error occurred."));
        }
    }

    @GetMapping
    @PreAuthorize("hasAuthority('" + Permissions.QUALITY_TASK_SETUP_READ_PERMISSION + "')")
    public ResponseEntity<CustomResponse<List<QualityTaskSetupResponseDto>>> getAllSetups() {
        try {
            List<QualityTaskSetupResponseDto> responseDtos = service.getAllTasks();
            return ResponseEntity.ok(new CustomResponse<>(1000, responseDtos));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null, "An unexpected error occurred."));
        }
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('" + Permissions.QUALITY_TASK_SETUP_READ_PERMISSION + "')")
    public ResponseEntity<CustomResponse<QualityTaskSetupResponseDto>> getSetupById(@PathVariable Long id) {
        try {
            QualityTaskSetupResponseDto responseDto = service.getTaskById(id);
            return ResponseEntity.ok(new CustomResponse<>(1000, responseDto));
        } catch (ResourceNotFoundException ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new CustomResponse<>(1003, null, ex.getMessage()));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null, "An unexpected error occurred."));
        }
    }

    @GetMapping("/by-family-name-id")
    @PreAuthorize("hasAuthority('" + Permissions.QUALITY_TASK_SETUP_READ_PERMISSION + "')")
    public ResponseEntity<CustomResponse<List<QualityTaskSetupResponseDto>>> getSetupByFamilyNameId(@RequestParam Long familyNameId) {
        try {
            List<QualityTaskSetupResponseDto> responseDtos = service.getTasksByFamilyNameId(familyNameId);
            return ResponseEntity.ok(new CustomResponse<>(1000, responseDtos));
        } catch (ResourceNotFoundException ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new CustomResponse<>(1003, null, ex.getMessage()));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null, "An unexpected error occurred."));
        }
    }
}
