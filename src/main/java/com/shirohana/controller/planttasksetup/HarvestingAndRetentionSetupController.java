package com.shirohana.controller.planttasksetup;


import com.shirohana.constants.Permissions;
import com.shirohana.dto.CustomResponse;
import com.shirohana.dto.planttasksetup.harvestingandretention.HarvestingAndRetentionSetupRequestDto;
import com.shirohana.dto.planttasksetup.harvestingandretention.HarvestingAndRetentionSetupResponseDto;
import com.shirohana.exception.ResourceNotFoundException;
import com.shirohana.service.interfaces.planttasksetup.HarvestingAndRetentionSetupService;
import jakarta.validation.ValidationException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/harvesting-and-retention-setup")
@RequiredArgsConstructor
public class HarvestingAndRetentionSetupController {

    private final HarvestingAndRetentionSetupService harvestingAndRetentionSetupService;

    @PostMapping
    @PreAuthorize("hasAuthority('" + Permissions.HARVESTING_AND_RETENTION_CREATE_PERMISSION + "')") // Check for create permission
    public ResponseEntity<CustomResponse<HarvestingAndRetentionSetupResponseDto>> createSetup(@Validated @RequestBody HarvestingAndRetentionSetupRequestDto requestDto) {
        try {
            HarvestingAndRetentionSetupResponseDto responseDto = harvestingAndRetentionSetupService.createHarvestingAndRetentionSetup(requestDto);
            return ResponseEntity.status(HttpStatus.CREATED).body(new CustomResponse<>(1001, responseDto));
        } catch (ValidationException ex) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new CustomResponse<>(1002, null, ex.getMessage()));
        } catch (ResourceNotFoundException ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new CustomResponse<>(1003, null, ex.getMessage()));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null, "An unexpected error occurred."));
        }
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('" + Permissions.HARVESTING_AND_RETENTION_UPDATE_PERMISSION + "')") // Check for update permission
    public ResponseEntity<CustomResponse<HarvestingAndRetentionSetupResponseDto>> updateSetup(@PathVariable Long id, @Validated @RequestBody HarvestingAndRetentionSetupRequestDto requestDto) {
        try {
            HarvestingAndRetentionSetupResponseDto responseDto = harvestingAndRetentionSetupService.updateHarvestingAndRetentionSetup(id, requestDto);
            return ResponseEntity.ok(new CustomResponse<>(1000, responseDto));
        } catch (ValidationException ex) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new CustomResponse<>(1002, null, ex.getMessage()));
        } catch (ResourceNotFoundException ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new CustomResponse<>(1003, null, ex.getMessage()));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null, "An unexpected error occurred."));
        }
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('" + Permissions.HARVESTING_AND_RETENTION_DELETE_PERMISSION + "')") // Check for delete permission
    public ResponseEntity<CustomResponse<Void>> deleteSetup(@PathVariable Long id) {
        try {
            harvestingAndRetentionSetupService.deleteHarvestingAndRetentionSetup(id);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body(new CustomResponse<>(1000, null));
        } catch (ResourceNotFoundException ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new CustomResponse<>(1003, null, ex.getMessage()));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null, "An unexpected error occurred."));
        }
    }

    @GetMapping
    @PreAuthorize("hasAuthority('" + Permissions.HARVESTING_AND_RETENTION_READ_PERMISSION + "')") // Check for read permission
    public ResponseEntity<CustomResponse<List<HarvestingAndRetentionSetupResponseDto>>> getAllSetups() {
        try {
            List<HarvestingAndRetentionSetupResponseDto> responseDtos = harvestingAndRetentionSetupService.getAllHarvestingAndRetentionSetups();
            return ResponseEntity.ok(new CustomResponse<>(1000, responseDtos));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null, "An unexpected error occurred."));
        }
    }

    @GetMapping("/by-family-name-id")
    @PreAuthorize("hasAuthority('" + Permissions.HARVESTING_AND_RETENTION_READ_PERMISSION + "')") // Check for read permission
    public ResponseEntity<CustomResponse<HarvestingAndRetentionSetupResponseDto>> getSetupByFamilyNameId(@RequestParam Long familyNameId) {
        try {
            HarvestingAndRetentionSetupResponseDto responseDto = harvestingAndRetentionSetupService.getHarvestingAndRetentionSetupByFamilyNameId(familyNameId);
            return ResponseEntity.ok(new CustomResponse<>(1000, responseDto));
        } catch (ResourceNotFoundException ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new CustomResponse<>(1003, null, ex.getMessage()));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null, "An unexpected error occurred."));
        }
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('" + Permissions.HARVESTING_AND_RETENTION_READ_PERMISSION + "')") // Check for read permission
    public ResponseEntity<CustomResponse<HarvestingAndRetentionSetupResponseDto>> getSetupById(@PathVariable Long id) {
        try {
            HarvestingAndRetentionSetupResponseDto responseDto = harvestingAndRetentionSetupService.getHarvestingAndRetentionSetupById(id);
            return ResponseEntity.ok(new CustomResponse<>(1000, responseDto));
        } catch (ResourceNotFoundException ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new CustomResponse<>(1003, null, ex.getMessage()));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null, "An unexpected error occurred."));
        }
    }

}
