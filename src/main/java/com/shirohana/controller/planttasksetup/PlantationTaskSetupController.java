package com.shirohana.controller.planttasksetup;


import com.shirohana.constants.Permissions;
import com.shirohana.dto.CustomResponse;
import com.shirohana.dto.planttasksetup.plantationtasksetup.requestdtos.create.PlantationTaskSetupCreateRequestDto;
import com.shirohana.dto.planttasksetup.plantationtasksetup.requestdtos.update.PlantationTaskSetupUpdateRequestDto;
import com.shirohana.dto.planttasksetup.plantationtasksetup.responsedtos.PlantationTaskSetupResponseDto;
import com.shirohana.exception.ResourceNotFoundException;
import com.shirohana.service.interfaces.planttasksetup.PlantationTaskSetupService;
import jakarta.validation.ValidationException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/plantation-task-setup")
@RequiredArgsConstructor
public class PlantationTaskSetupController {

    private final PlantationTaskSetupService service;

    @PostMapping
    @PreAuthorize("hasAuthority('" + Permissions.PLANTATION_TASK_SETUP_CREATE_PERMISSION + "')")
    public ResponseEntity<CustomResponse<PlantationTaskSetupResponseDto>> createSetup(@Validated @RequestBody PlantationTaskSetupCreateRequestDto requestDto) {
        try {
            PlantationTaskSetupResponseDto responseDto = service.createTask(requestDto);
            return ResponseEntity.status(HttpStatus.CREATED).body(new CustomResponse<>(1001, responseDto));
        } catch (ValidationException ex) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new CustomResponse<>(1002, null, ex.getMessage()));
        } catch (ResourceNotFoundException ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new CustomResponse<>(1003, null, ex.getMessage()));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null, "An unexpected error occurred."));
        }
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('" + Permissions.PLANTATION_TASK_SETUP_UPDATE_PERMISSION + "')")
    public ResponseEntity<CustomResponse<PlantationTaskSetupResponseDto>> updateSetup(@PathVariable Long id, @Validated @RequestBody PlantationTaskSetupUpdateRequestDto requestDto) {
        try {
            PlantationTaskSetupResponseDto responseDto = service.updateTask(id, requestDto);
            return ResponseEntity.ok(new CustomResponse<>(1000, responseDto));
        } catch (ValidationException ex) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new CustomResponse<>(1002, null, ex.getMessage()));
        } catch (ResourceNotFoundException ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new CustomResponse<>(1003, null, ex.getMessage()));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null, "An unexpected error occurred."));
        }
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('" + Permissions.PLANTATION_TASK_SETUP_DELETE_PERMISSION + "')")
    public ResponseEntity<CustomResponse<Void>> deleteSetup(@PathVariable Long id) {
        try {
            service.deleteTask(id);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body(new CustomResponse<>(1000, null));
        } catch (ResourceNotFoundException ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new CustomResponse<>(1003, null, ex.getMessage()));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null, "An unexpected error occurred."));
        }
    }

    @GetMapping
    @PreAuthorize("hasAuthority('" + Permissions.PLANTATION_TASK_SETUP_READ_PERMISSION + "')")
    public ResponseEntity<CustomResponse<List<PlantationTaskSetupResponseDto>>> getAllSetups() {
        try {
            List<PlantationTaskSetupResponseDto> responseDtos = service.getAllTasks();
            return ResponseEntity.ok(new CustomResponse<>(1000, responseDtos));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null, "An unexpected error occurred."));
        }
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('" + Permissions.PLANTATION_TASK_SETUP_READ_PERMISSION + "')")
    public ResponseEntity<CustomResponse<PlantationTaskSetupResponseDto>> getSetupById(@PathVariable Long id) {
        try {
            PlantationTaskSetupResponseDto responseDto = service.getTaskById(id);
            return ResponseEntity.ok(new CustomResponse<>(1000, responseDto));
        } catch (ResourceNotFoundException ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new CustomResponse<>(1003, null, ex.getMessage()));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null, "An unexpected error occurred."));
        }
    }

    @GetMapping("/by-family-name-id")
    @PreAuthorize("hasAuthority('" + Permissions.PLANTATION_TASK_SETUP_READ_PERMISSION + "')")
    public ResponseEntity<CustomResponse<List<PlantationTaskSetupResponseDto>>> getSetupByFamilyNameId(@RequestParam Long familyNameId) {
        try {
            List<PlantationTaskSetupResponseDto> responseDtos = service.getTasksByFamilyNameId(familyNameId);
            return ResponseEntity.ok(new CustomResponse<>(1000, responseDtos));
        } catch (ResourceNotFoundException ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new CustomResponse<>(1003, null, ex.getMessage()));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null, "An unexpected error occurred."));
        }
    }
}
