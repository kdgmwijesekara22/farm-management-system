package com.shirohana.controller.planttasksetup;


import com.shirohana.constants.Permissions;
import com.shirohana.dto.CustomResponse;
import com.shirohana.dto.planttasksetup.maintenancetasksetup.requestDtos.create.MaintenanceTaskSetupCreateRequestDto;
import com.shirohana.dto.planttasksetup.maintenancetasksetup.MaintenanceTaskSetupResponseDto;
import com.shirohana.dto.planttasksetup.maintenancetasksetup.requestDtos.update.MaintenanceTaskSetupUpdateRequestDto;
import com.shirohana.exception.ResourceNotFoundException;
import com.shirohana.service.interfaces.planttasksetup.MaintenanceTaskSetupService;
import jakarta.validation.ValidationException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/maintenance-task-setup")
@RequiredArgsConstructor
public class MaintenanceTaskSetupController {
    private final MaintenanceTaskSetupService service;

    @PostMapping
    @PreAuthorize("hasAuthority('" + Permissions.MAINTENANCE_TASK_SETUP_CREATE_PERMISSION + "')")
    public ResponseEntity<CustomResponse<MaintenanceTaskSetupResponseDto>> createSetup(@Validated @RequestBody MaintenanceTaskSetupCreateRequestDto requestDto) {
        try {
            MaintenanceTaskSetupResponseDto responseDto = service.createTask(requestDto);
            return ResponseEntity.status(HttpStatus.CREATED).body(new CustomResponse<>(1001, responseDto));
        } catch (ValidationException ex) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new CustomResponse<>(1002, null, ex.getMessage()));
        } catch (ResourceNotFoundException ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new CustomResponse<>(1003, null, ex.getMessage()));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null, "An unexpected error occurred."));
        }
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('" + Permissions.MAINTENANCE_TASK_SETUP_UPDATE_PERMISSION + "')")
    public ResponseEntity<CustomResponse<MaintenanceTaskSetupResponseDto>> updateSetup(@PathVariable Long id, @Validated @RequestBody MaintenanceTaskSetupUpdateRequestDto requestDto) {
        try {
            MaintenanceTaskSetupResponseDto responseDto = service.updateTask(id, requestDto);
            return ResponseEntity.ok(new CustomResponse<>(1000, responseDto));
        } catch (ValidationException ex) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new CustomResponse<>(1002, null, ex.getMessage()));
        } catch (ResourceNotFoundException ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new CustomResponse<>(1003, null, ex.getMessage()));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null, "An unexpected error occurred."));
        }
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('" + Permissions.MAINTENANCE_TASK_SETUP_DELETE_PERMISSION + "')")
    public ResponseEntity<CustomResponse<Void>> deleteSetup(@PathVariable Long id) {
        try {
            service.deleteTask(id);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body(new CustomResponse<>(1000, null));
        } catch (ResourceNotFoundException ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new CustomResponse<>(1003, null, ex.getMessage()));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null, "An unexpected error occurred."));
        }
    }

    @GetMapping
    @PreAuthorize("hasAuthority('" + Permissions.MAINTENANCE_TASK_SETUP_READ_PERMISSION + "')")
    public ResponseEntity<CustomResponse<List<MaintenanceTaskSetupResponseDto>>> getAllSetups() {
        try {
            List<MaintenanceTaskSetupResponseDto> responseDtos = service.getAllTasks();
            return ResponseEntity.ok(new CustomResponse<>(1000, responseDtos));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null, "An unexpected error occurred."));
        }
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('" + Permissions.MAINTENANCE_TASK_SETUP_READ_PERMISSION + "')")
    public ResponseEntity<CustomResponse<MaintenanceTaskSetupResponseDto>> getSetupById(@PathVariable Long id) {
        try {
            MaintenanceTaskSetupResponseDto responseDto = service.getTaskById(id);
            return ResponseEntity.ok(new CustomResponse<>(1000, responseDto));
        } catch (ResourceNotFoundException ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new CustomResponse<>(1003, null, ex.getMessage()));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null, "An unexpected error occurred."));
        }
    }

    @GetMapping("/by-family-name-id")
    @PreAuthorize("hasAuthority('" + Permissions.MAINTENANCE_TASK_SETUP_READ_PERMISSION + "')")
    public ResponseEntity<CustomResponse<List<MaintenanceTaskSetupResponseDto>>> getSetupByFamilyNameId(@RequestParam Long familyNameId) {
        try {
            List<MaintenanceTaskSetupResponseDto> responseDtos = service.getMaintenanceTaskSetupByFamilyNameId(familyNameId);
            return ResponseEntity.ok(new CustomResponse<>(1000, responseDtos));
        } catch (ResourceNotFoundException ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new CustomResponse<>(1003, null, ex.getMessage()));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null, "An unexpected error occurred."));
        }
    }
}
