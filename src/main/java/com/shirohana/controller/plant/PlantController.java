package com.shirohana.controller.plant;

import com.shirohana.constants.Permissions;
import com.shirohana.dto.CustomResponse;
import com.shirohana.dto.plant.PlantRequestDto;
import com.shirohana.dto.plant.PlantResponseDto;
import com.shirohana.exception.ResourceNotFoundException;
import com.shirohana.service.interfaces.plant.PlantService;
import jakarta.validation.Valid;
import jakarta.validation.ValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/plants")
public class PlantController {
    private final PlantService plantService;

    @Autowired
    public PlantController(PlantService plantService) {
        this.plantService = plantService;
    }

    @PostMapping
    @PreAuthorize("hasAuthority('" + Permissions.PLANT_CREATE_PERMISSION + "')") // Check for create permission
    public ResponseEntity<CustomResponse<PlantResponseDto>> createPlant(@Valid @RequestBody PlantRequestDto plantRequestDto) {
        try {
            PlantResponseDto createdPlant = plantService.createPlant(plantRequestDto);
            return new ResponseEntity<>(new CustomResponse<>(1001, createdPlant), HttpStatus.CREATED);
        } catch (ValidationException ex) {
            return new ResponseEntity<>(new CustomResponse<>(1002, null, ex.getMessage()), HttpStatus.BAD_REQUEST);
        } catch (Exception ex) {
            return new ResponseEntity<>(new CustomResponse<>(1004, null, ex.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('" + Permissions.PLANT_UPDATE_PERMISSION + "')") // Check for update permission
    public ResponseEntity<CustomResponse<PlantResponseDto>> updatePlant(@PathVariable Long id, @Valid @RequestBody PlantRequestDto plantRequestDto) {
        try {
            PlantResponseDto updatedPlant = plantService.updatePlant(id, plantRequestDto);
            return new ResponseEntity<>(new CustomResponse<>(1000, updatedPlant), HttpStatus.OK);
        } catch (ResourceNotFoundException ex) {
            return new ResponseEntity<>(new CustomResponse<>(1003, null, "Plant not found"), HttpStatus.NOT_FOUND);
        } catch (ValidationException ex) {
            return new ResponseEntity<>(new CustomResponse<>(1002, null, "Validation error"), HttpStatus.BAD_REQUEST);
        } catch (Exception ex) {
            return new ResponseEntity<>(new CustomResponse<>(1004, null, ex.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('" + Permissions.PLANT_DELETE_PERMISSION + "')") // Check for delete permission
    public ResponseEntity<CustomResponse<Void>> deletePlant(@PathVariable Long id) {
        try {
            plantService.deletePlant(id);
            return new ResponseEntity<>(new CustomResponse<>(1000, null), HttpStatus.NO_CONTENT);
        } catch (ResourceNotFoundException ex) {
            return new ResponseEntity<>(new CustomResponse<>(1003, null, "Plant not found"), HttpStatus.NOT_FOUND);
        } catch (Exception ex) {
            return new ResponseEntity<>(new CustomResponse<>(1004, null, ex.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/{familyName}")
    @PreAuthorize("hasAuthority('" + Permissions.PLANT_READ_PERMISSION + "')") // Check for read permission
    public ResponseEntity<CustomResponse<PlantResponseDto>> getPlantByFamilyName(@PathVariable String familyName) {
        try {
            PlantResponseDto plant = plantService.getPlantByFamilyName(familyName);
            return new ResponseEntity<>(new CustomResponse<>(1000, plant), HttpStatus.OK);
        } catch (ResourceNotFoundException ex) {
            return new ResponseEntity<>(new CustomResponse<>(1003, null, "Plant not found"), HttpStatus.NOT_FOUND);
        } catch (Exception ex) {
            return new ResponseEntity<>(new CustomResponse<>(1004, null, ex.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping
    @PreAuthorize("hasAuthority('" + Permissions.PLANT_READ_PERMISSION + "')") // Check for read permission
    public ResponseEntity<CustomResponse<List<PlantResponseDto>>> getAllPlants() {
        try {
            List<PlantResponseDto> plants = plantService.getAllPlants();
            return new ResponseEntity<>(new CustomResponse<>(1000, plants), HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(new CustomResponse<>(1004, null, ex.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
