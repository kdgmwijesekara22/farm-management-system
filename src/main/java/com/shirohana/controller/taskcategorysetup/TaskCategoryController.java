package com.shirohana.controller.taskcategorysetup;

import com.shirohana.constants.Permissions;
import com.shirohana.dto.CustomResponse;
import com.shirohana.dto.taskcategorysetup.TaskCategorySetupRequestDto;
import com.shirohana.dto.taskcategorysetup.TaskCategorySetupResponseDto;
import com.shirohana.exception.ResourceNotFoundException;
import com.shirohana.service.interfaces.taskcategorysetup.TaskCategorySetupService;
import jakarta.validation.ValidationException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/task-category-setups")
@RequiredArgsConstructor
public class TaskCategoryController {
    private final TaskCategorySetupService taskCategorySetupService;

    @PostMapping
    @PreAuthorize("hasAuthority('" + Permissions.TASK_CATEGORY_SETUP_CREATE_PERMISSION + "')")
    public ResponseEntity<CustomResponse<TaskCategorySetupResponseDto>> createTaskCategorySetup(@Validated @RequestBody TaskCategorySetupRequestDto requestDto) {
        try {
            TaskCategorySetupResponseDto responseDto = taskCategorySetupService.createTaskCategorySetup(requestDto);
            return ResponseEntity.status(HttpStatus.CREATED).body(new CustomResponse<>(1001, responseDto));
        } catch (ValidationException ex) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new CustomResponse<>(1002, null, ex.getMessage()));
        } catch (ResourceNotFoundException ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new CustomResponse<>(1003, null, ex.getMessage()));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null, ex.getMessage()));
        }
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('" + Permissions.TASK_CATEGORY_SETUP_UPDATE_PERMISSION + "')")
    public ResponseEntity<CustomResponse<TaskCategorySetupResponseDto>> updateTaskCategorySetup(@PathVariable Long id, @Validated @RequestBody TaskCategorySetupRequestDto requestDto) {
        try {
            TaskCategorySetupResponseDto responseDto = taskCategorySetupService.updateTaskCategorySetup(id, requestDto);
            return ResponseEntity.ok(new CustomResponse<>(1000, responseDto));
        } catch (ValidationException ex) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new CustomResponse<>(1002, null, ex.getMessage()));
        } catch (ResourceNotFoundException ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new CustomResponse<>(1003, null, ex.getMessage()));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null, ex.getMessage()));
        }
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('" + Permissions.TASK_CATEGORY_SETUP_DELETE_PERMISSION + "')")
    public ResponseEntity<CustomResponse<Void>> deleteTaskCategorySetup(@PathVariable Long id) {
        try {
            taskCategorySetupService.deleteTaskCategorySetup(id);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body(new CustomResponse<>(1000, null));
        } catch (ResourceNotFoundException ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new CustomResponse<>(1003, null, ex.getMessage()));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null, ex.getMessage()));
        }
    }

    @GetMapping
    @PreAuthorize("hasAuthority('" + Permissions.TASK_CATEGORY_SETUP_READ_PERMISSION + "')")
    public ResponseEntity<CustomResponse<List<TaskCategorySetupResponseDto>>> getAllTaskCategorySetups() {
        try {
            List<TaskCategorySetupResponseDto> responseDtos = taskCategorySetupService.getAllTaskCategorySetups();
            return ResponseEntity.ok(new CustomResponse<>(1000, responseDtos));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null, ex.getMessage()));
        }
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('" + Permissions.TASK_CATEGORY_SETUP_READ_PERMISSION + "')")
    public ResponseEntity<CustomResponse<TaskCategorySetupResponseDto>> getTaskCategorySetupById(@PathVariable Long id) {
        try {
            TaskCategorySetupResponseDto responseDto = taskCategorySetupService.getTaskCategorySetupById(id);
            return ResponseEntity.ok(new CustomResponse<>(1000, responseDto));
        } catch (ResourceNotFoundException ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new CustomResponse<>(1003, null, ex.getMessage()));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null, ex.getMessage()));
        }
    }

    @GetMapping("/task-type/{taskType}")
    @PreAuthorize("hasAuthority('" + Permissions.TASK_CATEGORY_SETUP_READ_PERMISSION + "')")
    public ResponseEntity<CustomResponse<List<TaskCategorySetupResponseDto>>> getTaskCategorySetupsByTaskType(@PathVariable String taskType) {
        try {
            List<TaskCategorySetupResponseDto> responseDtos = taskCategorySetupService.getTaskCategorySetupsByTaskType(taskType);
            return ResponseEntity.ok(new CustomResponse<>(1000, responseDtos));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null, ex.getMessage()));
        }
    }
}
