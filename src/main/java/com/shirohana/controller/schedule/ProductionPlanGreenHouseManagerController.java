package com.shirohana.controller.schedule;

import com.shirohana.constants.Permissions;
import com.shirohana.dto.CustomResponse;
import com.shirohana.dto.schedule.production_plan_green_house.*;
import com.shirohana.service.interfaces.schedule.ProductionPlanGreenHouseManagerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/production-plans/greenhouse")
@RequiredArgsConstructor
@Slf4j
public class ProductionPlanGreenHouseManagerController {

    private final ProductionPlanGreenHouseManagerService productionPlanGreenHouseManagerService;

    @PostMapping
    @PreAuthorize("hasAuthority('" + Permissions.PRIVILEGE_CREATE_PRODUCTION_PLAN + "')")
    public ResponseEntity<CustomResponse<ProductionPlanGHManagerResponseDto>> createProductionPlan(@RequestBody ProductionPlanGHManagerRequestDto requestDto) {
        try {
            log.info("Received request: {}", requestDto);
            ProductionPlanGHManagerResponseDto responseDto = productionPlanGreenHouseManagerService.saveProductionPlanGreenHouseManager(requestDto);
            return ResponseEntity.status(201).body(new CustomResponse<>(1001, responseDto));
        } catch (Exception e) {
            log.error("Error creating production plan for greenhouse", e);
            return ResponseEntity.status(500).body(new CustomResponse<>(1004, null, e.getMessage()));
        }
    }

    @GetMapping("/greenhouse/{greenhouseId}")
    @PreAuthorize("hasAuthority('" + Permissions.PRIVILEGE_VIEW_PRODUCTION_PLAN + "')")
    public ResponseEntity<CustomResponse<List<ProductionPlanGHManagerResponseDto>>> getProductionPlansByGreenHouseId(@PathVariable Long greenhouseId) {
        try {
            List<ProductionPlanGHManagerResponseDto> productionPlans = productionPlanGreenHouseManagerService.getProductionPlansByGreenHouseId(greenhouseId);
            return ResponseEntity.ok(new CustomResponse<>(1000, productionPlans));
        } catch (Exception e) {
            log.error("Error fetching production plans by greenhouse ID", e);
            return ResponseEntity.status(500).body(new CustomResponse<>(1004, null, e.getMessage()));
        }
    }
}