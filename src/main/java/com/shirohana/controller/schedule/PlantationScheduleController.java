package com.shirohana.controller.schedule;

import com.shirohana.constants.Permissions;
import com.shirohana.dto.CustomResponse;
import com.shirohana.dto.schedule.pshedule.FamilyResponseDto;
import com.shirohana.dto.schedule.pshedule.PlantationScheduleRequestDto;
import com.shirohana.dto.schedule.pshedule.PlantationScheduleResponseDto;
import com.shirohana.service.interfaces.schedule.PlantationScheduleService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/schedules")
@RequiredArgsConstructor
public class PlantationScheduleController {

    private static final Logger logger = LoggerFactory.getLogger(PlantationScheduleController.class);
    private final PlantationScheduleService plantationScheduleService;

    @PostMapping
    @PreAuthorize("hasAuthority('" + Permissions.PRIVILEGE_CREATE_SCHEDULE + "')")
    public ResponseEntity<CustomResponse<PlantationScheduleResponseDto>> createSchedule(@RequestBody PlantationScheduleRequestDto plantationScheduleRequestDto) {
        try {
            logger.info("Received request: {}", plantationScheduleRequestDto);
            if (plantationScheduleRequestDto.getPScheduleVarietyRequestDtoList() == null) {
                logger.error("pScheduleVarietyRequestDtoList is null");
                return ResponseEntity.badRequest().body(new CustomResponse<>(1002, null, "pScheduleVarietyRequestDtoList cannot be null"));
            }
            PlantationScheduleResponseDto savedSchedule = plantationScheduleService.saveSchedule(plantationScheduleRequestDto);
            return ResponseEntity.status(201).body(new CustomResponse<>(1001, savedSchedule));
        } catch (Exception e) {
            logger.error("Error creating schedule", e);
            return ResponseEntity.status(500).body(new CustomResponse<>(1004, null, e.getMessage()));
        }
    }

    @GetMapping
    @PreAuthorize("hasAuthority('" + Permissions.PRIVILEGE_VIEW_FAMILIES + "')")
    public ResponseEntity<CustomResponse<List<FamilyResponseDto>>> getAllFamilies() {
        try {
            List<FamilyResponseDto> families = plantationScheduleService.getAllFamilies();
            return ResponseEntity.ok(new CustomResponse<>(1000, families));
        } catch (Exception e) {
            return ResponseEntity.status(500).body(new CustomResponse<>(1004, null, e.getMessage()));
        }
    }

    @DeleteMapping("/schedule/{scheduleId}")
    @PreAuthorize("hasAuthority('" + Permissions.PRIVILEGE_DELETE_PLANTATION_SCHEDULE + "')")
    public ResponseEntity<CustomResponse<Boolean>> deletePlantationSchedule(@PathVariable Long scheduleId) {
        try {
            boolean deleted = plantationScheduleService.deletePlantationSchedule(scheduleId);
            if (deleted) {
                return ResponseEntity.ok(new CustomResponse<>(1000, true, "Plantation Schedule deleted successfully."));
            } else {
                return ResponseEntity.status(400).body(new CustomResponse<>(1002, false, "Plantation Schedule cannot be deleted due to associated Plantation Plans."));
            }
        } catch (Exception e) {
            logger.error("Error deleting plantation schedule", e);
            return ResponseEntity.status(500).body(new CustomResponse<>(1004, false, e.getMessage()));
        }
    }

}
