package com.shirohana.controller.schedule;

import com.shirohana.constants.Permissions;
import com.shirohana.dto.CustomResponse;
import com.shirohana.dto.schedule.pshedule.FamilyResponseDto;
import com.shirohana.dto.schedule.pshedule.PlantationScheduleResponseDto;
import com.shirohana.dto.schedule.psplan.PlantationPlanRequestDto;
import com.shirohana.dto.schedule.psplan.PlantationPlanResponseDto;
import com.shirohana.service.interfaces.schedule.PlantationScheduleService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/plantation-plans")
@RequiredArgsConstructor
public class PlantationPlanController {

    private static final Logger logger = LoggerFactory.getLogger(PlantationPlanController.class);
    private final PlantationScheduleService plantationScheduleService;

    @PostMapping
    @PreAuthorize("hasAuthority('" + Permissions.PRIVILEGE_CREATE_PLANTATION_PLAN + "')")
    public ResponseEntity<CustomResponse<PlantationPlanResponseDto>> createPlantationPlan(@RequestBody PlantationPlanRequestDto plantationPlanRequestDto) {
        try {
            logger.info("Received request: {}", plantationPlanRequestDto);
            PlantationPlanResponseDto responseDto = plantationScheduleService.savePlantationPlan(plantationPlanRequestDto);
            return ResponseEntity.status(201).body(new CustomResponse<>(1001, responseDto));
        } catch (Exception e) {
            logger.error("Error creating plantation plan", e);
            return ResponseEntity.status(500).body(new CustomResponse<>(1004, null, e.getMessage()));
        }
    }

    @GetMapping("/families")
    @PreAuthorize("hasAuthority('" + Permissions.PRIVILEGE_VIEW_FAMILIES_PLANTATION_PLAN+ "')")
    public ResponseEntity<CustomResponse<List<FamilyResponseDto>>> getAllFamilies() {
        try {
            List<FamilyResponseDto> families = plantationScheduleService.getAllFamilies();
            return ResponseEntity.ok(new CustomResponse<>(1000, families));
        } catch (Exception e) {
            logger.error("Error fetching families", e);
            return ResponseEntity.status(500).body(new CustomResponse<>(1004, null, e.getMessage()));
        }
    }

    @GetMapping("/schedules")
    @PreAuthorize("hasAuthority('" + Permissions.PRIVILEGE_VIEW_PLANTATION_SCHEDULES + "')")
    public ResponseEntity<CustomResponse<List<PlantationScheduleResponseDto>>> getAllPlantationSchedules() {
        try {
            List<PlantationScheduleResponseDto> plantationSchedules = plantationScheduleService.getAllPlantationSchedules();
            return ResponseEntity.ok(new CustomResponse<>(1000, plantationSchedules));
        } catch (Exception e) {
            logger.error("Error fetching plantation schedules", e);
            return ResponseEntity.status(500).body(new CustomResponse<>(1004, null, e.getMessage()));
        }
    }

    @GetMapping("/schedule/{plantationScheduleId}")
    @PreAuthorize("hasAuthority('" + Permissions.PRIVILEGE_VIEW_PLANTATION_PLANS + "')")
    public ResponseEntity<CustomResponse<List<PlantationPlanResponseDto>>> getAllPlansByPlantationScheduleId(@PathVariable Long plantationScheduleId) {
        try {
            List<PlantationPlanResponseDto> plantationPlans = plantationScheduleService.getAllPlansByPlantationScheduleId(plantationScheduleId);
            return ResponseEntity.ok(new CustomResponse<>(1000, plantationPlans));
        } catch (Exception e) {
            logger.error("Error fetching plantation plans by schedule ID", e);
            return ResponseEntity.status(500).body(new CustomResponse<>(1004, null, e.getMessage()));
        }
    }

    @GetMapping("/plans")
    @PreAuthorize("hasAuthority('" + Permissions.PRIVILEGE_VIEW_ALL_PLANTATION_PLANS + "')")
    public ResponseEntity<CustomResponse<List<PlantationPlanResponseDto>>> getAllPlantationPlans() {
        try {
            List<PlantationPlanResponseDto> plans = plantationScheduleService.getAllPlantationPlans();
            return ResponseEntity.ok(new CustomResponse<>(1000, plans));
        } catch (Exception e) {
            logger.error("Error fetching plantation plans", e);
            return ResponseEntity.status(500).body(new CustomResponse<>(1004, null, e.getMessage()));
        }
    }

    @GetMapping("/farm/{farmId}")
    @PreAuthorize("hasAuthority('" + Permissions.PRIVILEGE_VIEW_PLANTATION_PLANS + "')")
    public ResponseEntity<CustomResponse<List<PlantationPlanResponseDto>>> getPlantationPlansByFarmId(@PathVariable Long farmId) {
        try {
            List<PlantationPlanResponseDto> plantationPlans = plantationScheduleService.getPlantationPlansByFarmId(farmId);
            return ResponseEntity.ok(new CustomResponse<>(1000, plantationPlans));
        } catch (IllegalArgumentException e) {
            logger.error("Validation error: {}", e.getMessage());
            return ResponseEntity.status(400).body(new CustomResponse<>(1002, null, e.getMessage()));
        } catch (Exception e) {
            logger.error("Error fetching plantation plans by farm ID", e);
            return ResponseEntity.status(500).body(new CustomResponse<>(1004, null, e.getMessage()));
        }
    }

    @DeleteMapping("/{planId}")
    @PreAuthorize("hasAuthority('" + Permissions.PRIVILEGE_DELETE_PLANTATION_PLAN + "')")
    public ResponseEntity<CustomResponse<Boolean>> deletePlantationPlan(@PathVariable Long planId) {
        try {
            boolean isDeleted = plantationScheduleService.deletePlantationPlan(planId);

            if (isDeleted) {
                return ResponseEntity.ok(new CustomResponse<>(1000, true, "Plantation Plan deleted successfully."));
            } else {
                return ResponseEntity.status(400).body(new CustomResponse<>(1002, false, "Failed to delete Plantation Plan. It may be already deleted, expired, or referenced by active records."));
            }
        } catch (Exception e) {
            logger.error("Error deleting plantation plan", e);
            return ResponseEntity.status(500).body(new CustomResponse<>(1004, false, "An unexpected error occurred while deleting the Plantation Plan."));
        }
    }


}
