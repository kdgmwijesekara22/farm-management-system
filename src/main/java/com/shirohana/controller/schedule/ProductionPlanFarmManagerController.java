package com.shirohana.controller.schedule;

import com.shirohana.constants.Permissions;
import com.shirohana.dto.CustomResponse;
import com.shirohana.dto.schedule.production_plan.ProductionPlanFarmManagerRequestDto;
import com.shirohana.dto.schedule.production_plan.ProductionPlanFarmManagerResponseDto;
import com.shirohana.dto.schedule.psplan.PlantationPlanResponseDto;
import com.shirohana.entity.user.User;
import com.shirohana.service.interfaces.schedule.ProductionPlanFarmManagerService;
import com.shirohana.service.interfaces.schedule.PlantationScheduleService;
import com.shirohana.service.interfaces.user.UserService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/production-plans")
@RequiredArgsConstructor
public class ProductionPlanFarmManagerController {

    private static final Logger logger = LoggerFactory.getLogger(ProductionPlanFarmManagerController.class);
    private final ProductionPlanFarmManagerService productionPlanFarmManagerService;
    private final PlantationScheduleService plantationScheduleService;
    private final UserService userService;

    @PostMapping
    @PreAuthorize("hasAuthority('" + Permissions.PRIVILEGE_CREATE_PRODUCTION_PLAN + "')")
    public ResponseEntity<CustomResponse<ProductionPlanFarmManagerResponseDto>> createProductionPlan(@RequestBody ProductionPlanFarmManagerRequestDto productionPlanFarmManagerRequestDto) {
        try {
            logger.info("Received request: {}", productionPlanFarmManagerRequestDto);
            ProductionPlanFarmManagerResponseDto responseDto = productionPlanFarmManagerService.saveProductionPlanFarmManager(productionPlanFarmManagerRequestDto);
            return ResponseEntity.status(201).body(new CustomResponse<>(1001, responseDto));
        } catch (Exception e) {
            logger.error("Error creating production plan", e);
            return ResponseEntity.status(500).body(new CustomResponse<>(1004, null, e.getMessage()));
        }
    }

    @GetMapping
    @PreAuthorize("hasAuthority('" + Permissions.PRIVILEGE_VIEW_PRODUCTION_PLANS + "')")
    public ResponseEntity<CustomResponse<List<ProductionPlanFarmManagerResponseDto>>> getAllProductionPlans() {
        try {
            List<ProductionPlanFarmManagerResponseDto> productionPlans = productionPlanFarmManagerService.getAllProductionPlanFarmManager();
            return ResponseEntity.ok(new CustomResponse<>(1000, productionPlans));
        } catch (Exception e) {
            logger.error("Error fetching production plans", e);
            return ResponseEntity.status(500).body(new CustomResponse<>(1004, null, e.getMessage()));
        }
    }

    @GetMapping("/farms")
    @PreAuthorize("hasAuthority('" + Permissions.PRIVILEGE_VIEW_USER_FARM_PRODUCTION_PLANS + "')")
    public ResponseEntity<CustomResponse<List<ProductionPlanFarmManagerResponseDto>>> getAllProductionPlansByUserFarms() {
        try {
            User currentUser = userService.getCurrentUser();
            Set<Long> farmIds = currentUser.getFarms().stream().map(farm -> farm.getId()).collect(Collectors.toSet());
            List<ProductionPlanFarmManagerResponseDto> productionPlans = productionPlanFarmManagerService.getAllProductionPlansByFarms(farmIds);
            return ResponseEntity.ok(new CustomResponse<>(1000, productionPlans));
        } catch (Exception e) {
            logger.error("Error fetching production plans by user farms", e);
            return ResponseEntity.status(500).body(new CustomResponse<>(1004, null, e.getMessage()));
        }
    }

    @GetMapping("/plantation-plans")
    @PreAuthorize("hasAuthority('" + Permissions.PRIVILEGE_VIEW_USER_FARM_PLANTATION_PLANS + "')")
    public ResponseEntity<CustomResponse<List<PlantationPlanResponseDto>>> getAllPlantationPlansByUserFarms() {
        try {
            User currentUser = userService.getCurrentUser();
            Set<Long> farmIds = currentUser.getFarms().stream().map(farm -> farm.getId()).collect(Collectors.toSet());
            List<PlantationPlanResponseDto> plantationPlans = plantationScheduleService.getAllPlantationPlansByFarmIds(farmIds);
            return ResponseEntity.ok(new CustomResponse<>(1000, plantationPlans));
        } catch (Exception e) {
            logger.error("Error fetching plantation plans by user farms", e);
            return ResponseEntity.status(500).body(new CustomResponse<>(1004, null, e.getMessage()));
        }
    }

    @GetMapping("/farms/{farmId}")
    @PreAuthorize("hasAuthority('" + Permissions.PRIVILEGE_VIEW_FARM_PRODUCTION_PLANS + "')")
    public ResponseEntity<CustomResponse<List<ProductionPlanFarmManagerResponseDto>>> getAllProductionPlansByFarmId(@PathVariable Long farmId) {
        try {
            List<ProductionPlanFarmManagerResponseDto> productionPlans = productionPlanFarmManagerService.getAllProductionPlansByFarmId(farmId);
            return ResponseEntity.ok(new CustomResponse<>(1000, productionPlans));
        } catch (Exception e) {
            logger.error("Error fetching production plans by farm ID {}", farmId, e);
            return ResponseEntity.status(500).body(new CustomResponse<>(1004, null, e.getMessage()));
        }
    }

    @GetMapping("/greenhouses/{greenhouseId}/pending")
    @PreAuthorize("hasAuthority('" + Permissions.PRIVILEGE_VIEW_PENDING_PRODUCTION_PLANS + "')")
    public ResponseEntity<CustomResponse<List<ProductionPlanFarmManagerResponseDto>>> getAllPendingProdPlanFarmManagerByGreenHouseId(@PathVariable Long greenhouseId) {
        try {
            List<ProductionPlanFarmManagerResponseDto> pendingPlans = productionPlanFarmManagerService.getAllPendingProdPlanFarmManagerByGreenHouseId(greenhouseId);
            return ResponseEntity.ok(new CustomResponse<>(1000, pendingPlans));
        } catch (Exception e) {
            logger.error("Error fetching pending production plans by greenhouse ID {}", greenhouseId, e);
            return ResponseEntity.status(500).body(new CustomResponse<>(1004, null, e.getMessage()));
        }
    }

    @DeleteMapping("/{managerId}")
    @PreAuthorize("hasAuthority('" + Permissions.PRIVILEGE_DELETE_PRODUCTION_PLAN_FARM_MANAGER + "')")
    public ResponseEntity<CustomResponse<Boolean>> deleteProductionPlanFarmManager(@PathVariable Long managerId) {
        try {
            boolean isDeleted = productionPlanFarmManagerService.deleteProductionPlanFarmManager(managerId);
            if (isDeleted) {
                return ResponseEntity.ok(new CustomResponse<>(1000, true, "Production Plan Farm Manager deleted successfully."));
            } else {
                return ResponseEntity.status(400).body(new CustomResponse<>(1002, false, "Could not delete Production Plan Farm Manager."));
            }
        } catch (Exception e) {
            logger.error("Error deleting Production Plan Farm Manager with ID {}", managerId, e);
            return ResponseEntity.status(500).body(new CustomResponse<>(1004, false, e.getMessage()));
        }
    }

}
