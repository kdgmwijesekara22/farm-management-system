package com.shirohana.controller.user;

import com.shirohana.dto.CustomResponse;
import com.shirohana.dto.user.PermissionGroupDTO;
import com.shirohana.service.interfaces.user.PermissionGroupService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/privileges")
@RequiredArgsConstructor
@Slf4j
public class PrivilegeController {

    private final PermissionGroupService permissionGroupService;

    @GetMapping
    public ResponseEntity<CustomResponse<List<PermissionGroupDTO>>> getAllPrivileges() {
        try {
            List<PermissionGroupDTO> groups = permissionGroupService.getAllPermissionGroupsWithPrivileges();
            return ResponseEntity.ok(new CustomResponse<>(1000, groups));
        } catch (Exception ex) {
            log.error("Error fetching privileges", ex);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null, ex.getMessage()));
        }
    }
}
