package com.shirohana.controller.user;

import com.shirohana.constants.Permissions;
import com.shirohana.dto.CustomResponse;
import com.shirohana.dto.user.FarmAssignmentRequest;
import com.shirohana.dto.user.RoleDTO;
import com.shirohana.service.interfaces.user.RoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/roles")
@RequiredArgsConstructor
public class RoleController {

    private final RoleService roleServiceImpl;

    @GetMapping
    @PreAuthorize("hasAuthority('" + Permissions.PRIVILEGE_VIEW_ROLES + "') or hasAuthority('" + Permissions.PRIVILEGE_VIEW_ALL_ROLES + "')")
    public ResponseEntity<CustomResponse<List<RoleDTO>>> getAllRoles() {
        try {
            List<RoleDTO> roles = roleServiceImpl.getAllRoles();
            return ResponseEntity.ok(new CustomResponse<>(1000, roles));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null, ex.getMessage()));
        }
    }

    @PostMapping
    @PreAuthorize("hasAuthority('" + Permissions.PRIVILEGE_CREATE_ROLE + "')")
    public ResponseEntity<CustomResponse<RoleDTO>> createRole(@RequestBody RoleDTO roleDTO) {
        try {
            RoleDTO createdRole = roleServiceImpl.createRole(roleDTO);
            return ResponseEntity.status(HttpStatus.CREATED).body(new CustomResponse<>(1001, createdRole));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null, ex.getMessage()));
        }
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('" + Permissions.PRIVILEGE_DELETE_ROLE + "')")
    public ResponseEntity<CustomResponse<Void>> deleteRole(@PathVariable Long id) {
        try {
            if (roleServiceImpl.deleteRole(id)) {
                return ResponseEntity.ok(new CustomResponse<>(1000, null));
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new CustomResponse<>(1003, null, "Role not found"));
            }
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null, ex.getMessage()));
        }
    }

    @PostMapping("/{id}/privileges")
    @PreAuthorize("hasAuthority('" + Permissions.ROLE_ASSIGN_PRIVILEGES + "')")
    public ResponseEntity<CustomResponse<RoleDTO>> assignPrivilegesToRole(@PathVariable Long id, @RequestBody List<Long> privilegeIds) {
        try {
            Optional<RoleDTO> updatedRole = roleServiceImpl.assignPrivilegesToRole(id, privilegeIds);
            return updatedRole.map(roleDTO -> ResponseEntity.ok(new CustomResponse<>(1000, roleDTO)))
                    .orElseGet(() -> ResponseEntity.status(HttpStatus.NOT_FOUND).body(new CustomResponse<>(1003, null, "Role not found")));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null, ex.getMessage()));
        }
    }

    @PostMapping("/{id}/farms")
    @PreAuthorize("hasAuthority('" + Permissions.PRIVILEGE_ASSIGN_FARMS + "')")
    public ResponseEntity<CustomResponse<RoleDTO>> assignFarmsToRole(@PathVariable Long id, @RequestBody FarmAssignmentRequest farmAssignmentRequest) {
        try {
            RoleDTO updatedRole = roleServiceImpl.assignFarmsToRole(id, farmAssignmentRequest.getFarmIds());
            return ResponseEntity.ok(new CustomResponse<>(1000, updatedRole));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null, e.getMessage()));
        }
    }
}
