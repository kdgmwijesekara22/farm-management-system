package com.shirohana.controller.user;

import com.shirohana.dto.CustomResponse;
import com.shirohana.dto.user.CurrentUserDTO;
import com.shirohana.dto.user.FarmDetailDTO;
import com.shirohana.dto.user.UserDTO;
import com.shirohana.dto.user.UserDetailDTO;
import com.shirohana.entity.user.User;
import com.shirohana.service.interfaces.user.UserService;
import jakarta.validation.ValidationException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.shirohana.constants.Permissions.*;

@RestController
@RequestMapping("/api/users")
@RequiredArgsConstructor
@Slf4j
public class UserController {

    private final UserService userServiceImpl;

    @GetMapping
    @PreAuthorize("hasAuthority('" + USER_READ_PERMISSION + "')")
    public ResponseEntity<CustomResponse<List<User>>> getAllUsers() {
        try {
            List<User> users = userServiceImpl.findAll();
            return ResponseEntity.ok(new CustomResponse<>(1000, users));
        } catch (Exception e) {
            log.error("Error fetching all users: {}", e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null,  e.getMessage()));
        }
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('" + USER_READ_PERMISSION + "')")
    public ResponseEntity<CustomResponse<CurrentUserDTO>> getUserById(@PathVariable Long id) {
        try {
            Optional<User> user = userServiceImpl.findById(id);
            return user.map(u -> {
                CurrentUserDTO userDto = userServiceImpl.getCurrentUserDetails(u);
                return ResponseEntity.ok(new CustomResponse<>(1000, userDto));
            }).orElseGet(() -> ResponseEntity.status(HttpStatus.NOT_FOUND).body(new CustomResponse<>(1003, null, "User not found")));
        } catch (Exception e) {
            log.error("Error fetching user by ID {}: {}", id, e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null,  e.getMessage()));
        }
    }

    @PutMapping("/{id}/roles")
    @PreAuthorize("hasAuthority('" + USER_UPDATE_PERMISSION + "')")
    public ResponseEntity<CustomResponse<Void>> assignRolesToUser(@PathVariable Long id, @RequestBody List<Long> roleIds) {
        try {
            userServiceImpl.assignRolesToUser(id, roleIds);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body(new CustomResponse<>(1000, null));
        } catch (IllegalArgumentException e) {
            log.error("Error assigning roles to user with ID {}: {}", id, e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new CustomResponse<>(1002, null, "Error assigning roles: " + e.getMessage()));
        } catch (Exception e) {
            log.error("Unexpected error assigning roles to user with ID {}: {}", id, e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null,  e.getMessage()));
        }
    }

    @PostMapping
    @PreAuthorize("hasAuthority('" + USER_CREATE_PERMISSION + "')")
    public ResponseEntity<CustomResponse<CurrentUserDTO>> createUser(@RequestBody UserDTO userDto) {
        User createdBy = getCurrentUser();
        try {
            User newUser = userServiceImpl.createUser(userDto, createdBy);
            CurrentUserDTO currentUserDTO = userServiceImpl.getCurrentUserDetails(newUser);
            return ResponseEntity.status(HttpStatus.CREATED).body(new CustomResponse<>(1001, currentUserDTO));
        } catch (ValidationException e) {
            log.error("Validation error while creating user: {}", e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new CustomResponse<>(1002, null, e.getMessage()));
        } catch (Exception e) {
            log.error("Unexpected error creating user: {}", e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null, e.getMessage()));
        }
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('" + USER_UPDATE_PERMISSION + "')")
    public ResponseEntity<CustomResponse<CurrentUserDTO>> updateUser(@PathVariable Long id, @RequestBody UserDTO userDto) {
        User updatedBy = getCurrentUser();
        try {
            Optional<User> updatedUser = userServiceImpl.updateUser(id, userDto, updatedBy);
            return updatedUser.map(user -> {
                CurrentUserDTO currentUserDTO = userServiceImpl.getCurrentUserDetails(user);
                return ResponseEntity.ok(new CustomResponse<>(1000, currentUserDTO));
            }).orElseGet(() -> ResponseEntity.status(HttpStatus.NOT_FOUND).body(new CustomResponse<>(1003, null, "User not found")));
        } catch (IllegalArgumentException e) {
            log.error("Error updating user with ID {}: {}", id, e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new CustomResponse<>(1002, null, "Validation error: " + e.getMessage()));
        } catch (Exception e) {
            log.error("Unexpected error updating user with ID {}: {}", id, e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null,  e.getMessage()));
        }
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('" + USER_DELETE_PERMISSION + "')")
    public ResponseEntity<CustomResponse<Void>> deleteUser(@PathVariable Long id) {
        User deletedBy = getCurrentUser();
        try {
            userServiceImpl.deleteUser(id, deletedBy);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body(new CustomResponse<>(1000, null));
        } catch (IllegalArgumentException e) {
            log.error("Error deleting user with ID {}: {}", id, e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new CustomResponse<>(1002, null, "Validation error: " + e.getMessage()));
        } catch (Exception e) {
            log.error("Unexpected error deleting user with ID {}: {}", id, e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null,  e.getMessage()));
        }
    }

    @PostMapping("/public")
    public ResponseEntity<CustomResponse<CurrentUserDTO>> createPublicUser(@RequestBody UserDTO userDto) {
        try {
            User newUser = userServiceImpl.createUser(userDto, null); // Public user creation doesn't have a creator
            CurrentUserDTO currentUserDTO = userServiceImpl.getCurrentUserDetails(newUser);
            return ResponseEntity.status(HttpStatus.CREATED).body(new CustomResponse<>(1001, currentUserDTO));
        } catch (IllegalArgumentException e) {
            log.error("Error creating public user: {}", e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new CustomResponse<>(1002, null, "Validation error: " + e.getMessage()));
        } catch (Exception e) {
            log.error("Unexpected error creating public user: {}", e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new CustomResponse<>(1002, null,  e.getMessage()));
        }
    }

    @PostMapping("/add")
    public ResponseEntity<CustomResponse<CurrentUserDTO>> addUserWithoutAuth(@RequestBody UserDTO userDto) {
        try {
            User newUser = userServiceImpl.createUser(userDto, null);
            CurrentUserDTO currentUserDTO = userServiceImpl.getCurrentUserDetails(newUser);
            return ResponseEntity.status(HttpStatus.CREATED).body(new CustomResponse<>(1001, currentUserDTO));
        } catch (IllegalArgumentException e) {
            log.error("Error adding user without auth: {}", e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new CustomResponse<>(1002, null, "Validation error: " + e.getMessage()));
        } catch (Exception e) {
            log.error("Unexpected error adding user without auth: {}", e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new CustomResponse<>(1002, null,  e.getMessage()));
        }
    }

    @GetMapping("/names")
    public ResponseEntity<CustomResponse<List<String>>> getAllUserNames() {
        try {
            List<String> userNames = userServiceImpl.findAllUserNames();
            return ResponseEntity.ok(new CustomResponse<>(1000, userNames));
        } catch (Exception e) {
            log.error("Error fetching all user names: {}", e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null,  e.getMessage()));
        }
    }

    @GetMapping("/current")
    public ResponseEntity<CustomResponse<CurrentUserDTO>> getCurrentUserDetails() {
        try {
            User currentUser = getCurrentUser();
            if (currentUser != null) {
                CurrentUserDTO userDto = userServiceImpl.getCurrentUserDetails(currentUser);
                return ResponseEntity.ok(new CustomResponse<>(1000, userDto));
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new CustomResponse<>(1003, null, "User not found"));
            }
        } catch (Exception e) {
            log.error("Error fetching current user details: {}", e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null,  e.getMessage()));
        }
    }

    @GetMapping("/details")
    public ResponseEntity<CustomResponse<List<UserDetailDTO>>> getAllUserDetails() {
        try {
            List<UserDetailDTO> userDetails = userServiceImpl.getAllUserDetails();
            return ResponseEntity.ok(new CustomResponse<>(1000, userDetails));
        } catch (Exception e) {
            log.error("Error fetching all user details: {}", e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null,  e.getMessage()));
        }
    }

    @GetMapping("/current/farms")
    public ResponseEntity<CustomResponse<List<FarmDetailDTO>>> getCurrentUserFarms() {
        try {
            List<FarmDetailDTO> farms = userServiceImpl.getCurrentUserFarms();
            return ResponseEntity.ok(new CustomResponse<>(1000, farms));
        } catch (Exception e) {
            log.error("Error fetching current user's farms: {}", e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null,  e.getMessage()));
        }
    }

    private User getCurrentUser() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof org.springframework.security.core.userdetails.User) {
            String username = ((org.springframework.security.core.userdetails.User) principal).getUsername();
            User user = userServiceImpl.findByUsername(username).orElse(null);
            return user;
        }
        return null;
    }
}
