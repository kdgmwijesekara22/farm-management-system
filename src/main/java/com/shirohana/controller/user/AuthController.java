package com.shirohana.controller.user;
import com.shirohana.dto.user.AuthRequest;
import com.shirohana.dto.user.AuthResponse;
import com.shirohana.service.interfaces.user.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/auth")
@RequiredArgsConstructor
public class AuthController {

    private final AuthService authServiceImpl;

    @PostMapping("/login")
    public ResponseEntity<AuthResponse> login(@RequestBody AuthRequest authRequest){
        String token = authServiceImpl.authenticate(authRequest.getUsername() , authRequest.getPassword());
        return ResponseEntity.ok(new AuthResponse(token));
    }
}
