package com.shirohana.controller.prodplantaskmanagement.prodplantmaintenancetask;


import com.shirohana.constants.Permissions;
import com.shirohana.dto.CustomResponse;
import com.shirohana.dto.prodplantaskmanagement.prodplanmaintenancetask.ProdPlanMaintenanceTaskRequestDto;
import com.shirohana.dto.prodplantaskmanagement.prodplanmaintenancetask.ProdPlanMaintenanceTaskResponseDto;
import com.shirohana.dto.prodplantaskmanagement.prodplanmaintenancetask.filtered.ProdPlanMaintenanceTaskFilterByPurposeCombinedResponseDto;
import com.shirohana.dto.prodplantaskmanagement.prodplanmaintenancetask.filtered.ProdPlanMaintenanceTaskFilterByPurposeResponseDto;
import com.shirohana.exception.ResourceNotFoundException;
import com.shirohana.service.interfaces.prodplantaskmanagement.prodplantmaintenancetask.ProdPlanMaintenanceTaskService;
import jakarta.validation.ValidationException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/prod-plan-maintenance-tasks")
@RequiredArgsConstructor
@Slf4j
public class ProdPlanMaintenanceTaskController {
    private final ProdPlanMaintenanceTaskService prodPlanMaintenanceTaskService;

    @PostMapping
    @PreAuthorize("hasAuthority('" + Permissions.PROD_PLAN_MAINTENANCE_TASK_CREATE_PERMISSION + "')")
    public ResponseEntity<CustomResponse<ProdPlanMaintenanceTaskResponseDto>> createTask(@Validated @RequestBody ProdPlanMaintenanceTaskRequestDto requestDto) {
        try {
            log.debug("Controller class: {}", requestDto);
            ProdPlanMaintenanceTaskResponseDto responseDto = prodPlanMaintenanceTaskService.createProdPlanMaintenanceTask(requestDto);
            return ResponseEntity.status(HttpStatus.CREATED).body(new CustomResponse<>(1001, responseDto));
        } catch (ValidationException ex) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new CustomResponse<>(1002, null, ex.getMessage()));
        } catch (ResourceNotFoundException ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new CustomResponse<>(1003, null, ex.getMessage()));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null, "An unexpected error occurred."));
        }
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('" + Permissions.PROD_PLAN_MAINTENANCE_TASK_UPDATE_PERMISSION + "')")
    public ResponseEntity<CustomResponse<ProdPlanMaintenanceTaskResponseDto>> updateTask(@PathVariable Long id, @Validated @RequestBody ProdPlanMaintenanceTaskRequestDto requestDto) {
        try {
            ProdPlanMaintenanceTaskResponseDto responseDto = prodPlanMaintenanceTaskService.updateProdPlanMaintenanceTask(id, requestDto);
            return ResponseEntity.ok(new CustomResponse<>(1000, responseDto));
        } catch (ValidationException ex) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new CustomResponse<>(1002, null, ex.getMessage()));
        } catch (ResourceNotFoundException ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new CustomResponse<>(1003, null, ex.getMessage()));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null, "An unexpected error occurred."));
        }
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('" + Permissions.PROD_PLAN_MAINTENANCE_TASK_DELETE_PERMISSION + "')")
    public ResponseEntity<CustomResponse<Void>> deleteTask(@PathVariable Long id) {
        try {
            prodPlanMaintenanceTaskService.softDeleteProdPlanMaintenanceTask(id);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body(new CustomResponse<>(1000, null));
        } catch (ResourceNotFoundException ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new CustomResponse<>(1003, null, ex.getMessage()));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null, "An unexpected error occurred."));
        }
    }

    @GetMapping
    @PreAuthorize("hasAuthority('" + Permissions.PROD_PLAN_MAINTENANCE_TASK_READ_PERMISSION + "')")
    public ResponseEntity<CustomResponse<List<ProdPlanMaintenanceTaskResponseDto>>> getAllTasks() {
        try {
            List<ProdPlanMaintenanceTaskResponseDto> responseDtos = prodPlanMaintenanceTaskService.getAllProdPlanMaintenanceTasks();
            return ResponseEntity.ok(new CustomResponse<>(1000, responseDtos));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null, "An unexpected error occurred."));
        }
    }

    @GetMapping("/by-prod-plan-id")
    @PreAuthorize("hasAuthority('" + Permissions.PROD_PLAN_MAINTENANCE_TASK_READ_PERMISSION + "')")
    public ResponseEntity<CustomResponse<List<ProdPlanMaintenanceTaskResponseDto>>> getTasksByProdPlanId(@RequestParam Long prodPlanId, @RequestParam Long familyNameId) {
        try {
            List<ProdPlanMaintenanceTaskResponseDto> responseDtos = prodPlanMaintenanceTaskService.getAllProdPlanMaintenanceTasksByProdPlanId(prodPlanId, familyNameId);
            return ResponseEntity.ok(new CustomResponse<>(1000, responseDtos));
        } catch (ResourceNotFoundException ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new CustomResponse<>(1003, null, ex.getMessage()));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null, "An unexpected error occurred."));
        }
    }

    @GetMapping("/by-prod-plan-id-purpose")
    @PreAuthorize("hasAuthority('" + Permissions.PROD_PLAN_MAINTENANCE_TASK_READ_PERMISSION + "')")
    public ResponseEntity<CustomResponse<List<ProdPlanMaintenanceTaskFilterByPurposeCombinedResponseDto>>> getTasksByProdPlanIdAndPurpose(
            @RequestParam Long prodPlanId,
            @RequestParam Long familyNameId,
            @RequestParam String purpose,
            @RequestParam Long farmId,
            @RequestParam Long plantationScheduleId,
            @RequestParam Long plantationPlanId,
            @RequestParam Long userId
    ) {
        try {
            List<ProdPlanMaintenanceTaskFilterByPurposeCombinedResponseDto> responseDtos = prodPlanMaintenanceTaskService.getAllProdPlanMaintenanceTasksByProdPlanIdAndPurpose(
                    prodPlanId, familyNameId, purpose, farmId, plantationScheduleId, plantationPlanId, userId);
            return ResponseEntity.ok(new CustomResponse<>(1000, responseDtos));
        } catch (ResourceNotFoundException ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new CustomResponse<>(1003, null, ex.getMessage()));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null, "An unexpected error occurred."));
        }
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('" + Permissions.PROD_PLAN_MAINTENANCE_TASK_READ_PERMISSION + "')")
    public ResponseEntity<CustomResponse<ProdPlanMaintenanceTaskResponseDto>> getTaskById(@PathVariable Long id) {
        try {
            ProdPlanMaintenanceTaskResponseDto responseDto = prodPlanMaintenanceTaskService.getProdPlanMaintenanceTaskById(id);
            return ResponseEntity.ok(new CustomResponse<>(1000, responseDto));
        } catch (ResourceNotFoundException ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new CustomResponse<>(1003, null, ex.getMessage()));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null, "An unexpected error occurred."));
        }
    }
}
