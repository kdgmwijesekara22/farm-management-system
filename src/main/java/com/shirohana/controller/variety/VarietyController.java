package com.shirohana.controller.variety;

import com.shirohana.constants.Permissions;
import com.shirohana.dto.CustomResponse;
import com.shirohana.dto.variety.VarietyHarvestingResponseDto;
import com.shirohana.dto.variety.VarietyRequestDto;
import com.shirohana.dto.variety.VarietyResponseDto;
import com.shirohana.exception.ResourceNotFoundException;
import com.shirohana.service.interfaces.variety.VarietyService;
import jakarta.validation.ValidationException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/varieties")
@RequiredArgsConstructor
public class VarietyController {
    private final VarietyService varietyService;

    @PostMapping
    @PreAuthorize("hasAuthority('" + Permissions.VARIETY_CREATE_PERMISSION + "')") // Check for create permission
    public ResponseEntity<CustomResponse<VarietyResponseDto>> createVariety(@Validated @RequestBody VarietyRequestDto varietyRequestDto) {
        try {
            VarietyResponseDto varietyResponseDto = varietyService.createVariety(varietyRequestDto);
            return ResponseEntity.status(HttpStatus.CREATED).body(new CustomResponse<>(1001, varietyResponseDto));
        } catch (ValidationException ex) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new CustomResponse<>(1002, null, ex.getMessage()));
        } catch (ResourceNotFoundException ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new CustomResponse<>(1003, null, ex.getMessage()));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null, ex.getMessage()));
        }
    }

    @GetMapping("/by-family-id/{familyId}/with-harvesting")
    @PreAuthorize("hasAuthority('" + Permissions.VARIETY_READ_PERMISSION + "')") // Check for read permission
    public ResponseEntity<CustomResponse<List<VarietyHarvestingResponseDto>>> getVarietiesByFamilyIdWithHarvesting(@PathVariable Long familyId) {
        try {
            List<VarietyHarvestingResponseDto> varietyResponseDtos = varietyService.getVarietiesByFamilyIdWithHarvesting(familyId);
            return ResponseEntity.ok(new CustomResponse<>(1000, varietyResponseDtos));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null, ex.getMessage()));
        }
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('" + Permissions.VARIETY_UPDATE_PERMISSION + "')") // Check for update permission
    public ResponseEntity<CustomResponse<VarietyResponseDto>> updateVariety(@PathVariable Long id, @Validated @RequestBody VarietyRequestDto varietyRequestDto) {
        try {
            VarietyResponseDto varietyResponseDto = varietyService.updateVariety(id, varietyRequestDto);
            return ResponseEntity.ok(new CustomResponse<>(1000, varietyResponseDto));
        } catch (ValidationException ex) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new CustomResponse<>(1002, null, ex.getMessage()));
        } catch (ResourceNotFoundException ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new CustomResponse<>(1003, null, ex.getMessage()));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null, ex.getMessage()));
        }
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('" + Permissions.VARIETY_DELETE_PERMISSION + "')") // Check for delete permission
    public ResponseEntity<CustomResponse<Void>> deleteVariety(@PathVariable Long id) {
        try {
            varietyService.deleteVariety(id);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body(new CustomResponse<>(1000, null));
        } catch (ResourceNotFoundException ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new CustomResponse<>(1003, null, ex.getMessage()));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null, ex.getMessage()));
        }
    }

    @GetMapping
    @PreAuthorize("hasAuthority('" + Permissions.VARIETY_READ_PERMISSION + "')") // Check for read permission
    public ResponseEntity<CustomResponse<List<VarietyResponseDto>>> getAllVarieties() {
        try {
            List<VarietyResponseDto> varietyResponseDtos = varietyService.getAllVarieties();
            return ResponseEntity.ok(new CustomResponse<>(1000, varietyResponseDtos));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null, ex.getMessage()));
        }
    }

    @GetMapping("/by-family-name-id")
    @PreAuthorize("hasAuthority('" + Permissions.VARIETY_READ_PERMISSION + "')") // Check for read permission
    public ResponseEntity<CustomResponse<List<VarietyResponseDto>>> getVarietyByFamilyNameId(
            @RequestParam Long familyNameId) {
        try {
            List<VarietyResponseDto> varietyResponseDtos = varietyService.getVarietyByFamilyNameId(familyNameId);
            return ResponseEntity.ok(new CustomResponse<>(1000, varietyResponseDtos));
        } catch (ResourceNotFoundException ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new CustomResponse<>(1003, null, ex.getMessage()));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null, ex.getMessage()));
        }
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('" + Permissions.VARIETY_READ_PERMISSION + "')") // Check for read permission
    public ResponseEntity<CustomResponse<VarietyResponseDto>> getVarietyById(@PathVariable Long id) {
        try {
            VarietyResponseDto varietyResponseDto = varietyService.getVarietyById(id);
            return ResponseEntity.ok(new CustomResponse<>(1000, varietyResponseDto));
        } catch (ResourceNotFoundException ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new CustomResponse<>(1003, null, ex.getMessage()));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null, ex.getMessage()));
        }
    }

    @GetMapping("/by-plant-id/{plantId}")
    @PreAuthorize("hasAuthority('" + Permissions.FAMILY_RELATED_VARIETY_READ_PERMISSION + "')") // Check for read permission
    public ResponseEntity<CustomResponse<List<VarietyResponseDto>>> getVarietiesByPlantId(@PathVariable Long plantId) {
        try {
            List<VarietyResponseDto> varietyResponseDtos = varietyService.getVarietiesByPlantId(plantId);
            return ResponseEntity.ok(new CustomResponse<>(1000, varietyResponseDtos));
        } catch (ResourceNotFoundException ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new CustomResponse<>(1003, null, ex.getMessage()));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null, ex.getMessage()));
        }
    }
}
