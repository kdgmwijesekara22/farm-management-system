package com.shirohana.controller.farm;

import com.shirohana.constants.Permissions;
import com.shirohana.dto.CustomResponse;
import com.shirohana.dto.farm.*;
import com.shirohana.service.interfaces.farm.GreenhouseService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/greenhouses")
@RequiredArgsConstructor
public class GreenhouseController {
    private final GreenhouseService greenhouseServiceImpl;

    @GetMapping("/farm/{farmId}")
    @PreAuthorize("hasAuthority('" + Permissions.GREENHOUSE_READ_PERMISSION + "')") // Check for read permission
    public ResponseEntity<CustomResponse<List<GreenhouseResponseDto>>> getAllGreenhousesByFarmId(@PathVariable Long farmId) {
        try {
            List<GreenhouseResponseDto> greenhouses = greenhouseServiceImpl.getAllGreenhousesByFarmId(farmId);
            return ResponseEntity.ok(new CustomResponse<>(1000, greenhouses));
        } catch (Exception e) {
            return ResponseEntity.status(500).body(new CustomResponse<>(1004, null, "An unexpected error occurred."));
        }
    }

    @PostMapping("/farm/availability")
    @PreAuthorize("hasAuthority('" + Permissions.GREENHOUSE_AVAILABILITY_READ_PERMISSION + "')") // Check for read permission
    public ResponseEntity<CustomResponse<GHBayAvailableResponseDto>> getAvailableGreenhousesByFarmId(@RequestBody GHBayBedCountRequestDto request) {
        try {
            GHBayAvailableResponseDto availableGreenhouses = greenhouseServiceImpl.getAvailableGreenhousesByFarmId(request);
            return ResponseEntity.ok(new CustomResponse<>(1000, availableGreenhouses));
        } catch (Exception e) {
            return ResponseEntity.status(500).body(new CustomResponse<>(1004, null, "An unexpected error occurred."));
        }
    }


    @GetMapping("/farm/{farmId}/bays")
    @PreAuthorize("hasAuthority('" + Permissions.GREENHOUSE_READ_PERMISSION + "')") // Check for read permission
    public ResponseEntity<CustomResponse<GreenhouseWithBaysResponseDto>> getAllGreenhousesAndBaysByFarmId(@PathVariable Long farmId) {
        try {
            GreenhouseWithBaysResponseDto greenhousesWithBays = greenhouseServiceImpl.getAllGreenhousesAndBaysByFarmId(farmId);
            return ResponseEntity.ok(new CustomResponse<>(1000, greenhousesWithBays));
        } catch (Exception e) {
            return ResponseEntity.status(500).body(new CustomResponse<>(1004, null, "An unexpected error occurred."));
        }
    }

    @PostMapping
    @PreAuthorize("hasAuthority('" + Permissions.GREENHOUSE_CREATE_PERMISSION + "')") // Check for create permission
    public ResponseEntity<CustomResponse<GreenhouseResponseDto>> createGreenhouse(@RequestBody GreenhouseRequestDto greenhouseRequestDto) {
        try {
            GreenhouseResponseDto createdGreenhouse = greenhouseServiceImpl.createGreenhouse(greenhouseRequestDto);
            return ResponseEntity.status(201).body(new CustomResponse<>(1001, createdGreenhouse));
        } catch (Exception e) {
            return ResponseEntity.status(500).body(new CustomResponse<>(1004, null, e.getMessage()));
        }
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('" + Permissions.GREENHOUSE_UPDATE_PERMISSION + "')") // Check for update permission
    public ResponseEntity<CustomResponse<GreenhouseResponseDto>> updateGreenhouse(@PathVariable Long id, @RequestBody GreenhouseRequestDto greenhouseRequestDto) {
        try {
            GreenhouseResponseDto updatedGreenhouse = greenhouseServiceImpl.updateGreenhouse(id, greenhouseRequestDto);
            return ResponseEntity.ok(new CustomResponse<>(1000, updatedGreenhouse));
        } catch (Exception e) {
            return ResponseEntity.status(500).body(new CustomResponse<>(1004, null, e.getMessage()));
        }
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('" + Permissions.GREENHOUSE_DELETE_PERMISSION + "')") // Check for delete permission
    public ResponseEntity<CustomResponse<Void>> deleteGreenhouse(@PathVariable Long id) {
        try {
            greenhouseServiceImpl.deleteGreenhouse(id);
            return ResponseEntity.status(204).body(new CustomResponse<>(1000, null));
        } catch (Exception e) {
            return ResponseEntity.status(500).body(new CustomResponse<>(1004, null, e.getMessage()));
        }
    }
}
