package com.shirohana.controller.farm;

import com.shirohana.constants.Permissions;
import com.shirohana.dto.CustomResponse;
import com.shirohana.dto.farm.*;
import com.shirohana.service.interfaces.farm.BayService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/bays")
@RequiredArgsConstructor
public class BayController {
    private final BayService bayServiceImpl;

    @GetMapping("/greenhouse/{greenhouseId}")
    @PreAuthorize("hasAuthority('" + Permissions.BAY_READ_PERMISSION + "')") // Check for read permission
    public ResponseEntity<CustomResponse<List<BayResponseDto>>> getAllBaysByGreenhouseId(@PathVariable Long greenhouseId) {
        try {
            List<BayResponseDto> bays = bayServiceImpl.getAllBaysByGreenhouseId(greenhouseId);
            return ResponseEntity.ok(new CustomResponse<>(1000, bays));
        } catch (Exception e) {
            return ResponseEntity.status(500).body(new CustomResponse<>(1004, null, "An unexpected error occurred."));
        }
    }

    @PostMapping
    @PreAuthorize("hasAuthority('" + Permissions.BAY_CREATE_PERMISSION + "')") // Check for create permission
    public ResponseEntity<CustomResponse<BayResponseDto>> createBay(@RequestBody BayRequestDto bayRequestDto) {
        try {
            BayResponseDto createdBay = bayServiceImpl.createBay(bayRequestDto);
            return ResponseEntity.status(201).body(new CustomResponse<>(1001, createdBay));
        } catch (Exception e) {
            return ResponseEntity.status(500).body(new CustomResponse<>(1004, null, e.getMessage()));
        }
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('" + Permissions.BAY_UPDATE_PERMISSION + "')") // Check for update permission
    public ResponseEntity<CustomResponse<BayResponseDto>> updateBay(@PathVariable Long id, @RequestBody BayRequestDto bayRequestDto) {
        try {
            BayResponseDto updatedBay = bayServiceImpl.updateBay(id, bayRequestDto);
            return ResponseEntity.ok(new CustomResponse<>(1000, updatedBay));
        } catch (Exception e) {
            return ResponseEntity.status(500).body(new CustomResponse<>(1004, null, e.getMessage()));
        }
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('" + Permissions.BAY_DELETE_PERMISSION + "')") // Check for delete permission
    public ResponseEntity<CustomResponse<Void>> deleteBay(@PathVariable Long id) {
        try {
            bayServiceImpl.deleteBay(id);
            return ResponseEntity.status(204).body(new CustomResponse<>(1000, null));
        } catch (Exception e) {
            return ResponseEntity.status(500).body(new CustomResponse<>(1004, null, e.getMessage()));
        }
    }


    @PostMapping("/availability")
    @PreAuthorize("hasAuthority('" + Permissions.BAY_AVAILABILITY_READ_PERMISSION + "')") // Check for read permission
    public ResponseEntity<CustomResponse<BayBedAvailableResponseDto>> getBayBedAvailability(@RequestBody BayBedCountRequestDto bayBedCountRequestDto) {
        try {
            BayBedAvailableResponseDto responseDto = bayServiceImpl.getBayBedAvailability(bayBedCountRequestDto);
            return ResponseEntity.ok(new CustomResponse<>(1000, responseDto));
        } catch (Exception e) {
            return ResponseEntity.status(500).body(new CustomResponse<>(1004, null, "An unexpected error occurred."));
        }
    }

}
