package com.shirohana.controller.farm;

import com.shirohana.constants.Permissions;
import com.shirohana.dto.CustomResponse;
import com.shirohana.dto.farm.FarmBayCountRequestDto;
import com.shirohana.dto.farm.FarmGreenHouseAvailableResponseDto;
import com.shirohana.dto.farm.FarmRequestDto;
import com.shirohana.dto.farm.FarmResponseDto;
import com.shirohana.service.interfaces.farm.FarmService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/farms")
@RequiredArgsConstructor
public class FarmController {

    private final FarmService farmServiceImpl;

    @GetMapping
    @PreAuthorize("hasAuthority('" + Permissions.FARM_READ_PERMISSION + "')") // Check for read permission
    public ResponseEntity<CustomResponse<List<FarmResponseDto>>> getAllFarms() {
        try {
            List<FarmResponseDto> farms = farmServiceImpl.getAllFarms();
            return ResponseEntity.ok(new CustomResponse<>(1000, farms));
        } catch (Exception e) {
            return ResponseEntity.status(500).body(new CustomResponse<>(1004, null, e.getMessage()));
        }
    }

    @PostMapping
    @PreAuthorize("hasAuthority('" + Permissions.FARM_CREATE_PERMISSION + "')") // Check for create permission
    public ResponseEntity<CustomResponse<FarmResponseDto>> createFarm(@RequestBody FarmRequestDto farmRequestDto) {
        try {
            FarmResponseDto createdFarm = farmServiceImpl.createFarm(farmRequestDto);
            return ResponseEntity.status(201).body(new CustomResponse<>(1001, createdFarm));
        } catch (Exception e) {
            return ResponseEntity.status(500).body(new CustomResponse<>(1004, null, e.getMessage()));
        }
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('" + Permissions.FARM_UPDATE_PERMISSION + "')") // Check for update permission
    public ResponseEntity<CustomResponse<FarmResponseDto>> updateFarm(@PathVariable Long id, @RequestBody FarmRequestDto farmRequestDto) {
        try {
            FarmResponseDto updatedFarm = farmServiceImpl.updateFarm(id, farmRequestDto);
            return ResponseEntity.ok(new CustomResponse<>(1000, updatedFarm));
        } catch (Exception e) {
            return ResponseEntity.status(500).body(new CustomResponse<>(1004, null, e.getMessage()));
        }
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('" + Permissions.FARM_DELETE_PERMISSION + "')") // Check for delete permission
    public ResponseEntity<CustomResponse<Void>> deleteFarm(@PathVariable Long id) {
        try {
            farmServiceImpl.deleteFarm(id);
            return ResponseEntity.status(204).body(new CustomResponse<>(1000, null));
        } catch (Exception e) {
            return ResponseEntity.status(500).body(new CustomResponse<>(1004, null, e.getMessage()));
        }
    }

    // New method to get available farms based on greenhouse and bay availability
    @PostMapping("/availability")
    @PreAuthorize("hasAuthority('" + Permissions.FARM_AVAILABILITY_READ_PERMISSION + "')") // Check for read permission
    public ResponseEntity<CustomResponse<FarmGreenHouseAvailableResponseDto>> getAvailableFarms(@RequestBody FarmBayCountRequestDto request) {
        try {
            FarmGreenHouseAvailableResponseDto availableFarms = farmServiceImpl.getAvailableFarms(request);
            return ResponseEntity.ok(new CustomResponse<>(1000, availableFarms));
        } catch (Exception e) {
            return ResponseEntity.status(500).body(new CustomResponse<>(1004, null, e.getMessage()));
        }
    }
}
