package com.shirohana.controller.farm;

import com.shirohana.constants.Permissions;
import com.shirohana.dto.CustomResponse;
import com.shirohana.dto.farm.*;
import com.shirohana.service.interfaces.farm.BedService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/beds")
@RequiredArgsConstructor
public class BedController {

    private final BedService bedService;

    @GetMapping("/bay/{bayId}")
    @PreAuthorize("hasAuthority('" + Permissions.BED_READ_PERMISSION + "')") // Check for read permission
    public ResponseEntity<CustomResponse<List<BedResponseDto>>> getAllBedsByBayId(@PathVariable Long bayId) {
        try {
            List<BedResponseDto> beds = bedService.getAllBedsByBayId(bayId);
            return ResponseEntity.ok(new CustomResponse<>(1000, beds));
        } catch (Exception e) {
            return ResponseEntity.status(500).body(new CustomResponse<>(1004, null, "An unexpected error occurred."));
        }
    }

    @PostMapping
    @PreAuthorize("hasAuthority('" + Permissions.BED_CREATE_PERMISSION + "')") // Check for create permission
    public ResponseEntity<CustomResponse<BedResponseDto>> createBed(@RequestBody BedRequestDto bedRequestDto) {
        try {
            BedResponseDto createdBed = bedService.createBed(bedRequestDto);
            return ResponseEntity.status(201).body(new CustomResponse<>(1001, createdBed));
        } catch (Exception e) {
            return ResponseEntity.status(500).body(new CustomResponse<>(1004, null, e.getMessage()));
        }
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('" + Permissions.BED_UPDATE_PERMISSION + "')") // Check for update permission
    public ResponseEntity<CustomResponse<BedResponseDto>> updateBed(@PathVariable Long id, @RequestBody BedRequestDto bedRequestDto) {
        try {
            BedResponseDto updatedBed = bedService.updateBed(id, bedRequestDto);
            return ResponseEntity.ok(new CustomResponse<>(1000, updatedBed));
        } catch (Exception e) {
            return ResponseEntity.status(500).body(new CustomResponse<>(1004, null, e.getMessage()));
        }
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('" + Permissions.BED_DELETE_PERMISSION + "')") // Check for delete permission
    public ResponseEntity<CustomResponse<Void>> deleteBed(@PathVariable Long id) {
        try {
            bedService.deleteBed(id);
            return ResponseEntity.status(204).body(new CustomResponse<>(1000, null));
        } catch (Exception e) {
            return ResponseEntity.status(500).body(new CustomResponse<>(1004, null, e.getMessage()));
        }
    }
    @PostMapping("/search")
    @PreAuthorize("hasAuthority('" + Permissions.BED_AVAILABILITY_READ_PERMISSION + "')") // Check for read permission
    public ResponseEntity<CustomResponse<BedAvailableResponseDto>> getBedsByBayWeekAndYear(@RequestBody BayWeekRequestDto bayWeekRequestDto) {
        try {
            BedAvailableResponseDto beds = bedService.getBedsByBayWeekAndYear(bayWeekRequestDto);
            return ResponseEntity.ok(new CustomResponse<>(1000, beds));
        } catch (Exception e) {
            return ResponseEntity.status(500).body(new CustomResponse<>(1004, null, e.getMessage()));
        }
    }

}
