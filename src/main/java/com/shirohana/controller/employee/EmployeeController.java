package com.shirohana.controller.employee;


import com.shirohana.constants.Permissions;
import com.shirohana.dto.CustomResponse;
import com.shirohana.dto.employee.EmployeeRequestDto;
import com.shirohana.dto.employee.EmployeeResponseDto;
import com.shirohana.exception.ResourceNotFoundException;
import com.shirohana.service.interfaces.employee.EmployeeService;
import jakarta.validation.ValidationException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/employees")
@RequiredArgsConstructor
public class EmployeeController {
    private final EmployeeService employeeService;

    @PostMapping
    @PreAuthorize("hasAuthority('" + Permissions.EMPLOYEE_CREATE_PERMISSION + "')") // Check for create permission
    public ResponseEntity<CustomResponse<EmployeeResponseDto>> createEmployee(@Validated @RequestBody EmployeeRequestDto employeeRequestDto) {
        try {
            EmployeeResponseDto employeeResponseDto = employeeService.createEmployee(employeeRequestDto);
            return ResponseEntity.status(HttpStatus.CREATED).body(new CustomResponse<>(1001, employeeResponseDto));
        } catch (ValidationException ex) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new CustomResponse<>(1002, null, ex.getMessage()));
        } catch (ResourceNotFoundException ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new CustomResponse<>(1003, null, ex.getMessage()));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null, ex.getMessage()));
        }
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('" + Permissions.EMPLOYEE_UPDATE_PERMISSION + "')") // Check for update permission
    public ResponseEntity<CustomResponse<EmployeeResponseDto>> updateEmployee(@PathVariable Long id, @Validated @RequestBody EmployeeRequestDto employeeRequestDto) {
        try {
            EmployeeResponseDto employeeResponseDto = employeeService.updateEmployee(id, employeeRequestDto);
            return ResponseEntity.ok(new CustomResponse<>(1000, employeeResponseDto));
        } catch (ValidationException ex) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new CustomResponse<>(1002, null, ex.getMessage()));
        } catch (ResourceNotFoundException ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new CustomResponse<>(1003, null, ex.getMessage()));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null, ex.getMessage()));
        }
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('" + Permissions.EMPLOYEE_DELETE_PERMISSION + "')") // Check for delete permission
    public ResponseEntity<CustomResponse<Void>> deleteEmployee(@PathVariable Long id) {
        try {
            employeeService.deleteEmployee(id);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body(new CustomResponse<>(1000, null));
        } catch (ResourceNotFoundException ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new CustomResponse<>(1003, null, ex.getMessage()));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null, ex.getMessage()));
        }
    }

    @GetMapping
    @PreAuthorize("hasAuthority('" + Permissions.EMPLOYEE_READ_PERMISSION + "')") // Check for read permission
    public ResponseEntity<CustomResponse<List<EmployeeResponseDto>>> getAllEmployees() {
        try {
            List<EmployeeResponseDto> employeeResponseDtos = employeeService.getAllEmployees();
            return ResponseEntity.ok(new CustomResponse<>(1000, employeeResponseDtos));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null, ex.getMessage()));
        }
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('" + Permissions.EMPLOYEE_READ_PERMISSION + "')") // Check for read permission
    public ResponseEntity<CustomResponse<EmployeeResponseDto>> getEmployeeById(@PathVariable Long id) {
        try {
            EmployeeResponseDto employeeResponseDto = employeeService.getEmployeeById(id);
            return ResponseEntity.ok(new CustomResponse<>(1000, employeeResponseDto));
        } catch (ResourceNotFoundException ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new CustomResponse<>(1003, null, ex.getMessage()));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null, ex.getMessage()));
        }
    }

    @GetMapping("/given-id/{employeeGivenId}")
    @PreAuthorize("hasAuthority('" + Permissions.EMPLOYEE_READ_PERMISSION + "')") // Check for read permission
    public ResponseEntity<CustomResponse<EmployeeResponseDto>> getEmployeeByEmployeeGivenId(@PathVariable String employeeGivenId) {
        try {
            EmployeeResponseDto employeeResponseDto = employeeService.getEmployeeByEmployeeGivenId(employeeGivenId);
            return ResponseEntity.ok(new CustomResponse<>(1000, employeeResponseDto));
        } catch (ResourceNotFoundException ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new CustomResponse<>(1003, null, ex.getMessage()));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomResponse<>(1004, null, ex.getMessage()));
        }
    }
}
