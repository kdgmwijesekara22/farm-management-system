package com.shirohana.service.implementation.farm;

import com.shirohana.dto.farm.BayWeekRequestDto;
import com.shirohana.dto.farm.BedAvailableResponseDto;
import com.shirohana.entity.schedule.production_plan_green_house.ProductionPlanGHMBayBed;
import com.shirohana.repository.farm.BayRepository;
import com.shirohana.repository.farm.BedRepository;
import com.shirohana.dto.farm.BedRequestDto;
import com.shirohana.dto.farm.BedResponseDto;
import com.shirohana.entity.farm.Bed;
import com.shirohana.exception.ResourceAlreadyExistsException;
import com.shirohana.exception.ResourceNotFoundException;
import com.shirohana.repository.production_plan_green_house.ProductionPlanGreenHouseManagerBayBedRepository;
import com.shirohana.service.interfaces.farm.BedService;
import jakarta.transaction.Transactional;
import jakarta.validation.ValidationException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class BedServiceImpl implements BedService {
    private final BedRepository bedRepository;
    private final BayRepository bayRepository;
    private final ProductionPlanGreenHouseManagerBayBedRepository productionPlanGreenHouseManagerBayBedRepository;
    public List<BedResponseDto> getAllBedsByBayId(Long bayId) {
        try {
            return bedRepository.findByBayIdAndDeletedFalse(bayId).stream()
                    .map(this::convertToResponseDto)
                    .collect(Collectors.toList());
        } catch (Exception ex) {
            log.error("Error getting all beds by bay ID: {}", bayId, ex);
            throw ex;
        }
    }

    public BedResponseDto createBed(BedRequestDto bedRequestDto) {
        try {
            String sanitizedName = sanitizeInput(bedRequestDto.getName());
            validateUniqueBedNameInBay(sanitizedName, bedRequestDto.getBayId());

            Bed bed = convertToEntity(bedRequestDto, sanitizedName);
            bed.setCreatedAt(LocalDateTime.now());
            bed.setCreatedBy(getCurrentUsername());
            bed.setHarvestWeek(0);
            bed.setTrayPlantWeek(0);
            bed.setTransplantEndYear(0);
            bed.setTransplantStartYear(0);
            Bed savedBed = bedRepository.save(bed);
            return convertToResponseDto(savedBed);
        } catch (Exception ex) {
            log.error("Error creating bed: {}", bedRequestDto, ex);
            throw ex;
        }
    }

    public BedResponseDto updateBed(Long id, BedRequestDto bedRequestDto) {
        Bed existingBed = bedRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Bed not found with id " + id));

        String sanitizedName = sanitizeInput(bedRequestDto.getName());
        validateUniqueBedNameInBay(sanitizedName, bedRequestDto.getBayId());

        existingBed.setName(sanitizedName);
        existingBed.setBayId(bedRequestDto.getBayId());
        existingBed.setUpdatedAt(LocalDateTime.now());
        existingBed.setUpdatedBy(getCurrentUsername());
        Bed updatedBed = bedRepository.save(existingBed);
        return convertToResponseDto(updatedBed);
    }

    @Transactional
    public void deleteBed(Long bedId) {
        try {
            String userRole = getCurrentUsername();

            // Use the custom query to fetch the bed if it exists and is not deleted
            Optional<Bed> optionalBed = bedRepository.findByIdAndDeletedFalse(bedId);

            if (optionalBed.isEmpty()) {
                log.error("Bed not found or already deleted with ID: {}", bedId);
                throw new ResourceNotFoundException("Bed not found or already deleted with ID: " + bedId);
            }

            Bed bed = optionalBed.get();

            // Check if the bed is associated with any active ProductionPlanGHMBayBed
            List<ProductionPlanGHMBayBed> associatedPlans = productionPlanGreenHouseManagerBayBedRepository.findByBedIdAndDeletedFalse(bedId);
            if (!associatedPlans.isEmpty()) {
                throw new ValidationException("Bed cannot be deleted as it is associated with active production plans.");
            }

            // Soft delete the bed
            bed.setDeletedBy(userRole);
            bed.setDeletedAt(LocalDateTime.now());
            bed.setDeleted(true);
            bedRepository.save(bed);
        } catch (ValidationException ve) {
            log.error("Validation error while deleting bed with ID {}: {}", bedId, ve.getMessage());
            throw ve;
        } catch (Exception ex) {
            log.error("Error deleting bed with ID: {}", bedId, ex);
            throw new RuntimeException("Error deleting bed", ex);
        }
    }


    public List<Bed> findByBayId(Long bayId) {
        try {
            return bedRepository.findByBayIdAndDeletedFalse(bayId);
        } catch (Exception ex) {
            log.error("Error finding beds by bay ID: {}", bayId, ex);
            throw ex;
        }
    }

    @Override
    public BedAvailableResponseDto getBedsByBayWeekAndYear(BayWeekRequestDto bayWeekRequestDto) {
        List<Bed> beds = bedRepository.findByBayIdAndDeletedFalse(bayWeekRequestDto.getBayId());

        // Filter the beds based on the logic you described
        List<Bed> filteredBeds = beds.stream()
                .filter(bed -> {
                    // If the transplant end year is less than the request year, include the bed
                    if (bed.getTransplantEndYear() < bayWeekRequestDto.getStartYear()) {
                        return true;
                    }

                    // If the transplant end year is the same as the request year, check the week
                    if (bed.getTransplantEndYear().equals(bayWeekRequestDto.getStartYear())) {
                        return bed.getHarvestWeek() < bayWeekRequestDto.getStartWeek();
                    }

                    return false;
                })
                .collect(Collectors.toList());

        int count = filteredBeds.size(); // Calculate the count of filtered beds

        // Convert the filtered beds to BedResponseDto
        List<BedResponseDto> bedResponseDtos = filteredBeds.stream()
                .map(this::convertToResponseDto)
                .collect(Collectors.toList());

        // Return the response DTO with count and list of beds
        return BedAvailableResponseDto.builder()
                .count(count)
                .beds(bedResponseDtos)
                .build();
    }

    private String getCurrentUsername() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication.getPrincipal() instanceof UserDetails) {
            return ((UserDetails) authentication.getPrincipal()).getUsername();
        }
        return null;
    }

    private BedResponseDto convertToResponseDto(Bed bed) {
        return BedResponseDto.builder()
                .id(bed.getId())
                .name(bed.getName())
                .bayId(bed.getBayId())
                .createdBy(bed.getCreatedBy())
                .updatedBy(bed.getUpdatedBy())
                .deletedBy(bed.getDeletedBy())
                .createdAt(bed.getCreatedAt())
                .updatedAt(bed.getUpdatedAt())
                .build();
    }

    private Bed convertToEntity(BedRequestDto bedRequestDto, String sanitizedName) {
        return Bed.builder()
                .name(sanitizedName)
                .bayId(bedRequestDto.getBayId())
                .build();
    }

    private String sanitizeInput(String input) {
        return (input != null) ? input.trim() : "";
    }

    private void validateUniqueBedNameInBay(String name, Long bayId) {
        try {
            boolean exists = bedRepository.existsByNameAndBayIdAndDeletedFalse(name, bayId);
            if (exists) {
                throw new ResourceAlreadyExistsException("Bed with name '" + name + "' already exists in the bay.");
            }
        } catch (Exception ex) {
            log.error("Error validating unique bed name in bay with ID: {}", bayId, ex);
            throw ex;
        }
    }
}
