package com.shirohana.service.implementation.farm;


import com.shirohana.dto.farm.*;
import com.shirohana.repository.farm.BayRepository;
import com.shirohana.repository.farm.BedRepository;
import com.shirohana.entity.farm.Bay;
import com.shirohana.entity.farm.Bed;
import com.shirohana.exception.ResourceAlreadyExistsException;
import com.shirohana.exception.ResourceNotFoundException;
import com.shirohana.service.interfaces.farm.BayService;
import jakarta.transaction.Transactional;
import jakarta.validation.ValidationException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class BayServiceImpl implements BayService {
    private final BayRepository bayRepository;
    private final BedRepository bedRepository;
    private final BedServiceImpl bedServiceImpl;

    public List<BayResponseDto> getAllBaysByGreenhouseId(Long greenhouseId) {
        try {
            return bayRepository.findByGreenhouseIdAndDeletedFalse(greenhouseId).stream()
                    .map(this::convertToResponseDto)
                    .collect(Collectors.toList());
        } catch (Exception ex) {
            log.error("Error getting all bays by greenhouse ID: {}", greenhouseId, ex);
            throw ex;
        }
    }

    public BayResponseDto createBay(BayRequestDto bayRequestDto) {
        try {
            String name = sanitizeInput(bayRequestDto.getName());

            // Check if bay name already exists in the greenhouse
            Optional<Bay> existingBayWithName = bayRepository.findByNameAndGreenhouseIdAndDeletedFalse(name, bayRequestDto.getGreenhouseId());
            if (existingBayWithName.isPresent()) {
                throw new ResourceAlreadyExistsException("Bay with name '" + name + "' already exists in this greenhouse.");
            }

            Bay bay = convertToEntity(bayRequestDto);
            bay.setCreatedAt(LocalDateTime.now());
            bay.setCreatedBy(getCurrentUsername());
            Bay savedBay = bayRepository.save(bay);
            return convertToResponseDto(savedBay);
        } catch (Exception ex) {
            log.error("Error creating bay: {}", bayRequestDto, ex);
            throw ex;
        }
    }

    public BayResponseDto updateBay(Long id, BayRequestDto bayRequestDto) {
        try {
            String name = sanitizeInput(bayRequestDto.getName());

            // Check if bay name already exists for a different bay in the same greenhouse
            Optional<Bay> existingBayWithName = bayRepository.findByNameAndGreenhouseIdAndDeletedFalse(name, bayRequestDto.getGreenhouseId());
            if (existingBayWithName.isPresent() && !existingBayWithName.get().getId().equals(id)) {
                throw new ResourceAlreadyExistsException("Bay with name '" + name + "' already exists in this greenhouse.");
            }

            Bay existingBay = bayRepository.findById(id)
                    .orElseThrow(() -> new ResourceNotFoundException("Bay not found with id " + id));

            existingBay.setName(name);
            existingBay.setGreenhouseId(bayRequestDto.getGreenhouseId());
            existingBay.setUpdatedAt(LocalDateTime.now());
            existingBay.setUpdatedBy(getCurrentUsername());
            Bay updatedBay = bayRepository.save(existingBay);
            return convertToResponseDto(updatedBay);
        } catch (Exception ex) {
            log.error("Error updating bay with ID: {}", id, ex);
            throw ex;
        }
    }

//todo not remove
//    @Transactional
//    public void deleteBay(Long bayId, String userRole) {
//        if (!"superUser".equals(userRole)) {
//            throw new UnauthorizedException("Only super users can delete bays");
//        }
//
//        Bay bay = bayRepository.findById(bayId)
//                .orElseThrow(() -> new ResourceNotFoundException("Bay not found with id " + bayId));
//
//        List<Bed> beds = bedRepository.findByBayId(bayId);
//        for (Bed bed : beds) {
//            bedServiceImpl.deleteBed(bed.getId(), userRole);
//        }
//
//        bay.setDeletedBy(userRole);
//        bay.setDeletedAt(LocalDateTime.now());
//        bay.setDeleted(true);
//        bayRepository.save(bay);
//    }

    @Transactional
    public void deleteBay(Long bayId) {
        try {
            String userRole = getCurrentUsername();

            // Fetch the bay, and if it's not found or already deleted, throw an exception
           Bay bay = bayRepository.findByIdAndDeletedFalse(bayId);

           if (bay == null){
               log.error("Bay not found or already deleted with ID: {}", bayId);
               throw new ResourceNotFoundException("Bay not found or already deleted  ");
           }
            // Validate if any associated beds are not deleted
            List<Bed> activeBeds = bedRepository.findByBayIdAndDeletedFalse(bayId);
            if (!activeBeds.isEmpty()) {
                throw new ValidationException("Bay cannot be deleted as it has associated active beds.");
            }

            // Soft delete the bay
            bay.setDeletedBy(userRole);
            bay.setDeletedAt(LocalDateTime.now());
            bay.setDeleted(true);
            bayRepository.save(bay);
        } catch (ValidationException ve) {
            log.error("Validation error while deleting bay with ID {}: {}", bayId, ve.getMessage());
            throw ve;
        } catch (Exception ex) {
            log.error("Error deleting bay with ID: {}", bayId, ex);
            throw ex;
        }
    }


    public List<Bay> findByGreenhouseId(Long greenhouseId) {
        return bayRepository.findByGreenhouseIdAndDeletedFalse(greenhouseId);
    }


    private String getCurrentUsername() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication.getPrincipal() instanceof UserDetails) {
            return ((UserDetails) authentication.getPrincipal()).getUsername();
        }
        return null;
    }
    @Override
    public BayBedAvailableResponseDto getBayBedAvailability(BayBedCountRequestDto bayBedCountRequestDto) {
        List<Bay> bays = bayRepository.findByGreenhouseIdAndDeletedFalse(bayBedCountRequestDto.getGreenhouseId());

        List<BayResponseDto> bayResponseDtos = bays.stream()
                .map(bay -> {
                    List<Bed> beds = bedRepository.findByBayIdAndDeletedFalse(bay.getId());

                    // Filter the beds based on the logic you provided
                    List<Bed> filteredBeds = beds.stream()
                            .filter(bed -> {
                                if (bed.getTransplantEndYear() < bayBedCountRequestDto.getStartYear()) {
                                    return true;
                                }
                                if (bed.getTransplantEndYear().equals(bayBedCountRequestDto.getStartYear())) {
                                    return bed.getHarvestWeek() < bayBedCountRequestDto.getStartWeek();
                                }
                                return false;
                            })
                            .collect(Collectors.toList());

                    int availableBeds = filteredBeds.size();

                    return BayResponseDto.builder()
                            .id(bay.getId())
                            .name(bay.getName())
                            .greenhouseId(bay.getGreenhouseId())
                            .createdBy(bay.getCreatedBy())
                            .updatedBy(bay.getUpdatedBy())
                            .deletedBy(bay.getDeletedBy())
                            .createdAt(bay.getCreatedAt())
                            .updatedAt(bay.getUpdatedAt())
                            .availableBeds(availableBeds)
                            .build();
                })
                .filter(bayResponseDto -> bayResponseDto.getAvailableBeds() > 0) // Filter out bays with no available beds
                .collect(Collectors.toList());

        int availableBayCount = bayResponseDtos.size();

        return BayBedAvailableResponseDto.builder()
                .availableBayCount(availableBayCount)
                .bays(bayResponseDtos)
                .build();
    }

    private BayResponseDto convertToResponseDto(Bay bay) {
        return BayResponseDto.builder()
                .id(bay.getId())
                .name(bay.getName())
                .greenhouseId(bay.getGreenhouseId())
                .createdBy(bay.getCreatedBy())
                .updatedBy(bay.getUpdatedBy())
                .deletedBy(bay.getDeletedBy())
                .createdAt(bay.getCreatedAt())
                .updatedAt(bay.getUpdatedAt())
                .availableBeds(0)
                .build();
    }

    private Bay convertToEntity(BayRequestDto bayRequestDto) {
        return Bay.builder()
                .name(bayRequestDto.getName())
                .greenhouseId(bayRequestDto.getGreenhouseId())
                .build();
    }

    private String sanitizeInput(String input) {
        return (input != null) ? input.trim() : "";
    }
}
