package com.shirohana.service.implementation.farm;

import com.shirohana.dto.farm.*;
import com.shirohana.repository.farm.BayRepository;
import com.shirohana.repository.farm.BedRepository;
import com.shirohana.repository.farm.FarmRepository;
import com.shirohana.repository.farm.GreenhouseRepository;
import com.shirohana.entity.farm.Farm;
import com.shirohana.entity.farm.Greenhouse;
import com.shirohana.exception.ResourceAlreadyExistsException;
import com.shirohana.exception.ResourceNotFoundException;
import com.shirohana.repository.shedule.pplan.PlantationPlanFarmRepository;
import com.shirohana.repository.shedule.pplan.PlantationPlanRepository;
import com.shirohana.service.interfaces.farm.FarmService;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import javax.xml.bind.ValidationException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class FarmServiceImpl implements FarmService {

    private final FarmRepository farmRepository;
    private final GreenhouseRepository greenhouseRepository;
    private final BayRepository bayRepository;
    private final BedRepository bedRepository;
    private final GreenhouseServiceImpl greenhouseServiceImpl;
    private final PlantationPlanFarmRepository plantationPlanFarmRepository;

    public List<FarmResponseDto> getAllFarms() {
        try {
            return farmRepository.findByDeletedFalse().stream()
                    .map(this::convertToResponseDto)
                    .collect(Collectors.toList());
        } catch (Exception ex) {
            log.error("Error fetching all farms", ex);
            throw new RuntimeException("Error fetching all farms", ex);
        }
    }

    @Override
    public FarmGreenHouseAvailableResponseDto getAvailableFarms(FarmBayCountRequestDto request) {
        List<Farm> farms = farmRepository.findByDeletedFalse();

        List<FarmResponseDto> farmResponseDtos = farms.stream()
                .map(farm -> {
                    // Reuse the logic to get available greenhouses for each farm
                    GHBayBedCountRequestDto greenhouseRequestDto = GHBayBedCountRequestDto.builder()
                            .startWeek(request.getStartWeek())
                            .startYear(request.getStartYear())
                            .endWeek(request.getEndWeek())
                            .endYear(request.getEndYear())
                            .farmId(farm.getId())
                            .build();

                    GHBayAvailableResponseDto availableGreenhouses = greenhouseServiceImpl.getAvailableGreenhousesByFarmId(greenhouseRequestDto);

                    // If the farm has no available greenhouses, skip it
                    if (availableGreenhouses.getCount() == 0) {
                        return null;
                    }

                    int totalAvailableBayCount = availableGreenhouses.getBays().stream()
                            .mapToInt(GreenhouseResponseDto::getAvailableBayCount).sum();

                    int totalAvailableBedCount = availableGreenhouses.getBays().stream()
                            .mapToInt(GreenhouseResponseDto::getAvailableTotalBedCounts).sum();

                    return FarmResponseDto.builder()
                            .id(farm.getId())
                            .name(farm.getName())
                            .location(farm.getLocation())
                            .farmSize(farm.getFarmSize())
                            .createdBy(farm.getCreatedBy())
                            .updatedBy(farm.getUpdatedBy())
                            .deletedBy(farm.getDeletedBy())
                            .createdAt(farm.getCreatedAt())
                            .updatedAt(farm.getUpdatedAt())
                            .availableGreenhouseCount(availableGreenhouses.getCount())
                            .availableBayCount(totalAvailableBayCount)
                            .availableBedCount(totalAvailableBedCount)
                            .build();
                })
                .filter(farm -> farm != null)
                .collect(Collectors.toList());

        return FarmGreenHouseAvailableResponseDto.builder()
                .availableFarmCount(farmResponseDtos.size())
                .farms(farmResponseDtos)
                .build();
    }

    public FarmResponseDto createFarm(FarmRequestDto farmRequestDto) {
        try {
            String name = sanitizeInput(farmRequestDto.getName());
            String location = sanitizeInput(farmRequestDto.getLocation());

            if (farmRepository.findByNameAndDeletedFalse(name).isPresent()) {
                throw new ResourceNotFoundException("Farm with name '" + name + "' already exists.");
            }

            Farm farm = Farm.builder()
                    .name(name)
                    .location(location)
                    .farmSize(farmRequestDto.getFarmSize())
                    .createdAt(LocalDateTime.now())
                    .createdBy(getCurrentUsername())
                    .build();

            Farm savedFarm = farmRepository.save(farm);
            return convertToResponseDto(savedFarm);
        } catch (ResourceNotFoundException ex) {
            log.error("Farm already exists: {}", farmRequestDto.getName(), ex);
            throw ex;
        } catch (Exception ex) {
            log.error("Error creating farm", ex);
            throw new RuntimeException("Error creating farm", ex);
        }
    }

    private String getCurrentUsername() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication.getPrincipal() instanceof UserDetails) {
            return ((UserDetails) authentication.getPrincipal()).getUsername();
        }
        return null;
    }

    public FarmResponseDto updateFarm(Long id, FarmRequestDto farmRequestDto) {
        try {
            String name = sanitizeInput(farmRequestDto.getName());
            String location = sanitizeInput(farmRequestDto.getLocation());

            Optional<Farm> existingFarmWithName = farmRepository.findByNameAndDeletedFalse(name);
            if (existingFarmWithName.isPresent() && !existingFarmWithName.get().getId().equals(id)) {
                throw new ResourceAlreadyExistsException("Farm name already exists.");
            }

            Farm existingFarm = farmRepository.findById(id)
                    .orElseThrow(() -> new ResourceNotFoundException("Farm not found with id " + id));

            existingFarm.setName(name);
            existingFarm.setLocation(location);
            existingFarm.setFarmSize(farmRequestDto.getFarmSize());
            existingFarm.setUpdatedAt(LocalDateTime.now());
            existingFarm.setUpdatedBy(getCurrentUsername());

            Farm updatedFarm = farmRepository.save(existingFarm);
            return convertToResponseDto(updatedFarm);
        } catch (ResourceNotFoundException | ResourceAlreadyExistsException ex) {
            log.error("Error updating farm with id {}: {}", id, ex.getMessage(), ex);
            throw ex;
        } catch (Exception ex) {
            log.error("Error updating farm with id {}", id, ex);
            throw new RuntimeException("Error updating farm", ex);
        }
    }

    @Transactional
    public void deleteFarm(Long farmId) {
        try {
            String userRole = getCurrentUsername();
            Farm farm = farmRepository.findById(farmId)
                    .orElseThrow(() -> new ResourceNotFoundException("Farm not found with id " + farmId));

            if (farm.isDeleted()) {
                throw new IllegalArgumentException("Farm name already exists.");
            }

            boolean hasActiveGreenHouse = greenhouseRepository.existsByFarmIdAndDeletedFalse(farmId);

            if (hasActiveGreenHouse){
                throw new ValidationException("Farm cannot be deleted because it is associated with active Plantation Plans.");
            }

            boolean hasActivePlantationPlan = plantationPlanFarmRepository.existsByFarmIdAndDeletedFalse(farmId);

            if (hasActivePlantationPlan){
                throw new ValidationException("Farm cannot be deleted because it is associated with active Green Houses s.");
            }

            farm.setDeletedBy(userRole);
            farm.setDeletedAt(LocalDateTime.now());
            farm.setDeleted(true);
            farmRepository.save(farm);
        } catch (ResourceNotFoundException | IllegalArgumentException ex) {
            log.error("Error deleting farm with id {}: {}", farmId, ex.getMessage(), ex);
            throw ex;
        } catch (Exception ex) {
            log.error("Error deleting farm with id {}", farmId, ex);
            throw new RuntimeException(ex);
        }
    }

    private FarmResponseDto convertToResponseDto(Farm farm) {
        return FarmResponseDto.builder()
                .id(farm.getId())
                .name(farm.getName())
                .location(farm.getLocation())
                .farmSize(farm.getFarmSize())
                .createdBy(farm.getCreatedBy())
                .updatedBy(farm.getUpdatedBy())
                .deletedBy(farm.getDeletedBy())
                .createdAt(farm.getCreatedAt())
                .updatedAt(farm.getUpdatedAt())
                .build();
    }

    private Farm convertToEntity(FarmRequestDto farmRequestDto) {
        return Farm.builder()
                .name(farmRequestDto.getName())
                .location(farmRequestDto.getLocation())
                .farmSize(farmRequestDto.getFarmSize())
                .build();
    }


    private String sanitizeInput(String input) {
        return (input != null) ? input.trim() : "";
    }




}
