package com.shirohana.service.implementation.farm;

import com.shirohana.dto.farm.*;
import com.shirohana.entity.farm.Bed;
import com.shirohana.repository.farm.BayRepository;
import com.shirohana.repository.farm.BedRepository;
import com.shirohana.repository.farm.FarmRepository;
import com.shirohana.repository.farm.GreenhouseRepository;
import com.shirohana.entity.farm.Bay;
import com.shirohana.entity.farm.Farm;
import com.shirohana.entity.farm.Greenhouse;
import com.shirohana.exception.ResourceAlreadyExistsException;
import com.shirohana.exception.ResourceNotFoundException;
import com.shirohana.service.interfaces.farm.GreenhouseService;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class GreenhouseServiceImpl implements GreenhouseService {
    private final GreenhouseRepository greenhouseRepository;
    private final FarmRepository farmRepository;
    private final BayRepository bayRepository;
    private final BedRepository bedRepository;
    private final BayServiceImpl bayServiceImpl;

    public List<GreenhouseResponseDto> getAllGreenhousesByFarmId(Long farmId) {
        try {
            List<Greenhouse> greenhouses = greenhouseRepository.findByFarmIdAndDeletedFalse(farmId);

            return greenhouses.stream()
                    .map(this::convertToResponseDto)
                    .collect(Collectors.toList());
        } catch (Exception ex) {
            log.error("Error getting all greenhouses by farm ID: {}", farmId, ex);
            throw ex;
        }
    }

    @Override
    public GHBayAvailableResponseDto getAvailableGreenhousesByFarmId(GHBayBedCountRequestDto ghBayBedCountRequestDto) {
        List<Greenhouse> greenhouses = greenhouseRepository.findByFarmIdAndDeletedFalse(ghBayBedCountRequestDto.getFarmId());

        List<GreenhouseResponseDto> greenhouseResponseDtos = greenhouses.stream()
                .map(greenhouse -> {
                    // Get bays associated with the greenhouse
                    List<Bay> bays = bayRepository.findByGreenhouseIdAndDeletedFalse(greenhouse.getId());

                    // Variables to count available bays and total available beds
                    int availableBayCount = 0;
                    int availableTotalBedCounts = 0;

                    for (Bay bay : bays) {
                        List<Bed> beds = bedRepository.findByBayIdAndDeletedFalse(bay.getId());

                        // Check if any bed meets the criteria and count available beds
                        boolean isBayAvailable = beds.stream().anyMatch(bed -> {
                            if (bed.getTransplantEndYear() < ghBayBedCountRequestDto.getStartYear()) {
                                return true;
                            }
                            if (bed.getTransplantEndYear().equals(ghBayBedCountRequestDto.getStartYear())) {
                                return bed.getHarvestWeek() < ghBayBedCountRequestDto.getStartWeek();
                            }
                            return false;
                        });

                        if (isBayAvailable) {
                            availableBayCount++;
                            // Sum up the available beds that meet the criteria
                            availableTotalBedCounts += (int) beds.stream()
                                    .filter(bed -> {
                                        if (bed.getTransplantEndYear() < ghBayBedCountRequestDto.getStartYear()) {
                                            return true;
                                        }
                                        if (bed.getTransplantEndYear().equals(ghBayBedCountRequestDto.getStartYear())) {
                                            return bed.getHarvestWeek() < ghBayBedCountRequestDto.getStartWeek();
                                        }
                                        return false;
                                    })
                                    .count();
                        }
                    }

                    // If the greenhouse has no available bays, skip it
                    if (availableBayCount == 0) {
                        return null;
                    }

                    // Create the response DTO
                    return GreenhouseResponseDto.builder()
                            .id(greenhouse.getId())
                            .name(greenhouse.getName())
                            .farmId(greenhouse.getFarmId())
                            .createdBy(greenhouse.getCreatedBy())
                            .updatedBy(greenhouse.getUpdatedBy())
                            .deletedBy(greenhouse.getDeletedBy())
                            .createdAt(greenhouse.getCreatedAt())
                            .updatedAt(greenhouse.getUpdatedAt())
                            .availableBayCount(availableBayCount) // Set available bays count
                            .availableTotalBedCounts(availableTotalBedCounts) // Set available total bed count
                            .build();
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());

        // Create the final response DTO with the count of available greenhouses and the list of GreenhouseResponseDto
        return GHBayAvailableResponseDto.builder()
                .count(greenhouseResponseDtos.size()) // Set available greenhouse count
                .bays(greenhouseResponseDtos) // Set the list of GreenhouseResponseDto
                .build();
    }

    @Override
    public List<GreenhouseResponseDto> getAllGreenhouses() {
        try {
            List<Greenhouse> greenhouses = greenhouseRepository.findAll()
                    .stream()
                    .filter(greenhouse -> !greenhouse.isDeleted())
                    .toList();
            if (greenhouses.isEmpty()) {
                throw new ResourceNotFoundException("No greenhouses found");
            }
            return greenhouses.stream()
                    .map(this::convertToResponseDto)
                    .collect(Collectors.toList());
        } catch (Exception ex) {
            log.error("Error getting all greenhouses", ex);
            throw ex;
        }
    }

    public GreenhouseWithBaysResponseDto getAllGreenhousesAndBaysByFarmId(Long farmId) {
        try {
            List<Greenhouse> greenhouses = greenhouseRepository.findByFarmIdAndDeletedFalse(farmId);

            if (greenhouses.isEmpty()) {
                throw new ResourceNotFoundException("No greenhouses found for farm ID " + farmId);
            }

            List<GreenhouseWithBaysResponseDto.GreenhouseDto> greenhouseDtos = greenhouses.stream()
                    .map(greenhouse -> {
                        List<Bay> bays = bayRepository.findByGreenhouseIdAndDeletedFalse(greenhouse.getId());
                        List<GreenhouseWithBaysResponseDto.GreenhouseDto.BayDto> bayDtos = bays.stream()
                                .map(bay -> GreenhouseWithBaysResponseDto.GreenhouseDto.BayDto.builder()
                                        .bayId(bay.getId())
                                        .bayName(bay.getName())
                                        .build())
                                .collect(Collectors.toList());

                        return GreenhouseWithBaysResponseDto.GreenhouseDto.builder()
                                .greenhouseId(greenhouse.getId())
                                .greenhouseName(greenhouse.getName())
                                .bays(bayDtos)
                                .build();
                    })
                    .collect(Collectors.toList());

            return GreenhouseWithBaysResponseDto.builder()
                    .farmId(farmId)
                    .greenhouses(greenhouseDtos)
                    .build();
        } catch (Exception ex) {
            log.error("Error getting all greenhouses and bays by farm ID: {}", farmId, ex);
            throw ex;
        }
    }

    public GreenhouseResponseDto createGreenhouse(GreenhouseRequestDto greenhouseRequestDto) {
        try {
            String name = sanitizeInput(greenhouseRequestDto.getName());

            if (greenhouseRepository.findByNameAndDeletedFalse(name).isPresent()) {
                throw new ResourceAlreadyExistsException("Greenhouse with name '" + name + "' already exists.");
            }

            Farm farm = farmRepository.findById(greenhouseRequestDto.getFarmId())
                    .orElseThrow(() -> new ResourceNotFoundException("Farm not found with id " + greenhouseRequestDto.getFarmId()));

            Greenhouse greenhouse = Greenhouse.builder()
                    .name(name)
                    .farmId(farm.getId())
                    .createdAt(LocalDateTime.now())
                    .createdBy(getCurrentUsername())
                    .build();

            Greenhouse savedGreenhouse = greenhouseRepository.save(greenhouse);
            return convertToResponseDto(savedGreenhouse);
        } catch (Exception ex) {
            log.error("Error creating greenhouse: {}", greenhouseRequestDto, ex);
            throw ex;
        }
    }

    public GreenhouseResponseDto updateGreenhouse(Long id, GreenhouseRequestDto greenhouseRequestDto) {
        try {
            String name = sanitizeInput(greenhouseRequestDto.getName());

            Optional<Greenhouse> existingGreenhouseWithName = greenhouseRepository.findByNameAndDeletedFalse(name);
            if (existingGreenhouseWithName.isPresent() && !existingGreenhouseWithName.get().getId().equals(id)) {
                throw new ResourceAlreadyExistsException("Greenhouse with name " + greenhouseRequestDto.getName() + " already exists in this farm.");
            }

            Greenhouse existingGreenhouse = greenhouseRepository.findById(id)
                    .orElseThrow(() -> new ResourceNotFoundException("Greenhouse not found with id " + id));

            existingGreenhouse.setName(name);
            existingGreenhouse.setUpdatedAt(LocalDateTime.now());
            existingGreenhouse.setUpdatedBy(getCurrentUsername());

            Greenhouse updatedGreenhouse = greenhouseRepository.save(existingGreenhouse);
            return convertToResponseDto(updatedGreenhouse);
        } catch (Exception ex) {
            log.error("Error updating greenhouse with ID: {}", id, ex);
            throw ex;
        }
    }

    @Transactional
    public void deleteGreenhouse(Long greenhouseId) {
        try {
            String userRole = getCurrentUsername();

            Greenhouse greenhouse = greenhouseRepository.findById(greenhouseId)
                    .orElseThrow(() -> new ResourceNotFoundException("Greenhouse not found with id " + greenhouseId));

            if (greenhouse.isDeleted()) {
                throw new IllegalArgumentException("Greenhouse with id " + greenhouseId + " is already deleted.");
            }

            List<Bay> bays = bayRepository.findByGreenhouseIdAndDeletedFalse(greenhouseId);
            for (Bay bay : bays) {
                bayServiceImpl.deleteBay(bay.getId());
            }

            greenhouse.setDeletedBy(userRole);
            greenhouse.setDeletedAt(LocalDateTime.now());
            greenhouse.setDeleted(true);
            greenhouseRepository.save(greenhouse);
        } catch (Exception ex) {
            log.error("Error deleting greenhouse with ID: {}", greenhouseId, ex);
            throw ex;
        }
    }

    public List<Greenhouse> findByFarmId(Long farmId) {
        return greenhouseRepository.findByFarmIdAndDeletedFalse(farmId);
    }


    private String getCurrentUsername() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication.getPrincipal() instanceof UserDetails) {
            return ((UserDetails) authentication.getPrincipal()).getUsername();
        }
        return null;
    }

    private GreenhouseResponseDto convertToResponseDto(Greenhouse greenhouse) {
        return GreenhouseResponseDto.builder()
                .id(greenhouse.getId())
                .name(greenhouse.getName())
                .farmId(greenhouse.getFarmId())
                .createdBy(greenhouse.getCreatedBy())
                .updatedBy(greenhouse.getUpdatedBy())
                .deletedBy(greenhouse.getDeletedBy())
                .createdAt(greenhouse.getCreatedAt())
                .updatedAt(greenhouse.getUpdatedAt())
                .build();
    }

    private Greenhouse convertToEntity(GreenhouseRequestDto greenhouseRequestDto) {
        return Greenhouse.builder()
                .name(greenhouseRequestDto.getName())
                .build();
    }

    private String sanitizeInput(String input) {
        return (input != null) ? input.trim() : "";
    }

}
