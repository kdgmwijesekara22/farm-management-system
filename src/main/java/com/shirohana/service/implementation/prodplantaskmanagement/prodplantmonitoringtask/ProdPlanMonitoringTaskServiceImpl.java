package com.shirohana.service.implementation.prodplantaskmanagement.prodplantmonitoringtask;


import com.shirohana.constants.TaskTypeConstants;
import com.shirohana.dto.planttasksetup.monitoringtasksetup.MonitoringTaskSetupResponseDto;
import com.shirohana.dto.prodplantaskmanagement.prodplanmonitoringtask.ProdPlanMonitoringTaskRequestDto;
import com.shirohana.dto.prodplantaskmanagement.prodplanmonitoringtask.ProdPlanMonitoringTaskResponseDto;
import com.shirohana.dto.prodplantaskmanagement.prodplanmonitoringtask.filtered.ProdPlanMonitoringTaskFilterByPurposeCombinedResponseDto;
import com.shirohana.dto.prodplantaskmanagement.prodplanmonitoringtask.filtered.ProdPlanMonitoringTaskFilterByPurposeResponseDto;
import com.shirohana.entity.prodplantaskmanagement.prodplanmonitoringtask.ProdPlanMonitoringTask;

import com.shirohana.entity.prodplantaskmanagement.prodplanmonitoringtask.filtered.ProdPlanCombinedMonitoringTask;
import com.shirohana.exception.ResourceNotFoundException;
import com.shirohana.repository.prodplantaskmanagement.prodplantmonitoringtask.ProdPlanMonitoringTaskRepository;
import com.shirohana.repository.prodplantaskmanagement.prodplantmonitoringtask.filtered.ProdPlanCombinedMonitoringTaskRepository;
import com.shirohana.service.interfaces.planttasksetup.MonitoringTaskSetupService;
import com.shirohana.service.interfaces.prodplantaskmanagement.prodplantmonitoringtask.ProdPlanMonitoringTaskService;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@Service
@RequiredArgsConstructor
@Slf4j
public class ProdPlanMonitoringTaskServiceImpl implements ProdPlanMonitoringTaskService {

    private final ProdPlanMonitoringTaskRepository prodPlanMonitoringTaskRepository;

    private final MonitoringTaskSetupService monitoringTaskSetupService;

    private final ProdPlanCombinedMonitoringTaskRepository prodPlanCombinedMonitoringTaskRepository;

    @Override
    public ProdPlanMonitoringTaskResponseDto createProdPlanMonitoringTask(ProdPlanMonitoringTaskRequestDto requestDto) {
        ProdPlanMonitoringTask task = mapToEntity(requestDto);
        ProdPlanMonitoringTask savedTask = prodPlanMonitoringTaskRepository.save(task);
        return mapToDto(savedTask);
    }

    @Override
    public ProdPlanMonitoringTaskResponseDto updateProdPlanMonitoringTask(Long id, ProdPlanMonitoringTaskRequestDto requestDto) {
        ProdPlanMonitoringTask task = prodPlanMonitoringTaskRepository.findByIdAndDeletedFalse(id)
                .orElseThrow(() -> new ResourceNotFoundException("Task not found with id " + id));
        updateEntityFromDto(task, requestDto);
        ProdPlanMonitoringTask updatedTask = prodPlanMonitoringTaskRepository.save(task);
        return mapToDto(updatedTask);
    }

    @Override
    public ProdPlanMonitoringTaskResponseDto getProdPlanMonitoringTaskById(Long id) {
        ProdPlanMonitoringTask task = prodPlanMonitoringTaskRepository.findByIdAndDeletedFalse(id)
                .orElseThrow(() -> new ResourceNotFoundException("Task not found with id " + id));
        return mapToDto(task);
    }

    @Override
    public List<ProdPlanMonitoringTaskResponseDto> getAllProdPlanMonitoringTasks() {
        List<ProdPlanMonitoringTask> tasks = prodPlanMonitoringTaskRepository.findAllByDeletedFalse();
        return tasks.stream().map(this::mapToDto).collect(Collectors.toList());
    }

    @Override
    public List<ProdPlanMonitoringTaskResponseDto> getAllProdPlanMonitoringTasksByProdPlanId(Long prodPlanId, Long familyNameId) {

        // Fetch monitoring tasks from the MonitoringTaskSetupService
        List<MonitoringTaskSetupResponseDto> monitoringTasks = monitoringTaskSetupService.getMonitoringTaskSetupByFamilyNameId(familyNameId);

        // Fetch production plan monitoring tasks from the repository
        List<ProdPlanMonitoringTask> tasks = prodPlanMonitoringTaskRepository.findAllByProdPlanIdAndDeletedFalse(prodPlanId);

        // Map monitoring tasks to ProdPlanMonitoringTaskResponseDto
        List<ProdPlanMonitoringTaskResponseDto> monitoringTaskDtos = monitoringTasks.stream()
                .map(this::mapMonitoringTaskToProdPlanMonitoringTaskResponseDto)
                .toList();

        // Map production plan monitoring tasks to ProdPlanMonitoringTaskResponseDto
        List<ProdPlanMonitoringTaskResponseDto> prodPlanTaskDtos = tasks.stream()
                .map(this::mapToDto)
                .toList();

        // Combine both lists into one response list, with monitoring tasks first
        List<ProdPlanMonitoringTaskResponseDto> combinedTasks = new ArrayList<>(monitoringTaskDtos);
        combinedTasks.addAll(prodPlanTaskDtos);

        return combinedTasks;
    }

    @Override
    public List<ProdPlanMonitoringTaskFilterByPurposeCombinedResponseDto> getAllProdPlanMonitoringTasksByProdPlanIdAndPurpose(
            Long prodPlanId, Long familyNameId, String purpose, Long farmId, Long plantationScheduleId, Long plantationPlanId, Long userId)  {

        // Fetch monitoring tasks from the MonitoringTaskSetupService
        List<MonitoringTaskSetupResponseDto> monitoringTasks = monitoringTaskSetupService.getMonitoringTaskSetupByFamilyNameId(familyNameId);

        // Fetch production plan monitoring tasks from the repository
        List<ProdPlanMonitoringTask> prodPlanTasks = prodPlanMonitoringTaskRepository.findAllByProdPlanIdAndDeletedFalse(prodPlanId);

        // Map and combine the fetched data
        List<ProdPlanMonitoringTaskFilterByPurposeResponseDto> combinedTasks = new ArrayList<>();
        combinedTasks.addAll(monitoringTasks.stream().map(task -> mapToFilteredDto(task, purpose, prodPlanId)).collect(Collectors.toList()));
        combinedTasks.addAll(prodPlanTasks.stream().map(task -> mapToFilteredDto(task, purpose)).collect(Collectors.toList()));

        // Save the combined list to the database
        List<ProdPlanMonitoringTaskFilterByPurposeCombinedResponseDto> responseDtos = saveCombinedTasks(combinedTasks, prodPlanId, purpose, farmId, plantationScheduleId, plantationPlanId, userId);

        return responseDtos;

//        todo remove

        //        // Fetch maintenance tasks from the MaintenanceTaskSetupService
//        List<MonitoringTaskSetupResponseDto> monitoringTasks = monitoringTaskSetupService.getMonitoringTaskSetupByFamilyNameId(familyNameId);
//
//        // Fetch production plan maintenance tasks from the repository
//        List<ProdPlanMonitoringTask> tasks = prodPlanMonitoringTaskRepository.findAllByProdPlanIdAndDeletedFalse(prodPlanId);
//
//        // Map maintenance tasks to ProdPlanMaintenanceTaskFilterByPurposeResponseDto
//        List<ProdPlanMonitoringTaskFilterByPurposeResponseDto> monitoringTaskDtos = monitoringTasks.stream()
//                .map(task -> mapToFilteredDto(task, purpose,prodPlanId))
//                .toList();
//
//        // Map production plan maintenance tasks to ProdPlanMaintenanceTaskFilterByPurposeResponseDto
//        List<ProdPlanMonitoringTaskFilterByPurposeResponseDto> prodPlanTaskDtos = tasks.stream()
//                .map(task -> mapToFilteredDto(task, purpose))
//                .toList();
//
//        // Combine both lists into one response list, with maintenance tasks first
//        List<ProdPlanMonitoringTaskFilterByPurposeResponseDto> combinedTasks = new ArrayList<>(monitoringTaskDtos);
//        combinedTasks.addAll(prodPlanTaskDtos);
//
//        return combinedTasks;
    }

    @Override
    public void softDeleteProdPlanMonitoringTask(Long id) {
        ProdPlanMonitoringTask task = prodPlanMonitoringTaskRepository.findByIdAndDeletedFalse(id)
                .orElseThrow(() -> new ResourceNotFoundException("Task not found with id " + id));
        task.setDeleted(true);
        task.setDeletedBy(getCurrentUsername());
        task.setDeletedAt(LocalDateTime.now());
        prodPlanMonitoringTaskRepository.save(task);
    }

    private ProdPlanMonitoringTask mapToEntity(ProdPlanMonitoringTaskRequestDto requestDto) {
        log.debug("Mapping DTO to Entity: {}", requestDto);
        ProdPlanMonitoringTask task = ProdPlanMonitoringTask.builder()
                .familyNameId(requestDto.getFamilyNameId())
                .farmId(requestDto.getFarmId())
                .plantationScheduleId(requestDto.getPlantationScheduleId())
                .plantationPlanId(requestDto.getPlantationPlanId())
                .prodPlanId(requestDto.getProdPlanId())
                .userId(requestDto.getUserId())
                .taskName(requestDto.getTaskName())
                .taskDescription(requestDto.getTaskDescription())
                .production(requestDto.isProduction())
                .prodNoOfTerms(requestDto.getProdNoOfTerms())
                .prodTimeUnit(requestDto.getProdTimeUnit())
                .prodUptoEffectiveStatus(requestDto.isProdUptoEffectiveStatus())
                .prodUptoEffectiveWeek(requestDto.getProdUptoEffectiveWeek())
                .nursery(requestDto.isNursery())
                .nurseryNoOfTerms(requestDto.getNurseryNoOfTerms())
                .nurseryTimeUnit(requestDto.getNurseryTimeUnit())
                .nurseryUptoEffectiveStatus(requestDto.isNurseryUptoEffectiveStatus())
                .nurseryUptoEffectiveWeek(requestDto.getNurseryUptoEffectiveWeek())
                .build();

        log.debug("Mapped Entity: {}", task);
        return task;
    }

    private void updateEntityFromDto(ProdPlanMonitoringTask task, ProdPlanMonitoringTaskRequestDto requestDto) {
        task.setFamilyNameId(requestDto.getFamilyNameId());
        task.setFarmId(requestDto.getFarmId());
        task.setPlantationScheduleId(requestDto.getPlantationScheduleId());
        task.setPlantationPlanId(requestDto.getPlantationPlanId());
        task.setProdPlanId(requestDto.getProdPlanId());
        task.setUserId(requestDto.getUserId());
        task.setTaskName(requestDto.getTaskName());
        task.setTaskDescription(requestDto.getTaskDescription());
        task.setProduction(requestDto.isProduction());
        task.setProdNoOfTerms(requestDto.getProdNoOfTerms());
        task.setProdTimeUnit(requestDto.getProdTimeUnit());
        task.setProdUptoEffectiveStatus(requestDto.isProdUptoEffectiveStatus());
        task.setProdUptoEffectiveWeek(requestDto.getProdUptoEffectiveWeek());
        task.setNursery(requestDto.isNursery());
        task.setNurseryNoOfTerms(requestDto.getNurseryNoOfTerms());
        task.setNurseryTimeUnit(requestDto.getNurseryTimeUnit());
        task.setNurseryUptoEffectiveStatus(requestDto.isNurseryUptoEffectiveStatus());
        task.setNurseryUptoEffectiveWeek(requestDto.getNurseryUptoEffectiveWeek());
    }

    private ProdPlanMonitoringTaskResponseDto mapToDto(ProdPlanMonitoringTask task) {
        return ProdPlanMonitoringTaskResponseDto.builder()
                .id(task.getId())
                .familyNameId(task.getFamilyNameId())
                .prodPlanId(task.getProdPlanId())
                .taskName(task.getTaskName())
                .taskDescription(task.getTaskDescription())
                .production(task.isProduction())
                .prodNoOfTerms(task.getProdNoOfTerms() != null ? task.getProdNoOfTerms() : 0)
                .prodTimeUnit(task.getProdTimeUnit())
                .prodUptoEffectiveStatus(task.isProdUptoEffectiveStatus())
                .prodUptoEffectiveWeek(task.getProdUptoEffectiveWeek() != null ? task.getProdUptoEffectiveWeek() : 0)
                .nursery(task.isNursery())
                .nurseryNoOfTerms(task.getNurseryNoOfTerms() != null ? task.getNurseryNoOfTerms() : 0)
                .nurseryTimeUnit(task.getNurseryTimeUnit())
                .nurseryUptoEffectiveStatus(task.isNurseryUptoEffectiveStatus())
                .nurseryUptoEffectiveWeek(task.getNurseryUptoEffectiveWeek() != null ? task.getNurseryUptoEffectiveWeek() : 0)
                .createdBy(task.getCreatedBy())
                .createdDate(task.getCreatedDate())
                .lastModifiedBy(task.getLastModifiedBy())
                .lastModifiedDate(task.getLastModifiedDate())
                .deletedBy(task.getDeletedBy())
                .deletedAt(task.getDeletedAt())
                .build();
    }

    private ProdPlanMonitoringTaskFilterByPurposeResponseDto mapToFilteredDto(MonitoringTaskSetupResponseDto task, String purpose, Long prodPlanId) {
        boolean isProduction = "production".equalsIgnoreCase(purpose);

        return ProdPlanMonitoringTaskFilterByPurposeResponseDto.builder()
                .id(task.getId())
                .taskName(task.getTaskName())
                .taskDescription(task.getTaskDescription())
                .familyNameId(task.getFamilyNameId())
                .production(isProduction)
                .masterData(true)
                .taskType(TaskTypeConstants.MONITORING)
                .noOfTerms(isProduction ? task.getProdNoOfTerms() : task.getNurseryNoOfTerms())
                .timeUnit(isProduction ? task.getProdTimeUnit() : task.getNurseryTimeUnit())
                .monitoring(isProduction ? task.isProdMonitoring() : task.isNurseryMonitoring())
                .stdValueMin(isProduction ? task.getProdStdValueMin() : task.getNurseryStdValueMin())
                .stdValueMax(isProduction ? task.getProdStdValueMax() : task.getNurseryStdValueMax())
                .stdUnit(isProduction ? task.getProdUnit() : task.getNurseryUnit())
                .uptoEffectiveStatus(isProduction ? task.isProdUptoEffectiveStatus() : task.isNurseryUptoEffectiveStatus())
                .uptoEffectiveWeek(isProduction ? task.getProdUptoEffectiveWeek() : task.getNurseryUptoEffectiveWeek())
                .subTasks(isProduction ? task.getProdSubTasks() : task.getNurserySubTasks())
                .prodPlanId(prodPlanId)
                .createdBy(task.getCreatedBy())
                .createdDate(task.getCreatedDate())
                .lastModifiedBy(task.getLastModifiedBy())
                .lastModifiedDate(task.getLastModifiedDate())
                .deletedAt(task.getDeletedAt())
                .deletedBy(task.getDeletedBy())
                .build();
    }

    private ProdPlanMonitoringTaskFilterByPurposeResponseDto mapToFilteredDto(ProdPlanMonitoringTask task, String purpose) {
        boolean isProduction = "production".equalsIgnoreCase(purpose);

        return ProdPlanMonitoringTaskFilterByPurposeResponseDto.builder()
                .id(task.getId())
                .taskName(task.getTaskName())
                .taskDescription(task.getTaskDescription())
                .familyNameId(task.getFamilyNameId())
                .production(isProduction)
                .masterData(false)
                .taskType("MONITORING")
                .noOfTerms(isProduction ? task.getProdNoOfTerms() : task.getNurseryNoOfTerms())
                .timeUnit(isProduction ? task.getProdTimeUnit() : task.getNurseryTimeUnit())
                .monitoring(false)
                .stdValueMin(null)
                .stdValueMax(null)
                .stdUnit(null)
                .uptoEffectiveStatus(isProduction ? task.isProdUptoEffectiveStatus() : task.isNurseryUptoEffectiveStatus())
                .uptoEffectiveWeek(isProduction ? task.getProdUptoEffectiveWeek() : task.getNurseryUptoEffectiveWeek())
                .subTasks(null)
                .prodPlanId(task.getProdPlanId())
                .createdBy(task.getCreatedBy())
                .createdDate(task.getCreatedDate())
                .lastModifiedBy(task.getLastModifiedBy())
                .lastModifiedDate(task.getLastModifiedDate())
                .deletedAt(task.getDeletedAt())
                .deletedBy(task.getDeletedBy())
                .build();
    }

    private ProdPlanMonitoringTaskResponseDto mapMonitoringTaskToProdPlanMonitoringTaskResponseDto(MonitoringTaskSetupResponseDto monitoringTask) {
        return ProdPlanMonitoringTaskResponseDto.builder()
                .taskName(monitoringTask.getTaskName())
                .taskDescription(monitoringTask.getTaskDescription())
                .build();
    }

    private String getCurrentUsername() {
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (authentication != null && authentication.getPrincipal() instanceof UserDetails) {
                return ((UserDetails) authentication.getPrincipal()).getUsername();
            }
        } catch (Exception e) {
            log.error("Error retrieving current username", e);
        }
        return null;
    }
    @Transactional
    public List<ProdPlanMonitoringTaskFilterByPurposeCombinedResponseDto> saveCombinedTasks(List<ProdPlanMonitoringTaskFilterByPurposeResponseDto> combinedTasks, Long prodPlanId, String purpose, Long farmId, Long plantationScheduleId, Long plantationPlanId, Long userId) {
        List<ProdPlanCombinedMonitoringTask> entities = new ArrayList<>();
        for (ProdPlanMonitoringTaskFilterByPurposeResponseDto dto : combinedTasks) {
            Long checkMasterDataId = dto.isMasterData()?dto.getId():null;
            Long checkSupervisorDataId = dto.isMasterData()?null: dto.getId();
            boolean exists = prodPlanCombinedMonitoringTaskRepository.existsByMasterDataIdAndSupervisorTaskIdAndProdPlanId(
                    checkMasterDataId, checkSupervisorDataId, prodPlanId);
            if (!exists) {
                ProdPlanCombinedMonitoringTask entity = new ProdPlanCombinedMonitoringTask();
                if (dto.isMasterData()) {
                    entity.setMasterDataId(dto.getId());
                    entity.setSupervisorTaskId(null);
                } else {
                    entity.setMasterDataId(null);
                    entity.setSupervisorTaskId(dto.getId());
                }
                entity.setTaskType(dto.getTaskType());
                entity.setProdPlanId(prodPlanId);
                entity.setPurpose(purpose);
                entity.setDeleted(false);
                entity.setFarmId(farmId);
                entity.setPlantationScheduleId(plantationScheduleId);
                entity.setPlantationPlanId(plantationPlanId);
                entity.setUserId(userId);
                entities.add(entity);
            }
        }
        prodPlanCombinedMonitoringTaskRepository.saveAll(entities);

        return entities.stream().map(this::mapToCombinedResponseDto).collect(Collectors.toList());
    }

    private ProdPlanMonitoringTaskFilterByPurposeCombinedResponseDto mapToCombinedResponseDto(ProdPlanCombinedMonitoringTask entity) {
        boolean isProduction = "production".equalsIgnoreCase(entity.getPurpose());
        ProdPlanMonitoringTaskFilterByPurposeCombinedResponseDto responseDto = new ProdPlanMonitoringTaskFilterByPurposeCombinedResponseDto();
        responseDto.setId(entity.getId());
        responseDto.setMasterDataId(entity.getMasterDataId());
        responseDto.setSupervisorTaskId(entity.getSupervisorTaskId());
        responseDto.setProdPlanId(entity.getProdPlanId());
        responseDto.setProduction(isProduction);
        responseDto.setTaskType(entity.getTaskType());

        if (entity.getMasterDataId() != null) {
            MonitoringTaskSetupResponseDto masterData = monitoringTaskSetupService.getTaskById(entity.getMasterDataId());
            populateResponseDtoFromMasterData(responseDto, masterData);
        } else if (entity.getSupervisorTaskId() != null) {
            ProdPlanMonitoringTask supervisorTask = prodPlanMonitoringTaskRepository.findByIdAndDeletedFalse(entity.getSupervisorTaskId())
                    .orElseThrow(() -> new RuntimeException("Supervisor task not found"));
            populateResponseDtoFromSupervisorTask(responseDto, supervisorTask);
        }

        return responseDto;
    }
    private void populateResponseDtoFromMasterData(ProdPlanMonitoringTaskFilterByPurposeCombinedResponseDto responseDto, MonitoringTaskSetupResponseDto masterData) {
        boolean isProduction = responseDto.isProduction();
        responseDto.setTaskName(masterData.getTaskName());
        responseDto.setTaskDescription(masterData.getTaskDescription());
        responseDto.setFamilyNameId(masterData.getFamilyNameId());
        responseDto.setProduction(masterData.isProduction());
        responseDto.setMasterData(true);
        responseDto.setNoOfTerms(isProduction ? masterData.getProdNoOfTerms() : masterData.getNurseryNoOfTerms());
        responseDto.setTimeUnit(isProduction ? masterData.getProdTimeUnit() : masterData.getNurseryTimeUnit());
        responseDto.setMonitoring(isProduction ? masterData.isProdMonitoring() : masterData.isNurseryMonitoring());
        responseDto.setStdValueMin(isProduction ? masterData.getProdStdValueMin() : masterData.getNurseryStdValueMin());
        responseDto.setStdValueMax(isProduction ? masterData.getProdStdValueMax() : masterData.getNurseryStdValueMax());
        responseDto.setStdUnit(isProduction ? masterData.getProdUnit() : masterData.getNurseryUnit());
        responseDto.setUptoEffectiveStatus(isProduction ? masterData.isProdUptoEffectiveStatus() : masterData.isNurseryUptoEffectiveStatus());
        responseDto.setUptoEffectiveWeek(isProduction ? masterData.getProdUptoEffectiveWeek() : masterData.getNurseryUptoEffectiveWeek());
        responseDto.setSubTasks(isProduction ? masterData.getProdSubTasks() : masterData.getNurserySubTasks());
        responseDto.setCreatedBy(masterData.getCreatedBy());
        responseDto.setCreatedDate(masterData.getCreatedDate());
        responseDto.setLastModifiedBy(masterData.getLastModifiedBy());
        responseDto.setLastModifiedDate(masterData.getLastModifiedDate());
    }

    private void populateResponseDtoFromSupervisorTask(ProdPlanMonitoringTaskFilterByPurposeCombinedResponseDto responseDto, ProdPlanMonitoringTask supervisorTask) {
        boolean isProduction = responseDto.isProduction();
        responseDto.setTaskName(supervisorTask.getTaskName());
        responseDto.setTaskDescription(supervisorTask.getTaskDescription());
        responseDto.setFamilyNameId(supervisorTask.getFamilyNameId());
        responseDto.setProduction(supervisorTask.isProduction());
        responseDto.setMasterData(false);
        responseDto.setNoOfTerms(isProduction ? supervisorTask.getProdNoOfTerms() : supervisorTask.getNurseryNoOfTerms());
        responseDto.setTimeUnit(isProduction ? supervisorTask.getProdTimeUnit() : supervisorTask.getNurseryTimeUnit());
        responseDto.setMonitoring(false);
        responseDto.setStdValueMin(null);
        responseDto.setStdValueMax(null);
        responseDto.setStdUnit(null);
        responseDto.setUptoEffectiveStatus(isProduction ? supervisorTask.isProdUptoEffectiveStatus() : supervisorTask.isNurseryUptoEffectiveStatus());
        responseDto.setUptoEffectiveWeek(isProduction ? supervisorTask.getProdUptoEffectiveWeek() : supervisorTask.getNurseryUptoEffectiveWeek());
        responseDto.setSubTasks(null);
        responseDto.setCreatedBy(supervisorTask.getCreatedBy());
        responseDto.setCreatedDate(supervisorTask.getCreatedDate());
        responseDto.setLastModifiedBy(supervisorTask.getLastModifiedBy());
        responseDto.setLastModifiedDate(supervisorTask.getLastModifiedDate());
    }
}
