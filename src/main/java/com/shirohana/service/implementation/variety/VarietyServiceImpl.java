package com.shirohana.service.implementation.variety;

import com.shirohana.dto.variety.VarietyHarvestingResponseDto;
import com.shirohana.entity.plant.Plant;
import com.shirohana.entity.schedule.plantation_schedule.PlantationSchedule;
import com.shirohana.entity.task.setup.harvestingretension.HarvestingRetentionVariety;
import com.shirohana.repository.plant.PlantRepository;
import com.shirohana.repository.shedule.pschedule.PlantationScheduleRepository;
import com.shirohana.repository.task.setup.harvestingretension.HarvestingRetentionVarietyRepository;
import com.shirohana.repository.variety.VarietyRepository;
import com.shirohana.dto.variety.VarietyRequestDto;
import com.shirohana.dto.variety.VarietyResponseDto;
import com.shirohana.entity.variety.Variety;
import com.shirohana.exception.ResourceNotFoundException;
import com.shirohana.service.interfaces.variety.VarietyService;
import jakarta.transaction.Transactional;
import jakarta.validation.ValidationException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
@RequiredArgsConstructor
@Slf4j
public class VarietyServiceImpl implements VarietyService {
    private final VarietyRepository varietyRepository;
    private final HarvestingRetentionVarietyRepository harvestingRetentionVarietyRepository;
    private final PlantationScheduleRepository plantationScheduleRepository;

    private final PlantRepository plantRepository;

    @Transactional
    @Override
    public List<VarietyResponseDto> getVarietiesByPlantId(Long plantId) {
        try {
            Plant plant = plantRepository.findByIdAndDeletedFalse(plantId)
                    .orElseThrow(() -> new ResourceNotFoundException("Plant not found "));

            List<Variety> varieties = varietyRepository.findByFamilyNameIdAndDeletedFalse(plantId);
            return varieties.stream()
                    .map(this::convertToResponseDto)
                    .collect(Collectors.toList());
        } catch (ResourceNotFoundException ex) {
            log.error("Plant not found with id: {}", plantId, ex);
            throw ex;
        } catch (Exception ex) {
            log.error("Unexpected error while retrieving varieties for plant id: {}", plantId, ex);
            throw new RuntimeException("Error retrieving varieties", ex);
        }
    }

    @Transactional
    @Override
    public VarietyResponseDto createVariety(VarietyRequestDto varietyRequestDto) {
        try {
            String sanitizedVarietyName = sanitizeInput(varietyRequestDto.getVarietyName());
            Long familyNameId = varietyRequestDto.getFamilyNameId();

            if (varietyRepository.findByFamilyNameIdAndVarietyNameAndDeletedFalse(familyNameId, sanitizedVarietyName).isPresent()) {
                throw new ValidationException("Variety name already exists for this family ID.");
            }

            Variety variety = Variety.builder()
                    .varietyName(sanitizedVarietyName)
                    .varietyColor(sanitizeInput(varietyRequestDto.getVarietyColor()))
                    .commonColor(sanitizeInput(varietyRequestDto.getCommonColor()))
                    .familyNameId(familyNameId)
                    .build();

            variety.setCreatedBy(getCurrentUsername());
            variety.setCreatedDate(LocalDateTime.now());

            Variety savedVariety = varietyRepository.save(variety);
            return convertToResponseDto(savedVariety);
        } catch (ValidationException ex) {
            log.error("Validation error while creating variety: {}", varietyRequestDto, ex);
            throw ex;
        } catch (Exception ex) {
            log.error("Unexpected error while creating variety: {}", varietyRequestDto, ex);
            throw new RuntimeException("Error creating variety", ex);
        }
    }

    @Transactional
    @Override
    public List<VarietyHarvestingResponseDto> getVarietiesByFamilyIdWithHarvesting(Long familyId) {
        try {
            // Fetch the plant by familyId to ensure it exists
            Plant plant = plantRepository.findByIdAndDeletedFalse(familyId)
                    .orElseThrow(() -> new ResourceNotFoundException("Plant not found "));

            // Fetch varieties by familyId
            List<Variety> varieties = varietyRepository.findByFamilyNameIdAndDeletedFalse(familyId);

            // Map each variety to a VarietyHarvestingResponseDto, including harvesting durations if available
            return varieties.stream()
                    .map(variety -> {
                        // Fetch the HarvestingRetentionVariety record if it exists
                        Optional<HarvestingRetentionVariety> harvestingRetentionVarietyOpt =
                                harvestingRetentionVarietyRepository.findByFamilyNameIdAndVarietyIdAndDeletedFalse(familyId, variety.getId());

                        // Only return varieties that have a matching HarvestingRetentionVariety
                        if (harvestingRetentionVarietyOpt.isPresent()) {
                            HarvestingRetentionVariety harvestingRetentionVariety = harvestingRetentionVarietyOpt.get();

                            // Create the response DTO
                            return VarietyHarvestingResponseDto.builder()
                                    .id(variety.getId())
                                    .varietyName(variety.getVarietyName())
                                    .varietyColor(variety.getVarietyColor())
                                    .commonColor(variety.getCommonColor())
                                    .familyId(variety.getFamilyNameId())
                                    .familyName(plant.getFamilyName())
                                    .deleted(variety.isDeleted())
                                    .createdBy(variety.getCreatedBy())
                                    .createdAt(variety.getCreatedDate())
                                    .updatedBy(variety.getLastModifiedBy())
                                    .updatedAt(variety.getLastModifiedDate())
                                    .deletedAt(variety.getDeletedAt())
                                    .deletedBy(variety.getDeletedBy())
                                    .prodPlantHarvestDuration(harvestingRetentionVariety.getProdPlantHarvestDuration())
                                    .mothPlantHarvestingDuration(harvestingRetentionVariety.getMothPlantHarvestingDuration())
                                    .nurseryDuration(harvestingRetentionVariety.getNurseryDuration())
                                    .build();
                        } else {
                            return null; // Exclude varieties without harvesting durations
                        }
                    })
                    .filter(varietyDto -> varietyDto != null) // Filter out null values
                    .collect(Collectors.toList());

        } catch (ResourceNotFoundException ex) {
            log.error("Plant not found with id: {}", familyId, ex);
            throw ex;
        } catch (Exception ex) {
            log.error("Unexpected error while retrieving varieties by family id: {}", familyId, ex);
            throw new RuntimeException(ex);
        }
    }

    @Transactional
    @Override
    public VarietyResponseDto updateVariety(Long id, VarietyRequestDto varietyRequestDto) {
        try {
            // Fetch the existing variety
            Variety existingVariety = varietyRepository.findByIdAndDeletedFalse(id)
                    .orElseThrow(() -> new ResourceNotFoundException("Variety not found  "));

            String sanitizedVarietyName = sanitizeInput(varietyRequestDto.getVarietyName());
            Long familyNameId = varietyRequestDto.getFamilyNameId();

            // Check if the variety is associated with any active PlantationSchedule
            boolean associatedWithPlantationSchedule = plantationScheduleRepository.existsByFamilyIdAndDeletedFalse(existingVariety.getFamilyNameId());
            if (associatedWithPlantationSchedule) {
                throw new ValidationException("Cannot update variety with id " + id + " because it is associated with an active plantation schedule.");
            }

            // Check if the variety is associated with any active HarvestingRetentionVariety
            boolean associatedWithHarvestingRetention = harvestingRetentionVarietyRepository.existsByVarietyIdAndDeletedFalse(id);
            if (associatedWithHarvestingRetention) {
                throw new ValidationException("Cannot delete variety with id " + id + " because it is associated with an active HarvestingRetentionVariety.");
            }

            // Proceed with the update logic
            existingVariety.setVarietyName(sanitizedVarietyName);
            existingVariety.setVarietyColor(sanitizeInput(varietyRequestDto.getVarietyColor()));
            existingVariety.setCommonColor(sanitizeInput(varietyRequestDto.getCommonColor()));
            existingVariety.setFamilyNameId(familyNameId);

            existingVariety.setLastModifiedBy(getCurrentUsername());
            existingVariety.setLastModifiedDate(LocalDateTime.now());

            // Save the updated variety
            Variety updatedVariety = varietyRepository.save(existingVariety);
            return convertToResponseDto(updatedVariety);
        } catch (ResourceNotFoundException ex) {
            log.error("Variety not found with id: {}", id, ex);
            throw ex;
        } catch (ValidationException ex) {
            log.error("Validation error while updating variety with id {}: {}", id, varietyRequestDto, ex);
            throw ex;
        } catch (Exception ex) {
            log.error("Unexpected error while updating variety with id {}: {}", id, varietyRequestDto, ex);
            throw new RuntimeException("Error updating variety", ex);
        }
    }


    @Transactional
    @Override
    public void deleteVariety(Long id) {
        try {
            // Fetch the variety by ID
            Variety existingVariety = varietyRepository.findByIdAndDeletedFalse(id)
                    .orElseThrow(() -> new ResourceNotFoundException("Variety not found "));

            // Check if the variety is associated with any active PlantationSchedule
            boolean associatedWithPlantationSchedule = plantationScheduleRepository.existsByFamilyIdAndDeletedFalse(existingVariety.getFamilyNameId());
            if (associatedWithPlantationSchedule) {
                throw new ValidationException("Cannot delete variety with id " + id + " because it is associated with an active plantation schedule.");
            }

            // Check if the variety is associated with any active HarvestingRetentionVariety
            boolean associatedWithHarvestingRetention = harvestingRetentionVarietyRepository.existsByVarietyIdAndDeletedFalse(id);
            if (associatedWithHarvestingRetention) {
                throw new ValidationException("Cannot delete variety with id " + id + " because it is associated with an active HarvestingRetentionVariety.");
            }
            // Perform the soft delete
            existingVariety.setDeleted(true);
            existingVariety.setDeletedAt(LocalDateTime.now());
            existingVariety.setDeletedBy(getCurrentUsername());

            // Save the changes
            varietyRepository.save(existingVariety);

        } catch (ResourceNotFoundException ex) {
            log.error("Variety not found with id: {}", id, ex);
            throw ex;
        } catch (ValidationException ex) {
            log.error("Validation error while deleting variety with id: {}", id, ex);
            throw ex;
        } catch (Exception ex) {
            log.error("Unexpected error while deleting variety with id: {}", id, ex);
            throw new RuntimeException("Error deleting variety", ex);
        }
    }

    @Override
    public List<VarietyResponseDto> getVarietyByFamilyNameId(Long familyNameId) {
        try {
            List<Variety> varieties = varietyRepository.findByFamilyNameIdAndDeletedFalse(familyNameId);
            return varieties.stream()
                    .map(this::convertToResponseDto)
                    .collect(Collectors.toList());
        } catch (ResourceNotFoundException ex) {
            log.error("Variety not found with family ID {}", familyNameId, ex);
            throw ex;
        } catch (Exception ex) {
            log.error("Unexpected error while retrieving variety with family ID {}", familyNameId, ex);
            throw new RuntimeException("Error retrieving variety", ex);
        }
    }

    @Override
    public VarietyResponseDto getVarietyById(Long id) {
        try {
            Variety variety = varietyRepository.findByIdAndDeletedFalse(id)
                    .orElseThrow(() -> new ResourceNotFoundException("Variety not found "));
            return convertToResponseDto(variety);
        } catch (ResourceNotFoundException ex) {
            log.error("Variety not found with id: {}", id, ex);
            throw ex;
        } catch (Exception ex) {
            log.error("Unexpected error while retrieving variety with id: {}", id, ex);
            throw new RuntimeException("Error retrieving variety", ex);
        }
    }

    @Override
    public List<VarietyResponseDto> getAllVarieties() {
        try {
            List<Variety> varieties = varietyRepository.findAllByDeletedFalse();
            return varieties.stream()
                    .map(this::convertToResponseDto)
                    .collect(Collectors.toList());
        } catch (Exception ex) {
            log.error("Unexpected error while retrieving all varieties", ex);
            throw new RuntimeException("Error retrieving varieties", ex);
        }
    }

    private VarietyResponseDto convertToResponseDto(Variety variety) {
        Plant existingPlant = plantRepository.findByIdAndDeletedFalse(variety.getFamilyNameId())
                .orElseThrow(() -> new ResourceNotFoundException("Plant not found  "));
        return VarietyResponseDto.builder()
                .id(variety.getId())
                .varietyName(variety.getVarietyName())
                .varietyColor(variety.getVarietyColor())
                .commonColor(variety.getCommonColor())
                .familyId(variety.getFamilyNameId())
                .familyName(existingPlant.getFamilyName())
                .deleted(variety.isDeleted())
                .createdBy(variety.getCreatedBy())
                .createdAt(variety.getCreatedDate())
                .updatedBy(variety.getLastModifiedBy())
                .updatedAt(variety.getLastModifiedDate())
                .deletedAt(variety.getDeletedAt())
                .deletedBy(variety.getDeletedBy())
                .build();
    }

    private String getCurrentUsername() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication.getPrincipal() instanceof UserDetails) {
            return ((UserDetails) authentication.getPrincipal()).getUsername();
        }
        return null;
    }

    private String sanitizeInput(String input) {
        if (input == null) {
            throw new ValidationException("Variety Name cannot be null.");
        }
        return input;
    }
}
