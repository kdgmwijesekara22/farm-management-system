package com.shirohana.service.implementation.plant;


import com.shirohana.entity.schedule.plantation_schedule.PlantationSchedule;
import com.shirohana.repository.plant.PlantRepository;
import com.shirohana.dto.plant.PlantRequestDto;
import com.shirohana.dto.plant.PlantResponseDto;
import com.shirohana.entity.plant.Plant;
import com.shirohana.exception.ResourceNotFoundException;
import com.shirohana.repository.shedule.pschedule.PlantationScheduleRepository;
import com.shirohana.repository.task.setup.harvestingretension.HarvestingAndRetentionSetupRepository;
import com.shirohana.repository.variety.VarietyRepository;
import com.shirohana.service.interfaces.plant.PlantService;
import jakarta.validation.ValidationException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class PlantServiceImpl implements PlantService {
    private final PlantRepository plantRepository;
    private final HarvestingAndRetentionSetupRepository harvestingAndRetentionSetupRepository;
    private final VarietyRepository varietyRepository;
    private final PlantationScheduleRepository plantationScheduleRepository;

    @Override
    public PlantResponseDto createPlant(PlantRequestDto plantRequestDto) {
        try {
            String sanitizedFamilyName = sanitizeInput(plantRequestDto.getFamilyName());

            if (plantRepository.findByFamilyNameAndDeletedFalse(sanitizedFamilyName).isPresent()) {
                throw new ValidationException("Family name already exists.");
            }

            Plant plant = Plant.builder()
                    .familyName(sanitizedFamilyName)
                    .scientificName(sanitizeInput(plantRequestDto.getScientificName()))
                    .commonName(sanitizeInput(plantRequestDto.getCommonName()))
                    .reproduced(plantRequestDto.isReproduced())
                    .recoveryTime(plantRequestDto.getRecoveryTime())
                    .build();

            plant.setCreatedBy(getCurrentUsername());
            plant.setCreatedDate(LocalDateTime.now());

            Plant savedPlant = plantRepository.save(plant);
            return convertToResponseDto(savedPlant);
        } catch (ValidationException ex) {
            log.error("Validation exception while creating plant: {}", ex.getMessage());
            throw ex;
        } catch (Exception ex) {
            log.error("Unexpected error while creating plant", ex);
            throw new RuntimeException(ex.getMessage());
        }
    }

    @Override
    public PlantResponseDto updatePlant(Long id, PlantRequestDto plantRequestDto) {
        try {
            // Fetch the existing plant to update
            Plant existingPlant = plantRepository.findByIdAndDeletedFalse(id)
                    .orElseThrow(() -> new ResourceNotFoundException("Plant not found with id " + id));

            String sanitizedFamilyName = sanitizeInput(plantRequestDto.getFamilyName());

            // Validate if family name already exists for another plant
            Optional<Plant> existingPlantByFamilyName = plantRepository.findByFamilyNameAndDeletedFalse(sanitizedFamilyName);
            if (existingPlantByFamilyName.isPresent() && !existingPlantByFamilyName.get().getId().equals(id)) {
                throw new ValidationException("Family name already exists.");
            }

            // Check if the plant is associated with any Harvesting and Retention setup
            boolean harvestingAndRetention = harvestingAndRetentionSetupRepository.existsByFamilyNameIdAndDeletedFalse(id);
            if (harvestingAndRetention) {
                throw new ValidationException("Cannot update plant with id " + id + " because it is associated with an existing harvesting and retention setup.");
            }

            // Check if the plant is associated with any Variety
            boolean associatedWithVariety = varietyRepository.existsByFamilyNameIdAndDeletedFalse(id);
            if (associatedWithVariety) {
                throw new ValidationException("Cannot update plant with id " + id + " because it is associated with existing varieties.");
            }

            // Check if the plant is associated with any Plantation Schedule
            boolean associatedWithPlantationSchedule = plantationScheduleRepository.existsByFamilyIdAndDeletedFalse(id);
            if (associatedWithPlantationSchedule) {
                throw new ValidationException("Cannot update plant with id " + id + " because it is associated with an existing plantation schedule.");
            }

            // Proceed with updating the plant
            existingPlant.setFamilyName(sanitizedFamilyName);
            existingPlant.setScientificName(sanitizeInput(plantRequestDto.getScientificName()));
            existingPlant.setCommonName(sanitizeInput(plantRequestDto.getCommonName()));
            existingPlant.setReproduced(plantRequestDto.isReproduced());
            existingPlant.setRecoveryTime(plantRequestDto.getRecoveryTime());
            existingPlant.setLastModifiedDate(LocalDateTime.now());
            existingPlant.setLastModifiedBy(getCurrentUsername());

            Plant updatedPlant = plantRepository.save(existingPlant);
            return convertToResponseDto(updatedPlant);
        } catch (ResourceNotFoundException ex) {
            log.error("Resource not found while updating plant: {}", ex.getMessage());
            throw ex;
        } catch (ValidationException ex) {
            log.error("Validation exception while updating plant: {}", ex.getMessage());
            throw ex;
        } catch (Exception ex) {
            log.error("Unexpected error while updating plant", ex);
            throw new RuntimeException(ex.getMessage());
        }
    }


    @Override
    public void deletePlant(Long id) {
        try {
            // Fetch the plant by its ID
            Plant existingPlant = plantRepository.findByIdAndDeletedFalse(id)
                    .orElseThrow(() -> new ResourceNotFoundException("Plant not found with id " + id));

            // Check if the plant is associated with any harvesting and retention setup
            boolean harvestingAndRetention = harvestingAndRetentionSetupRepository.existsByFamilyNameIdAndDeletedFalse(id);
            if (harvestingAndRetention) {
                throw new ValidationException("Cannot delete plant with id " + id + " because it is associated with an existing harvesting and retention setup.");
            }

            // Check if the plant is associated with any variety
            boolean associatedWithVariety = varietyRepository.existsByFamilyNameIdAndDeletedFalse(id);
            if (associatedWithVariety) {
                throw new ValidationException("Cannot delete plant with id " + id + " because it is associated with existing varieties.");
            }

            // Check if the plant is associated with any plantation schedule
            boolean associatedWithPlantationSchedule = plantationScheduleRepository.existsByFamilyIdAndDeletedFalse(id);
            if (associatedWithPlantationSchedule) {
                throw new ValidationException("Cannot delete plant with id " + id + " because it is associated with an existing plantation schedule.");
            }

            // Perform the soft delete
            existingPlant.setDeleted(true);
            existingPlant.setDeletedAt(LocalDateTime.now());
            existingPlant.setDeletedBy(getCurrentUsername());

            // Save the changes
            plantRepository.save(existingPlant);

        } catch (ResourceNotFoundException ex) {
            log.error("Resource not found while deleting plant: {}", ex.getMessage());
            throw ex;
        } catch (ValidationException ex) {
            log.error("Validation exception while deleting plant: {}", ex.getMessage());
            throw ex;
        } catch (Exception ex) {
            log.error("Unexpected error while deleting plant", ex);
            throw new RuntimeException(ex.getMessage());
        }
    }



    @Override
    public PlantResponseDto getPlantByFamilyName(String familyName) {
        try {
            Plant plant = plantRepository.findByFamilyNameAndDeletedFalse(sanitizeInput(familyName))
                    .orElseThrow(() -> new ResourceNotFoundException("Plant not found with family name " + familyName));
            return convertToResponseDto(plant);
        } catch (ResourceNotFoundException ex) {
            log.error("Resource not found while retrieving plant by family name: {}", ex.getMessage());
            throw ex;
        } catch (Exception ex) {
            log.error("Unexpected error while retrieving plant by family name", ex);
            throw new RuntimeException( ex);
        }
    }

    @Override
    public List<PlantResponseDto> getAllPlants() {
        try {
            List<Plant> plants = plantRepository.findAllByDeletedFalse();
            return plants.stream()
                    .map(this::convertToResponseDto)
                    .collect(Collectors.toList());
        } catch (Exception ex) {
            log.error("Unexpected error while retrieving all plants", ex);
            throw new RuntimeException("An unexpected error occurred while retrieving the plants.", ex);
        }
    }

    private PlantResponseDto convertToResponseDto(Plant plant) {
        return PlantResponseDto.builder()
                .id(plant.getId())
                .familyName(plant.getFamilyName())
                .scientificName(plant.getScientificName())
                .commonName(plant.getCommonName())
                .reproduced(plant.isReproduced())
                .recoveryTime(plant.getRecoveryTime())
                .deleted(plant.isDeleted())
                .createdBy(plant.getCreatedBy())
                .createdAt(plant.getCreatedDate())
                .updatedBy(plant.getLastModifiedBy())
                .updatedAt(plant.getLastModifiedDate())
                .deletedAt(plant.getDeletedAt())
                .deletedBy(plant.getDeletedBy())
                .build();
    }

    private String getCurrentUsername() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication.getPrincipal() instanceof UserDetails) {
            return ((UserDetails) authentication.getPrincipal()).getUsername();
        }
        return null;
    }

    private String sanitizeInput(String input) {
        if (input == null) {
            return "";
        }
        return input;
    }
}
