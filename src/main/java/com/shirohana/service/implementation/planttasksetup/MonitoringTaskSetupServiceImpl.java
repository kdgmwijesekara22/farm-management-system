package com.shirohana.service.implementation.planttasksetup;



import com.shirohana.constants.TaskTypeConstants;
import com.shirohana.dto.planttasksetup.monitoringtasksetup.requestdtos.create.MonitoringTaskSetupCreateRequestDto;
import com.shirohana.dto.planttasksetup.monitoringtasksetup.MonitoringTaskSetupResponseDto;

import com.shirohana.dto.planttasksetup.monitoringtasksetup.SubTaskResponseDto;
import com.shirohana.dto.planttasksetup.monitoringtasksetup.requestdtos.update.MonitoringTaskSetupUpdateRequestDto;
import com.shirohana.dto.planttasksetup.monitoringtasksetup.requestdtos.update.SubTaskWithIdRequestDto;
import com.shirohana.entity.planttasksetup.monitoringtasksetup.MonitoringTSNursery;
import com.shirohana.entity.planttasksetup.monitoringtasksetup.MonitoringTSProd;
import com.shirohana.entity.planttasksetup.monitoringtasksetup.MonitoringTSSubTask;
import com.shirohana.entity.planttasksetup.monitoringtasksetup.MonitoringTaskSetup;
import com.shirohana.entity.taskcategorysetup.TaskCategorySetup;
import com.shirohana.exception.ResourceNotFoundException;
import com.shirohana.repository.planttasksetup.monitoringtasksetup.MonitoringTSNurseryRepository;
import com.shirohana.repository.planttasksetup.monitoringtasksetup.MonitoringTSProdRepository;
import com.shirohana.repository.planttasksetup.monitoringtasksetup.MonitoringTSSubTaskRepository;
import com.shirohana.repository.planttasksetup.monitoringtasksetup.MonitoringTaskSetupRepository;
import com.shirohana.repository.taskcategorysetup.TaskCategorySetupRepository;
import com.shirohana.service.interfaces.planttasksetup.MonitoringTaskSetupService;
import jakarta.persistence.EntityNotFoundException;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class MonitoringTaskSetupServiceImpl implements MonitoringTaskSetupService {

    private final MonitoringTaskSetupRepository monitoringTaskSetupRepository;

    private final MonitoringTSProdRepository monitoringTSProdRepository;

    private final MonitoringTSNurseryRepository monitoringTSNurseryRepository;

    private final MonitoringTSSubTaskRepository monitoringTSSubTaskRepository;

    private final TaskCategorySetupRepository taskCategorySetupRepository;

    @Transactional
    @Override
    public MonitoringTaskSetupResponseDto createTask(MonitoringTaskSetupCreateRequestDto requestDto) {
        try {
            validateTaskNameUniqueness(requestDto.getFamilyNameId(), requestDto.getTaskNameId());

            MonitoringTaskSetup taskSetup = mapToEntity(requestDto);
            taskSetup.setDeleted(false);

            if (requestDto.isProduction()) {
                MonitoringTSProd production = createProduction(requestDto);
                taskSetup.setProductionId(production.getId());
            }

            if (requestDto.isNursery()) {
                MonitoringTSNursery nursery = createNursery(requestDto);
                taskSetup.setNurseryId(nursery.getId());
            }

            taskSetup = monitoringTaskSetupRepository.save(taskSetup);
            return mapToDto(taskSetup);
        } catch (DataIntegrityViolationException e) {
            log.error("Data integrity violation: {}", e.getMessage(), e);
            throw new RuntimeException("Data integrity violation: " + e.getMessage(), e);
        } catch (EntityNotFoundException e) {
            log.error("Entity not found: {}", e.getMessage(), e);
            throw new RuntimeException("Entity not found: " + e.getMessage(), e);
        } catch (RuntimeException e) {
            log.error("Validation error: {}", e.getMessage(), e);
            throw e;
        } catch (Exception e) {
            log.error("Unexpected error creating task: {}", e.getMessage(), e);
            throw new RuntimeException("Unexpected error creating task", e);
        }
    }

    @Transactional
    @Override
    public void deleteTask(Long id) {
        try {
            MonitoringTaskSetup taskSetup = monitoringTaskSetupRepository.findById(id)
                    .orElseThrow(() -> new RuntimeException("Task not found"));

            // Soft delete the main task setup
            taskSetup.setDeleted(true);
            taskSetup.setDeletedAt(LocalDateTime.now());
            taskSetup.setDeletedBy(getCurrentUsername());

            // Soft delete the related production entity
            if (taskSetup.getProductionId() != null) {
                MonitoringTSProd production = monitoringTSProdRepository.findById(taskSetup.getProductionId())
                        .orElseThrow(() -> new RuntimeException("Production entity not found"));
                production.setDeleted(true);
                production.setDeletedAt(LocalDateTime.now());
                production.setDeletedBy(getCurrentUsername());
                monitoringTSProdRepository.save(production);

                // Soft delete the related sub-tasks for production
                List<MonitoringTSSubTask> prodSubTasks = monitoringTSSubTaskRepository.findByProductionId(production.getId());
                for (MonitoringTSSubTask subTask : prodSubTasks) {
                    subTask.setDeleted(true);
                    subTask.setDeletedAt(LocalDateTime.now());
                    subTask.setDeletedBy(getCurrentUsername());
                    monitoringTSSubTaskRepository.save(subTask);
                }
            }

            // Soft delete the related nursery entity
            if (taskSetup.getNurseryId() != null) {
                MonitoringTSNursery nursery = monitoringTSNurseryRepository.findById(taskSetup.getNurseryId())
                        .orElseThrow(() -> new RuntimeException("Nursery entity not found"));
                nursery.setDeleted(true);
                nursery.setDeletedAt(LocalDateTime.now());
                nursery.setDeletedBy(getCurrentUsername());
                monitoringTSNurseryRepository.save(nursery);

                // Soft delete the related sub-tasks for nursery
                List<MonitoringTSSubTask> nurserySubTasks = monitoringTSSubTaskRepository.findByNurseryId(nursery.getId());
                for (MonitoringTSSubTask subTask : nurserySubTasks) {
                    subTask.setDeleted(true);
                    subTask.setDeletedAt(LocalDateTime.now());
                    subTask.setDeletedBy(getCurrentUsername());
                    monitoringTSSubTaskRepository.save(subTask);
                }
            }

            // Save the main task setup after updating related entities
            monitoringTaskSetupRepository.save(taskSetup);
        } catch (ResourceNotFoundException e) {
            log.error("Resource not found: {}", e.getMessage(), e);
            throw e;  // Re-throw the exception to be caught by the controller
        } catch (Exception e) {
            log.error("Error deleting task: {}", e.getMessage(), e);
            throw new RuntimeException("Error deleting task", e);
        }
    }

    @Transactional
    @Override
    public MonitoringTaskSetupResponseDto getTaskById(Long id) {
        try {
            MonitoringTaskSetup taskSetup = monitoringTaskSetupRepository.findById(id)
                    .orElseThrow(() -> new ResourceNotFoundException("Task not found with id " + id));
            return mapToDto(taskSetup);
        } catch (ResourceNotFoundException e) {
            log.error("Task not found: {}", e.getMessage(), e);
            throw e;  // Re-throw the exception to be caught by the controller
        } catch (Exception e) {
            log.error("Error retrieving task: {}", e.getMessage(), e);
            throw new RuntimeException("Error retrieving task", e);
        }
    }

    @Transactional
    @Override
    public List<MonitoringTaskSetupResponseDto> getAllTasks() {
        try {
            return monitoringTaskSetupRepository.findByDeletedFalse().stream()
                    .map(this::mapToDto)
                    .collect(Collectors.toList());
        } catch (Exception e) {
            log.error("Error retrieving all tasks: {}", e.getMessage(), e);
            throw new RuntimeException("Error retrieving all tasks", e);
        }
    }

    @Override
    public List<MonitoringTaskSetupResponseDto> getMonitoringTaskSetupByFamilyNameId(Long familyNameId) {
        List<MonitoringTaskSetup> setups = monitoringTaskSetupRepository.findByFamilyNameIdAndDeletedFalse(familyNameId);
        if (setups.isEmpty()) {
            throw new ResourceNotFoundException("No Monitoring Task Setup found for familyNameId: " + familyNameId);
        }
        return setups.stream().map(this::mapToDto).collect(Collectors.toList());
    }

    private void validateTaskNameUniqueness(Long familyNameId, Long taskNameId) {
        Optional<MonitoringTaskSetup> existingTask = monitoringTaskSetupRepository.findByFamilyNameIdAndTaskNameIdAndDeletedFalse(familyNameId, taskNameId);
        if (existingTask.isPresent()) {
            throw new RuntimeException("Task name already exists");
        }
    }

    private void validateTaskNameUniqueness(Long familyNameId, Long taskNameId, Long taskId) {
        Optional<MonitoringTaskSetup> existingTask = monitoringTaskSetupRepository.findByFamilyNameIdAndTaskNameIdAndDeletedFalse(familyNameId, taskNameId);
        if (existingTask.isPresent() && !existingTask.get().getId().equals(taskId)) {
            throw new RuntimeException("Task name must be unique for the same family name ID");
        }
    }

    private MonitoringTSProd createProduction(MonitoringTaskSetupCreateRequestDto requestDto) {
        try {
            MonitoringTSProd production = MonitoringTSProd.builder()
                    .noOfTerms(requestDto.getProdNoOfTerms())
                    .timeUnit(requestDto.getProdTimeUnit())
                    .monitoring(requestDto.isProdMonitoring())
                    .stdValueMin(requestDto.getProdStdValueMin())
                    .stdValueMax(requestDto.getProdStdValueMax())
                    .unit(requestDto.getProdUnit())
                    .uptoEffectiveStatus(requestDto.isProdUptoEffectiveStatus())
                    .uptoEffectiveWeek(requestDto.getProdUptoEffectiveWeek())
                    .deleted(false)
                    .build();

            production.setCreatedBy(getCurrentUsername());
            production.setCreatedDate(LocalDateTime.now());

            MonitoringTSProd savedProduction = monitoringTSProdRepository.save(production);

            requestDto.getProdSubTasks().forEach(subTaskDto -> {
                MonitoringTSSubTask subTask = MonitoringTSSubTask.builder()
                        .subTaskName(subTaskDto.getSubTaskName())
                        .description(subTaskDto.getDescription())
                        .productionId(savedProduction.getId())
                        .deleted(false)
                        .build();

                subTask.setCreatedBy(getCurrentUsername());
                subTask.setCreatedDate(LocalDateTime.now());
                monitoringTSSubTaskRepository.save(subTask);
            });

            return savedProduction;
        } catch (Exception e) {
            log.error("Error creating production: {}", e.getMessage(), e);
            throw new RuntimeException("Error creating production", e);
        }
    }

    private MonitoringTSProd createProduction(MonitoringTaskSetupUpdateRequestDto requestDto) {
        try {
            MonitoringTSProd production = MonitoringTSProd.builder()
                    .noOfTerms(requestDto.getProdNoOfTerms())
                    .timeUnit(requestDto.getProdTimeUnit())
                    .monitoring(requestDto.isProdMonitoring())
                    .stdValueMin(requestDto.getProdStdValueMin())
                    .stdValueMax(requestDto.getProdStdValueMax())
                    .unit(requestDto.getProdUnit())
                    .uptoEffectiveStatus(requestDto.isProdUptoEffectiveStatus())
                    .uptoEffectiveWeek(requestDto.getProdUptoEffectiveWeek())
                    .deleted(false)
                    .build();

            production.setCreatedBy(getCurrentUsername());
            production.setCreatedDate(LocalDateTime.now());

            MonitoringTSProd savedProduction = monitoringTSProdRepository.save(production);

            requestDto.getProdSubTasks().forEach(subTaskDto -> {
                MonitoringTSSubTask subTask = MonitoringTSSubTask.builder()
                        .subTaskName(subTaskDto.getSubTaskName())
                        .description(subTaskDto.getDescription())
                        .productionId(savedProduction.getId())
                        .deleted(false)
                        .build();

                subTask.setCreatedBy(getCurrentUsername());
                subTask.setCreatedDate(LocalDateTime.now());
                monitoringTSSubTaskRepository.save(subTask);
            });

            return savedProduction;
        } catch (Exception e) {
            log.error("Error creating production: {}", e.getMessage(), e);
            throw new RuntimeException("Error creating production", e);
        }
    }

    private MonitoringTSNursery createNursery(MonitoringTaskSetupCreateRequestDto requestDto) {
        try {
            MonitoringTSNursery nursery = MonitoringTSNursery.builder()
                    .noOfTerms(requestDto.getNurseryNoOfTerms())
                    .timeUnit(requestDto.getNurseryTimeUnit())
                    .monitoring(requestDto.isNurseryMonitoring())
                    .stdValueMin(requestDto.getNurseryStdValueMin())
                    .stdValueMax(requestDto.getNurseryStdValueMax())
                    .unit(requestDto.getNurseryUnit())
                    .uptoEffectiveStatus(requestDto.isNurseryUptoEffectiveStatus())
                    .uptoEffectiveWeek(requestDto.getNurseryUptoEffectiveWeek())
                    .deleted(false)
                    .build();

            nursery.setCreatedBy(getCurrentUsername());
            nursery.setCreatedDate(LocalDateTime.now());

            MonitoringTSNursery savedNursery = monitoringTSNurseryRepository.save(nursery);

            requestDto.getNurserySubTasks().forEach(subTaskDto -> {
                MonitoringTSSubTask subTask = MonitoringTSSubTask.builder()
                        .subTaskName(subTaskDto.getSubTaskName())
                        .description(subTaskDto.getDescription())
                        .nurseryId(savedNursery.getId())
                        .deleted(false)
                        .build();

                subTask.setCreatedBy(getCurrentUsername());
                subTask.setCreatedDate(LocalDateTime.now());
                monitoringTSSubTaskRepository.save(subTask);
            });

            return savedNursery;
        } catch (Exception e) {
            log.error("Error creating nursery: {}", e.getMessage(), e);
            throw new RuntimeException("Error creating nursery", e);
        }
    }

    private MonitoringTSNursery createNursery(MonitoringTaskSetupUpdateRequestDto requestDto) {
        try {
            MonitoringTSNursery nursery = MonitoringTSNursery.builder()
                    .noOfTerms(requestDto.getNurseryNoOfTerms())
                    .timeUnit(requestDto.getNurseryTimeUnit())
                    .monitoring(requestDto.isNurseryMonitoring())
                    .stdValueMin(requestDto.getNurseryStdValueMin())
                    .stdValueMax(requestDto.getNurseryStdValueMax())
                    .unit(requestDto.getNurseryUnit())
                    .uptoEffectiveStatus(requestDto.isNurseryUptoEffectiveStatus())
                    .uptoEffectiveWeek(requestDto.getNurseryUptoEffectiveWeek())
                    .deleted(false)
                    .build();

            nursery.setCreatedBy(getCurrentUsername());
            nursery.setCreatedDate(LocalDateTime.now());

            MonitoringTSNursery savedNursery = monitoringTSNurseryRepository.save(nursery);

            requestDto.getNurserySubTasks().forEach(subTaskDto -> {
                MonitoringTSSubTask subTask = MonitoringTSSubTask.builder()
                        .subTaskName(subTaskDto.getSubTaskName())
                        .description(subTaskDto.getDescription())
                        .nurseryId(savedNursery.getId())
                        .deleted(false)
                        .build();

                subTask.setCreatedBy(getCurrentUsername());
                subTask.setCreatedDate(LocalDateTime.now());
                monitoringTSSubTaskRepository.save(subTask);
            });

            return savedNursery;
        } catch (Exception e) {
            log.error("Error creating nursery: {}", e.getMessage(), e);
            throw new RuntimeException("Error creating nursery", e);
        }
    }
    private MonitoringTaskSetup mapToEntity(MonitoringTaskSetupCreateRequestDto requestDto) {
        return MonitoringTaskSetup.builder()
                .taskNameId(requestDto.getTaskNameId())
                .familyNameId(requestDto.getFamilyNameId())
                .deleted(false)
                .build();
    }

    private void mapToEntity(MonitoringTaskSetup taskSetup, MonitoringTaskSetupUpdateRequestDto requestDto) {
        if (requestDto.getTaskNameId() != null) {
            taskSetup.setTaskNameId(requestDto.getTaskNameId());
        }
        if (requestDto.getFamilyNameId() != null) {
            taskSetup.setFamilyNameId(requestDto.getFamilyNameId());
        }
    }

    private MonitoringTaskSetupResponseDto mapToDto(MonitoringTaskSetup taskSetup) {

        Long taskId = taskSetup.getTaskNameId();
        TaskCategorySetup taskCategorySetup = taskCategorySetupRepository.findByIdAndDeletedFalse(taskId)
                .orElseThrow(() -> new RuntimeException("Task category setup not found"));
        MonitoringTaskSetupResponseDto responseDto = MonitoringTaskSetupResponseDto.builder()
                .id(taskSetup.getId())
                .taskName(taskCategorySetup.getTaskName())
                .taskDescription(taskCategorySetup.getTaskDescription())
                .familyNameId(taskSetup.getFamilyNameId())
                .taskType(TaskTypeConstants.MONITORING)
                .production(taskSetup.getProductionId() != null)
                .prodPlanId(taskSetup.getProdPlanId())
                .nursery(taskSetup.getNurseryId() != null)
                .createdBy(taskSetup.getCreatedBy())
                .createdDate(taskSetup.getCreatedDate())
                .lastModifiedBy(taskSetup.getLastModifiedBy())
                .lastModifiedDate(taskSetup.getLastModifiedDate())
                .deletedAt(taskSetup.getDeletedAt())
                .deletedBy(taskSetup.getDeletedBy())
                .build();

        if (taskSetup.getProductionId() != null) {
            MonitoringTSProd production = monitoringTSProdRepository.findById(taskSetup.getProductionId())
                    .orElseThrow(() -> new RuntimeException("Production details not found"));
            responseDto.setProdNoOfTerms(production.getNoOfTerms());
            responseDto.setProdTimeUnit(production.getTimeUnit());
            responseDto.setProdMonitoring(production.isMonitoring());
            responseDto.setProdStdValueMin(production.getStdValueMin());
            responseDto.setProdStdValueMax(production.getStdValueMax());
            responseDto.setProdUnit(production.getUnit());
            responseDto.setProdUptoEffectiveStatus(production.isUptoEffectiveStatus());
            responseDto.setProdUptoEffectiveWeek(production.getUptoEffectiveWeek());
            List<SubTaskResponseDto> prodSubTasks = monitoringTSSubTaskRepository.findByProductionId(production.getId())
                    .stream()
                    .map(subTask -> SubTaskResponseDto.builder()
                            .id(subTask.getId())
                            .subTaskName(subTask.getSubTaskName())
                            .description(subTask.getDescription())
                            .createdBy(subTask.getCreatedBy())
                            .createdDate(subTask.getCreatedDate())
                            .lastModifiedBy(subTask.getLastModifiedBy())
                            .lastModifiedDate(subTask.getLastModifiedDate())
                            .deletedAt(subTask.getDeletedAt())
                            .deletedBy(subTask.getDeletedBy())
                            .build())
                    .collect(Collectors.toList());
            responseDto.setProdSubTasks(prodSubTasks);
        }

        if (taskSetup.getNurseryId() != null) {
            MonitoringTSNursery nursery = monitoringTSNurseryRepository.findById(taskSetup.getNurseryId())
                    .orElseThrow(() -> new RuntimeException("Nursery details not found"));
            responseDto.setNurseryNoOfTerms(nursery.getNoOfTerms());
            responseDto.setNurseryTimeUnit(nursery.getTimeUnit());
            responseDto.setNurseryMonitoring(nursery.isMonitoring());
            responseDto.setNurseryStdValueMin(nursery.getStdValueMin());
            responseDto.setNurseryStdValueMax(nursery.getStdValueMax());
            responseDto.setNurseryUnit(nursery.getUnit());
            responseDto.setNurseryUptoEffectiveStatus(nursery.isUptoEffectiveStatus());
            responseDto.setNurseryUptoEffectiveWeek(nursery.getUptoEffectiveWeek());
            List<SubTaskResponseDto> nurserySubTasks = monitoringTSSubTaskRepository.findByNurseryId(nursery.getId())
                    .stream()
                    .map(subTask -> SubTaskResponseDto.builder()
                            .id(subTask.getId())
                            .subTaskName(subTask.getSubTaskName())
                            .description(subTask.getDescription())
                            .createdBy(subTask.getCreatedBy())
                            .createdDate(subTask.getCreatedDate())
                            .lastModifiedBy(subTask.getLastModifiedBy())
                            .lastModifiedDate(subTask.getLastModifiedDate())
                            .deletedAt(subTask.getDeletedAt())
                            .deletedBy(subTask.getDeletedBy())
                            .build())
                    .collect(Collectors.toList());
            responseDto.setNurserySubTasks(nurserySubTasks);
        }

        return responseDto;
    }

    private String getCurrentUsername() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication.getPrincipal() instanceof UserDetails) {
            return ((UserDetails) authentication.getPrincipal()).getUsername();
        }
        return "Unknown";
    }

    @Transactional
    @Override
    public MonitoringTaskSetupResponseDto updateTask(Long id, MonitoringTaskSetupUpdateRequestDto requestDto) {
        try {
            validateTaskNameUniqueness(requestDto.getFamilyNameId(), requestDto.getTaskNameId(), id);

            MonitoringTaskSetup taskSetup = monitoringTaskSetupRepository.findById(id)
                    .orElseThrow(() -> new RuntimeException("Task not found"));

            mapToEntity(taskSetup, requestDto);

            // Handle Production subtasks
            if (requestDto.isProduction()) {
                handleProductionUpdate(taskSetup, requestDto);
            } else if (taskSetup.getProductionId() != null) {
                handleProductionDeletion(taskSetup);
            }

            // Handle Nursery subtasks
            if (requestDto.isNursery()) {
                handleNurseryUpdate(taskSetup, requestDto);
            } else if (taskSetup.getNurseryId() != null) {
                handleNurseryDeletion(taskSetup);
            }

            taskSetup = monitoringTaskSetupRepository.save(taskSetup);
            return mapToDto(taskSetup);
        } catch (DataIntegrityViolationException e) {
            log.error("Data integrity violation: {}", e.getMessage(), e);
            throw new RuntimeException("Data integrity violation: " + e.getMessage(), e);
        } catch (EntityNotFoundException e) {
            log.error("Entity not found: {}", e.getMessage(), e);
            throw new RuntimeException("Entity not found: " + e.getMessage(), e);
        } catch (ResourceNotFoundException e) {
            log.error("Resource not found: {}", e.getMessage(), e);
            throw e;  // Re-throw the exception to be caught by the controller
        } catch (RuntimeException e) {
            log.error("Validation error: {}", e.getMessage(), e);
            throw e;
        } catch (Exception e) {
            log.error("Unexpected error updating task: {}", e.getMessage(), e);
            throw new RuntimeException("Unexpected error updating task", e);
        }
    }

    private void handleProductionUpdate(MonitoringTaskSetup taskSetup, MonitoringTaskSetupUpdateRequestDto requestDto) {
        MonitoringTSProd production = null;
        if (taskSetup.getProductionId() == null) {
            production = createProduction(requestDto);
            taskSetup.setProductionId(production.getId());
        } else {
            production = monitoringTSProdRepository.findById(taskSetup.getProductionId())
                    .orElseThrow(() -> new RuntimeException("Production entity not found"));
            updateProduction(production, requestDto);
        }
        updateSubTasks(production.getId(), requestDto.getProdSubTasks(), true);
    }

    private void handleNurseryUpdate(MonitoringTaskSetup taskSetup, MonitoringTaskSetupUpdateRequestDto requestDto) {
        MonitoringTSNursery nursery = null;
        if (taskSetup.getNurseryId() == null) {
            nursery = createNursery(requestDto);
            taskSetup.setNurseryId(nursery.getId());
        } else {
            nursery = monitoringTSNurseryRepository.findById(taskSetup.getNurseryId())
                    .orElseThrow(() -> new RuntimeException("Nursery entity not found"));
            updateNursery(nursery, requestDto);
        }
        updateSubTasks(nursery.getId(), requestDto.getNurserySubTasks(), false);
    }

    private void updateProduction(MonitoringTSProd production, MonitoringTaskSetupUpdateRequestDto requestDto) {
        production.setNoOfTerms(requestDto.getProdNoOfTerms());
        production.setTimeUnit(requestDto.getProdTimeUnit());
        production.setMonitoring(requestDto.isProdMonitoring());
        production.setStdValueMin(requestDto.getProdStdValueMin());
        production.setStdValueMax(requestDto.getProdStdValueMax());
        production.setUnit(requestDto.getProdUnit());
        production.setUptoEffectiveStatus(requestDto.isProdUptoEffectiveStatus());
        production.setUptoEffectiveWeek(requestDto.getProdUptoEffectiveWeek());
        production.setLastModifiedBy(getCurrentUsername());
        production.setLastModifiedDate(LocalDateTime.now());
        monitoringTSProdRepository.save(production);
    }

    private void updateNursery(MonitoringTSNursery nursery, MonitoringTaskSetupUpdateRequestDto requestDto) {
        nursery.setNoOfTerms(requestDto.getNurseryNoOfTerms());
        nursery.setTimeUnit(requestDto.getNurseryTimeUnit());
        nursery.setMonitoring(requestDto.isNurseryMonitoring());
        nursery.setStdValueMin(requestDto.getNurseryStdValueMin());
        nursery.setStdValueMax(requestDto.getNurseryStdValueMax());
        nursery.setUnit(requestDto.getNurseryUnit());
        nursery.setUptoEffectiveStatus(requestDto.isNurseryUptoEffectiveStatus());
        nursery.setUptoEffectiveWeek(requestDto.getNurseryUptoEffectiveWeek());
        nursery.setLastModifiedBy(getCurrentUsername());
        nursery.setLastModifiedDate(LocalDateTime.now());
        monitoringTSNurseryRepository.save(nursery);
    }

    private void updateSubTasks(Long parentId, List<SubTaskWithIdRequestDto> subTaskDtos, boolean isProduction) {
        List<MonitoringTSSubTask> existingSubTasks;
        if (isProduction) {
            existingSubTasks = monitoringTSSubTaskRepository.findByProductionId(parentId);
        } else {
            existingSubTasks = monitoringTSSubTaskRepository.findByNurseryId(parentId);
        }

        for (SubTaskWithIdRequestDto subTaskDto : subTaskDtos) {
            if (subTaskDto.getId() != null) {
                // Update existing subtask
                MonitoringTSSubTask existingSubTask = existingSubTasks.stream()
                        .filter(st -> st.getId().equals(subTaskDto.getId()))
                        .findFirst()
                        .orElseThrow(() -> new RuntimeException("Subtask not found with id " + subTaskDto.getId()));
                existingSubTask.setSubTaskName(subTaskDto.getSubTaskName());
                existingSubTask.setDescription(subTaskDto.getDescription());
                existingSubTask.setLastModifiedBy(getCurrentUsername());
                existingSubTask.setLastModifiedDate(LocalDateTime.now());
                monitoringTSSubTaskRepository.save(existingSubTask);
            } else {
                // Create new subtask
                MonitoringTSSubTask newSubTask = MonitoringTSSubTask.builder()
                        .subTaskName(subTaskDto.getSubTaskName())
                        .description(subTaskDto.getDescription())
                        .productionId(isProduction ? parentId : null)
                        .nurseryId(isProduction ? null : parentId)
                        .deleted(false)
                        .build();
                newSubTask.setCreatedBy(getCurrentUsername());
                newSubTask.setCreatedDate(LocalDateTime.now());
                monitoringTSSubTaskRepository.save(newSubTask);
            }
        }

        // Handle deletions
        List<Long> updatedSubTaskIds = subTaskDtos.stream()
                .map(SubTaskWithIdRequestDto::getId)
                .collect(Collectors.toList());

        for (MonitoringTSSubTask existingSubTask : existingSubTasks) {
            if (!updatedSubTaskIds.contains(existingSubTask.getId())) {
                existingSubTask.setDeleted(true);
                existingSubTask.setDeletedAt(LocalDateTime.now());
                existingSubTask.setDeletedBy(getCurrentUsername());
                monitoringTSSubTaskRepository.save(existingSubTask);
            }
        }
    }

    private void handleProductionDeletion(MonitoringTaskSetup taskSetup) {
        MonitoringTSProd production = monitoringTSProdRepository.findById(taskSetup.getProductionId())
                .orElseThrow(() -> new RuntimeException("Production entity not found"));
        production.setDeleted(true);
        production.setDeletedAt(LocalDateTime.now());
        production.setDeletedBy(getCurrentUsername());
        monitoringTSProdRepository.save(production);

        List<MonitoringTSSubTask> prodSubTasks = monitoringTSSubTaskRepository.findByProductionId(production.getId());
        for (MonitoringTSSubTask subTask : prodSubTasks) {
            subTask.setDeleted(true);
            subTask.setDeletedAt(LocalDateTime.now());
            subTask.setDeletedBy(getCurrentUsername());
            monitoringTSSubTaskRepository.save(subTask);
        }
        taskSetup.setProductionId(null);
    }

    private void handleNurseryDeletion(MonitoringTaskSetup taskSetup) {
        MonitoringTSNursery nursery = monitoringTSNurseryRepository.findById(taskSetup.getNurseryId())
                .orElseThrow(() -> new RuntimeException("Nursery entity not found"));
        nursery.setDeleted(true);
        nursery.setDeletedAt(LocalDateTime.now());
        nursery.setDeletedBy(getCurrentUsername());
        monitoringTSNurseryRepository.save(nursery);

        List<MonitoringTSSubTask> nurserySubTasks = monitoringTSSubTaskRepository.findByNurseryId(nursery.getId());
        for (MonitoringTSSubTask subTask : nurserySubTasks) {
            subTask.setDeleted(true);
            subTask.setDeletedAt(LocalDateTime.now());
            subTask.setDeletedBy(getCurrentUsername());
            monitoringTSSubTaskRepository.save(subTask);
        }
        taskSetup.setNurseryId(null);
    }
}
