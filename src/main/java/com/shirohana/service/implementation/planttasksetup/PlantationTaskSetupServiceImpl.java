package com.shirohana.service.implementation.planttasksetup;


import com.shirohana.constants.TaskTypeConstants;
import com.shirohana.dto.planttasksetup.plantationtasksetup.requestdtos.create.PlantationTaskSetupCreateRequestDto;
import com.shirohana.dto.planttasksetup.plantationtasksetup.requestdtos.create.SubTaskRequestDto;
import com.shirohana.dto.planttasksetup.plantationtasksetup.requestdtos.update.PlantationTaskSetupUpdateRequestDto;
import com.shirohana.dto.planttasksetup.plantationtasksetup.requestdtos.update.SubTaskWithIdRequestDto;
import com.shirohana.dto.planttasksetup.plantationtasksetup.responsedtos.PlantationTaskSetupResponseDto;
import com.shirohana.dto.planttasksetup.plantationtasksetup.responsedtos.SubTaskResponseDto;
import com.shirohana.entity.planttasksetup.plantationtasksetup.PlantationTSSubTask;
import com.shirohana.entity.planttasksetup.plantationtasksetup.PlantationTaskSetup;
import com.shirohana.repository.planttasksetup.plantationtasksetup.PlantationTSSubTaskRepository;
import com.shirohana.repository.planttasksetup.plantationtasksetup.PlantationTaskSetupRepository;
import com.shirohana.service.interfaces.planttasksetup.PlantationTaskSetupService;
import jakarta.persistence.EntityNotFoundException;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class PlantationTaskSetupServiceImpl implements PlantationTaskSetupService {

    private final PlantationTaskSetupRepository plantationTaskSetupRepository;
    private final PlantationTSSubTaskRepository plantationTSSubTaskRepository;

    @Transactional
    @Override
    public PlantationTaskSetupResponseDto createTask(PlantationTaskSetupCreateRequestDto requestDto) {
        try {
            validateTaskNameUniqueness(requestDto.getFamilyNameId(), requestDto.getTaskName());

            PlantationTaskSetup taskSetup = mapToEntity(requestDto);
            taskSetup.setDeleted(false);

            taskSetup = plantationTaskSetupRepository.save(taskSetup);
            saveSubTasks(requestDto.getSubTasks(), taskSetup);

            return mapToDto(taskSetup);
        } catch (DataIntegrityViolationException e) {
            log.error("Data integrity violation: {}", e.getMessage(), e);
            throw new RuntimeException("Data integrity violation: " + e.getMessage(), e);
        } catch (EntityNotFoundException e) {
            log.error("Entity not found: {}", e.getMessage(), e);
            throw new RuntimeException("Entity not found: " + e.getMessage(), e);
        } catch (RuntimeException e) {
            log.error("Validation error: {}", e.getMessage(), e);
            throw e;
        } catch (Exception e) {
            log.error("Unexpected error creating task: {}", e.getMessage(), e);
            throw new RuntimeException("Unexpected error creating task", e);
        }
    }

    @Transactional
    @Override
    public PlantationTaskSetupResponseDto updateTask(Long id, PlantationTaskSetupUpdateRequestDto requestDto) {
        try {
            validateTaskNameUniqueness(requestDto.getFamilyNameId(), requestDto.getTaskName(), id);

            PlantationTaskSetup taskSetup = plantationTaskSetupRepository.findByIdAndDeletedFalse(id)
                    .orElseThrow(() -> new EntityNotFoundException("Task not found"));

            updateEntity(taskSetup, requestDto);
            taskSetup = plantationTaskSetupRepository.save(taskSetup);
            updateSubTasks(requestDto.getSubTasks(), taskSetup);

            return mapToDto(taskSetup);
        } catch (DataIntegrityViolationException e) {
            log.error("Data integrity violation: {}", e.getMessage(), e);
            throw new RuntimeException("Data integrity violation: " + e.getMessage(), e);
        } catch (EntityNotFoundException e) {
            log.error("Entity not found: {}", e.getMessage(), e);
            throw new RuntimeException("Entity not found: " + e.getMessage(), e);
        } catch (RuntimeException e) {
            log.error("Validation error: {}", e.getMessage(), e);
            throw e;
        } catch (Exception e) {
            log.error("Unexpected error updating task: {}", e.getMessage(), e);
            throw new RuntimeException("Unexpected error updating task", e);
        }
    }

    @Transactional
    @Override
    public void deleteTask(Long id) {
        try {
            PlantationTaskSetup taskSetup = plantationTaskSetupRepository.findByIdAndDeletedFalse(id)
                    .orElseThrow(() -> new EntityNotFoundException("Task not found"));

            taskSetup.setDeleted(true);
            taskSetup.setDeletedBy(getCurrentUsername());
            taskSetup.setDeletedAt(LocalDateTime.now());
            plantationTaskSetupRepository.save(taskSetup);

            List<PlantationTSSubTask> subTasks = plantationTSSubTaskRepository.findByPlantationTaskSetupIdAndDeletedFalse(taskSetup.getId());
            for (PlantationTSSubTask subTask : subTasks) {
                subTask.setDeleted(true);
                subTask.setDeletedAt(LocalDateTime.now());
                subTask.setDeletedBy(getCurrentUsername());
                plantationTSSubTaskRepository.save(subTask);
            }
        } catch (EntityNotFoundException e) {
            log.error("Entity not found: {}", e.getMessage(), e);
            throw new RuntimeException("Entity not found: " + e.getMessage(), e);
        } catch (Exception e) {
            log.error("Unexpected error deleting task: {}", e.getMessage(), e);
            throw new RuntimeException("Unexpected error deleting task", e);
        }
    }

    @Override
    public PlantationTaskSetupResponseDto getTaskById(Long id) {
        try {
            PlantationTaskSetup taskSetup = plantationTaskSetupRepository.findByIdAndDeletedFalse(id)
                    .orElseThrow(() -> new EntityNotFoundException("Task not found"));
            return mapToDto(taskSetup);
        } catch (EntityNotFoundException e) {
            log.error("Entity not found: {}", e.getMessage(), e);
            throw new RuntimeException("Entity not found: " + e.getMessage(), e);
        }
    }

    @Override
    public List<PlantationTaskSetupResponseDto> getTasksByFamilyNameId(Long familyNameId) {
        List<PlantationTaskSetup> tasks = plantationTaskSetupRepository.findByFamilyNameIdAndDeletedFalse(familyNameId);
        return tasks.stream().map(this::mapToDto).collect(Collectors.toList());
    }

    @Override
    public List<PlantationTaskSetupResponseDto> getAllTasks() {
        List<PlantationTaskSetup> tasks = plantationTaskSetupRepository.findByDeletedFalse();
        return tasks.stream().map(this::mapToDto).collect(Collectors.toList());
    }

    private void validateTaskNameUniqueness(Long familyNameId, String taskName) {
        Optional<PlantationTaskSetup> existingTask = plantationTaskSetupRepository.findByFamilyNameIdAndTaskNameAndDeletedFalse(familyNameId, taskName);
        if (existingTask.isPresent()) {
            throw new RuntimeException("Task name must be unique for the same family name ID");
        }
    }

    private void validateTaskNameUniqueness(Long familyNameId, String taskName, Long id) {
        Optional<PlantationTaskSetup> existingTask = plantationTaskSetupRepository.findByFamilyNameIdAndTaskNameAndDeletedFalse(familyNameId, taskName);
        if (existingTask.isPresent() && !existingTask.get().getId().equals(id)) {
            throw new RuntimeException("Task name must be unique for the same family name ID");
        }
    }

    private PlantationTaskSetup mapToEntity(PlantationTaskSetupCreateRequestDto requestDto) {
        return PlantationTaskSetup.builder()
                .familyNameId(requestDto.getFamilyNameId())
                .taskName(requestDto.getTaskName())
                .taskDescription(requestDto.getDescription())
                .noOfTerms(requestDto.getNoOfTerms())
                .timeUnit(requestDto.getTimeUnit())
                .uptoEffectiveStatus(requestDto.isUptoEffectiveStatus())
                .uptoEffectiveWeek(requestDto.getUptoEffectiveWeek())
                .deleted(false)
                .build();
    }

    private void updateEntity(PlantationTaskSetup taskSetup, PlantationTaskSetupUpdateRequestDto requestDto) {
        taskSetup.setFamilyNameId(requestDto.getFamilyNameId());
        taskSetup.setTaskName(requestDto.getTaskName());
        taskSetup.setTaskDescription(requestDto.getDescription());
        taskSetup.setNoOfTerms(requestDto.getNoOfTerms());
        taskSetup.setTimeUnit(requestDto.getTimeUnit());
        taskSetup.setUptoEffectiveStatus(requestDto.isUptoEffectiveStatus());
        taskSetup.setUptoEffectiveWeek(requestDto.getUptoEffectiveWeek());
    }

    private void saveSubTasks(List<SubTaskRequestDto> subTaskDtos, PlantationTaskSetup taskSetup) {
        for (SubTaskRequestDto subTaskDto : subTaskDtos) {
            PlantationTSSubTask subTask = PlantationTSSubTask.builder()
                    .subTaskName(subTaskDto.getSubTaskName())
                    .description(subTaskDto.getDescription())
                    .plantationTaskSetupId(taskSetup.getId())
                    .deleted(false)
                    .build();
            plantationTSSubTaskRepository.save(subTask);
        }
    }

    private void updateSubTasks(List<SubTaskWithIdRequestDto> subTaskDtos, PlantationTaskSetup taskSetup) {
        List<Long> updatedSubTaskIds = subTaskDtos.stream()
                .map(SubTaskWithIdRequestDto::getId)
                .collect(Collectors.toList());

        List<PlantationTSSubTask> existingSubTasks = plantationTSSubTaskRepository.findByPlantationTaskSetupIdAndDeletedFalse(taskSetup.getId());

        for (PlantationTSSubTask existingSubTask : existingSubTasks) {
            if (!updatedSubTaskIds.contains(existingSubTask.getId())) {
                existingSubTask.setDeleted(true);
                existingSubTask.setDeletedAt(LocalDateTime.now());
                existingSubTask.setDeletedBy(getCurrentUsername());
                plantationTSSubTaskRepository.save(existingSubTask);
            }
        }

        for (SubTaskWithIdRequestDto subTaskDto : subTaskDtos) {
            PlantationTSSubTask subTask;
            if (subTaskDto.getId() != null) {
                subTask = plantationTSSubTaskRepository.findById(subTaskDto.getId())
                        .orElseThrow(() -> new RuntimeException("Sub-task not found with id " + subTaskDto.getId()));
                subTask.setSubTaskName(subTaskDto.getSubTaskName());
                subTask.setDescription(subTaskDto.getDescription());
                subTask.setLastModifiedBy(getCurrentUsername());
                subTask.setLastModifiedDate(LocalDateTime.now());
            } else {
                subTask = PlantationTSSubTask.builder()
                        .subTaskName(subTaskDto.getSubTaskName())
                        .description(subTaskDto.getDescription())
                        .plantationTaskSetupId(taskSetup.getId())
                        .deleted(false)
                        .build();
                subTask.setCreatedBy(getCurrentUsername());
                subTask.setCreatedDate(LocalDateTime.now());
            }
            plantationTSSubTaskRepository.save(subTask);
        }
    }

    private PlantationTaskSetupResponseDto mapToDto(PlantationTaskSetup taskSetup) {
        List<PlantationTSSubTask> subTasks = plantationTSSubTaskRepository.findByPlantationTaskSetupIdAndDeletedFalse(taskSetup.getId());

        List<SubTaskResponseDto> subTaskResponseDtos = subTasks.stream()
                .map(subTask -> SubTaskResponseDto.builder()
                        .id(subTask.getId())
                        .subTaskName(subTask.getSubTaskName())
                        .description(subTask.getDescription())
                        .createdBy(subTask.getCreatedBy())
                        .createdDate(subTask.getCreatedDate())
                        .lastModifiedBy(subTask.getLastModifiedBy())
                        .lastModifiedDate(subTask.getLastModifiedDate())
                        .deletedAt(subTask.getDeletedAt())
                        .deletedBy(subTask.getDeletedBy())
                        .build())
                .collect(Collectors.toList());

        return PlantationTaskSetupResponseDto.builder()
                .id(taskSetup.getId())
                .taskName(taskSetup.getTaskName())
                .taskDescription(taskSetup.getTaskDescription())
                .familyNameId(taskSetup.getFamilyNameId())
                .taskType(TaskTypeConstants.PLANTATION)
                .noOfTerms(taskSetup.getNoOfTerms())
                .timeUnit(taskSetup.getTimeUnit())
                .uptoEffectiveStatus(taskSetup.isUptoEffectiveStatus())
                .uptoEffectiveWeek(taskSetup.getUptoEffectiveWeek())
                .subTasks(subTaskResponseDtos)
                .prodPlanId(taskSetup.getProdPlanId())
                .createdBy(taskSetup.getCreatedBy())
                .createdDate(taskSetup.getCreatedDate())
                .lastModifiedBy(taskSetup.getLastModifiedBy())
                .lastModifiedDate(taskSetup.getLastModifiedDate())
                .deletedAt(taskSetup.getDeletedAt())
                .deletedBy(taskSetup.getDeletedBy())
                .build();
    }

    private String getCurrentUsername() {
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (authentication != null && authentication.getPrincipal() instanceof UserDetails) {
                return ((UserDetails) authentication.getPrincipal()).getUsername();
            }
        } catch (Exception e) {
            log.error("Error retrieving current username", e);
        }
        return null;
    }
}
