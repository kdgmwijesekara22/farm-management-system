package com.shirohana.service.implementation.planttasksetup;

import com.shirohana.constants.TaskTypeConstants;
import com.shirohana.dto.planttasksetup.maintenancetasksetup.requestDtos.create.MaintenanceTaskSetupCreateRequestDto;
import com.shirohana.dto.planttasksetup.maintenancetasksetup.MaintenanceTaskSetupResponseDto;

import com.shirohana.dto.planttasksetup.maintenancetasksetup.SubTaskResponseDto;
import com.shirohana.dto.planttasksetup.maintenancetasksetup.requestDtos.update.MaintenanceTaskSetupUpdateRequestDto;
import com.shirohana.dto.planttasksetup.maintenancetasksetup.requestDtos.update.SubTaskWithIdRequestDto;
import com.shirohana.entity.planttasksetup.maintenancetasksetup.MaintenanceTSNursery;
import com.shirohana.entity.planttasksetup.maintenancetasksetup.MaintenanceTSProd;
import com.shirohana.entity.planttasksetup.maintenancetasksetup.MaintenanceTSSubTask;
import com.shirohana.entity.planttasksetup.maintenancetasksetup.MaintenanceTaskSetup;
import com.shirohana.entity.taskcategorysetup.TaskCategorySetup;
import com.shirohana.exception.ResourceNotFoundException;
import com.shirohana.repository.planttasksetup.maintenancetasksetup.MaintenanceTSNurseryRepository;
import com.shirohana.repository.planttasksetup.maintenancetasksetup.MaintenanceTSProdRepository;
import com.shirohana.repository.planttasksetup.maintenancetasksetup.MaintenanceTSSubTaskRepository;
import com.shirohana.repository.planttasksetup.maintenancetasksetup.MaintenanceTaskSetupRepository;
import com.shirohana.repository.taskcategorysetup.TaskCategorySetupRepository;
import com.shirohana.service.interfaces.planttasksetup.MaintenanceTaskSetupService;
import jakarta.persistence.EntityNotFoundException;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class MaintenanceTaskSetupServiceImpl implements MaintenanceTaskSetupService {

    private final MaintenanceTaskSetupRepository maintenanceTaskSetupRepository;

    private final MaintenanceTSProdRepository maintenanceTSProdRepository;

    private final MaintenanceTSNurseryRepository maintenanceTSNurseryRepository;

    private final MaintenanceTSSubTaskRepository maintenanceTSSubTaskRepository;

    private final TaskCategorySetupRepository taskCategorySetupRepository;

    @Transactional
    @Override
    public MaintenanceTaskSetupResponseDto createTask(MaintenanceTaskSetupCreateRequestDto requestDto) {
        try {
            validateTaskNameUniqueness(requestDto.getFamilyNameId(), requestDto.getTaskNameId());

            MaintenanceTaskSetup taskSetup = mapToEntity(requestDto);
            taskSetup.setDeleted(false);

            if (requestDto.isProduction()) {
                MaintenanceTSProd production = createProduction(requestDto);
                taskSetup.setProductionId(production.getId());
            }

            if (requestDto.isNursery()) {
                MaintenanceTSNursery nursery = createNursery(requestDto);
                taskSetup.setNurseryId(nursery.getId());
            }

            taskSetup = maintenanceTaskSetupRepository.save(taskSetup);
            return mapToDto(taskSetup);
        } catch (DataIntegrityViolationException e) {
            log.error("Data integrity violation: {}", e.getMessage(), e);
            throw new RuntimeException("Data integrity violation: " + e.getMessage(), e);
        } catch (EntityNotFoundException e) {
            log.error("Entity not found: {}", e.getMessage(), e);
            throw new RuntimeException("Entity not found: " + e.getMessage(), e);
        } catch (RuntimeException e) {
            log.error("Validation error: {}", e.getMessage(), e);
            throw e;
        } catch (Exception e) {
            log.error("Unexpected error creating task: {}", e.getMessage(), e);
            throw new RuntimeException("Unexpected error creating task", e);
        }
    }

//    @Transactional
//    @Override
//    public MaintenanceTaskSetupResponseDto updateTask(Long id, MaintenanceTaskSetupCreateRequestDto requestDto) {
//        try {
//            validateTaskNameUniqueness(requestDto.getFamilyNameId(), requestDto.getTaskNameId(), id);
//
//            MaintenanceTaskSetup taskSetup = maintenanceTaskSetupRepository.findById(id)
//                    .orElseThrow(() -> new RuntimeException("Task not found"));
//
//            mapToEntity(taskSetup, requestDto);
//
//            if (requestDto.isProduction() && taskSetup.getProductionId() == null) {
//                MaintenanceTSProd production = createProduction(requestDto);
//                taskSetup.setProductionId(production.getId());
//            }
//
//            if (requestDto.isNursery() && taskSetup.getNurseryId() == null) {
//                MaintenanceTSNursery nursery = createNursery(requestDto);
//                taskSetup.setNurseryId(nursery.getId());
//            }
//
//            taskSetup = maintenanceTaskSetupRepository.save(taskSetup);
//            return mapToDto(taskSetup);
//        } catch (DataIntegrityViolationException e) {
//            log.error("Data integrity violation: {}", e.getMessage(), e);
//            throw new RuntimeException("Data integrity violation: " + e.getMessage(), e);
//        } catch (EntityNotFoundException e) {
//            log.error("Entity not found: {}", e.getMessage(), e);
//            throw new RuntimeException("Entity not found: " + e.getMessage(), e);
//        }catch (ResourceNotFoundException e) {
//            log.error("Resource not found: {}", e.getMessage(), e);
//            throw e;  // Re-throw the exception to be caught by the controller
//        } catch (RuntimeException e) {
//            log.error("Validation error: {}", e.getMessage(), e);
//            throw e;
//        } catch (Exception e) {
//            log.error("Unexpected error updating task: {}", e.getMessage(), e);
//            throw new RuntimeException("Unexpected error updating task", e);
//        }
//    }

    @Transactional
    @Override
    public void deleteTask(Long id) {
        try {
            MaintenanceTaskSetup taskSetup = maintenanceTaskSetupRepository.findById(id)
                    .orElseThrow(() -> new RuntimeException("Task not found"));

            // Soft delete the main task setup
            taskSetup.setDeleted(true);
            taskSetup.setDeletedAt(LocalDateTime.now());
            taskSetup.setDeletedBy(getCurrentUsername());

            // Soft delete the related production entity
            if (taskSetup.getProductionId() != null) {
                MaintenanceTSProd production = maintenanceTSProdRepository.findById(taskSetup.getProductionId())
                        .orElseThrow(() -> new RuntimeException("Production entity not found"));
                production.setDeleted(true);
                production.setDeletedAt(LocalDateTime.now());
                production.setDeletedBy(getCurrentUsername());
                maintenanceTSProdRepository.save(production);

                // Soft delete the related sub-tasks for production
                List<MaintenanceTSSubTask> prodSubTasks = maintenanceTSSubTaskRepository.findByProductionId(production.getId());
                for (MaintenanceTSSubTask subTask : prodSubTasks) {
                    subTask.setDeleted(true);
                    subTask.setDeletedAt(LocalDateTime.now());
                    subTask.setDeletedBy(getCurrentUsername());
                    maintenanceTSSubTaskRepository.save(subTask);
                }
            }

            // Soft delete the related nursery entity
            if (taskSetup.getNurseryId() != null) {
                MaintenanceTSNursery nursery = maintenanceTSNurseryRepository.findById(taskSetup.getNurseryId())
                        .orElseThrow(() -> new RuntimeException("Nursery entity not found"));
                nursery.setDeleted(true);
                nursery.setDeletedAt(LocalDateTime.now());
                nursery.setDeletedBy(getCurrentUsername());
                maintenanceTSNurseryRepository.save(nursery);

                // Soft delete the related sub-tasks for nursery
                List<MaintenanceTSSubTask> nurserySubTasks = maintenanceTSSubTaskRepository.findByNurseryId(nursery.getId());
                for (MaintenanceTSSubTask subTask : nurserySubTasks) {
                    subTask.setDeleted(true);
                    subTask.setDeletedAt(LocalDateTime.now());
                    subTask.setDeletedBy(getCurrentUsername());
                    maintenanceTSSubTaskRepository.save(subTask);
                }
            }

            // Save the main task setup after updating related entities
            maintenanceTaskSetupRepository.save(taskSetup);
        } catch (ResourceNotFoundException e) {
            log.error("Resource not found: {}", e.getMessage(), e);
            throw e;  // Re-throw the exception to be caught by the controller
        } catch (Exception e) {
            log.error("Error deleting task: {}", e.getMessage(), e);
            throw new RuntimeException("Error deleting task", e);
        }
    }

    @Transactional
    @Override
    public MaintenanceTaskSetupResponseDto getTaskById(Long id) {
        try {
            MaintenanceTaskSetup taskSetup = maintenanceTaskSetupRepository.findById(id)
                    .orElseThrow(() -> new ResourceNotFoundException("Task not found with id " + id));
            return mapToDto(taskSetup);
        } catch (ResourceNotFoundException e) {
            log.error("Task not found: {}", e.getMessage(), e);
            throw e;  // Re-throw the exception to be caught by the controller
        } catch (Exception e) {
            log.error("Error retrieving task: {}", e.getMessage(), e);
            throw new RuntimeException("Error retrieving task", e);
        }
    }

    @Transactional
    @Override
    public List<MaintenanceTaskSetupResponseDto> getAllTasks() {
        try {
            return maintenanceTaskSetupRepository.findByDeletedFalse().stream()
                    .map(this::mapToDto)
                    .collect(Collectors.toList());
        } catch (Exception e) {
            log.error("Error retrieving all tasks: {}", e.getMessage(), e);
            throw new RuntimeException("Error retrieving all tasks", e);
        }
    }

    @Override
    public List<MaintenanceTaskSetupResponseDto> getMaintenanceTaskSetupByFamilyNameId(Long familyNameId) {
        List<MaintenanceTaskSetup> setups = maintenanceTaskSetupRepository.findByFamilyNameIdAndDeletedFalse(familyNameId);
        if (setups.isEmpty()) {
            throw new ResourceNotFoundException("No Maintenance Task Setup found for familyNameId: " + familyNameId);
        }
        return setups.stream().map(this::mapToDto).collect(Collectors.toList());
    }

    private void validateTaskNameUniqueness(Long familyNameId, Long taskNameId) {
        Optional<MaintenanceTaskSetup> existingTask = maintenanceTaskSetupRepository.findByFamilyNameIdAndTaskNameIdAndDeletedFalse(familyNameId, taskNameId);
        if (existingTask.isPresent()) {
            throw new RuntimeException("Task name must be unique for the same family name ID");
        }
    }

    private void validateTaskNameUniqueness(Long familyNameId, Long taskNameId, Long id) {
        Optional<MaintenanceTaskSetup> existingTask = maintenanceTaskSetupRepository.findByFamilyNameIdAndTaskNameIdAndDeletedFalse(familyNameId, taskNameId);
        if (existingTask.isPresent() && !existingTask.get().getId().equals(id)) {
            throw new RuntimeException("Task name must be unique for the same family name ID");
        }
    }

    private MaintenanceTaskSetup mapToEntity(MaintenanceTaskSetupCreateRequestDto requestDto) {
        return MaintenanceTaskSetup.builder()
                .taskNameId(requestDto.getTaskNameId())
                .familyNameId(requestDto.getFamilyNameId())
                .deleted(false)
                .build();
    }

    private void mapToEntity(MaintenanceTaskSetup taskSetup, MaintenanceTaskSetupUpdateRequestDto requestDto) {
        if (requestDto.getTaskNameId() != null) {
            taskSetup.setTaskNameId(requestDto.getTaskNameId());
        }
        if (requestDto.getFamilyNameId() != null) {
            taskSetup.setFamilyNameId(requestDto.getFamilyNameId());
        }
    }

    private MaintenanceTSProd createProduction(MaintenanceTaskSetupCreateRequestDto requestDto) {
        try {
            MaintenanceTSProd production = MaintenanceTSProd.builder()
                    .noOfTerms(requestDto.getProdNoOfTerms())
                    .timeUnit(requestDto.getProdTimeUnit())
                    .monitoring(requestDto.isProdMonitoring())
                    .stdValueMin(requestDto.getProdStdValueMin())
                    .stdValueMax(requestDto.getProdStdValueMax())
                    .unit(requestDto.getProdUnit())
                    .uptoEffectiveStatus(requestDto.isProdUptoEffectiveStatus())
                    .uptoEffectiveWeek(requestDto.getProdUptoEffectiveWeek())
                    .deleted(false)
                    .build();

            production.setCreatedBy(getCurrentUsername());
            production.setCreatedDate(LocalDateTime.now());

            MaintenanceTSProd savedProduction = maintenanceTSProdRepository.save(production);

            requestDto.getProdSubTasks().forEach(subTaskDto -> {
                MaintenanceTSSubTask subTask = MaintenanceTSSubTask.builder()
                        .subTaskName(subTaskDto.getSubTaskName())
                        .description(subTaskDto.getDescription())
                        .productionId(savedProduction.getId())
                        .deleted(false)
                        .build();

                subTask.setCreatedBy(getCurrentUsername());
                subTask.setCreatedDate(LocalDateTime.now());
                maintenanceTSSubTaskRepository.save(subTask);
            });

            return savedProduction;
        } catch (Exception e) {
            log.error("Error creating production: {}", e.getMessage(), e);
            throw new RuntimeException("Error creating production", e);
        }
    }

    private MaintenanceTSProd createProduction(MaintenanceTaskSetupUpdateRequestDto requestDto) {
        try {
            MaintenanceTSProd production = MaintenanceTSProd.builder()
                    .noOfTerms(requestDto.getProdNoOfTerms())
                    .timeUnit(requestDto.getProdTimeUnit())
                    .monitoring(requestDto.isProdMonitoring())
                    .stdValueMin(requestDto.getProdStdValueMin())
                    .stdValueMax(requestDto.getProdStdValueMax())
                    .unit(requestDto.getProdUnit())
                    .uptoEffectiveStatus(requestDto.isProdUptoEffectiveStatus())
                    .uptoEffectiveWeek(requestDto.getProdUptoEffectiveWeek())
                    .deleted(false)
                    .build();

            production.setCreatedBy(getCurrentUsername());
            production.setCreatedDate(LocalDateTime.now());

            MaintenanceTSProd savedProduction = maintenanceTSProdRepository.save(production);

            requestDto.getProdSubTasks().forEach(subTaskDto -> {
                MaintenanceTSSubTask subTask = MaintenanceTSSubTask.builder()
                        .subTaskName(subTaskDto.getSubTaskName())
                        .description(subTaskDto.getDescription())
                        .productionId(savedProduction.getId())
                        .deleted(false)
                        .build();

                subTask.setCreatedBy(getCurrentUsername());
                subTask.setCreatedDate(LocalDateTime.now());
                maintenanceTSSubTaskRepository.save(subTask);
            });

            return savedProduction;
        } catch (Exception e) {
            log.error("Error creating production: {}", e.getMessage(), e);
            throw new RuntimeException("Error creating production", e);
        }
    }

    private MaintenanceTSNursery createNursery(MaintenanceTaskSetupCreateRequestDto requestDto) {
        try {
            MaintenanceTSNursery nursery = MaintenanceTSNursery.builder()
                    .noOfTerms(requestDto.getNurseryNoOfTerms())
                    .timeUnit(requestDto.getNurseryTimeUnit())
                    .monitoring(requestDto.isNurseryMonitoring())
                    .stdValueMin(requestDto.getNurseryStdValueMin())
                    .stdValueMax(requestDto.getNurseryStdValueMax())
                    .unit(requestDto.getNurseryUnit())
                    .uptoEffectiveStatus(requestDto.isNurseryUptoEffectiveStatus())
                    .uptoEffectiveWeek(requestDto.getNurseryUptoEffectiveWeek())
                    .deleted(false)
                    .build();
            nursery.setCreatedBy(getCurrentUsername());
            nursery.setCreatedDate(LocalDateTime.now());

            MaintenanceTSNursery savedNursery = maintenanceTSNurseryRepository.save(nursery);

            requestDto.getNurserySubTasks().forEach(subTaskDto -> {
                MaintenanceTSSubTask subTask = MaintenanceTSSubTask.builder()
                        .subTaskName(subTaskDto.getSubTaskName())
                        .description(subTaskDto.getDescription())
                        .nurseryId(savedNursery.getId())
                        .deleted(false)
                        .build();

                subTask.setCreatedBy(getCurrentUsername());
                subTask.setCreatedDate(LocalDateTime.now());
                maintenanceTSSubTaskRepository.save(subTask);
            });

            return savedNursery;
        } catch (Exception e) {
            log.error("Error creating nursery: {}", e.getMessage(), e);
            throw new RuntimeException("Error creating nursery", e);
        }
    }
    private MaintenanceTSNursery createNursery(MaintenanceTaskSetupUpdateRequestDto requestDto) {
        try {
            MaintenanceTSNursery nursery = MaintenanceTSNursery.builder()
                    .noOfTerms(requestDto.getNurseryNoOfTerms())
                    .timeUnit(requestDto.getNurseryTimeUnit())
                    .monitoring(requestDto.isNurseryMonitoring())
                    .stdValueMin(requestDto.getNurseryStdValueMin())
                    .stdValueMax(requestDto.getNurseryStdValueMax())
                    .unit(requestDto.getNurseryUnit())
                    .uptoEffectiveStatus(requestDto.isNurseryUptoEffectiveStatus())
                    .uptoEffectiveWeek(requestDto.getNurseryUptoEffectiveWeek())
                    .deleted(false)
                    .build();
            nursery.setCreatedBy(getCurrentUsername());
            nursery.setCreatedDate(LocalDateTime.now());

            MaintenanceTSNursery savedNursery = maintenanceTSNurseryRepository.save(nursery);

            requestDto.getNurserySubTasks().forEach(subTaskDto -> {
                MaintenanceTSSubTask subTask = MaintenanceTSSubTask.builder()
                        .subTaskName(subTaskDto.getSubTaskName())
                        .description(subTaskDto.getDescription())
                        .nurseryId(savedNursery.getId())
                        .deleted(false)
                        .build();

                subTask.setCreatedBy(getCurrentUsername());
                subTask.setCreatedDate(LocalDateTime.now());
                maintenanceTSSubTaskRepository.save(subTask);
            });

            return savedNursery;
        } catch (Exception e) {
            log.error("Error creating nursery: {}", e.getMessage(), e);
            throw new RuntimeException("Error creating nursery", e);
        }
    }

private MaintenanceTaskSetupResponseDto mapToDto(MaintenanceTaskSetup taskSetup) {
        Long taskId = taskSetup.getTaskNameId();
    TaskCategorySetup taskCategorySetup = taskCategorySetupRepository.findByIdAndDeletedFalse(taskId)
            .orElseThrow(() -> new RuntimeException("Task category setup not found"));
    MaintenanceTaskSetupResponseDto responseDto = MaintenanceTaskSetupResponseDto.builder()
            .id(taskSetup.getId())
            .taskName(taskCategorySetup.getTaskName())
            .taskDescription(taskCategorySetup.getTaskDescription())
            .familyNameId(taskSetup.getFamilyNameId())
            .taskType(TaskTypeConstants.MAINTENANCE)
            .production(taskSetup.getProductionId() != null)
            .prodPlanId(taskSetup.getProdPlanId())
            .nursery(taskSetup.getNurseryId() != null)
            .createdBy(taskSetup.getCreatedBy())
            .createdDate(taskSetup.getCreatedDate())
            .lastModifiedBy(taskSetup.getLastModifiedBy())
            .lastModifiedDate(taskSetup.getLastModifiedDate())
            .deletedAt(taskSetup.getDeletedAt())
            .deletedBy(taskSetup.getDeletedBy())
            .build();

    if (taskSetup.getProductionId() != null) {
        MaintenanceTSProd production = maintenanceTSProdRepository.findById(taskSetup.getProductionId())
                .orElseThrow(() -> new RuntimeException("Production details not found"));
        responseDto.setProdNoOfTerms(production.getNoOfTerms());
        responseDto.setProdTimeUnit(production.getTimeUnit());
        responseDto.setProdMonitoring(production.isMonitoring());
        responseDto.setProdStdValueMin(production.getStdValueMin());
        responseDto.setProdStdValueMax(production.getStdValueMax());
        responseDto.setProdUnit(production.getUnit());
        responseDto.setProdUptoEffectiveStatus(production.isUptoEffectiveStatus());
        responseDto.setProdUptoEffectiveWeek(production.getUptoEffectiveWeek());
        List<SubTaskResponseDto> prodSubTasks = maintenanceTSSubTaskRepository.findByProductionId(production.getId())
                .stream()
                .map(subTask -> SubTaskResponseDto.builder()
                        .id(subTask.getId())
                        .subTaskName(subTask.getSubTaskName())
                        .description(subTask.getDescription())
                        .createdBy(subTask.getCreatedBy())
                        .createdDate(subTask.getCreatedDate())
                        .lastModifiedBy(subTask.getLastModifiedBy())
                        .lastModifiedDate(subTask.getLastModifiedDate())
                        .deletedAt(subTask.getDeletedAt())
                        .deletedBy(subTask.getDeletedBy())
                        .build())
                .collect(Collectors.toList());
        responseDto.setProdSubTasks(prodSubTasks);
    }

    if (taskSetup.getNurseryId() != null) {
        MaintenanceTSNursery nursery = maintenanceTSNurseryRepository.findById(taskSetup.getNurseryId())
                .orElseThrow(() -> new RuntimeException("Nursery details not found"));
        responseDto.setNurseryNoOfTerms(nursery.getNoOfTerms());
        responseDto.setNurseryTimeUnit(nursery.getTimeUnit());
        responseDto.setNurseryMonitoring(nursery.isMonitoring());
        responseDto.setNurseryStdValueMin(nursery.getStdValueMin());
        responseDto.setNurseryStdValueMax(nursery.getStdValueMax());
        responseDto.setNurseryUnit(nursery.getUnit());
        responseDto.setNurseryUptoEffectiveStatus(nursery.isUptoEffectiveStatus());
        responseDto.setNurseryUptoEffectiveWeek(nursery.getUptoEffectiveWeek());
        List<SubTaskResponseDto> nurserySubTasks = maintenanceTSSubTaskRepository.findByNurseryId(nursery.getId())
                .stream()
                .map(subTask -> SubTaskResponseDto.builder()
                        .id(subTask.getId())
                        .subTaskName(subTask.getSubTaskName())
                        .description(subTask.getDescription())
                        .createdBy(subTask.getCreatedBy())
                        .createdDate(subTask.getCreatedDate())
                        .lastModifiedBy(subTask.getLastModifiedBy())
                        .lastModifiedDate(subTask.getLastModifiedDate())
                        .deletedAt(subTask.getDeletedAt())
                        .deletedBy(subTask.getDeletedBy())
                        .build())
                .collect(Collectors.toList());
        responseDto.setNurserySubTasks(nurserySubTasks);
    }

    return responseDto;
}

    private String getCurrentUsername() {
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (authentication != null && authentication.getPrincipal() instanceof UserDetails) {
                return ((UserDetails) authentication.getPrincipal()).getUsername();
            }
        } catch (Exception e) {
            log.error("Error retrieving current username", e);
        }
        return null;
    }

    @Transactional
    @Override
    public MaintenanceTaskSetupResponseDto updateTask(Long id, MaintenanceTaskSetupUpdateRequestDto requestDto) {
        try {
            validateTaskNameUniqueness(requestDto.getFamilyNameId(), requestDto.getTaskNameId(), id);

            MaintenanceTaskSetup taskSetup = maintenanceTaskSetupRepository.findById(id)
                    .orElseThrow(() -> new RuntimeException("Task not found"));

            mapToEntity(taskSetup, requestDto);

            // Handle Production
            if (requestDto.isProduction()) {
                handleProductionUpdate(taskSetup, requestDto);
            } else if (taskSetup.getProductionId() != null) {
                handleProductionDeletion(taskSetup);
            }

            // Handle Nursery
            if (requestDto.isNursery()) {
                handleNurseryUpdate(taskSetup, requestDto);
            } else if (taskSetup.getNurseryId() != null) {
                handleNurseryDeletion(taskSetup);
            }

            // Save the updated taskSetup
            taskSetup = maintenanceTaskSetupRepository.save(taskSetup);

            return mapToDto(taskSetup);
        } catch (DataIntegrityViolationException e) {
            log.error("Data integrity violation: {}", e.getMessage(), e);
            throw new RuntimeException("Data integrity violation: " + e.getMessage(), e);
        } catch (EntityNotFoundException e) {
            log.error("Entity not found: {}", e.getMessage(), e);
            throw new RuntimeException("Entity not found: " + e.getMessage(), e);
        } catch (ResourceNotFoundException e) {
            log.error("Resource not found: {}", e.getMessage(), e);
            throw e;  // Re-throw the exception to be caught by the controller
        } catch (RuntimeException e) {
            log.error("Validation error: {}", e.getMessage(), e);
            throw e;
        } catch (Exception e) {
            log.error("Unexpected error updating task: {}", e.getMessage(), e);
            throw new RuntimeException("Unexpected error updating task", e);
        }
    }

    private void handleProductionUpdate(MaintenanceTaskSetup taskSetup, MaintenanceTaskSetupUpdateRequestDto requestDto) {
        MaintenanceTSProd production;
        if (taskSetup.getProductionId() == null) {
            production = createProduction(requestDto);
            taskSetup.setProductionId(production.getId());
        } else {
            production = maintenanceTSProdRepository.findById(taskSetup.getProductionId())
                    .orElseThrow(() -> new RuntimeException("Production entity not found"));
            updateProduction(production, requestDto);
        }
        updateSubTasks(production.getId(), requestDto.getProdSubTasks(), true);
    }


    private void handleNurseryUpdate(MaintenanceTaskSetup taskSetup, MaintenanceTaskSetupUpdateRequestDto requestDto) {
        MaintenanceTSNursery nursery;
        if (taskSetup.getNurseryId() == null) {
            nursery = createNursery(requestDto);
            taskSetup.setNurseryId(nursery.getId());
        } else {
            nursery = maintenanceTSNurseryRepository.findById(taskSetup.getNurseryId())
                    .orElseThrow(() -> new RuntimeException("Nursery entity not found"));
            updateNursery(nursery, requestDto);
        }
        updateSubTasks(nursery.getId(), requestDto.getNurserySubTasks(), false);
    }

    private void handleProductionDeletion(MaintenanceTaskSetup taskSetup) {
        MaintenanceTSProd production = maintenanceTSProdRepository.findById(taskSetup.getProductionId())
                .orElseThrow(() -> new RuntimeException("Production entity not found"));
        production.setDeleted(true);
        production.setDeletedAt(LocalDateTime.now());
        production.setDeletedBy(getCurrentUsername());
        maintenanceTSProdRepository.save(production);

        List<MaintenanceTSSubTask> prodSubTasks = maintenanceTSSubTaskRepository.findByProductionId(production.getId());
        for (MaintenanceTSSubTask subTask : prodSubTasks) {
            subTask.setDeleted(true);
            subTask.setDeletedAt(LocalDateTime.now());
            subTask.setDeletedBy(getCurrentUsername());
            maintenanceTSSubTaskRepository.save(subTask);
        }
        taskSetup.setProductionId(null);
    }


    private void handleNurseryDeletion(MaintenanceTaskSetup taskSetup) {
        MaintenanceTSNursery nursery = maintenanceTSNurseryRepository.findById(taskSetup.getNurseryId())
                .orElseThrow(() -> new RuntimeException("Nursery entity not found"));
        nursery.setDeleted(true);
        nursery.setDeletedAt(LocalDateTime.now());
        nursery.setDeletedBy(getCurrentUsername());
        maintenanceTSNurseryRepository.save(nursery);

        List<MaintenanceTSSubTask> nurserySubTasks = maintenanceTSSubTaskRepository.findByNurseryId(nursery.getId());
        for (MaintenanceTSSubTask subTask : nurserySubTasks) {
            subTask.setDeleted(true);
            subTask.setDeletedAt(LocalDateTime.now());
            subTask.setDeletedBy(getCurrentUsername());
            maintenanceTSSubTaskRepository.save(subTask);
        }
        taskSetup.setNurseryId(null);
    }

    private void updateProduction(MaintenanceTSProd production, MaintenanceTaskSetupUpdateRequestDto requestDto) {
        production.setNoOfTerms(requestDto.getProdNoOfTerms());
        production.setTimeUnit(requestDto.getProdTimeUnit());
        production.setMonitoring(requestDto.isProdMonitoring());
        production.setStdValueMin(requestDto.getProdStdValueMin());
        production.setStdValueMax(requestDto.getProdStdValueMax());
        production.setUnit(requestDto.getProdUnit());
        production.setUptoEffectiveStatus(requestDto.isProdUptoEffectiveStatus());
        production.setUptoEffectiveWeek(requestDto.getProdUptoEffectiveWeek());
        production.setLastModifiedBy(getCurrentUsername());
        production.setLastModifiedDate(LocalDateTime.now());
        maintenanceTSProdRepository.save(production);
    }

    private void updateNursery(MaintenanceTSNursery nursery, MaintenanceTaskSetupUpdateRequestDto requestDto) {
        nursery.setNoOfTerms(requestDto.getNurseryNoOfTerms());
        nursery.setTimeUnit(requestDto.getNurseryTimeUnit());
        nursery.setMonitoring(requestDto.isNurseryMonitoring());
        nursery.setStdValueMin(requestDto.getNurseryStdValueMin());
        nursery.setStdValueMax(requestDto.getNurseryStdValueMax());
        nursery.setUnit(requestDto.getNurseryUnit());
        nursery.setUptoEffectiveStatus(requestDto.isNurseryUptoEffectiveStatus());
        nursery.setUptoEffectiveWeek(requestDto.getNurseryUptoEffectiveWeek());
        nursery.setLastModifiedBy(getCurrentUsername());
        nursery.setLastModifiedDate(LocalDateTime.now());
        maintenanceTSNurseryRepository.save(nursery);
    }

    private void updateSubTasks(Long parentId, List<SubTaskWithIdRequestDto> subTaskDtos, boolean isProduction) {
        List<MaintenanceTSSubTask> existingSubTasks;
        if (isProduction) {
            existingSubTasks = maintenanceTSSubTaskRepository.findByProductionId(parentId);
        } else {
            existingSubTasks = maintenanceTSSubTaskRepository.findByNurseryId(parentId);
        }

        for (SubTaskWithIdRequestDto subTaskDto : subTaskDtos) {
            if (subTaskDto.getId() != null) {
                // Update existing subtask
                MaintenanceTSSubTask existingSubTask = existingSubTasks.stream()
                        .filter(st -> st.getId().equals(subTaskDto.getId()))
                        .findFirst()
                        .orElseThrow(() -> new RuntimeException("Subtask not found with id " + subTaskDto.getId()));
                existingSubTask.setSubTaskName(subTaskDto.getSubTaskName());
                existingSubTask.setDescription(subTaskDto.getDescription());
                existingSubTask.setLastModifiedBy(getCurrentUsername());
                existingSubTask.setLastModifiedDate(LocalDateTime.now());
                maintenanceTSSubTaskRepository.save(existingSubTask);
            } else {
                // Create new subtask
                MaintenanceTSSubTask newSubTask = MaintenanceTSSubTask.builder()
                        .subTaskName(subTaskDto.getSubTaskName())
                        .description(subTaskDto.getDescription())
                        .productionId(isProduction ? parentId : null)
                        .nurseryId(isProduction ? null : parentId)
                        .deleted(false)
                        .build();
                newSubTask.setCreatedBy(getCurrentUsername());
                newSubTask.setCreatedDate(LocalDateTime.now());
                maintenanceTSSubTaskRepository.save(newSubTask);
            }
        }

        // Handle deletions
        List<Long> updatedSubTaskIds = subTaskDtos.stream()
                .map(SubTaskWithIdRequestDto::getId)
                .collect(Collectors.toList());

        for (MaintenanceTSSubTask existingSubTask : existingSubTasks) {
            if (!updatedSubTaskIds.contains(existingSubTask.getId())) {
                existingSubTask.setDeleted(true);
                existingSubTask.setDeletedAt(LocalDateTime.now());
                existingSubTask.setDeletedBy(getCurrentUsername());
                maintenanceTSSubTaskRepository.save(existingSubTask);
            }
        }
    }



}
