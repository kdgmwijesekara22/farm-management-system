package com.shirohana.service.implementation.planttasksetup;


import com.shirohana.constants.TaskTypeConstants;
import com.shirohana.dto.planttasksetup.harvestingtasksetup.requestDtos.create.HarvestingTaskSetupCreateRequestDto;
import com.shirohana.dto.planttasksetup.harvestingtasksetup.requestDtos.create.SubTaskRequestDto;
import com.shirohana.dto.planttasksetup.harvestingtasksetup.requestDtos.update.HarvestingTaskSetupUpdateRequestDto;
import com.shirohana.dto.planttasksetup.harvestingtasksetup.requestDtos.update.SubTaskWithIdRequestDto;
import com.shirohana.dto.planttasksetup.harvestingtasksetup.responseDtos.HarvestingTaskSetupResponseDto;
import com.shirohana.dto.planttasksetup.harvestingtasksetup.responseDtos.SubTaskResponseDto;
import com.shirohana.entity.planttasksetup.harvestingtasksetup.HarvestingTSSubTask;
import com.shirohana.entity.planttasksetup.harvestingtasksetup.HarvestingTaskSetup;
import com.shirohana.repository.planttasksetup.harvestingtasksetup.HarvestingTSSubTaskRepository;
import com.shirohana.repository.planttasksetup.harvestingtasksetup.HarvestingTaskSetupRepository;
import com.shirohana.service.interfaces.planttasksetup.HarvestingTaskSetupService;
import jakarta.persistence.EntityNotFoundException;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class HarvestingTaskSetupServiceImpl implements HarvestingTaskSetupService {

    private final HarvestingTaskSetupRepository harvestingTaskSetupRepository;
    private final HarvestingTSSubTaskRepository harvestingTSSubTaskRepository;

    @Transactional
    @Override
    public HarvestingTaskSetupResponseDto createTask(HarvestingTaskSetupCreateRequestDto requestDto) {
        try {
            validateTaskNameUniqueness(requestDto.getFamilyNameId(), requestDto.getTaskName());

            HarvestingTaskSetup taskSetup = mapToEntity(requestDto);
            taskSetup.setDeleted(false);

            taskSetup = harvestingTaskSetupRepository.save(taskSetup);
            saveSubTasks(requestDto.getSubTasks(), taskSetup);

            return mapToDto(taskSetup);
        } catch (DataIntegrityViolationException e) {
            log.error("Data integrity violation: {}", e.getMessage(), e);
            throw new RuntimeException("Data integrity violation: " + e.getMessage(), e);
        } catch (EntityNotFoundException e) {
            log.error("Entity not found: {}", e.getMessage(), e);
            throw new RuntimeException("Entity not found: " + e.getMessage(), e);
        } catch (RuntimeException e) {
            log.error("Validation error: {}", e.getMessage(), e);
            throw e;
        } catch (Exception e) {
            log.error("Unexpected error creating task: {}", e.getMessage(), e);
            throw new RuntimeException("Unexpected error creating task", e);
        }
    }

    @Transactional
    @Override
    public HarvestingTaskSetupResponseDto updateTask(Long id, HarvestingTaskSetupUpdateRequestDto requestDto) {
        try {
            validateTaskNameUniqueness(requestDto.getFamilyNameId(), requestDto.getTaskName(), id);

            HarvestingTaskSetup taskSetup = harvestingTaskSetupRepository.findByIdAndDeletedFalse(id)
                    .orElseThrow(() -> new EntityNotFoundException("Task not found"));

            updateEntity(taskSetup, requestDto);
            taskSetup = harvestingTaskSetupRepository.save(taskSetup);
            updateSubTasks(requestDto.getSubTasks(), taskSetup);

            return mapToDto(taskSetup);
        } catch (DataIntegrityViolationException e) {
            log.error("Data integrity violation: {}", e.getMessage(), e);
            throw new RuntimeException("Data integrity violation: " + e.getMessage(), e);
        } catch (EntityNotFoundException e) {
            log.error("Entity not found: {}", e.getMessage(), e);
            throw new RuntimeException("Entity not found: " + e.getMessage(), e);
        } catch (RuntimeException e) {
            log.error("Validation error: {}", e.getMessage(), e);
            throw e;
        } catch (Exception e) {
            log.error("Unexpected error updating task: {}", e.getMessage(), e);
            throw new RuntimeException("Unexpected error updating task", e);
        }
    }

    @Transactional
    @Override
    public void deleteTask(Long id) {
        try {
            HarvestingTaskSetup taskSetup = harvestingTaskSetupRepository.findByIdAndDeletedFalse(id)
                    .orElseThrow(() -> new EntityNotFoundException("Task not found"));

            taskSetup.setDeleted(true);
            taskSetup.setDeletedBy(getCurrentUsername());
            taskSetup.setDeletedAt(LocalDateTime.now());
            harvestingTaskSetupRepository.save(taskSetup);

            List<HarvestingTSSubTask> subTasks = harvestingTSSubTaskRepository.findByHarvestingTaskSetupIdAndDeletedFalse(taskSetup.getId());
            for (HarvestingTSSubTask subTask : subTasks) {
                subTask.setDeleted(true);
                subTask.setDeletedAt(LocalDateTime.now());
                subTask.setDeletedBy(getCurrentUsername());
                harvestingTSSubTaskRepository.save(subTask);
            }
        } catch (EntityNotFoundException e) {
            log.error("Entity not found: {}", e.getMessage(), e);
            throw new RuntimeException("Entity not found: " + e.getMessage(), e);
        } catch (Exception e) {
            log.error("Unexpected error deleting task: {}", e.getMessage(), e);
            throw new RuntimeException("Unexpected error deleting task", e);
        }
    }

    @Override
    public HarvestingTaskSetupResponseDto getTaskById(Long id) {
        try {
            HarvestingTaskSetup taskSetup = harvestingTaskSetupRepository.findByIdAndDeletedFalse(id)
                    .orElseThrow(() -> new EntityNotFoundException("Task not found"));
            return mapToDto(taskSetup);
        } catch (EntityNotFoundException e) {
            log.error("Entity not found: {}", e.getMessage(), e);
            throw new RuntimeException("Entity not found: " + e.getMessage(), e);
        }
    }

    @Override
    public List<HarvestingTaskSetupResponseDto> getTasksByFamilyNameId(Long familyNameId) {
        List<HarvestingTaskSetup> tasks = harvestingTaskSetupRepository.findByFamilyNameIdAndDeletedFalse(familyNameId);
        return tasks.stream().map(this::mapToDto).collect(Collectors.toList());
    }

    @Override
    public List<HarvestingTaskSetupResponseDto> getAllTasks() {
        List<HarvestingTaskSetup> tasks = harvestingTaskSetupRepository.findByDeletedFalse();
        return tasks.stream().map(this::mapToDto).collect(Collectors.toList());
    }

    private void validateTaskNameUniqueness(Long familyNameId, String taskName) {
        Optional<HarvestingTaskSetup> existingTask = harvestingTaskSetupRepository.findByFamilyNameIdAndTaskNameAndDeletedFalse(familyNameId, taskName);
        if (existingTask.isPresent()) {
            throw new RuntimeException("Task name must be unique for the same family name ID");
        }
    }

    private void validateTaskNameUniqueness(Long familyNameId, String taskName, Long id) {
        Optional<HarvestingTaskSetup> existingTask = harvestingTaskSetupRepository.findByFamilyNameIdAndTaskNameAndDeletedFalse(familyNameId, taskName);
        if (existingTask.isPresent() && !existingTask.get().getId().equals(id)) {
            throw new RuntimeException("Task name must be unique for the same family name ID");
        }
    }

    private HarvestingTaskSetup mapToEntity(HarvestingTaskSetupCreateRequestDto requestDto) {
        return HarvestingTaskSetup.builder()
                .familyNameId(requestDto.getFamilyNameId())
                .taskName(requestDto.getTaskName())
                .taskDescription(requestDto.getDescription())
                .noOfTerms(requestDto.getNoOfTerms())
                .timeUnit(requestDto.getTimeUnit())
                .uptoEffectiveStatus(requestDto.isUptoEffectiveStatus())
                .uptoEffectiveWeek(requestDto.getUptoEffectiveWeek())
                .deleted(false)
                .build();
    }

    private void updateEntity(HarvestingTaskSetup taskSetup, HarvestingTaskSetupUpdateRequestDto requestDto) {
        taskSetup.setFamilyNameId(requestDto.getFamilyNameId());
        taskSetup.setTaskName(requestDto.getTaskName());
        taskSetup.setTaskDescription(requestDto.getDescription());
        taskSetup.setNoOfTerms(requestDto.getNoOfTerms());
        taskSetup.setTimeUnit(requestDto.getTimeUnit());
        taskSetup.setUptoEffectiveStatus(requestDto.isUptoEffectiveStatus());
        taskSetup.setUptoEffectiveWeek(requestDto.getUptoEffectiveWeek());
    }

    private void saveSubTasks(List<SubTaskRequestDto> subTaskDtos, HarvestingTaskSetup taskSetup) {
        for (SubTaskRequestDto subTaskDto : subTaskDtos) {
            HarvestingTSSubTask subTask = HarvestingTSSubTask.builder()
                    .subTaskName(subTaskDto.getSubTaskName())
                    .description(subTaskDto.getDescription())
                    .harvestingTaskSetupId(taskSetup.getId())
                    .deleted(false)
                    .build();
            harvestingTSSubTaskRepository.save(subTask);
        }
    }

    private void updateSubTasks(List<SubTaskWithIdRequestDto> subTaskDtos, HarvestingTaskSetup taskSetup) {
        List<Long> updatedSubTaskIds = subTaskDtos.stream()
                .map(SubTaskWithIdRequestDto::getId)
                .collect(Collectors.toList());

        List<HarvestingTSSubTask> existingSubTasks = harvestingTSSubTaskRepository.findByHarvestingTaskSetupIdAndDeletedFalse(taskSetup.getId());

        for (HarvestingTSSubTask existingSubTask : existingSubTasks) {
            if (!updatedSubTaskIds.contains(existingSubTask.getId())) {
                existingSubTask.setDeleted(true);
                existingSubTask.setDeletedAt(LocalDateTime.now());
                existingSubTask.setDeletedBy(getCurrentUsername());
                harvestingTSSubTaskRepository.save(existingSubTask);
            }
        }

        for (SubTaskWithIdRequestDto subTaskDto : subTaskDtos) {
            HarvestingTSSubTask subTask;
            if (subTaskDto.getId() != null) {
                subTask = harvestingTSSubTaskRepository.findById(subTaskDto.getId())
                        .orElseThrow(() -> new RuntimeException("Sub-task not found with id " + subTaskDto.getId()));
                subTask.setSubTaskName(subTaskDto.getSubTaskName());
                subTask.setDescription(subTaskDto.getDescription());
                subTask.setLastModifiedBy(getCurrentUsername());
                subTask.setLastModifiedDate(LocalDateTime.now());
            } else {
                subTask = HarvestingTSSubTask.builder()
                        .subTaskName(subTaskDto.getSubTaskName())
                        .description(subTaskDto.getDescription())
                        .harvestingTaskSetupId(taskSetup.getId())
                        .deleted(false)
                        .build();
                subTask.setCreatedBy(getCurrentUsername());
                subTask.setCreatedDate(LocalDateTime.now());
            }
            harvestingTSSubTaskRepository.save(subTask);
        }
    }

    private HarvestingTaskSetupResponseDto mapToDto(HarvestingTaskSetup taskSetup) {
        List<HarvestingTSSubTask> subTasks = harvestingTSSubTaskRepository.findByHarvestingTaskSetupIdAndDeletedFalse(taskSetup.getId());

        List<SubTaskResponseDto> subTaskResponseDtos = subTasks.stream()
                .map(subTask -> SubTaskResponseDto.builder()
                        .id(subTask.getId())
                        .subTaskName(subTask.getSubTaskName())
                        .description(subTask.getDescription())
                        .createdBy(subTask.getCreatedBy())
                        .createdDate(subTask.getCreatedDate())
                        .lastModifiedBy(subTask.getLastModifiedBy())
                        .lastModifiedDate(subTask.getLastModifiedDate())
                        .deletedAt(subTask.getDeletedAt())
                        .deletedBy(subTask.getDeletedBy())
                        .build())
                .collect(Collectors.toList());

        return HarvestingTaskSetupResponseDto.builder()
                .id(taskSetup.getId())
                .taskName(taskSetup.getTaskName())
                .taskDescription(taskSetup.getTaskDescription())
                .familyNameId(taskSetup.getFamilyNameId())
                .taskType(TaskTypeConstants.HARVESTING)
                .noOfTerms(taskSetup.getNoOfTerms())
                .timeUnit(taskSetup.getTimeUnit())
                .uptoEffectiveStatus(taskSetup.isUptoEffectiveStatus())
                .uptoEffectiveWeek(taskSetup.getUptoEffectiveWeek())
                .subTasks(subTaskResponseDtos)
                .prodPlanId(taskSetup.getProdPlanId())
                .createdBy(taskSetup.getCreatedBy())
                .createdDate(taskSetup.getCreatedDate())
                .lastModifiedBy(taskSetup.getLastModifiedBy())
                .lastModifiedDate(taskSetup.getLastModifiedDate())
                .deletedAt(taskSetup.getDeletedAt())
                .deletedBy(taskSetup.getDeletedBy())
                .build();
    }

    private String getCurrentUsername() {
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (authentication != null && authentication.getPrincipal() instanceof UserDetails) {
                return ((UserDetails) authentication.getPrincipal()).getUsername();
            }
        } catch (Exception e) {
            log.error("Error retrieving current username", e);
        }
        return null;
    }
}
