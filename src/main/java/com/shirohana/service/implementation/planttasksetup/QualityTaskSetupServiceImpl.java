package com.shirohana.service.implementation.planttasksetup;


import com.shirohana.constants.TaskTypeConstants;
import com.shirohana.dto.planttasksetup.qualitytasksetup.requestdtos.create.QualityTaskSetupCreateRequestDto;
import com.shirohana.dto.planttasksetup.qualitytasksetup.requestdtos.create.SubTaskRequestDto;
import com.shirohana.dto.planttasksetup.qualitytasksetup.requestdtos.update.QualityTaskSetupUpdateRequestDto;
import com.shirohana.dto.planttasksetup.qualitytasksetup.requestdtos.update.SubTaskWithIdRequestDto;
import com.shirohana.dto.planttasksetup.qualitytasksetup.responsedtos.QualityTaskSetupResponseDto;
import com.shirohana.dto.planttasksetup.qualitytasksetup.responsedtos.SubTaskResponseDto;
import com.shirohana.entity.planttasksetup.qualitytasksetup.QualityTSSubTask;
import com.shirohana.entity.planttasksetup.qualitytasksetup.QualityTaskSetup;
import com.shirohana.repository.planttasksetup.qualitytasksetup.QualityTSSubTaskRepository;
import com.shirohana.repository.planttasksetup.qualitytasksetup.QualityTaskSetupRepository;
import com.shirohana.service.interfaces.planttasksetup.QualityTaskSetupService;
import jakarta.persistence.EntityNotFoundException;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class QualityTaskSetupServiceImpl implements QualityTaskSetupService {

    private final QualityTaskSetupRepository qualityTaskSetupRepository;
    private final QualityTSSubTaskRepository qualityTSSubTaskRepository;

    @Transactional
    @Override
    public QualityTaskSetupResponseDto createTask(QualityTaskSetupCreateRequestDto requestDto) {
        try {
            validateTaskNameUniqueness(requestDto.getFamilyNameId(), requestDto.getTaskName());

            QualityTaskSetup taskSetup = mapToEntity(requestDto);
            taskSetup.setDeleted(false);

            taskSetup = qualityTaskSetupRepository.save(taskSetup);
            saveSubTasks(requestDto.getSubTasks(), taskSetup);

            return mapToDto(taskSetup);
        } catch (DataIntegrityViolationException e) {
            log.error("Data integrity violation: {}", e.getMessage(), e);
            throw new RuntimeException("Data integrity violation: " + e.getMessage(), e);
        } catch (EntityNotFoundException e) {
            log.error("Entity not found: {}", e.getMessage(), e);
            throw new RuntimeException("Entity not found: " + e.getMessage(), e);
        } catch (RuntimeException e) {
            log.error("Validation error: {}", e.getMessage(), e);
            throw e;
        } catch (Exception e) {
            log.error("Unexpected error creating task: {}", e.getMessage(), e);
            throw new RuntimeException("Unexpected error creating task", e);
        }
    }

    @Transactional
    @Override
    public QualityTaskSetupResponseDto updateTask(Long id, QualityTaskSetupUpdateRequestDto requestDto) {
        try {
            validateTaskNameUniqueness(requestDto.getFamilyNameId(), requestDto.getTaskName(), id);

            QualityTaskSetup taskSetup = qualityTaskSetupRepository.findByIdAndDeletedFalse(id)
                    .orElseThrow(() -> new EntityNotFoundException("Task not found"));

            updateEntity(taskSetup, requestDto);
            taskSetup = qualityTaskSetupRepository.save(taskSetup);
            updateSubTasks(requestDto.getSubTasks(), taskSetup);

            return mapToDto(taskSetup);
        } catch (DataIntegrityViolationException e) {
            log.error("Data integrity violation: {}", e.getMessage(), e);
            throw new RuntimeException("Data integrity violation: " + e.getMessage(), e);
        } catch (EntityNotFoundException e) {
            log.error("Entity not found: {}", e.getMessage(), e);
            throw new RuntimeException("Entity not found: " + e.getMessage(), e);
        } catch (RuntimeException e) {
            log.error("Validation error: {}", e.getMessage(), e);
            throw e;
        } catch (Exception e) {
            log.error("Unexpected error updating task: {}", e.getMessage(), e);
            throw new RuntimeException("Unexpected error updating task", e);
        }
    }

    @Transactional
    @Override
    public void deleteTask(Long id) {
        try {
            QualityTaskSetup taskSetup = qualityTaskSetupRepository.findByIdAndDeletedFalse(id)
                    .orElseThrow(() -> new EntityNotFoundException("Task not found"));

            taskSetup.setDeleted(true);
            taskSetup.setDeletedBy(getCurrentUsername());
            taskSetup.setDeletedAt(LocalDateTime.now());
            qualityTaskSetupRepository.save(taskSetup);

            List<QualityTSSubTask> subTasks = qualityTSSubTaskRepository.findByQualityTaskSetupIdAndDeletedFalse(taskSetup.getId());
            for (QualityTSSubTask subTask : subTasks) {
                subTask.setDeleted(true);
                subTask.setDeletedAt(LocalDateTime.now());
                subTask.setDeletedBy(getCurrentUsername());
                qualityTSSubTaskRepository.save(subTask);
            }
        } catch (EntityNotFoundException e) {
            log.error("Entity not found: {}", e.getMessage(), e);
            throw new RuntimeException("Entity not found: " + e.getMessage(), e);
        } catch (Exception e) {
            log.error("Unexpected error deleting task: {}", e.getMessage(), e);
            throw new RuntimeException("Unexpected error deleting task", e);
        }
    }

    @Override
    public QualityTaskSetupResponseDto getTaskById(Long id) {
        try {
            QualityTaskSetup taskSetup = qualityTaskSetupRepository.findByIdAndDeletedFalse(id)
                    .orElseThrow(() -> new EntityNotFoundException("Task not found"));
            return mapToDto(taskSetup);
        } catch (EntityNotFoundException e) {
            log.error("Entity not found: {}", e.getMessage(), e);
            throw new RuntimeException("Entity not found: " + e.getMessage(), e);
        }
    }

    @Override
    public List<QualityTaskSetupResponseDto> getTasksByFamilyNameId(Long familyNameId) {
        List<QualityTaskSetup> tasks = qualityTaskSetupRepository.findByFamilyNameIdAndDeletedFalse(familyNameId);
        return tasks.stream().map(this::mapToDto).collect(Collectors.toList());
    }

    @Override
    public List<QualityTaskSetupResponseDto> getAllTasks() {
        List<QualityTaskSetup> tasks = qualityTaskSetupRepository.findByDeletedFalse();
        return tasks.stream().map(this::mapToDto).collect(Collectors.toList());
    }

    private void validateTaskNameUniqueness(Long familyNameId, String taskName) {
        Optional<QualityTaskSetup> existingTask = qualityTaskSetupRepository.findByFamilyNameIdAndTaskNameAndDeletedFalse(familyNameId, taskName);
        if (existingTask.isPresent()) {
            throw new RuntimeException("Task name must be unique for the same family name ID");
        }
    }

    private void validateTaskNameUniqueness(Long familyNameId, String taskName, Long id) {
        Optional<QualityTaskSetup> existingTask = qualityTaskSetupRepository.findByFamilyNameIdAndTaskNameAndDeletedFalse(familyNameId, taskName);
        if (existingTask.isPresent() && !existingTask.get().getId().equals(id)) {
            throw new RuntimeException("Task name must be unique for the same family name ID");
        }
    }

    private QualityTaskSetup mapToEntity(QualityTaskSetupCreateRequestDto requestDto) {
        return QualityTaskSetup.builder()
                .familyNameId(requestDto.getFamilyNameId())
                .taskName(requestDto.getTaskName())
                .taskDescription(requestDto.getDescription())
                .noOfTerms(requestDto.getNoOfTerms())
                .timeUnit(requestDto.getTimeUnit())
                .uptoEffectiveStatus(requestDto.isUptoEffectiveStatus())
                .uptoEffectiveWeek(requestDto.getUptoEffectiveWeek())
                .deleted(false)
                .build();
    }

    private void updateEntity(QualityTaskSetup taskSetup, QualityTaskSetupUpdateRequestDto requestDto) {
        taskSetup.setFamilyNameId(requestDto.getFamilyNameId());
        taskSetup.setTaskName(requestDto.getTaskName());
        taskSetup.setTaskDescription(requestDto.getDescription());
        taskSetup.setNoOfTerms(requestDto.getNoOfTerms());
        taskSetup.setTimeUnit(requestDto.getTimeUnit());
        taskSetup.setUptoEffectiveStatus(requestDto.isUptoEffectiveStatus());
        taskSetup.setUptoEffectiveWeek(requestDto.getUptoEffectiveWeek());
    }

    private void saveSubTasks(List<SubTaskRequestDto> subTaskDtos, QualityTaskSetup taskSetup) {
        for (SubTaskRequestDto subTaskDto : subTaskDtos) {
            QualityTSSubTask subTask = QualityTSSubTask.builder()
                    .subTaskName(subTaskDto.getSubTaskName())
                    .description(subTaskDto.getDescription())
                    .qualityTaskSetupId(taskSetup.getId())
                    .deleted(false)
                    .build();
            qualityTSSubTaskRepository.save(subTask);
        }
    }

    private void updateSubTasks(List<SubTaskWithIdRequestDto> subTaskDtos, QualityTaskSetup taskSetup) {
        List<Long> updatedSubTaskIds = subTaskDtos.stream()
                .map(SubTaskWithIdRequestDto::getId)
                .collect(Collectors.toList());

        List<QualityTSSubTask> existingSubTasks = qualityTSSubTaskRepository.findByQualityTaskSetupIdAndDeletedFalse(taskSetup.getId());

        for (QualityTSSubTask existingSubTask : existingSubTasks) {
            if (!updatedSubTaskIds.contains(existingSubTask.getId())) {
                existingSubTask.setDeleted(true);
                existingSubTask.setDeletedAt(LocalDateTime.now());
                existingSubTask.setDeletedBy(getCurrentUsername());
                qualityTSSubTaskRepository.save(existingSubTask);
            }
        }

        for (SubTaskWithIdRequestDto subTaskDto : subTaskDtos) {
            QualityTSSubTask subTask;
            if (subTaskDto.getId() != null) {
                subTask = qualityTSSubTaskRepository.findById(subTaskDto.getId())
                        .orElseThrow(() -> new RuntimeException("Sub-task not found with id " + subTaskDto.getId()));
                subTask.setSubTaskName(subTaskDto.getSubTaskName());
                subTask.setDescription(subTaskDto.getDescription());
                subTask.setLastModifiedBy(getCurrentUsername());
                subTask.setLastModifiedDate(LocalDateTime.now());
            } else {
                subTask = QualityTSSubTask.builder()
                        .subTaskName(subTaskDto.getSubTaskName())
                        .description(subTaskDto.getDescription())
                        .qualityTaskSetupId(taskSetup.getId())
                        .deleted(false)
                        .build();
                subTask.setCreatedBy(getCurrentUsername());
                subTask.setCreatedDate(LocalDateTime.now());
            }
            qualityTSSubTaskRepository.save(subTask);
        }
    }

    private QualityTaskSetupResponseDto mapToDto(QualityTaskSetup taskSetup) {
        List<QualityTSSubTask> subTasks = qualityTSSubTaskRepository.findByQualityTaskSetupIdAndDeletedFalse(taskSetup.getId());

        List<SubTaskResponseDto> subTaskResponseDtos = subTasks.stream()
                .map(subTask -> SubTaskResponseDto.builder()
                        .id(subTask.getId())
                        .subTaskName(subTask.getSubTaskName())
                        .description(subTask.getDescription())
                        .createdBy(subTask.getCreatedBy())
                        .createdDate(subTask.getCreatedDate())
                        .lastModifiedBy(subTask.getLastModifiedBy())
                        .lastModifiedDate(subTask.getLastModifiedDate())
                        .deletedAt(subTask.getDeletedAt())
                        .deletedBy(subTask.getDeletedBy())
                        .build())
                .collect(Collectors.toList());

        return QualityTaskSetupResponseDto.builder()
                .id(taskSetup.getId())
                .taskName(taskSetup.getTaskName())
                .taskDescription(taskSetup.getTaskDescription())
                .familyNameId(taskSetup.getFamilyNameId())
                .taskType(TaskTypeConstants.QUALITY)
                .noOfTerms(taskSetup.getNoOfTerms())
                .timeUnit(taskSetup.getTimeUnit())
                .uptoEffectiveStatus(taskSetup.isUptoEffectiveStatus())
                .uptoEffectiveWeek(taskSetup.getUptoEffectiveWeek())
                .subTasks(subTaskResponseDtos)
                .prodPlanId(taskSetup.getProdPlanId())
                .createdBy(taskSetup.getCreatedBy())
                .createdDate(taskSetup.getCreatedDate())
                .lastModifiedBy(taskSetup.getLastModifiedBy())
                .lastModifiedDate(taskSetup.getLastModifiedDate())
                .deletedAt(taskSetup.getDeletedAt())
                .deletedBy(taskSetup.getDeletedBy())
                .build();
    }

    private String getCurrentUsername() {
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (authentication != null && authentication.getPrincipal() instanceof UserDetails) {
                return ((UserDetails) authentication.getPrincipal()).getUsername();
            }
        } catch (Exception e) {
            log.error("Error retrieving current username", e);
        }
        return null;
    }
}
