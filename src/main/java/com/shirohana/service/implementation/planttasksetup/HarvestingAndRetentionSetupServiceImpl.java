package com.shirohana.service.implementation.planttasksetup;


import com.shirohana.dto.planttasksetup.harvestingandretention.HarvestingAndRetentionSetupRequestDto;
import com.shirohana.dto.planttasksetup.harvestingandretention.HarvestingAndRetentionSetupResponseDto;
import com.shirohana.entity.planttasksetup.HarvestingAndRetentionSetup;
import com.shirohana.exception.ResourceNotFoundException;
import com.shirohana.repository.planttasksetup.HarvestingAndRetentionSetupRepository;
import com.shirohana.service.interfaces.planttasksetup.HarvestingAndRetentionSetupService;
import jakarta.transaction.Transactional;
import jakarta.validation.ValidationException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class HarvestingAndRetentionSetupServiceImpl implements HarvestingAndRetentionSetupService {
    private final HarvestingAndRetentionSetupRepository repository;

    @Transactional
    @Override
    public HarvestingAndRetentionSetupResponseDto createHarvestingAndRetentionSetup(HarvestingAndRetentionSetupRequestDto requestDto) {
        try {
            validateUniqueFamilyNameId(requestDto.getFamilyNameId());

            HarvestingAndRetentionSetup setup = buildEntityFromRequest(requestDto);
            setup.setCreatedBy(getCurrentUsername());
            setup.setCreatedDate(LocalDateTime.now());

            HarvestingAndRetentionSetup savedSetup = repository.save(setup);
            return convertToResponseDto(savedSetup);
        } catch (ValidationException ex) {
            log.error("Validation error while creating setup: {}", requestDto, ex);
            throw ex;
        } catch (Exception ex) {
            log.error("Unexpected error while creating setup: {}", requestDto, ex);
            throw new RuntimeException("Error creating setup", ex);
        }
    }

    @Transactional
    @Override
    public HarvestingAndRetentionSetupResponseDto updateHarvestingAndRetentionSetup(Long id, HarvestingAndRetentionSetupRequestDto requestDto) {
        try {
            HarvestingAndRetentionSetup existingSetup = repository.findByIdAndDeletedFalse(id)
                    .orElseThrow(() -> new ResourceNotFoundException("Setup not found with id " + id));

            validateUniqueFamilyNameIdForUpdate(requestDto.getFamilyNameId(), id);

            updateEntityFromRequest(existingSetup, requestDto);
            existingSetup.setLastModifiedBy(getCurrentUsername());
            existingSetup.setLastModifiedDate(LocalDateTime.now());

            HarvestingAndRetentionSetup updatedSetup = repository.save(existingSetup);
            return convertToResponseDto(updatedSetup);
        } catch (ResourceNotFoundException ex) {
            log.error("Setup not found with id: {}", id, ex);
            throw ex;
        } catch (ValidationException ex) {
            log.error("Validation error while updating setup with id {}: {}", id, requestDto, ex);
            throw ex;
        } catch (Exception ex) {
            log.error("Unexpected error while updating setup with id {}: {}", id, requestDto, ex);
            throw new RuntimeException("Error updating setup", ex);
        }
    }

    @Transactional
    @Override
    public void deleteHarvestingAndRetentionSetup(Long id) {
        try {
            HarvestingAndRetentionSetup existingSetup = repository.findByIdAndDeletedFalse(id)
                    .orElseThrow(() -> new ResourceNotFoundException("Setup not found with id " + id));

            existingSetup.setDeleted(true);
            existingSetup.setDeletedBy(getCurrentUsername());
            existingSetup.setDeletedAt(LocalDateTime.now());

            repository.save(existingSetup);
        } catch (ResourceNotFoundException ex) {
            log.error("Setup not found with id: {}", id, ex);
            throw ex;
        } catch (Exception ex) {
            log.error("Unexpected error while deleting setup with id: {}", id, ex);
            throw new RuntimeException("Error deleting setup", ex);
        }
    }

    @Override
    public HarvestingAndRetentionSetupResponseDto getHarvestingAndRetentionSetupById(Long id) {
        try {
            HarvestingAndRetentionSetup setup = repository.findByIdAndDeletedFalse(id)
                    .orElseThrow(() -> new ResourceNotFoundException("Setup not found with id " + id));
            return convertToResponseDto(setup);
        } catch (ResourceNotFoundException ex) {
            log.error("Setup not found with id: {}", id, ex);
            throw ex;
        } catch (Exception ex) {
            log.error("Unexpected error while retrieving setup with id: {}", id, ex);
            throw new RuntimeException("Error retrieving setup", ex);
        }
    }

    @Override
    public List<HarvestingAndRetentionSetupResponseDto> getAllHarvestingAndRetentionSetups() {
        try {
            List<HarvestingAndRetentionSetup> setups = repository.findAllByDeletedFalse();
            return setups.stream()
                    .map(this::convertToResponseDto)
                    .collect(Collectors.toList());
        } catch (Exception ex) {
            log.error("Unexpected error while retrieving all setups", ex);
            throw new RuntimeException("Error retrieving setups", ex);
        }
    }

    @Override
    public HarvestingAndRetentionSetupResponseDto getHarvestingAndRetentionSetupByFamilyNameId(Long familyNameId) {
        try {
            HarvestingAndRetentionSetup setup = repository.findByFamilyNameIdAndDeletedFalse(familyNameId)
                    .orElseThrow(() -> new ResourceNotFoundException("Setup not found with family ID " + familyNameId));
            return convertToResponseDto(setup);
        } catch (ResourceNotFoundException ex) {
            log.error("Setup not found with family ID {}: {}", familyNameId, ex);
            throw ex;
        } catch (Exception ex) {
            log.error("Unexpected error while retrieving setup with family ID {}: {}", familyNameId, ex);
            throw new RuntimeException("Error retrieving setup", ex);
        }
    }

    private void validateUniqueFamilyNameId(Long familyNameId) {
        if (repository.findByFamilyNameIdAndDeletedFalse(familyNameId).isPresent()) {
            throw new ValidationException("Setup with family ID " + familyNameId + " already exists.");
        }
    }

    private void validateUniqueFamilyNameIdForUpdate(Long familyNameId, Long id) {
        Optional<HarvestingAndRetentionSetup> existingSetup = repository.findByFamilyNameIdAndDeletedFalse(familyNameId);
        if (existingSetup.isPresent() && !existingSetup.get().getId().equals(id)) {
            throw new ValidationException("Setup with family ID " + familyNameId + " already exists.");
        }
    }

    private HarvestingAndRetentionSetup buildEntityFromRequest(HarvestingAndRetentionSetupRequestDto requestDto) {
        return HarvestingAndRetentionSetup.builder()
                .familyNameId(requestDto.getFamilyNameId())
                .prodPlantHarvestDuration(requestDto.getProdPlantHarvestDuration())
                .retentionProdPlant(requestDto.isRetentionProdPlant())
                .prodPlantRetentionHarvestDuration(requestDto.getProdPlantRetentionHarvestDuration())
                .prodPlantNumOfRetentions(requestDto.getProdPlantNumOfRetentions())
                .mothPlantHarvestingDuration(requestDto.getMothPlantHarvestingDuration())
                .retentionMothPlant(requestDto.isRetentionMothPlant())
                .mothPlantRetentionHarvestDuration(requestDto.getMothPlantRetentionHarvestDuration())
                .mothPlantNumOfRetentions(requestDto.getMothPlantNumOfRetentions())
                .preNurseryDuration(requestDto.getPreNurseryDuration())
                .nurseryDuration(requestDto.getNurseryDuration())
                .build();
    }

    private void updateEntityFromRequest(HarvestingAndRetentionSetup existingSetup, HarvestingAndRetentionSetupRequestDto requestDto) {
        existingSetup.setFamilyNameId(requestDto.getFamilyNameId());
        existingSetup.setProdPlantHarvestDuration(requestDto.getProdPlantHarvestDuration());
        existingSetup.setRetentionProdPlant(requestDto.isRetentionProdPlant());
        existingSetup.setProdPlantRetentionHarvestDuration(requestDto.getProdPlantRetentionHarvestDuration());
        existingSetup.setProdPlantNumOfRetentions(requestDto.getProdPlantNumOfRetentions());
        existingSetup.setMothPlantHarvestingDuration(requestDto.getMothPlantHarvestingDuration());
        existingSetup.setRetentionMothPlant(requestDto.isRetentionMothPlant());
        existingSetup.setMothPlantRetentionHarvestDuration(requestDto.getMothPlantRetentionHarvestDuration());
        existingSetup.setMothPlantNumOfRetentions(requestDto.getMothPlantNumOfRetentions());
        existingSetup.setPreNurseryDuration(requestDto.getPreNurseryDuration());
        existingSetup.setNurseryDuration(requestDto.getNurseryDuration());
    }

    private HarvestingAndRetentionSetupResponseDto convertToResponseDto(HarvestingAndRetentionSetup setup) {
        return HarvestingAndRetentionSetupResponseDto.builder()
                .id(setup.getId())
                .familyNameId(setup.getFamilyNameId())
                .prodPlantHarvestDuration(setup.getProdPlantHarvestDuration())
                .retentionProdPlant(setup.isRetentionProdPlant())
                .prodPlantRetentionHarvestDuration(setup.getProdPlantRetentionHarvestDuration())
                .prodPlantNumOfRetentions(setup.getProdPlantNumOfRetentions())
                .mothPlantHarvestingDuration(setup.getMothPlantHarvestingDuration())
                .retentionMothPlant(setup.isRetentionMothPlant())
                .mothPlantRetentionHarvestDuration(setup.getMothPlantRetentionHarvestDuration())
                .mothPlantNumOfRetentions(setup.getMothPlantNumOfRetentions())
                .preNurseryDuration(setup.getPreNurseryDuration())
                .nurseryDuration(setup.getNurseryDuration())
                .createdBy(setup.getCreatedBy())
                .createdAt(setup.getCreatedDate())
                .updatedBy(setup.getLastModifiedBy())
                .updatedAt(setup.getLastModifiedDate())
                .deletedBy(setup.getDeletedBy())
                .deletedAt(setup.getDeletedAt())
                .build();
    }

    private String getCurrentUsername() {
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (authentication != null && authentication.getPrincipal() instanceof UserDetails) {
                return ((UserDetails) authentication.getPrincipal()).getUsername();
            }
        } catch (Exception e) {
            log.error("Error retrieving current username", e);
        }
        return null;
    }
    
}
