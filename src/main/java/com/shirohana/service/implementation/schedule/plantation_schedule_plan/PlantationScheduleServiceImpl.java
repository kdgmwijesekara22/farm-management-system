package com.shirohana.service.implementation.schedule.plantation_schedule_plan;

import com.shirohana.dto.schedule.pshedule.*;
import com.shirohana.dto.schedule.psplan.*;
import com.shirohana.entity.plant.Plant;
import com.shirohana.entity.schedule.plantation_plan.PlantationPlan;
import com.shirohana.entity.schedule.plantation_plan.PlantationPlanFarm;
import com.shirohana.entity.schedule.plantation_plan.PlantationPlanFarmTotalQuantityVariety;
import com.shirohana.entity.schedule.plantation_plan.PlantationPlanQuantityWeeks;
import com.shirohana.entity.schedule.plantation_schedule.PlantationScheduleVariety;
import com.shirohana.entity.schedule.plantation_schedule.PlantationScheduleQuantityWeeks;
import com.shirohana.entity.schedule.plantation_schedule.PlantationSchedule;
import com.shirohana.entity.schedule.production_plan.ProductionPlanFarmManager;
import com.shirohana.entity.variety.Variety;
import com.shirohana.repository.plant.PlantRepository;
import com.shirohana.repository.production_plan.ProductionPlanFarmManagerRepository;
import com.shirohana.repository.shedule.pplan.PlantationPlanFarmQuantityVarietyRepository;
import com.shirohana.repository.shedule.pplan.PlantationPlanFarmRepository;
import com.shirohana.repository.shedule.pplan.PlantationPlanQuantityWeeksRepository;
import com.shirohana.repository.shedule.pplan.PlantationPlanRepository;
import com.shirohana.repository.shedule.pschedule.PScheduleVarietyRepository;
import com.shirohana.repository.shedule.pschedule.PScheduleQuantityWeeksRepository;
import com.shirohana.repository.shedule.pschedule.PlantationScheduleRepository;
import com.shirohana.repository.variety.VarietyRepository;
import com.shirohana.service.interfaces.schedule.PlantationScheduleService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.temporal.WeekFields;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class PlantationScheduleServiceImpl implements PlantationScheduleService {

    private final PlantationScheduleRepository plantationScheduleRepository;
    private final PScheduleVarietyRepository pScheduleVarietyRepository;
    private final PScheduleQuantityWeeksRepository pScheduleQuantityWeeksRepository;
    private final PlantRepository plantRepository;
    private final VarietyRepository varietyRepository;
    private final PlantationPlanRepository plantationPlanRepository;
    private final PlantationPlanFarmRepository plantationPlanFarmRepository;
    private final PlantationPlanFarmQuantityVarietyRepository plantationPlanFarmQuantityVarietyRepository;
    private final PlantationPlanQuantityWeeksRepository plantationPlanQuantityWeeksRepository;
    private final ProductionPlanFarmManagerRepository productionPlanFarmManagerRepository;

    @Override
    @Transactional
    public PlantationScheduleResponseDto saveSchedule(PlantationScheduleRequestDto plantationScheduleRequestDto) {
        try {
            // Check if the family (plant) exists in the Plant table
            Plant plant = plantRepository.findById(plantationScheduleRequestDto.getFamilyId())
                    .orElseThrow(() -> new IllegalArgumentException("Family does not exist"));

            // Check if the varieties in the request already exist in the Variety table
            for (PScheduleVarietyRequestDto varietyDto : plantationScheduleRequestDto.getPScheduleVarietyRequestDtoList()) {
                Long varietyId = Long.parseLong(varietyDto.getVarietyId());
                Variety variety = varietyRepository.findById(varietyId)
                        .orElseThrow(() -> new IllegalArgumentException("Variety does not exist"));

                // Check if the variety belongs to the same family
                if (!variety.getFamilyNameId().equals(plantationScheduleRequestDto.getFamilyId())) {
                    throw new IllegalArgumentException("Variety ID " + varietyId + " does not belong to the family ID " + plantationScheduleRequestDto.getFamilyId());
                }
            }

            // Check if a schedule with the same name already exists
            if (plantationScheduleRepository.findByNameAndDeletedFalse(plantationScheduleRequestDto.getName()).isPresent()) {
                throw new IllegalArgumentException("A schedule with the name " + plantationScheduleRequestDto.getName() + " already exists.");
            }

            // Save PlantationSchedule
            PlantationSchedule plantationSchedule = PlantationSchedule.builder()
                    .name(plantationScheduleRequestDto.getName())
                    .familyId(plantationScheduleRequestDto.getFamilyId())
                    .purpose(plantationScheduleRequestDto.getPurpose())
                    .hDuration(plantationScheduleRequestDto.getHDuration())
                    .plantationStartDate(plantationScheduleRequestDto.getPlantationStartDate())
                    .plantationEndDate(plantationScheduleRequestDto.getPlantationEndDate())
                    .startWeek(plantationScheduleRequestDto.getStartWeek())
                    .endWeek(plantationScheduleRequestDto.getEndWeek())
                    .orientation(plantationScheduleRequestDto.getOrientation())  // <-- Add this line
                    .build();

            PlantationSchedule savedSchedule = plantationScheduleRepository.save(plantationSchedule);

            // Save PScheduleVariety and PScheduleQuantityWeeks
            List<PlantationScheduleVariety> savedVarieties = plantationScheduleRequestDto.getPScheduleVarietyRequestDtoList().stream()
                    .map(varietyDto -> {
                        PlantationScheduleVariety variety = PlantationScheduleVariety.builder()
                                .plantationScheduleId(savedSchedule.getId())
                                .verityId(Long.parseLong(varietyDto.getVarietyId()))
                                .verityName(varietyDto.getVarietyName())
                                .build();
                        PlantationScheduleVariety savedVariety = pScheduleVarietyRepository.save(variety);

                        List<PlantationScheduleQuantityWeeks> quantityWeeks = varietyDto.getPScheduleQuantityWeeksRequestDtoList().stream()
                                .map(weeksDto -> PlantationScheduleQuantityWeeks.builder()
                                        .PScheduleVarietyId(savedVariety.getId())
                                        .transplantWeek(Integer.parseInt(weeksDto.getTransplantWeek()))
                                        .harvestWeek(Integer.parseInt(weeksDto.getHarvestWeek()))
                                        .quantity(Integer.parseInt(weeksDto.getQuantity()))
                                        .build())
                                .collect(Collectors.toList());
                        pScheduleQuantityWeeksRepository.saveAll(quantityWeeks);

                        int totalQuantity = quantityWeeks.stream().mapToInt(PlantationScheduleQuantityWeeks::getQuantity).sum();
                        savedVariety.setTotalQuantity(totalQuantity);
                        pScheduleVarietyRepository.save(savedVariety);

                        return savedVariety;
                    })
                    .collect(Collectors.toList());

            return convertToResponseDto(savedSchedule, savedVarieties);
        } catch (Exception ex) {
            log.error("Error saving plantation schedule", ex);
            throw ex;
        }
    }

    @Override
    @Transactional
    public boolean deletePlantationSchedule(Long scheduleId) {
        try {
            // Find the PlantationSchedule by ID
            PlantationSchedule schedule = plantationScheduleRepository.findByIdAndDeletedFalse(scheduleId);
            if (schedule == null) {
                throw new IllegalArgumentException("PlantationSchedule does not exist or is already deleted.");
            }

            // Check if there are any associated PlantationPlans that are not deleted
            List<PlantationPlan> associatedPlans = plantationPlanRepository.findByPlantationScheduleIdAndDeletedFalse(schedule.getId());
            if (!associatedPlans.isEmpty()) {
                throw new IllegalStateException("Cannot delete PlantationSchedule because there are associated PlantationPlans that are not deleted.");
            }

            // Mark the schedule as deleted and expired
            schedule.setDeleted(true);
            schedule.setExpired(true);
            plantationScheduleRepository.save(schedule);

            return true; // Return true if deletion is successful
        } catch (IllegalStateException e) {
            log.error("Cannot delete PlantationSchedule: {}", e.getMessage());
            return false; // Return false if deletion cannot be performed due to associated plans
        } catch (Exception ex) {
            log.error("Error deleting plantation schedule", ex);
            return false; // Return false if an unexpected error occurs
        }
    }

    @Transactional
    @Override
    public boolean deletePlantationPlan(Long planId) {
        try {
            // Find the PlantationPlan by ID
            PlantationPlan plantationPlan = plantationPlanRepository.findById(planId)
                    .orElseThrow(() -> new IllegalArgumentException("PlantationPlan  does not exist."));

            // Check if the plan is already deleted or expired
            if (plantationPlan.isDeleted() || plantationPlan.isExpired()) {
                log.warn("Cannot delete PlantationPlan with ID {} because it is already deleted or expired.", planId);
                return false;
            }

            // Check if there are any associated ProductionPlanFarmManager records that are not deleted and reference this plan
            boolean isReferenced = productionPlanFarmManagerRepository.existsByPlantationPlanIdAndDeletedFalse(planId);
            if (isReferenced) {
                log.warn("Cannot delete PlantationPlan with ID {} because it is referenced by active ProductionPlanFarmManager records.", planId);
                return false;
            }

            // Mark the plan as deleted and expired
            plantationPlan.setDeleted(true);
            plantationPlan.setExpired(true);
            plantationPlanRepository.save(plantationPlan);

            log.info("PlantationPlan with ID {} was successfully deleted.", planId);
            return true;
        } catch (Exception ex) {
            log.error("Error deleting plantation plan with ID {}: {}", planId, ex.getMessage());
            return false;
        }
    }

    @Override
    public List<FamilyResponseDto> getAllFamilies() {
        List<Plant> plants = plantRepository.findAllByDeletedFalse();

        return plants.stream().map(plant -> {
            List<Variety> varieties = varietyRepository.findByFamilyNameIdAndDeletedFalse(plant.getId());

            PlantDto plantDto = PlantDto.builder()
                    .id(plant.getId())
                    .familyName(plant.getFamilyName())
                    .scientificName(plant.getScientificName())
                    .commonName(plant.getCommonName())
                    .reproduced(plant.isReproduced())
                    .recoveryTime(plant.getRecoveryTime())
                    .build();

            List<VarietyDto> varietyDtos = varieties.stream().map(variety -> VarietyDto.builder()
                    .id(variety.getId())
                    .varietyName(variety.getVarietyName())
                    .varietyColor(variety.getVarietyColor())
                    .commonColor(variety.getCommonColor())
                    .familyNameId(variety.getFamilyNameId())
                    .build()).collect(Collectors.toList());

            return FamilyResponseDto.builder()
                    .plant(plantDto)
                    .varieties(varietyDtos)
                    .build();
        }).collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public List<PlantationScheduleResponseDto> getAllPlantationSchedules() {
        try {
            List<PlantationSchedule> schedules = plantationScheduleRepository.findByDeletedFalse();
            LocalDate currentDate = LocalDate.now();
            int currentWeek = currentDate.get(WeekFields.of(Locale.getDefault()).weekOfWeekBasedYear());

            return schedules.stream()
                    .map(schedule -> {
                        // Parse the plantationEndDate
                        LocalDate plantationEndDate = LocalDate.parse(schedule.getPlantationEndDate());

                        // Determine if the schedule is expired
                        boolean isExpired = schedule.getEndWeek() < currentWeek || plantationEndDate.isBefore(currentDate);
                        schedule.setExpired(isExpired);

                        List<PlantationScheduleVariety> varieties = pScheduleVarietyRepository.findByPlantationScheduleIdAndDeletedFalse(schedule.getId());
                        List<Long> varietyIds = varieties.stream().map(PlantationScheduleVariety::getId).collect(Collectors.toList());
                        List<PlantationScheduleQuantityWeeks> quantities = pScheduleQuantityWeeksRepository.findByPScheduleVarietyIdInAndDeletedFalse(varietyIds);

                        return convertToResponseDto(schedule, varieties, quantities);
                    })
                    .collect(Collectors.toList());
        } catch (Exception ex) {
            log.error("Error fetching plantation schedules", ex);
            throw new RuntimeException("Error fetching plantation schedules", ex);
        }
    }

    @Override
    @Transactional
    public PlantationPlanResponseDto savePlantationPlan(PlantationPlanRequestDto plantationPlanRequestDto) {

        if (plantationPlanRequestDto == null) {
            throw new IllegalArgumentException("Request data cannot be null");
        }

        if (plantationPlanRequestDto.getFarms() == null || plantationPlanRequestDto.getFarms().isEmpty()) {
            throw new IllegalArgumentException("Farm list cannot be null or empty");
        }

        if (!isValidDateRange(plantationPlanRequestDto.getPlantationStartDate(), plantationPlanRequestDto.getPlantationEndDate())) {
            throw new IllegalArgumentException("Invalid date range");
        }

        // Save the PlantationPlan
        PlantationPlan plantationPlan = PlantationPlan.builder()
                .plantationStartDate(plantationPlanRequestDto.getPlantationStartDate())
                .plantationEndDate(plantationPlanRequestDto.getPlantationEndDate())
                .startWeek(plantationPlanRequestDto.getStartWeek())
                .endWeek(plantationPlanRequestDto.getEndWeek())
                .plantationPlanName(plantationPlanRequestDto.getPlantationPlanName())
                .plantationScheduleId(plantationPlanRequestDto.getPlantationScheduleId())
                .orientation(plantationPlanRequestDto.getOrientation())  // <-- Add this line
                .build();

        PlantationPlan savedPlan = plantationPlanRepository.save(plantationPlan);

        List<PlantationPlanFarm> savedFarms = plantationPlanRequestDto.getFarms().stream()
                .map(farmDto -> {
                    PlantationPlanFarm farm = PlantationPlanFarm.builder()
                            .plantationPlanId(savedPlan.getId())
                            .farmId(farmDto.getFarmId())
                            .build();

                    PlantationPlanFarm savedFarm = plantationPlanFarmRepository.save(farm);

                    List<PlantationPlanFarmTotalQuantityVariety> quantities = farmDto.getQuantities().stream()
                            .map(quantityDto -> {
                                Long varietyId = Long.valueOf(quantityDto.getVarietyId());

                                // Fetch the variety associated with both plantationScheduleId and varietyId
                                List<PlantationScheduleVariety> varieties = pScheduleVarietyRepository
                                        .findByPlantationScheduleIdAndVerityId(plantationPlanRequestDto.getPlantationScheduleId(), varietyId);

                                if (varieties.isEmpty()) {
                                    throw new RuntimeException("Variety not found for ID: " + varietyId);
                                }
                                if (varieties.size() > 1) {
                                    throw new RuntimeException("Multiple varieties found with the same ID, expected a unique result");
                                }
                                PlantationScheduleVariety variety = varieties.get(0);

                                // Validate the quantity and update it
                                int availableQuantity = variety.getTotalQuantity();
                                if (quantityDto.getQuantity() > availableQuantity) {
                                    throw new RuntimeException("The quantity for variety " + quantityDto.getVarietyName() + " exceeds the available quantity.");
                                }

                                variety.setTotalQuantity(availableQuantity - quantityDto.getQuantity());
                                pScheduleVarietyRepository.save(variety);

                                PlantationPlanFarmTotalQuantityVariety savedQuantityVariety = PlantationPlanFarmTotalQuantityVariety.builder()
                                        .plantationPlanFarmId(savedFarm.getId())
                                        .varietyName(quantityDto.getVarietyName())
                                        .varietyId(varietyId)
                                        .quantity(quantityDto.getQuantity())
                                        .build();
                                PlantationPlanFarmTotalQuantityVariety savedQuantity = plantationPlanFarmQuantityVarietyRepository.save(savedQuantityVariety);

                                // Save the quantity weeks associated with this variety
                                List<PlantationPlanQuantityWeeks> quantityWeeks = quantityDto.getQuantityWeeks().stream()
                                        .map(weeksDto -> PlantationPlanQuantityWeeks.builder()
                                                .plantationPlanFarmTotalQuantityVarietyId(savedQuantity.getId())
                                                .transplantWeek(weeksDto.getTransplantWeek())
                                                .harvestWeek(weeksDto.getHarvestWeek())
                                                .quantity(weeksDto.getQuantity())
                                                .build())
                                        .collect(Collectors.toList());
                                plantationPlanQuantityWeeksRepository.saveAll(quantityWeeks);

                                return savedQuantityVariety;
                            })
                            .collect(Collectors.toList());

                    plantationPlanFarmQuantityVarietyRepository.saveAll(quantities);

                    return savedFarm;
                })
                .collect(Collectors.toList());

        return convertToResponseDto(savedPlan, savedFarms);
    }

    // Helper method to validate date range
    private boolean isValidDateRange(String startDate, String endDate) {
        return true;
    }


    @Override
    public List<PlantationPlanResponseDto> getAllPlantationPlans() {
        List<PlantationPlan> plans = plantationPlanRepository.findByDeletedFalse();
        return plans.stream()
                .map(plan -> {
                    List<PlantationPlanFarm> farms = plantationPlanFarmRepository.findByPlantationPlanIdAndDeletedFalse(plan.getId());
                    List<Long> farmIds = farms.stream().map(PlantationPlanFarm::getId).collect(Collectors.toList());
                    List<PlantationPlanFarmTotalQuantityVariety> quantities = plantationPlanFarmQuantityVarietyRepository.findByPlantationPlanFarmIdInAndDeletedFalse(farmIds);
                    return convertToResponseDto(plan, farms, quantities);
                })
                .collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public List<PlantationPlanResponseDto> getAllPlansByPlantationScheduleId(Long plantationScheduleId) {
        List<PlantationPlan> plans = plantationPlanRepository.findByPlantationScheduleIdAndDeletedFalse(plantationScheduleId);
        return plans.stream()
                .map(plan -> {
                    List<PlantationPlanFarm> farms = plantationPlanFarmRepository.findByPlantationPlanIdAndDeletedFalse(plan.getId());
                    List<Long> farmIds = farms.stream().map(PlantationPlanFarm::getId).collect(Collectors.toList());
                    List<PlantationPlanFarmTotalQuantityVariety> quantities = plantationPlanFarmQuantityVarietyRepository.findByPlantationPlanFarmIdInAndDeletedFalse(farmIds);
                    return convertToResponseDto(plan, farms, quantities);
                })
                .collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public List<PlantationPlanResponseDto> getPlantationPlansByFarmId(Long farmId) {
        if (farmId == null || farmId <= 0) {
            log.error("Invalid farm ID: {}", farmId);
            throw new IllegalArgumentException("Invalid farm ID: " + farmId);
        }

        try {
            List<PlantationPlanFarm> planFarms = plantationPlanFarmRepository.findByFarmIdAndDeletedFalse(farmId);
            if (planFarms.isEmpty()) {
                log.warn("No plantation plans found for farm ID: {}", farmId);
                return Collections.emptyList();
            }

            List<Long> planIds = planFarms.stream()
                    .map(PlantationPlanFarm::getPlantationPlanId)
                    .distinct()
                    .collect(Collectors.toList());

            // Exclude plans that are saved in ProductionPlanFarmManager
            List<Long> excludedPlanIds = productionPlanFarmManagerRepository.findBySavedTrueAndPlantationPlanIdIn(planIds)
                    .stream()
                    .map(ProductionPlanFarmManager::getPlantationPlanId)
                    .collect(Collectors.toList());

            List<Long> finalPlanIds = planIds.stream()
                    .filter(planId -> !excludedPlanIds.contains(planId))
                    .collect(Collectors.toList());

            if (finalPlanIds.isEmpty()) {
                log.warn("All plantation plans for the provided farm ID are already saved in ProductionPlanFarmManager");
                return Collections.emptyList();
            }

            List<PlantationPlan> plans = plantationPlanRepository.findByIdInAndDeletedFalse(finalPlanIds);
            if (plans.isEmpty()) {
                log.warn("No plantation plans found for the provided plan IDs for farm ID: {}", farmId);
                return Collections.emptyList();
            }

            return plans.stream()
                    .map(plan -> {
                        List<PlantationPlanFarm> farms = planFarms.stream()
                                .filter(farm -> farm.getPlantationPlanId().equals(plan.getId()))
                                .collect(Collectors.toList());

                        List<Long> farmIdsForPlan = farms.stream()
                                .map(PlantationPlanFarm::getId)
                                .collect(Collectors.toList());

                        List<PlantationPlanFarmTotalQuantityVariety> quantities = plantationPlanFarmQuantityVarietyRepository.findByPlantationPlanFarmIdInAndDeletedFalse(farmIdsForPlan);

                        return convertToResponseDto(plan, farms, quantities);
                    })
                    .collect(Collectors.toList());
        } catch (IllegalArgumentException e) {
            log.error("Validation error while fetching plantation plans by farm ID: {}", e.getMessage());
            throw e;
        } catch (Exception e) {
            log.error("Unexpected error fetching plantation plans by farm ID", e);
            throw new RuntimeException("Error fetching plantation plans by farm ID", e);
        }
    }



    @Override
    @Transactional(readOnly = true)
    public List<PlantationPlanResponseDto> getAllPlantationPlansByFarmIds(Set<Long> farmIds) {
        // Retrieve all PlantationPlanFarm records associated with the specified farm IDs
        List<PlantationPlanFarm> farms = plantationPlanFarmRepository.findByFarmIdInAndDeletedFalse(farmIds);

        // Extract unique plantation plan IDs from the retrieved farms
        List<Long> planIds = farms.stream()
                .map(PlantationPlanFarm::getPlantationPlanId)
                .distinct()
                .collect(Collectors.toList());

        // Retrieve all PlantationPlan records associated with the extracted plan IDs
        List<PlantationPlan> plans = plantationPlanRepository.findByIdInAndDeletedFalse(planIds);

        // Convert each PlantationPlan entity to its corresponding response DTO
        return plans.stream()
                .map(plan -> {
                    // Filter the farms to include only those associated with the current plan
                    List<PlantationPlanFarm> planFarms = farms.stream()
                            .filter(farm -> farm.getPlantationPlanId().equals(plan.getId()))
                            .collect(Collectors.toList());

                    // Extract the IDs of the filtered farms
                    List<Long> farmIdsForPlan = planFarms.stream()
                            .map(PlantationPlanFarm::getId)
                            .collect(Collectors.toList());

                    // Retrieve all quantities associated with the filtered farms
                    List<PlantationPlanFarmTotalQuantityVariety> quantities = plantationPlanFarmQuantityVarietyRepository.findByPlantationPlanFarmIdInAndDeletedFalse(farmIdsForPlan);

                    // Convert the plan entity and its associated farms and quantities to a response DTO
                    return convertToResponseDto(plan, planFarms, quantities);
                })
                .collect(Collectors.toList());
    }

    private PlantationPlanResponseDto convertToResponseDto(PlantationPlan plan, List<PlantationPlanFarm> farms) {

        String plantationScheduleName = plantationScheduleRepository.findById(plan.getPlantationScheduleId())
                .map(PlantationSchedule::getName)
                .orElse("Unknown");

        List<PlantationPlanFarmResponseDto> farmDtos = farms.stream()
                .map(farm -> {
                    List<PlantationPlanFarmTotalQuantityVariety> quantities = plantationPlanFarmQuantityVarietyRepository.findByPlantationPlanFarmIdAndDeletedFalse(farm.getId());
                    List<PlantationPlanFarmQuantityVarietyResponseDto> quantityDtos = quantities.stream()
                            .map(quantity -> {
                                List<PlantationPlanQuantityWeeks> quantityWeeks = plantationPlanQuantityWeeksRepository.findByPlantationPlanFarmTotalQuantityVarietyIdAndDeletedFalse(quantity.getId());
                                List<PlantationPlanQuantityWeeksResponseDto> quantityWeeksDtos = quantityWeeks.stream()
                                        .map(weeks -> PlantationPlanQuantityWeeksResponseDto.builder()
                                                .id(weeks.getId())
                                                .plantationPlanFarmTotalQuantityVarietyId(weeks.getPlantationPlanFarmTotalQuantityVarietyId())
                                                .transplantWeek(weeks.getTransplantWeek())
                                                .harvestWeek(weeks.getHarvestWeek())
                                                .quantity(weeks.getQuantity())
                                                .deleted(weeks.isDeleted())
                                                .build())
                                        .collect(Collectors.toList());

                                Variety variety = varietyRepository.findById(quantity.getVarietyId()).orElse(null); // Fetch variety
                                String varietyName = variety != null ? variety.getVarietyName() : "Unknown"; // Handle null case

                                return PlantationPlanFarmQuantityVarietyResponseDto.builder()
                                        .id(quantity.getId())
                                        .quantity(quantity.getQuantity())
                                        .varietyId(String.valueOf(quantity.getVarietyId()))
                                        .varietyName(varietyName) // Set varietyName
                                        .quantityWeeks(quantityWeeksDtos)
                                        .build();
                            })
                            .collect(Collectors.toList());
                    return PlantationPlanFarmResponseDto.builder()
                            .id(farm.getId())
                            .farmId(farm.getFarmId())
                            .quantities(quantityDtos)
                            .build();
                })
                .collect(Collectors.toList());

        return PlantationPlanResponseDto.builder()
                .id(plan.getId())
                .plantationStartDate(plan.getPlantationStartDate())
                .plantationEndDate(plan.getPlantationEndDate())
                .startWeek(plan.getStartWeek())
                .endWeek(plan.getEndWeek())
                .plantationPlanName(plan.getPlantationPlanName())
                .plantationScheduleId(plan.getPlantationScheduleId())
                .plantationScheduleName(plantationScheduleName)
                .orientation(plan.getOrientation())  // <-- Add this line
                .farms(farmDtos)
                .build();
    }

    private PlantationPlanResponseDto convertToResponseDto(PlantationPlan plan, List<PlantationPlanFarm> farms,
                                                           List<PlantationPlanFarmTotalQuantityVariety> quantities) {


        // Fetch the plantation schedule name using the plantationScheduleId
        String plantationScheduleName = plantationScheduleRepository.findById(plan.getPlantationScheduleId())
                .map(PlantationSchedule::getName)
                .orElse("Unknown");

        List<PlantationPlanFarmResponseDto> farmDtos = farms.stream()
                .map(farm -> {
                    List<PlantationPlanFarmQuantityVarietyResponseDto> quantityDtos = quantities.stream()
                            .filter(quantity -> quantity.getPlantationPlanFarmId().equals(farm.getId()))
                            .map(quantity -> {
                                List<PlantationPlanQuantityWeeks> quantityWeeks = plantationPlanQuantityWeeksRepository.findByPlantationPlanFarmTotalQuantityVarietyIdAndDeletedFalse(quantity.getId());
                                List<PlantationPlanQuantityWeeksResponseDto> quantityWeeksDtos = quantityWeeks.stream()
                                        .map(weeks -> PlantationPlanQuantityWeeksResponseDto.builder()
                                                .id(weeks.getId())
                                                .plantationPlanFarmTotalQuantityVarietyId(weeks.getPlantationPlanFarmTotalQuantityVarietyId())
                                                .transplantWeek(weeks.getTransplantWeek())
                                                .harvestWeek(weeks.getHarvestWeek())
                                                .quantity(weeks.getQuantity())
                                                .deleted(weeks.isDeleted())
                                                .build())
                                        .collect(Collectors.toList());

                                Variety variety = varietyRepository.findById(quantity.getVarietyId()).orElse(null); // Fetch variety
                                String varietyName = variety != null ? variety.getVarietyName() : "Unknown"; // Handle null case

                                return PlantationPlanFarmQuantityVarietyResponseDto.builder()
                                        .id(quantity.getId())
                                        .quantity(quantity.getQuantity())
                                        .varietyId(String.valueOf(quantity.getVarietyId()))
                                        .varietyName(varietyName) // Set varietyName
                                        .quantityWeeks(quantityWeeksDtos)
                                        .build();
                            })
                            .collect(Collectors.toList());
                    return PlantationPlanFarmResponseDto.builder()
                            .id(farm.getId())
                            .farmId(farm.getFarmId())
                            .quantities(quantityDtos)
                            .build();
                })
                .collect(Collectors.toList());

        return PlantationPlanResponseDto.builder()
                .id(plan.getId())
                .plantationStartDate(plan.getPlantationStartDate())
                .plantationEndDate(plan.getPlantationEndDate())
                .startWeek(plan.getStartWeek())
                .endWeek(plan.getEndWeek())
                .plantationPlanName(plan.getPlantationPlanName())
                .plantationScheduleId(plan.getPlantationScheduleId())
                .plantationScheduleName(plantationScheduleName) // Set the plantationScheduleName
                .orientation(plan.getOrientation())
                .farms(farmDtos)
                .build();
    }


    private PlantationScheduleResponseDto convertToResponseDto(PlantationSchedule schedule, List<PlantationScheduleVariety> varieties) {
        List<PScheduleVarietyResponseDto> varietyDtos = varieties.stream()
                .map(variety -> {
                    List<PlantationScheduleQuantityWeeks> quantityWeeks = pScheduleQuantityWeeksRepository.findByPScheduleVarietyIdInAndDeletedFalse(
                            List.of(variety.getId())
                    );
                    List<PScheduleQuantityWeeksResponseDto> quantityWeeksDtos = quantityWeeks.stream()
                            .map(quantity -> PScheduleQuantityWeeksResponseDto.builder()
                                    .id(quantity.getId())
                                    .PScheduleVarietyId(quantity.getPScheduleVarietyId())
                                    .transplantWeek(quantity.getTransplantWeek())
                                    .harvestWeek(quantity.getHarvestWeek())
                                    .quantity(quantity.getQuantity())
                                    .build())
                            .collect(Collectors.toList());
                    return PScheduleVarietyResponseDto.builder()
                            .id(variety.getId())
                            .plantationScheduleId(variety.getPlantationScheduleId())
                            .varietyId(variety.getVerityId())
                            .varietyName(variety.getVerityName())
                            .pScheduleQuantityWeeksRequestDtoList(quantityWeeksDtos)
                            .build();
                })
                .collect(Collectors.toList());

        return PlantationScheduleResponseDto.builder()
                .id(schedule.getId())
                .name(schedule.getName())
                .familyId(schedule.getFamilyId())
                .purpose(schedule.getPurpose())
                .hDuration(schedule.getHDuration())
                .plantationStartDate(schedule.getPlantationStartDate())
                .plantationEndDate(schedule.getPlantationEndDate())
                .startWeek(schedule.getStartWeek())  // Bind startWeek
                .endWeek(schedule.getEndWeek())      // Bind endWeek
                .orientation(schedule.getOrientation())  // <-- Add this line
                .pScheduleVarietyRequestDtoList(varietyDtos)
                .build();
    }


    private PlantationScheduleResponseDto convertToResponseDto(PlantationSchedule schedule, List<PlantationScheduleVariety> varieties,
                                                               List<PlantationScheduleQuantityWeeks> quantities) {

        String familyName = plantRepository.findById(schedule.getFamilyId()).map(Plant::getFamilyName).orElse("Unknown");

        List<PScheduleVarietyResponseDto> varietyDtos = varieties.stream()
                .map(variety -> {
                    List<PScheduleQuantityWeeksResponseDto> quantityWeeksDtos = quantities.stream()
                            .filter(quantity -> quantity.getPScheduleVarietyId().equals(variety.getId()))
                            .map(quantity -> PScheduleQuantityWeeksResponseDto.builder()
                                    .id(quantity.getId())
                                    .PScheduleVarietyId(quantity.getPScheduleVarietyId())
                                    .transplantWeek(quantity.getTransplantWeek())
                                    .harvestWeek(quantity.getHarvestWeek())
                                    .quantity(quantity.getQuantity())
                                    .build())
                            .collect(Collectors.toList());
                    return PScheduleVarietyResponseDto.builder()
                            .id(variety.getId())
                            .plantationScheduleId(variety.getPlantationScheduleId())
                            .varietyId(variety.getVerityId())
                            .varietyName(variety.getVerityName())
                            .totalQuantity(variety.getTotalQuantity())
                            .pScheduleQuantityWeeksRequestDtoList(quantityWeeksDtos)
                            .build();
                })
                .collect(Collectors.toList());

        return PlantationScheduleResponseDto.builder()
                .id(schedule.getId())
                .name(schedule.getName())
                .familyId(schedule.getFamilyId())
                .familyName(familyName)
                .purpose(schedule.getPurpose())
                .hDuration(schedule.getHDuration())
                .plantationStartDate(schedule.getPlantationStartDate())
                .plantationEndDate(schedule.getPlantationEndDate())
                .startWeek(schedule.getStartWeek())
                .endWeek(schedule.getEndWeek())
                .expired(schedule.isExpired()) // Set the expired field
                .pScheduleVarietyRequestDtoList(varietyDtos)
                .orientation(schedule.getOrientation())
                .build();
    }


}
