package com.shirohana.service.implementation.schedule.production_plan_farm_manager;

import com.shirohana.dto.schedule.production_plan.*;
import com.shirohana.entity.farm.Greenhouse;
import com.shirohana.entity.schedule.plantation_plan.PlantationPlanFarmTotalQuantityVariety;
import com.shirohana.entity.schedule.plantation_schedule.PlantationSchedule;
import com.shirohana.entity.schedule.production_plan.*;
import com.shirohana.repository.farm.GreenhouseRepository;
import com.shirohana.repository.production_plan.*;
import com.shirohana.repository.shedule.pplan.PlantationPlanFarmQuantityVarietyRepository;
import com.shirohana.repository.shedule.pschedule.PlantationScheduleRepository;
import com.shirohana.service.interfaces.schedule.ProductionPlanFarmManagerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class ProductionPlanFarmManagerServiceImpl implements ProductionPlanFarmManagerService {

    private final ProductionPlanFarmManagerRepository productionPlanFarmManagerRepository;
    private final ProductionPlanFarmManagerGreenHouseRepository productionPlanFarmManagerGreenHouseRepository;
    private final ProductionPlanFarmManagerQuantityWeeksRepository productionPlanFarmManagerQuantityWeeksRepository;
    private final GreenhouseRepository greenhouseRepository;
    private final ProductionPlanFarmManagerGreenHouseTotalQuantityVarietyRepository productionPlanFarmManagerGreenHouseTotalQuantityVarietyRepository;
    private final PlantationPlanFarmQuantityVarietyRepository plantationPlanFarmQuantityVarietyRepository;
    private final PlantationScheduleRepository plantationScheduleRepository;
    @Override
    @Transactional
    public ProductionPlanFarmManagerResponseDto saveProductionPlanFarmManager(ProductionPlanFarmManagerRequestDto productionPlanFarmManagerRequestDto) {
        try {
            // Validate greenhouse IDs
            List<Long> greenHouseIds = productionPlanFarmManagerRequestDto.getGreenHouses().stream()
                    .map(ProductionPlanFarmManagerGreenHouseRequestDto::getGreenHouseId)
                    .collect(Collectors.toList());
            validateGreenHouseIds(greenHouseIds);

            // Fetch the PlantationSchedule by plantationScheduleId
            PlantationSchedule plantationSchedule = plantationScheduleRepository.findById(productionPlanFarmManagerRequestDto.getPlantationScheduleId())
                    .orElseThrow(() -> new IllegalArgumentException("PlantationSchedule does not exist"));

            String orientation = plantationSchedule.getOrientation(); // <-- Get the orientation from the schedule

            // Save ProductionPlanFarmManager entity
            ProductionPlanFarmManager productionPlanFarmManager = ProductionPlanFarmManager.builder()
                    .plantationPlanId(productionPlanFarmManagerRequestDto.getPlantationPlanId())
                    .plantationScheduleId(productionPlanFarmManagerRequestDto.getPlantationScheduleId())
                    .orientation(orientation)  // <-- Set the orientation from the PlantationSchedule
                    .planName(productionPlanFarmManagerRequestDto.getPlanName())
                    .plantationStartDate(productionPlanFarmManagerRequestDto.getPlantationStartDate())
                    .startWeek(productionPlanFarmManagerRequestDto.getStartWeek())
                    .endWeek(productionPlanFarmManagerRequestDto.getEndWeek())
                    .plantationEndDate(productionPlanFarmManagerRequestDto.getPlantationEndDate())
                    .farmId(productionPlanFarmManagerRequestDto.getFarmId())
                    .saved(true)
                    .build();
            ProductionPlanFarmManager savedSupervisor = productionPlanFarmManagerRepository.save(productionPlanFarmManager);

            // Save GreenHouses and related entities
            for (ProductionPlanFarmManagerGreenHouseRequestDto greenHouseDto : productionPlanFarmManagerRequestDto.getGreenHouses()) {
                // Save ProductionPlanFarmManagerGreenHouse entity
                ProductionPlanFarmManagerGreenHouse greenHouse = ProductionPlanFarmManagerGreenHouse.builder()
                        .prodPlanFarmMgrId(savedSupervisor.getId())
                        .greenHouseId(greenHouseDto.getGreenHouseId())
                        .greenHouseName(greenHouseDto.getGreenHouseName())
                        .farmId(greenHouseDto.getFarmId())
                        .build();
                ProductionPlanFarmManagerGreenHouse savedGreenHouse = productionPlanFarmManagerGreenHouseRepository.save(greenHouse);

                // Save each variety within the greenhouse
                for (ProductionPlanFarmManagerGreenHouseTotalQuantityVarietyRequestDto varietyDto : greenHouseDto.getTotalQuantities()) {
                    // Fetch the variety associated with both plantationPlanId and varietyId
                    List<PlantationPlanFarmTotalQuantityVariety> plantationPlanVarieties = plantationPlanFarmQuantityVarietyRepository
                            .findByPlantationPlanFarmIdAndVarietyIdAndDeletedFalse(productionPlanFarmManager.getPlantationPlanId(), varietyDto.getVarietyId());

                    if (plantationPlanVarieties.isEmpty()) {
                        throw new RuntimeException("Variety not found for ID: " + varietyDto.getVarietyId() + " in the associated plantation plan.");
                    }

                    PlantationPlanFarmTotalQuantityVariety plantationVariety = plantationPlanVarieties.get(0);

                    // Calculate total quantity for the production plan
                    int totalQuantity = varietyDto.getQuantityWeeks().stream()
                            .mapToInt(ProductionPlanFarmManagerQuantityWeeksRequestDto::getQuantity)
                            .sum();

                    // Validate the total quantity against the available quantity in the plantation plan
                    int availableQuantity = plantationVariety.getQuantity();
                    if (totalQuantity > availableQuantity) {
                        throw new RuntimeException("The quantity for variety " + varietyDto.getVarietyName() + " exceeds the available quantity in the plantation plan.");
                    }

                    // Adjust the available quantity in the plantation plan
                    plantationVariety.setQuantity(availableQuantity - totalQuantity);
                    plantationPlanFarmQuantityVarietyRepository.save(plantationVariety);

                    // Save ProductionPlanFarmManagerGreenHouseTotalQuantityVariety entity
                    ProductionPlanFarmManagerGreenHouseTotalQuantityVariety totalQuantityVariety = ProductionPlanFarmManagerGreenHouseTotalQuantityVariety.builder()
                            .prodPlanFarmMgrGreenHouseId(savedGreenHouse.getId())  // Link to the saved greenHouse
                            .varietyId(varietyDto.getVarietyId())
                            .varietyName(varietyDto.getVarietyName())
                            .totalQuantity(totalQuantity)
                            .build();
                    ProductionPlanFarmManagerGreenHouseTotalQuantityVariety savedTotalQuantityVariety = productionPlanFarmManagerGreenHouseTotalQuantityVarietyRepository.save(totalQuantityVariety);

                    // Save ProductionPlanFarmMangerQuantityWeeks entities
                    for (ProductionPlanFarmManagerQuantityWeeksRequestDto weeksDto : varietyDto.getQuantityWeeks()) {
                        ProductionPlanFarmMangerQuantityWeeks quantityWeeks = ProductionPlanFarmMangerQuantityWeeks.builder()
                                .prodPlanFarmMgrGreenHouseTotalQtyVarietyId(savedTotalQuantityVariety.getId())  // Link to the saved totalQuantityVariety
                                .transplantWeek(weeksDto.getTransplantWeek())
                                .harvestWeek(weeksDto.getHarvestWeek())
                                .quantity(weeksDto.getQuantity())
                                .build();
                        productionPlanFarmManagerQuantityWeeksRepository.save(quantityWeeks);
                    }
                }
            }

            // Convert and return the response DTO
            return convertToResponseDto(savedSupervisor);
        } catch (Exception ex) {
            log.error("Error saving production plan farm manager", ex);
            throw ex;
        }
    }

    @Override
    @Transactional
    public boolean deleteProductionPlanFarmManager(Long managerId) {
        try {
            // Find the ProductionPlanFarmManager by ID
            ProductionPlanFarmManager manager = productionPlanFarmManagerRepository.findById(managerId)
                    .orElseThrow(() -> new IllegalArgumentException("ProductionPlanFarmManager with ID " + managerId + " does not exist."));

            // Check if the manager is already deleted or expired
            if (manager.isDeleted() || manager.isExpired()) {
                log.warn("Cannot delete ProductionPlanFarmManager with ID {} because it is already deleted or expired.", managerId);
                return false;
            }

            // Mark the manager as deleted and expired
            manager.setDeleted(true);
            manager.setExpired(true);
            productionPlanFarmManagerRepository.save(manager);

            log.info("ProductionPlanFarmManager with ID {} was successfully deleted.", managerId);
            return true;
        } catch (Exception ex) {
            log.error("Error deleting ProductionPlanFarmManager with ID {}: {}", managerId, ex.getMessage());
            return false;
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<ProductionPlanFarmManagerResponseDto> getAllProductionPlanFarmManager() {
        List<ProductionPlanFarmManager> supervisors = productionPlanFarmManagerRepository.findByDeletedFalse();
        return supervisors.stream()
                .map(this::convertToResponseDto)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public List<ProductionPlanFarmManagerResponseDto> getAllProductionPlansByFarms(Set<Long> farmIds) {
        List<ProductionPlanFarmManager> supervisors = productionPlanFarmManagerRepository.findByFarmIdsAndDeletedFalse(farmIds);
        return supervisors.stream()
                .map(this::convertToResponseDto)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public List<ProductionPlanFarmManagerResponseDto> getAllProductionPlansByFarmId(Long farmId) {
        List<ProductionPlanFarmManager> supervisors = productionPlanFarmManagerRepository.findByFarmIdAndDeletedFalse(farmId);
        return supervisors.stream()
                .map(this::convertToResponseDto)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public List<ProductionPlanFarmManagerResponseDto> getAllPendingProdPlanFarmManagerByGreenHouseId(Long greenhouseId) {
        if (greenhouseId == null || greenhouseId <= 0) {
            log.error("Invalid greenhouse ID: {}", greenhouseId);
            throw new IllegalArgumentException("Invalid greenhouse ID: " + greenhouseId);
        }

        try {
            List<ProductionPlanFarmManager> pendingFarmManagers = productionPlanFarmManagerRepository.findPendingByGreenhouseId(greenhouseId);
            if (pendingFarmManagers.isEmpty()) {
                log.warn("No pending production plan farm managers found for the provided greenhouse ID: {}", greenhouseId);
                return List.of();
            }

            return pendingFarmManagers.stream()
                    .map(this::convertToResponseDto)
                    .collect(Collectors.toList());
        } catch (IllegalArgumentException e) {
            log.error("Validation error while fetching production plan farm managers by greenhouse ID: {}", e.getMessage());
            throw e;
        } catch (Exception e) {
            log.error("Unexpected error fetching production plan farm managers by greenhouse ID", e);
            throw new RuntimeException("Error fetching production plan farm managers by greenhouse ID", e);
        }
    }

    private ProductionPlanFarmManagerResponseDto convertToResponseDto(ProductionPlanFarmManager farmManager) {

        PlantationSchedule plantationSchedule = plantationScheduleRepository.findById(farmManager.getPlantationScheduleId())
                .orElseThrow(() -> new IllegalArgumentException("PlantationSchedule with ID " + farmManager.getPlantationScheduleId() + " not found."));

        List<ProductionPlanFarmManagerGreenHouse> greenHouses = productionPlanFarmManagerGreenHouseRepository.findByProdPlanFarmMgrIdAndDeletedFalse(farmManager.getId());
        List<ProductionPlanFarmManagerGreenHouseResponseDto> greenHouseDtos = greenHouses.stream()
                .map(greenHouse -> {
                    List<ProductionPlanFarmManagerGreenHouseTotalQuantityVariety> totalQuantities = productionPlanFarmManagerGreenHouseTotalQuantityVarietyRepository.findByProdPlanFarmMgrGreenHouseId(greenHouse.getId());
                    List<ProductionPlanFarmManagerGreenHouseTotalQuantityVarietyResponseDto> totalQuantityDtos = totalQuantities.stream()
                            .map(this::convertTotalQuantityToResponseDto)
                            .collect(Collectors.toList());

                    return ProductionPlanFarmManagerGreenHouseResponseDto.builder()
                            .id(greenHouse.getId())
                            .greenHouseId(greenHouse.getGreenHouseId())
                            .greenHouseName(greenHouse.getGreenHouseName())
                            .farmId(greenHouse.getFarmId())
                            .totalQuantities(totalQuantityDtos)
                            .build();
                })
                .collect(Collectors.toList());

        return ProductionPlanFarmManagerResponseDto.builder()
                .id(farmManager.getId())
                .plantationPlanId(farmManager.getPlantationPlanId())
                .plantationScheduleId(farmManager.getPlantationScheduleId())
                .orientation(farmManager.getOrientation())
                .planName(farmManager.getPlanName())
                .plantationStartDate(farmManager.getPlantationStartDate())
                .plantationEndDate(farmManager.getPlantationEndDate())
                .startWeek(farmManager.getStartWeek())
                .endWeek(farmManager.getEndWeek())
                .farmId(farmManager.getFarmId())
                .familyId(plantationSchedule.getFamilyId())
                .greenHouses(greenHouseDtos)
                .saved(farmManager.isSaved())
                .build();
    }

    private ProductionPlanFarmManagerGreenHouseTotalQuantityVarietyResponseDto convertTotalQuantityToResponseDto(ProductionPlanFarmManagerGreenHouseTotalQuantityVariety totalQuantity) {
        List<ProductionPlanFarmMangerQuantityWeeks> quantityWeeks = productionPlanFarmManagerQuantityWeeksRepository.findByProdPlanFarmMgrGreenHouseTotalQtyVarietyIdAndDeletedFalse(totalQuantity.getId());
        List<ProductionPlanFarmManagerQuantityWeeksResponseDto> quantityWeeksDtos = quantityWeeks.stream()
                .map(this::convertQuantityWeeksToResponseDto)
                .collect(Collectors.toList());

        return ProductionPlanFarmManagerGreenHouseTotalQuantityVarietyResponseDto.builder()
                .id(totalQuantity.getId())
                .varietyId(totalQuantity.getVarietyId())
                .varietyName(totalQuantity.getVarietyName())
                .totalQuantity(totalQuantity.getTotalQuantity())
                .quantityWeeks(quantityWeeksDtos)
                .build();
    }

    private ProductionPlanFarmManagerQuantityWeeksResponseDto convertQuantityWeeksToResponseDto(ProductionPlanFarmMangerQuantityWeeks quantityWeeks) {
        return ProductionPlanFarmManagerQuantityWeeksResponseDto.builder()
                .id(quantityWeeks.getId())
                .productionPlanSupervisorGreenHouseId(quantityWeeks.getProdPlanFarmMgrGreenHouseTotalQtyVarietyId())
                .transplantWeek(quantityWeeks.getTransplantWeek())
                .harvestWeek(quantityWeeks.getHarvestWeek())
                .quantity(quantityWeeks.getQuantity())
                .deleted(quantityWeeks.isDeleted())
                .build();
    }

    private void validateGreenHouseIds(List<Long> greenHouseIds) {
        List<Long> existingGreenHouseIds = greenhouseRepository.findAllById(greenHouseIds).stream()
                .map(Greenhouse::getId)
                .collect(Collectors.toList());

        greenHouseIds.forEach(id -> {
            if (!existingGreenHouseIds.contains(id)) {
                throw new IllegalArgumentException("Greenhouse ID " + id + " does not exist.");
            }
        });
    }
}
