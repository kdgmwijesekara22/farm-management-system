package com.shirohana.service.implementation.schedule.production_plan_green_house;

import com.shirohana.dto.schedule.production_plan_green_house.*;
import com.shirohana.entity.farm.Bay;
import com.shirohana.entity.farm.Bed;
import com.shirohana.entity.farm.Farm;
import com.shirohana.entity.farm.Greenhouse;
import com.shirohana.entity.plant.Plant;
import com.shirohana.entity.schedule.plantation_plan.PlantationPlan;
import com.shirohana.entity.schedule.plantation_schedule.PlantationSchedule;
import com.shirohana.entity.schedule.production_plan_green_house.*;
import com.shirohana.entity.task.core.TaskCategorySetup;
import com.shirohana.entity.task.schedule.TaskCategoryTaskVarietySchedule;
import com.shirohana.entity.task.schedule.TaskVarietySchedule;
import com.shirohana.entity.task.setup.maintenance.TaskMainVariety;
import com.shirohana.entity.task.setup.maintenance.TaskMaintenance;
import com.shirohana.entity.task.setup.monitoring.TaskMoniVariety;
import com.shirohana.entity.task.setup.monitoring.TaskMonitoring;
import com.shirohana.entity.variety.Variety;
import com.shirohana.repository.farm.BayRepository;
import com.shirohana.repository.farm.BedRepository;
import com.shirohana.repository.farm.FarmRepository;
import com.shirohana.repository.farm.GreenhouseRepository;
import com.shirohana.repository.plant.PlantRepository;
import com.shirohana.repository.production_plan_green_house.*;
import com.shirohana.repository.shedule.pplan.PlantationPlanRepository;
import com.shirohana.repository.shedule.pschedule.PlantationScheduleRepository;
import com.shirohana.repository.task.schedule.TaskCategoryTaskVarietyScheduleRepository;
import com.shirohana.repository.task.schedule.TaskVarietyScheduleRepository;
import com.shirohana.repository.task.setup.maintenance.TaskMainVarietyRepository;
import com.shirohana.repository.task.setup.maintenance.TaskMaintenanceRepository;
import com.shirohana.repository.task.setup.monitoring.TaskMoniVarietyRepository;
import com.shirohana.repository.task.setup.monitoring.TaskMonitoringRepository;
import com.shirohana.repository.taskcategorysetup.TaskCategorySetupRepository;
import com.shirohana.repository.variety.VarietyRepository;
import com.shirohana.service.interfaces.schedule.ProductionPlanGreenHouseManagerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class ProductionPlanGreenHouseManagerServiceImpl implements ProductionPlanGreenHouseManagerService {

    private final ProductionPlanGreenHouseManagerRepository productionPlanGreenHouseManagerRepository;
    private final ProductionPlanGreenHouseManagerBayBedRepository productionPlanGreenHouseManagerBayBedRepository;
    private final ProductionPlanGreenHouseManagerWeeksYearsRepository productionPlanGreenHouseManagerWeeksYearsRepository;
    private final ProductionPlanGreenHouseManagerVarietyQuantityRepository productionPlanGreenHouseManagerVarietyQuantityRepository;
    private final TaskCategorySetupRepository taskCategorySetupRepository;
    private final TaskVarietyScheduleRepository taskVarietyScheduleRepository;
    private final TaskCategoryTaskVarietyScheduleRepository taskCategoryTaskVarietyScheduleRepository;
    private final PlantationScheduleRepository plantationScheduleRepository;

    // Newly added repositories
    private final TaskMoniVarietyRepository taskMoniVarietyRepository;
    private final TaskMainVarietyRepository taskMainVarietyRepository;
    private final TaskMonitoringRepository taskMonitoringRepository;
    private final TaskMaintenanceRepository taskMaintenanceRepository;

    // Additional repositories
    private final BayRepository bayRepository;
    private final BedRepository bedRepository;
    private final FarmRepository farmRepository;
    private final GreenhouseRepository greenhouseRepository;
    private final PlantRepository plantRepository;
    private final VarietyRepository varietyRepository;
    private final PlantationPlanRepository plantationPlanRepository;

    @Override
    @Transactional
    public ProductionPlanGHManagerResponseDto saveProductionPlanGreenHouseManager(ProductionPlanGHManagerRequestDto requestDto) {
        validateDuplicatePlan(requestDto);

        ProductionPlanGHManager productionPlanGHManager = buildProductionPlanGHManager(requestDto);
        ProductionPlanGHManager savedGHManager = productionPlanGreenHouseManagerRepository.save(productionPlanGHManager);

        List<ProductionPlanGHMBayBed> savedBayBeds = saveBayBeds(requestDto, savedGHManager);
        saveWeeksYearsAndVarietyQuantities(requestDto, savedGHManager, savedBayBeds);

        return convertToResponseDto(savedGHManager, savedBayBeds);
    }

    private ProductionPlanGHManager buildProductionPlanGHManager(ProductionPlanGHManagerRequestDto requestDto) {

        // Fetch the PlantationSchedule using the plantationScheduleId
        PlantationSchedule plantationSchedule = plantationScheduleRepository.findById(requestDto.getPlantationScheduleId())
                .orElseThrow(() -> new RuntimeException("Plantation Schedule not found for id: " + requestDto.getPlantationScheduleId()));

        // Get the orientation from the PlantationSchedule
        String orientation = plantationSchedule.getOrientation();

        return ProductionPlanGHManager.builder()
                .productionPlanFarmManagerId(requestDto.getProductionPlanFarmManagerId())
                .farmId(requestDto.getFarmId())
                .greenhouseId(requestDto.getGreenhouseId())
                .plantationPlanId(requestDto.getPlantationPlanId())
                .plantationScheduleId(requestDto.getPlantationScheduleId())
                .familyId(requestDto.getFamilyId())
                .planName(requestDto.getPlanName())
                .plantationStartDate(requestDto.getPlantationStartDate())
                .plantationEndDate(requestDto.getPlantationEndDate())
                .startWeek(requestDto.getStartWeek())
                .endWeek(requestDto.getEndWeek())
                .startYear(requestDto.getStartYear())
                .endYear(requestDto.getEndYear())
                .orientation(orientation)
                .deleted(false)
                .saved(true)
                .build();
    }

    private List<ProductionPlanGHMBayBed> saveBayBeds(ProductionPlanGHManagerRequestDto requestDto, ProductionPlanGHManager savedGHManager) {
        return requestDto.getBayBeds().stream()
                .map(bayBedDto -> {
                    ProductionPlanGHMBayBed bayBed = ProductionPlanGHMBayBed.builder()
                            .productionPlanGHManagerId(savedGHManager.getId())
                            .bayId(bayBedDto.getBayId())
                            .bedId(bayBedDto.getBedId())
                            .deleted(false)
                            .build();
                    return productionPlanGreenHouseManagerBayBedRepository.save(bayBed);
                })
                .collect(Collectors.toList());
    }

    private void saveWeeksYearsAndVarietyQuantities(ProductionPlanGHManagerRequestDto requestDto, ProductionPlanGHManager savedGHManager, List<ProductionPlanGHMBayBed> savedBayBeds) {
        requestDto.getBayBeds().forEach(bayBedDto -> {
            bayBedDto.getWeeksYears().forEach(weeksYearsDto -> {
                ProductionPlanGHMWeeksYears savedWeeksYears = saveWeeksYears(savedBayBeds, bayBedDto, weeksYearsDto);
                saveVarietyQuantities(weeksYearsDto, savedGHManager, savedWeeksYears);
            });
        });
    }

    private ProductionPlanGHMWeeksYears saveWeeksYears(List<ProductionPlanGHMBayBed> savedBayBeds, ProductionPlanGHMBayBedRequestDto bayBedDto, ProductionPlanGHMWeeksYearsRequestDto weeksYearsDto) {
        ProductionPlanGHMBayBed matchedBayBed = savedBayBeds.stream()
                .filter(bayBed -> bayBed.getBayId().equals(bayBedDto.getBayId()) && bayBed.getBedId().equals(bayBedDto.getBedId()))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("BayBed not found"));

        ProductionPlanGHMWeeksYears savedWeeksYears = ProductionPlanGHMWeeksYears.builder()
                .productionPlanGHMBayBedId(matchedBayBed.getId())
                .trayplantWeek(weeksYearsDto.getTrayplantWeek())
                .harvestWeek(weeksYearsDto.getHarvestWeek())
                .trayplantStartYear(weeksYearsDto.getTrayplantStartYear())
                .transplantEndYear(weeksYearsDto.getTransplantEndYear())
                .build();

        return productionPlanGreenHouseManagerWeeksYearsRepository.save(savedWeeksYears);
    }

    private void saveVarietyQuantities(ProductionPlanGHMWeeksYearsRequestDto weeksYearsDto, ProductionPlanGHManager savedGHManager, ProductionPlanGHMWeeksYears savedWeeksYears) {
        weeksYearsDto.getVarietyQuantities().forEach(varietyQuantityDto -> {
            // Save the Variety Quantity
            ProductionPlanGHMVarietyQuantity varietyQuantity = saveVarietyQuantity(varietyQuantityDto, savedWeeksYears);

            // Debug: Check if varietyQuantity was saved correctly
            if (varietyQuantity.getId() == null) {
                log.error("Failed to save ProductionPlanGHMVarietyQuantity for varietyId: {}", varietyQuantityDto.getVarietyId());
                throw new RuntimeException("Failed to save ProductionPlanGHMVarietyQuantity");
            }

            // Save the TaskVarietySchedule first
            TaskVarietySchedule savedTaskVarietySchedule = saveTaskVarietySchedule(varietyQuantity, savedGHManager, savedWeeksYears);

            // Debug: Check if TaskVarietySchedule was saved correctly
            if (savedTaskVarietySchedule.getId() == null) {
                log.error("Failed to save TaskVarietySchedule for varietyId: {}", varietyQuantity.getVarietyId());
                throw new RuntimeException("Failed to save TaskVarietySchedule");
            }

            // Then save TaskCategoryTaskVarietySchedules for each task type by retrieving the appropriate TaskCategorySetup
            if (hasMaintenance(savedTaskVarietySchedule.getVariety().getId())) {
                saveTaskCategoryTaskVarietySchedule("MAINTENANCE", savedTaskVarietySchedule);
            }
            if (hasMonitoring(savedTaskVarietySchedule.getVariety().getId())) {
                saveTaskCategoryTaskVarietySchedule("MONITORING", savedTaskVarietySchedule);
            }
            saveTaskCategoryTaskVarietySchedule("PLANTATION", savedTaskVarietySchedule);
            saveTaskCategoryTaskVarietySchedule("HARVESTING", savedTaskVarietySchedule);
            saveTaskCategoryTaskVarietySchedule("QA", savedTaskVarietySchedule);
        });
    }

    private boolean hasMaintenance(Long varietyId) {
        return !taskMainVarietyRepository.findByVarietyIdAndDeletedFalse(varietyId).isEmpty();
    }

    private boolean hasMonitoring(Long varietyId) {
        return !taskMoniVarietyRepository.findByVarietyIdAndDeletedFalse(varietyId).isEmpty();
    }


    private ProductionPlanGHMVarietyQuantity saveVarietyQuantity(ProductionPlanGHMVarietyQuantityRequestDto varietyQuantityDto, ProductionPlanGHMWeeksYears savedWeeksYears) {
        ProductionPlanGHMVarietyQuantity varietyQuantity = ProductionPlanGHMVarietyQuantity.builder()
                .productionPlanGHMWeeksYearsId(savedWeeksYears.getId())
                .varietyId(varietyQuantityDto.getVarietyId())
                .varietyName(varietyQuantityDto.getVarietyName())
                .quantity(varietyQuantityDto.getQuantity())
                .build();

        return productionPlanGreenHouseManagerVarietyQuantityRepository.save(varietyQuantity);
    }


    private TaskVarietySchedule saveTaskVarietySchedule(ProductionPlanGHMVarietyQuantity varietyQuantity, ProductionPlanGHManager savedGHManager, ProductionPlanGHMWeeksYears savedWeeksYears) {
        // Fetch related entities
        Variety variety = varietyRepository.findByIdAndDeletedFalse(varietyQuantity.getVarietyId())
                .orElseThrow(() -> new RuntimeException("Variety not found for id: " + varietyQuantity.getVarietyId()));

        Farm farm = farmRepository.findByIdAndDeletedFalse(savedGHManager.getFarmId());

        Greenhouse greenhouse = greenhouseRepository.findByIdAndDeletedFalse(savedGHManager.getGreenhouseId());

        PlantationPlan plantationPlan = plantationPlanRepository.findByIdAndDeletedFalse(savedGHManager.getPlantationPlanId());

        Plant family = plantRepository.findByIdAndDeletedFalse(savedGHManager.getFamilyId())
                .orElseThrow(() -> new RuntimeException("Family not found for id: " + savedGHManager.getFamilyId()));

        // Fetch the associated ProductionPlanGHMBayBed entity
        ProductionPlanGHMBayBed bayBed = productionPlanGreenHouseManagerBayBedRepository.findById(savedWeeksYears.getProductionPlanGHMBayBedId())
                .orElseThrow(() -> new RuntimeException("ProductionPlanGHMBayBed not found for id: " + savedWeeksYears.getProductionPlanGHMBayBedId()));

        // Use the ProductionPlanGHMBayBed entity to get the associated Bay and Bed
        Bay bay = bayRepository.findByIdAndDeletedFalse(bayBed.getBayId());

        Bed bed = bedRepository.findByIdAndDeletedFalse(bayBed.getBedId())
                .orElseThrow(() -> new RuntimeException("Bed not found for id: " + bayBed.getBedId()));

        PlantationSchedule plantationSchedule = plantationScheduleRepository.findByIdAndDeletedFalse(savedGHManager.getPlantationScheduleId());


        List<ProductionPlanGHMWeeksYears> weeksYearsList = productionPlanGreenHouseManagerWeeksYearsRepository.findByProductionPlanGHMBayBedId(bayBed.getId());

        ProductionPlanGHMWeeksYears maxHarvestWeekData = weeksYearsList.stream()
                .max(Comparator.comparingInt(ProductionPlanGHMWeeksYears::getHarvestWeek))
                .orElseThrow(() -> new RuntimeException("No valid harvest week found"));

//        bed.setBedIsAvailable("AVAILABLE");
        bed.setTrayPlantWeek(maxHarvestWeekData.getTrayplantWeek());
        bed.setHarvestWeek(maxHarvestWeekData.getHarvestWeek());
        bed.setTransplantStartYear(maxHarvestWeekData.getTrayplantStartYear());
        bed.setTransplantEndYear(maxHarvestWeekData.getTransplantEndYear());
        bedRepository.save(bed);

        // Build the TaskVarietySchedule entity
        TaskVarietySchedule taskVarietySchedule = TaskVarietySchedule.builder()
                .variety(variety)
                .varietyName(variety.getVarietyName())
                .farm(farm)
                .greenhouse(greenhouse)
                .plantationSchedule(plantationSchedule)
                .plantationPlan(plantationPlan)
                .family(family)
                .bay(bay)
                .bed(bed)
                .trayPlantWeek(savedWeeksYears.getTrayplantWeek())
                .harvestWeek(savedWeeksYears.getHarvestWeek())
                .transplantStartYear(savedWeeksYears.getTrayplantStartYear())
                .transplantEndYear(savedWeeksYears.getTransplantEndYear())
                .quantity(varietyQuantity.getQuantity())
                .orientation(plantationSchedule.getStartWeek() + " - " + plantationSchedule.getEndWeek())
                .purpose(plantationSchedule.getPurpose())
                .build();

        return taskVarietyScheduleRepository.save(taskVarietySchedule);
    }

    private void saveTaskCategoryTaskVarietySchedule(String taskType,
                                                     TaskVarietySchedule savedTaskVarietySchedule) {

        Long varietyId = savedTaskVarietySchedule.getVariety().getId();

        if ("PLANTATION".equalsIgnoreCase(taskType) || "HARVESTING".equalsIgnoreCase(taskType) || "QA".equalsIgnoreCase(taskType)) {
            List<TaskCategorySetup> taskCategories = taskCategorySetupRepository.findByTaskTypeAndDeletedFalse(taskType);

            //todo remove after finalizing
//            if (taskCategories.isEmpty()) {
//                throw new RuntimeException("TaskCategorySetup not found for taskType: " + taskType);
//            }

            saveAllTaskCategoryTaskVarietySchedulesForTaskCategories(taskCategories, savedTaskVarietySchedule);
        } else if ("MONITORING".equalsIgnoreCase(taskType)) {
            List<TaskMoniVariety> taskMoniVarieties = taskMoniVarietyRepository.findByVarietyIdAndDeletedFalse(varietyId);

            if (!taskMoniVarieties.isEmpty()) {
                for (TaskMoniVariety taskMoniVariety : taskMoniVarieties) {
                    TaskMonitoring taskMonitoring = taskMonitoringRepository.findById(taskMoniVariety.getTaskMonitoringId())
                            .orElseThrow(() -> new RuntimeException("TaskMonitoring not found for id: " + taskMoniVariety.getTaskMonitoringId()));

                    // Set the monitoring task name
                    String monitoringTaskName = taskMonitoring.getMonitoringTaskName();
                    saveAllTaskCategoryTaskVarietySchedules(taskMonitoring.getTaskName(), taskMonitoring.getTaskType(), savedTaskVarietySchedule, monitoringTaskName);
                }
            } else {
                log.warn("No TaskMoniVariety found for varietyId: {}", varietyId);
            }
        } else if ("MAINTENANCE".equalsIgnoreCase(taskType)) {
            log.debug("Checking for maintenance tasks for varietyId: {}", varietyId);
            List<TaskMainVariety> taskMainVarieties = taskMainVarietyRepository.findByVarietyIdAndDeletedFalse(varietyId);
            log.debug("Found {} maintenance varieties for varietyId: {}", taskMainVarieties.size(), varietyId);

            if (!taskMainVarieties.isEmpty()) {
                for (TaskMainVariety taskMainVariety : taskMainVarieties) {
                    log.debug("Processing TaskMainVariety with id: {}", taskMainVariety.getId());
                    TaskMaintenance taskMaintenance = taskMaintenanceRepository.findById(taskMainVariety.getTaskMaintenanceId())
                            .orElseThrow(() -> new RuntimeException("TaskMaintenance not found for id: " + taskMainVariety.getTaskMaintenanceId()));

                    String maintenanceTaskName = taskMaintenance.getMaintenanceTaskName();
                    log.debug("Saving maintenance task for TaskCategoryId: {}", taskMaintenance.getTaskCategoryId());
                    saveAllTaskCategoryTaskVarietySchedules(taskMaintenance.getTaskName(), taskMaintenance.getTaskType(), savedTaskVarietySchedule, maintenanceTaskName);
                }
            } else {
                log.warn("No TaskMainVariety found for varietyId: {}", varietyId);
            }
        } else {
            throw new RuntimeException("Unsupported task type: " + taskType);
        }
    }

    private void saveAllTaskCategoryTaskVarietySchedules(Long taskCategoryId, TaskVarietySchedule savedTaskVarietySchedule, String taskName) {
        TaskCategorySetup taskCategorySetup = taskCategorySetupRepository.findById(taskCategoryId)
                .orElseThrow(() -> new RuntimeException("TaskCategorySetup not found for id: " + taskCategoryId));

        TaskCategoryTaskVarietySchedule taskCategoryTaskVarietySchedule = TaskCategoryTaskVarietySchedule.builder()
                .taskCategorySetup(taskCategorySetup)
                .taskCategoryName(taskCategorySetup.getTaskName())
                .taskType(taskCategorySetup.getTaskType())
                .taskName(taskName)
                .taskVarietySchedule(savedTaskVarietySchedule)
                .build();

        taskCategoryTaskVarietyScheduleRepository.save(taskCategoryTaskVarietySchedule);
    }

    private void saveAllTaskCategoryTaskVarietySchedules(String taskName, String taskType, TaskVarietySchedule savedTaskVarietySchedule, String monitoringTaskName) {
        List<TaskCategorySetup> taskCategorySetups = taskCategorySetupRepository.findByTaskNameAndTaskTypeAndDeletedFalse(taskName, taskType);

        if (taskCategorySetups.isEmpty()) {
            throw new RuntimeException("No TaskCategorySetup found for taskName: " + taskName + " and taskType: " + taskType);
        }

        for (TaskCategorySetup taskCategorySetup : taskCategorySetups) {
            TaskCategoryTaskVarietySchedule taskCategoryTaskVarietySchedule = TaskCategoryTaskVarietySchedule.builder()
                    .taskCategorySetup(taskCategorySetup)
                    .taskCategoryName(taskCategorySetup.getTaskName())
                    .taskType(taskCategorySetup.getTaskType())
                    .taskName(monitoringTaskName)
                    .taskVarietySchedule(savedTaskVarietySchedule)
                    .build();

            taskCategoryTaskVarietyScheduleRepository.save(taskCategoryTaskVarietySchedule);
        }
    }

    private void saveAllTaskCategoryTaskVarietySchedulesForTaskCategories(List<TaskCategorySetup> taskCategories, TaskVarietySchedule savedTaskVarietySchedule) {
        for (TaskCategorySetup taskCategorySetup : taskCategories) {
            saveAllTaskCategoryTaskVarietySchedules(taskCategorySetup.getId(), savedTaskVarietySchedule, "Default");
        }
    }

    private void validateDuplicatePlan(ProductionPlanGHManagerRequestDto requestDto) {
        if (productionPlanGreenHouseManagerRepository.existsByPlanName(requestDto.getPlanName())) {
            throw new IllegalArgumentException("A plan with the name " + requestDto.getPlanName() + " already exists.");
        }

        if (productionPlanGreenHouseManagerRepository.existsByGreenhouseIdAndProductionPlanFarmManagerId(requestDto.getGreenhouseId(), requestDto.getProductionPlanFarmManagerId())) {
            throw new IllegalArgumentException("A plan for greenhouse ID " + requestDto.getGreenhouseId() + " with farm manager ID " + requestDto.getProductionPlanFarmManagerId() + " already exists.");
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<ProductionPlanGHManagerResponseDto> getProductionPlansByGreenHouseId(Long greenhouseId) {
        List<ProductionPlanGHManager> ghManagers = productionPlanGreenHouseManagerRepository.findByGreenhouseIdAndDeletedFalse(greenhouseId);
        return ghManagers.stream()
                .map(this::convertToResponseDto)
                .collect(Collectors.toList());
    }

    private ProductionPlanGHManagerResponseDto convertToResponseDto(ProductionPlanGHManager ghManager, List<ProductionPlanGHMBayBed> bayBeds) {
        List<ProductionPlanGHMBayBedResponseDto> bayBedDtos = bayBeds.stream()
                .map(bayBed -> {
                    List<ProductionPlanGHMWeeksYears> weeksYears = productionPlanGreenHouseManagerWeeksYearsRepository.findByProductionPlanGHMBayBedId(bayBed.getId());
                    List<ProductionPlanGHMWeeksYearsResponseDto> weeksYearsDtos = weeksYears.stream()
                            .map(weeksYear -> {
                                List<ProductionPlanGHMVarietyQuantity> varietyQuantities = productionPlanGreenHouseManagerVarietyQuantityRepository.findByProductionPlanGHMWeeksYearsId(weeksYear.getId());
                                List<ProductionPlanGHMVarietyQuantityResponseDto> varietyQuantityDtos = varietyQuantities.stream()
                                        .map(varietyQuantity -> ProductionPlanGHMVarietyQuantityResponseDto.builder()
                                                .id(varietyQuantity.getId())
                                                .productionPlanGHMWeeksYearsId(varietyQuantity.getProductionPlanGHMWeeksYearsId())
                                                .varietyId(varietyQuantity.getVarietyId())
                                                .varietyName(varietyQuantity.getVarietyName())
                                                .quantity(varietyQuantity.getQuantity())
                                                .build())
                                        .collect(Collectors.toList());

                                return ProductionPlanGHMWeeksYearsResponseDto.builder()
                                        .id(weeksYear.getId())
                                        .trayplantWeek(weeksYear.getTrayplantWeek())
                                        .harvestWeek(weeksYear.getHarvestWeek())
                                        .productionPlanGHMBayBedId(weeksYear.getProductionPlanGHMBayBedId())
                                        .trayplantStartYear(weeksYear.getTrayplantStartYear())
                                        .transplantEndYear(weeksYear.getTransplantEndYear())
                                        .varietyQuantities(varietyQuantityDtos)
                                        .build();
                            })
                            .collect(Collectors.toList());

                    return ProductionPlanGHMBayBedResponseDto.builder()
                            .id(bayBed.getId())
                            .productionPlanGHManagerId(bayBed.getProductionPlanGHManagerId())
                            .bayId(bayBed.getBayId())
                            .bedId(bayBed.getBedId())
                            .weeksYears(weeksYearsDtos)
                            .build();
                })
                .collect(Collectors.toList());

        // Determine if the schedule is expired
        boolean expired = isScheduleExpired(ghManager.getPlantationEndDate());

        return ProductionPlanGHManagerResponseDto.builder()
                .id(ghManager.getId())
                .productionPlanFarmManagerId(ghManager.getProductionPlanFarmManagerId())
                .farmId(ghManager.getFarmId())
                .greenhouseId(ghManager.getGreenhouseId())
                .plantationPlanId(ghManager.getPlantationPlanId())
                .plantationScheduleId(ghManager.getPlantationScheduleId())
                .familyId(ghManager.getFamilyId())
                .planName(ghManager.getPlanName())
                .plantationStartDate(ghManager.getPlantationStartDate())
                .plantationEndDate(ghManager.getPlantationEndDate())
                .startWeek(ghManager.getStartWeek())  // Include these in the response
                .endWeek(ghManager.getEndWeek())      // Include these in the response
                .startYear(ghManager.getStartYear())  // Include these in the response
                .endYear(ghManager.getEndYear())      // Include these in the response
                .orientation(ghManager.getOrientation())  // Add orientation field
                .deleted(ghManager.isDeleted())
                .saved(ghManager.isSaved())
                .expired(expired)  // Set the expired field
                .bayBeds(bayBedDtos)
                .build();
    }

    private boolean isScheduleExpired(String plantationEndDate) {
        LocalDate endDate = LocalDate.parse(plantationEndDate, DateTimeFormatter.ISO_LOCAL_DATE);
        LocalDate currentDate = LocalDate.now();
        return endDate.isBefore(currentDate) || endDate.isEqual(currentDate);  // Expired if end date is before or equal to current date
    }

    private ProductionPlanGHManagerResponseDto convertToResponseDto(ProductionPlanGHManager ghManager) {
        List<ProductionPlanGHMBayBed> bayBeds = productionPlanGreenHouseManagerBayBedRepository.findByProductionPlanGHManagerIdAndDeletedFalse(ghManager.getId());
        return convertToResponseDto(ghManager, bayBeds);
    }

}
