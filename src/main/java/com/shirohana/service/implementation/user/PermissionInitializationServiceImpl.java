//package com.shirohana.service.implementation.user;
//
//import com.shirohana.entity.user.Controller;
//import com.shirohana.entity.user.PermissionGroup;
//import com.shirohana.repository.user.ControllerRepository;
//import com.shirohana.repository.user.PermissionGroupRepository;
//import com.shirohana.repository.user.PrivilegeRepository;
//import com.shirohana.service.interfaces.user.PermissionInitializationService;
//import jakarta.annotation.PostConstruct;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import com.shirohana.entity.user.Privilege;
//
//import java.util.Arrays;
//import java.util.List;
//import java.util.Optional;
//
//@Service
//public class PermissionInitializationServiceImpl implements PermissionInitializationService {
//
//    @Autowired
//    private PermissionGroupRepository permissionGroupRepository;
//
//    @Autowired
//    private PrivilegeRepository privilegeRepository;
//
//    @Autowired
//    private ControllerRepository controllerRepository;
//
//    @Override
//    @PostConstruct
//    public void init() {
//        initializePermissionGroups();
//        initializeControllers();
//        groupPermissionsAndSave();
//    }
//
//    private void initializePermissionGroups() {
//        List<PermissionGroup> groups = Arrays.asList(
//                new PermissionGroup("UserPermissions"),
//                new PermissionGroup("FarmPermissions"),
//                new PermissionGroup("BayPermissions"),
//                new PermissionGroup("BedPermissions"),
//                new PermissionGroup("GreenhousePermissions"),
//                new PermissionGroup("VarietyPermissions"),
//                new PermissionGroup("PlantPermissions"),
//                new PermissionGroup("PrivilegePermissions"),
//                new PermissionGroup("HarvestingAndRetentionPermissions"),
//                new PermissionGroup("EmployeePermissions"),
//                new PermissionGroup("TaskCategorySetupPermissions"),
//                new PermissionGroup("MaintenanceTaskSetupPermissions"),
//                new PermissionGroup("MonitoringTaskSetupPermissions"),
//                new PermissionGroup("ProdPlanMaintenanceTaskPermissions"),
//                new PermissionGroup("ProdPlanMonitoringTaskPermissions"),
//                new PermissionGroup("SchedulePermissions"),
//                new PermissionGroup("PlantationPlanPermissions"),
//                new PermissionGroup("PlantationTaskSetupPermissions"),
//                new PermissionGroup("HarvestingTaskSetupPermissions"),
//                new PermissionGroup("QualityTaskSetupPermissions"),
//                new PermissionGroup("ProductionPlanPermissions"),// New group for Production Plan related permissions
//                new PermissionGroup("ProductionPlanPermissions"), // New group for Production Plan related permissions
//                new PermissionGroup("TaskMaintenancePermissions"), // New group for Task Maintenance permissions
//                new PermissionGroup("TaskMonitoringPermissions"), // New group for Task Monitoring permissions
//                new PermissionGroup("TaskVarietySchedulePermissions") // New group for Task Variety Schedule permissions
//        );
//
//        for (PermissionGroup group : groups) {
//            PermissionGroup existingGroup = permissionGroupRepository.findByName(group.getName());
//            if (existingGroup == null) {
//                permissionGroupRepository.save(group);
//            } else {
//                existingGroup.setName(group.getName());
//                permissionGroupRepository.save(existingGroup);
//            }
//        }
//    }
//
//    private void initializeControllers() {
//        List<Controller> controllers = Arrays.asList(
//                new Controller("UserController"),
//                new Controller("FarmController"),
//                new Controller("BayController"),
//                new Controller("BedController"),
//                new Controller("GreenhouseController"),
//                new Controller("VarietyController"),
//                new Controller("PlantController"),
//                new Controller("PrivilegeController"),
//                new Controller("HarvestingAndRetentionController"),
//                new Controller("EmployeeController"),
//                new Controller("TaskCategorySetupController"),
//                new Controller("MaintenanceTaskSetupController"),
//                new Controller("MonitoringTaskSetupController"),
//                new Controller("ProdPlanMaintenanceTaskController"),
//                new Controller("ProdPlanMonitoringTaskController"),
//                new Controller("ScheduleController"),
//                new Controller("PlantationPlanController"),
//                new Controller("PlantationTaskSetupController"),
//                new Controller("HarvestingTaskSetupController"),
//                new Controller("QualityTaskSetupController"),
//                new Controller("ProductionPlanGreenHouseManagerController") // New controller for Production Plan Greenhouse management
//        );
//
//        for (Controller controller : controllers) {
//            Controller existingController = controllerRepository.findByName(controller.getName());
//            if (existingController == null) {
//                controllerRepository.save(controller);
//            } else {
//                existingController.setName(controller.getName());
//                controllerRepository.save(existingController);
//            }
//        }
//    }
//
//    private void groupPermissionsAndSave() {
//        List<PermissionGroup> groups = permissionGroupRepository.findAll();
//        List<Controller> controllers = controllerRepository.findAll();
//
//        for (String permission : getAllPermissions()) {
//            String groupName = getGroupName(permission);
//            String controllerName = getControllerName(permission);
//
//            PermissionGroup group = groups.stream()
//                    .filter(g -> g.getName().equals(groupName))
//                    .findFirst()
//                    .orElse(null);
//
//            Controller controller = controllers.stream()
//                    .filter(c -> c.getName().equals(controllerName))
//                    .findFirst()
//                    .orElse(null);
//
//            if (group != null && controller != null) {
//                Optional<Privilege> existingPrivilege = privilegeRepository.findByName(permission);
//                if (existingPrivilege.isEmpty()) {
//                    Privilege newPrivilege = new Privilege(permission, group, controller);
//                    privilegeRepository.save(newPrivilege);
//                } else {
//                    Privilege privilegeToUpdate = existingPrivilege.get();
//                    privilegeToUpdate.setGroup(group);
//                    privilegeToUpdate.setController(controller);
//                    privilegeRepository.save(privilegeToUpdate);
//                }
//            }
//        }
//    }
//
//    private List<String> getAllPermissions() {
//        return Arrays.asList(
//                "USER_CREATE_PERMISSION",
//                "USER_UPDATE_PERMISSION",
//                "USER_DELETE_PERMISSION",
//                "USER_READ_PERMISSION",
//                "FARM_CREATE_PERMISSION",
//                "FARM_UPDATE_PERMISSION",
//                "FARM_DELETE_PERMISSION",
//                "FARM_READ_PERMISSION",
//                "BAY_CREATE_PERMISSION",
//                "BAY_UPDATE_PERMISSION",
//                "BAY_DELETE_PERMISSION",
//                "BAY_READ_PERMISSION",
//                "BED_CREATE_PERMISSION",
//                "BED_UPDATE_PERMISSION",
//                "BED_DELETE_PERMISSION",
//                "BED_READ_PERMISSION",
//                "GREENHOUSE_CREATE_PERMISSION",
//                "GREENHOUSE_UPDATE_PERMISSION",
//                "GREENHOUSE_DELETE_PERMISSION",
//                "GREENHOUSE_READ_PERMISSION",
//                "VARIETY_CREATE_PERMISSION",
//                "VARIETY_UPDATE_PERMISSION",
//                "VARIETY_DELETE_PERMISSION",
//                "VARIETY_READ_PERMISSION",
//                "PLANT_CREATE_PERMISSION",
//                "PLANT_UPDATE_PERMISSION",
//                "PLANT_DELETE_PERMISSION",
//                "PLANT_READ_PERMISSION",
//                "PRIVILEGE_VIEW_ROLES",
//                "PRIVILEGE_VIEW_ALL_ROLES",
//                "PRIVILEGE_CREATE_ROLE",
//                "PRIVILEGE_DELETE_ROLE",
//                "ROLE_ASSIGN_PRIVILEGES",
//                "PRIVILEGE_READ_PRIVILEGES",
//                "PRIVILEGE_CREATE_PRIVILEGE",
//                "PRIVILEGE_UPDATE_PRIVILEGE",
//                "PRIVILEGE_DELETE_PRIVILEGE",
//                "HARVESTING_AND_RETENTION_CREATE_PERMISSION",
//                "HARVESTING_AND_RETENTION_UPDATE_PERMISSION",
//                "HARVESTING_AND_RETENTION_DELETE_PERMISSION",
//                "HARVESTING_AND_RETENTION_READ_PERMISSION",
//                "EMPLOYEE_CREATE_PERMISSION",
//                "EMPLOYEE_UPDATE_PERMISSION",
//                "EMPLOYEE_DELETE_PERMISSION",
//                "EMPLOYEE_READ_PERMISSION",
//                "TASK_CATEGORY_SETUP_CREATE_PERMISSION",
//                "TASK_CATEGORY_SETUP_UPDATE_PERMISSION",
//                "TASK_CATEGORY_SETUP_DELETE_PERMISSION",
//                "TASK_CATEGORY_SETUP_READ_PERMISSION",
//                "MAINTENANCE_TASK_SETUP_CREATE_PERMISSION",
//                "MAINTENANCE_TASK_SETUP_UPDATE_PERMISSION",
//                "MAINTENANCE_TASK_SETUP_DELETE_PERMISSION",
//                "MAINTENANCE_TASK_SETUP_READ_PERMISSION",
//                "MONITORING_TASK_SETUP_CREATE_PERMISSION",
//                "MONITORING_TASK_SETUP_UPDATE_PERMISSION",
//                "MONITORING_TASK_SETUP_DELETE_PERMISSION",
//                "MONITORING_TASK_SETUP_READ_PERMISSION",
//                "TASK_MAINTENANCE_CREATE_PERMISSION", // Added here
//                "TASK_MAINTENANCE_UPDATE_PERMISSION", // Added here
//                "TASK_MAINTENANCE_DELETE_PERMISSION", // Added here
//                "TASK_MAINTENANCE_READ_PERMISSION",   // Added here
//                "TASK_MONITORING_CREATE_PERMISSION",  // Added here
//                "TASK_MONITORING_UPDATE_PERMISSION",  // Added here
//                "TASK_MONITORING_DELETE_PERMISSION",  // Added here
//                "TASK_MONITORING_READ_PERMISSION",    // Added here
//                "PROD_PLAN_MAINTENANCE_TASK_CREATE_PERMISSION",
//                "PROD_PLAN_MAINTENANCE_TASK_UPDATE_PERMISSION",
//                "PROD_PLAN_MAINTENANCE_TASK_DELETE_PERMISSION",
//                "PROD_PLAN_MAINTENANCE_TASK_READ_PERMISSION",
//                "PROD_PLAN_MONITORING_TASK_CREATE_PERMISSION",
//                "PROD_PLAN_MONITORING_TASK_UPDATE_PERMISSION",
//                "PROD_PLAN_MONITORING_TASK_DELETE_PERMISSION",
//                "PROD_PLAN_MONITORING_TASK_READ_PERMISSION",
//                "PRIVILEGE_CREATE_SCHEDULE",
//                "PRIVILEGE_VIEW_FAMILIES",
//                "PRIVILEGE_CREATE_PLANTATION_PLAN",
//                "PRIVILEGE_VIEW_FAMILIES_PLANTATION_PLAN",
//                "PRIVILEGE_VIEW_PLANTATION_SCHEDULES",
//                "PRIVILEGE_VIEW_PLANTATION_PLANS",
//                "PLANTATION_TASK_SETUP_CREATE_PERMISSION",
//                "PLANTATION_TASK_SETUP_UPDATE_PERMISSION",
//                "PLANTATION_TASK_SETUP_DELETE_PERMISSION",
//                "PLANTATION_TASK_SETUP_READ_PERMISSION",
//                "HARVESTING_TASK_SETUP_CREATE_PERMISSION",
//                "HARVESTING_TASK_SETUP_UPDATE_PERMISSION",
//                "HARVESTING_TASK_SETUP_DELETE_PERMISSION",
//                "HARVESTING_TASK_SETUP_READ_PERMISSION",
//                "QUALITY_TASK_SETUP_CREATE_PERMISSION",
//                "QUALITY_TASK_SETUP_UPDATE_PERMISSION",
//                "QUALITY_TASK_SETUP_DELETE_PERMISSION",
//                "QUALITY_TASK_SETUP_READ_PERMISSION",
//                "PRIVILEGE_VIEW_PRODUCTION_PLAN",
//                "PRIVILEGE_DELETE_PLANTATION_SCHEDULE",
//                "PRIVILEGE_DELETE_PLANTATION_PLAN",
//                "PRIVILEGE_VIEW_ALL_PLANTATION_PLANS",
//                "PRIVILEGE_VIEW_PRODUCTION_PLANS",
//                "PRIVILEGE_CREATE_PRODUCTION_PLAN",
//                "PRIVILEGE_VIEW_USER_FARM_PRODUCTION_PLANS",
//                "PRIVILEGE_VIEW_USER_FARM_PLANTATION_PLANS",
//                "PRIVILEGE_VIEW_FARM_PRODUCTION_PLANS",
//                "PRIVILEGE_VIEW_PENDING_PRODUCTION_PLANS",
//                "PRIVILEGE_DELETE_PRODUCTION_PLAN_FARM_MANAGER",
//                "PRIVILEGE_VIEW_TASK_DETAILS",
//                "PRIVILEGE_VIEW_PROD_PLAN_BY_CATEGORY",
//                "PRIVILEGE_VIEW_PROD_PLAN_BY_CATEGORY_NAME",
//                "PRIVILEGE_VIEW_PROD_PLAN_BY_CATEGORY_TYPE",
//                "PRIVILEGE_SAVE_TASK_VARITY_SCHEDULE",
//                "PRIVILEGE_CREATE_ADDITIONAL_TASK",
//                "PRIVILEGE_VIEW_GROUPED_TASK_TYPES"
//        );
//    }
//
//
//    private String getGroupName(String permission) {
//        switch (permission) {
//            case "USER_CREATE_PERMISSION":
//            case "USER_UPDATE_PERMISSION":
//            case "USER_DELETE_PERMISSION":
//            case "USER_READ_PERMISSION":
//                return "UserPermissions";
//            case "FARM_CREATE_PERMISSION":
//            case "FARM_UPDATE_PERMISSION":
//            case "FARM_DELETE_PERMISSION":
//            case "FARM_READ_PERMISSION":
//                return "FarmPermissions";
//            case "BAY_CREATE_PERMISSION":
//            case "BAY_UPDATE_PERMISSION":
//            case "BAY_DELETE_PERMISSION":
//            case "BAY_READ_PERMISSION":
//                return "BayPermissions";
//            case "BED_CREATE_PERMISSION":
//            case "BED_UPDATE_PERMISSION":
//            case "BED_DELETE_PERMISSION":
//            case "BED_READ_PERMISSION":
//                return "BedPermissions";
//            case "GREENHOUSE_CREATE_PERMISSION":
//            case "GREENHOUSE_UPDATE_PERMISSION":
//            case "GREENHOUSE_DELETE_PERMISSION":
//            case "GREENHOUSE_READ_PERMISSION":
//                return "GreenhousePermissions";
//            case "VARIETY_CREATE_PERMISSION":
//            case "VARIETY_UPDATE_PERMISSION":
//            case "VARIETY_DELETE_PERMISSION":
//            case "VARIETY_READ_PERMISSION":
//                return "VarietyPermissions";
//            case "PLANT_CREATE_PERMISSION":
//            case "PLANT_UPDATE_PERMISSION":
//            case "PLANT_DELETE_PERMISSION":
//            case "PLANT_READ_PERMISSION":
//                return "PlantPermissions";
//            case "PRIVILEGE_VIEW_ROLES":
//            case "PRIVILEGE_VIEW_ALL_ROLES":
//            case "PRIVILEGE_CREATE_ROLE":
//            case "PRIVILEGE_DELETE_ROLE":
//            case "ROLE_ASSIGN_PRIVILEGES":
//            case "PRIVILEGE_READ_PRIVILEGES":
//            case "PRIVILEGE_CREATE_PRIVILEGE":
//            case "PRIVILEGE_UPDATE_PRIVILEGE":
//            case "PRIVILEGE_DELETE_PRIVILEGE":
//                return "PrivilegePermissions";
//            case "HARVESTING_AND_RETENTION_CREATE_PERMISSION":
//            case "HARVESTING_AND_RETENTION_UPDATE_PERMISSION":
//            case "HARVESTING_AND_RETENTION_DELETE_PERMISSION":
//            case "HARVESTING_AND_RETENTION_READ_PERMISSION":
//                return "HarvestingAndRetentionPermissions";
//            case "EMPLOYEE_CREATE_PERMISSION":
//            case "EMPLOYEE_UPDATE_PERMISSION":
//            case "EMPLOYEE_DELETE_PERMISSION":
//            case "EMPLOYEE_READ_PERMISSION":
//                return "EmployeePermissions";
//            case "TASK_CATEGORY_SETUP_CREATE_PERMISSION":
//            case "TASK_CATEGORY_SETUP_UPDATE_PERMISSION":
//            case "TASK_CATEGORY_SETUP_DELETE_PERMISSION":
//            case "TASK_CATEGORY_SETUP_READ_PERMISSION":
//                return "TaskCategorySetupPermissions";
//            case "MAINTENANCE_TASK_SETUP_CREATE_PERMISSION":
//            case "MAINTENANCE_TASK_SETUP_UPDATE_PERMISSION":
//            case "MAINTENANCE_TASK_SETUP_DELETE_PERMISSION":
//            case "MAINTENANCE_TASK_SETUP_READ_PERMISSION":
//                return "MaintenanceTaskSetupPermissions";
//            case "MONITORING_TASK_SETUP_CREATE_PERMISSION":
//            case "MONITORING_TASK_SETUP_UPDATE_PERMISSION":
//            case "MONITORING_TASK_SETUP_DELETE_PERMISSION":
//            case "MONITORING_TASK_SETUP_READ_PERMISSION":
//                return "MonitoringTaskSetupPermissions";
//            case "PROD_PLAN_MAINTENANCE_TASK_CREATE_PERMISSION":
//            case "PROD_PLAN_MAINTENANCE_TASK_UPDATE_PERMISSION":
//            case "PROD_PLAN_MAINTENANCE_TASK_DELETE_PERMISSION":
//            case "PROD_PLAN_MAINTENANCE_TASK_READ_PERMISSION":
//                return "ProdPlanMaintenanceTaskPermissions";
//            case "PROD_PLAN_MONITORING_TASK_CREATE_PERMISSION":
//            case "PROD_PLAN_MONITORING_TASK_UPDATE_PERMISSION":
//            case "PROD_PLAN_MONITORING_TASK_DELETE_PERMISSION":
//            case "PROD_PLAN_MONITORING_TASK_READ_PERMISSION":
//                return "ProdPlanMonitoringTaskPermissions";
//            case "PRIVILEGE_CREATE_SCHEDULE":
//            case "PRIVILEGE_VIEW_FAMILIES":
//                return "SchedulePermissions";
//            case "PRIVILEGE_CREATE_PLANTATION_PLAN":
//            case "PRIVILEGE_VIEW_FAMILIES_PLANTATION_PLAN":
//            case "PRIVILEGE_VIEW_PLANTATION_SCHEDULES":
//            case "PRIVILEGE_VIEW_PLANTATION_PLANS":
//            case "PRIVILEGE_DELETE_PLANTATION_PLAN":
//            case "PRIVILEGE_VIEW_ALL_PLANTATION_PLANS":
//                return "PlantationPlanPermissions";
//            case "PLANTATION_TASK_SETUP_CREATE_PERMISSION":
//            case "PLANTATION_TASK_SETUP_UPDATE_PERMISSION":
//            case "PLANTATION_TASK_SETUP_DELETE_PERMISSION":
//            case "PLANTATION_TASK_SETUP_READ_PERMISSION":
//                return "PlantationTaskSetupPermissions";
//            case "HARVESTING_TASK_SETUP_CREATE_PERMISSION":
//            case "HARVESTING_TASK_SETUP_UPDATE_PERMISSION":
//            case "HARVESTING_TASK_SETUP_DELETE_PERMISSION":
//            case "HARVESTING_TASK_SETUP_READ_PERMISSION":
//                return "HarvestingTaskSetupPermissions";
//            case "QUALITY_TASK_SETUP_CREATE_PERMISSION":
//            case "QUALITY_TASK_SETUP_UPDATE_PERMISSION":
//            case "QUALITY_TASK_SETUP_DELETE_PERMISSION":
//            case "QUALITY_TASK_SETUP_READ_PERMISSION":
//                return "QualityTaskSetupPermissions";
//            case "PRIVILEGE_VIEW_PRODUCTION_PLAN":
//            case "PRIVILEGE_VIEW_USER_FARM_PRODUCTION_PLANS":
//            case "PRIVILEGE_VIEW_USER_FARM_PLANTATION_PLANS":
//            case "PRIVILEGE_VIEW_FARM_PRODUCTION_PLANS":
//            case "PRIVILEGE_VIEW_PENDING_PRODUCTION_PLANS":
//            case "PRIVILEGE_CREATE_PRODUCTION_PLAN":
//            case "PRIVILEGE_DELETE_PRODUCTION_PLAN_FARM_MANAGER":
//                return "ProductionPlanPermissions";
//            case "TASK_MAINTENANCE_CREATE_PERMISSION":
//            case "TASK_MAINTENANCE_UPDATE_PERMISSION":
//            case "TASK_MAINTENANCE_DELETE_PERMISSION":
//            case "TASK_MAINTENANCE_READ_PERMISSION":
//                return "TaskMaintenancePermissions";
//            case "TASK_MONITORING_CREATE_PERMISSION":
//            case "TASK_MONITORING_UPDATE_PERMISSION":
//            case "TASK_MONITORING_DELETE_PERMISSION":
//            case "TASK_MONITORING_READ_PERMISSION":
//                return "TaskMonitoringPermissions";
//            case "PRIVILEGE_VIEW_GROUPED_TASK_TYPES":
//            case "PRIVILEGE_VIEW_TASK_VARITY_SCHEDULE":
//            case "PRIVILEGE_CREATE_ADDITIONAL_TASK":
//            case "PRIVILEGE_SAVE_TASK_VARITY_SCHEDULE":
//            case "PRIVILEGE_VIEW_PROD_PLAN_BY_CATEGORY_NAME":
//            case "PRIVILEGE_VIEW_PROD_PLAN_BY_CATEGORY_TYPE":
//            case "PRIVILEGE_VIEW_TASK_DETAILS":
//                return "TaskVarietySchedulePermissions";
//            default:
//                return null;
//        }
//    }
//
//    private String getControllerName(String permission) {
//        switch (permission) {
//            case "USER_CREATE_PERMISSION":
//            case "USER_UPDATE_PERMISSION":
//            case "USER_DELETE_PERMISSION":
//            case "USER_READ_PERMISSION":
//                return "UserController";
//            case "FARM_CREATE_PERMISSION":
//            case "FARM_UPDATE_PERMISSION":
//            case "FARM_DELETE_PERMISSION":
//            case "FARM_READ_PERMISSION":
//                return "FarmController";
//            case "BAY_CREATE_PERMISSION":
//            case "BAY_UPDATE_PERMISSION":
//            case "BAY_DELETE_PERMISSION":
//            case "BAY_READ_PERMISSION":
//                return "BayController";
//            case "BED_CREATE_PERMISSION":
//            case "BED_UPDATE_PERMISSION":
//            case "BED_DELETE_PERMISSION":
//            case "BED_READ_PERMISSION":
//                return "BedController";
//            case "GREENHOUSE_CREATE_PERMISSION":
//            case "GREENHOUSE_UPDATE_PERMISSION":
//            case "GREENHOUSE_DELETE_PERMISSION":
//            case "GREENHOUSE_READ_PERMISSION":
//                return "GreenhouseController";
//            case "VARIETY_CREATE_PERMISSION":
//            case "VARIETY_UPDATE_PERMISSION":
//            case "VARIETY_DELETE_PERMISSION":
//            case "VARIETY_READ_PERMISSION":
//                return "VarietyController";
//            case "PLANT_CREATE_PERMISSION":
//            case "PLANT_UPDATE_PERMISSION":
//            case "PLANT_DELETE_PERMISSION":
//            case "PLANT_READ_PERMISSION":
//                return "PlantController";
//            case "PRIVILEGE_VIEW_ROLES":
//            case "PRIVILEGE_VIEW_ALL_ROLES":
//            case "PRIVILEGE_CREATE_ROLE":
//            case "PRIVILEGE_DELETE_ROLE":
//            case "ROLE_ASSIGN_PRIVILEGES":
//            case "PRIVILEGE_READ_PRIVILEGES":
//            case "PRIVILEGE_CREATE_PRIVILEGE":
//            case "PRIVILEGE_UPDATE_PRIVILEGE":
//            case "PRIVILEGE_DELETE_PRIVILEGE":
//                return "PrivilegeController";
//            case "HARVESTING_AND_RETENTION_CREATE_PERMISSION":
//            case "HARVESTING_AND_RETENTION_UPDATE_PERMISSION":
//            case "HARVESTING_AND_RETENTION_DELETE_PERMISSION":
//            case "HARVESTING_AND_RETENTION_READ_PERMISSION":
//                return "HarvestingAndRetentionController";
//            case "EMPLOYEE_CREATE_PERMISSION":
//            case "EMPLOYEE_UPDATE_PERMISSION":
//            case "EMPLOYEE_DELETE_PERMISSION":
//            case "EMPLOYEE_READ_PERMISSION":
//                return "EmployeeController";
//            case "TASK_CATEGORY_SETUP_CREATE_PERMISSION":
//            case "TASK_CATEGORY_SETUP_UPDATE_PERMISSION":
//            case "TASK_CATEGORY_SETUP_DELETE_PERMISSION":
//            case "TASK_CATEGORY_SETUP_READ_PERMISSION":
//                return "TaskCategorySetupController";
//            case "MAINTENANCE_TASK_SETUP_CREATE_PERMISSION":
//            case "MAINTENANCE_TASK_SETUP_UPDATE_PERMISSION":
//            case "MAINTENANCE_TASK_SETUP_DELETE_PERMISSION":
//            case "MAINTENANCE_TASK_SETUP_READ_PERMISSION":
//                return "MaintenanceTaskSetupController";
//            case "MONITORING_TASK_SETUP_CREATE_PERMISSION":
//            case "MONITORING_TASK_SETUP_UPDATE_PERMISSION":
//            case "MONITORING_TASK_SETUP_DELETE_PERMISSION":
//            case "MONITORING_TASK_SETUP_READ_PERMISSION":
//                return "MonitoringTaskSetupController";
//            case "PROD_PLAN_MAINTENANCE_TASK_CREATE_PERMISSION":
//            case "PROD_PLAN_MAINTENANCE_TASK_UPDATE_PERMISSION":
//            case "PROD_PLAN_MAINTENANCE_TASK_DELETE_PERMISSION":
//            case "PROD_PLAN_MAINTENANCE_TASK_READ_PERMISSION":
//                return "ProdPlanMaintenanceTaskController";
//            case "PROD_PLAN_MONITORING_TASK_CREATE_PERMISSION":
//            case "PROD_PLAN_MONITORING_TASK_UPDATE_PERMISSION":
//            case "PROD_PLAN_MONITORING_TASK_DELETE_PERMISSION":
//            case "PROD_PLAN_MONITORING_TASK_READ_PERMISSION":
//                return "ProdPlanMonitoringTaskController";
//            case "PRIVILEGE_CREATE_SCHEDULE":
//            case "PRIVILEGE_VIEW_FAMILIES":
//                return "ScheduleController";
//            case "PRIVILEGE_CREATE_PLANTATION_PLAN":
//            case "PRIVILEGE_VIEW_FAMILIES_PLANTATION_PLAN":
//            case "PRIVILEGE_VIEW_PLANTATION_SCHEDULES":
//            case "PRIVILEGE_VIEW_PLANTATION_PLANS":
//            case "PRIVILEGE_DELETE_PLANTATION_PLAN":
//            case "PRIVILEGE_VIEW_ALL_PLANTATION_PLANS":
//                return "PlantationPlanController";
//            case "PLANTATION_TASK_SETUP_CREATE_PERMISSION":
//            case "PLANTATION_TASK_SETUP_UPDATE_PERMISSION":
//            case "PLANTATION_TASK_SETUP_DELETE_PERMISSION":
//            case "PLANTATION_TASK_SETUP_READ_PERMISSION":
//                return "PlantationTaskSetupController";
//            case "HARVESTING_TASK_SETUP_CREATE_PERMISSION":
//            case "HARVESTING_TASK_SETUP_UPDATE_PERMISSION":
//            case "HARVESTING_TASK_SETUP_DELETE_PERMISSION":
//            case "HARVESTING_TASK_SETUP_READ_PERMISSION":
//                return "HarvestingTaskSetupController";
//            case "QUALITY_TASK_SETUP_CREATE_PERMISSION":
//            case "QUALITY_TASK_SETUP_UPDATE_PERMISSION":
//            case "QUALITY_TASK_SETUP_DELETE_PERMISSION":
//            case "QUALITY_TASK_SETUP_READ_PERMISSION":
//                return "QualityTaskSetupController";
//            case "PRIVILEGE_VIEW_PRODUCTION_PLAN": // New case for viewing production plan permissions
//            case "PRIVILEGE_VIEW_USER_FARM_PRODUCTION_PLANS":
//            case "PRIVILEGE_VIEW_USER_FARM_PLANTATION_PLANS":
//            case "PRIVILEGE_VIEW_FARM_PRODUCTION_PLANS":
//            case "PRIVILEGE_VIEW_PENDING_PRODUCTION_PLANS":
//            case "PRIVILEGE_CREATE_PRODUCTION_PLAN":
//            case "PRIVILEGE_DELETE_PRODUCTION_PLAN_FARM_MANAGER":
//                return "ProductionPlanGreenHouseManagerController"; // New controller for production plan related permissions
//            default:
//                return null;
//        }
//    }
//}
