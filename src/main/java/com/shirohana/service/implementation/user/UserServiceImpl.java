package com.shirohana.service.implementation.user;

import com.shirohana.dto.user.*;
import com.shirohana.entity.user.*;
import com.shirohana.entity.employee.Employee;
import com.shirohana.entity.farm.Farm;
import com.shirohana.repository.user.ManagingLevelRepository;
import com.shirohana.repository.user.RoleRepository;
import com.shirohana.repository.user.UserRepository;
import com.shirohana.repository.employee.EmployeeRepository;
import com.shirohana.repository.farm.FarmRepository;
import com.shirohana.service.interfaces.user.PermissionGroupService;
import com.shirohana.service.interfaces.user.UserService;
import jakarta.validation.ValidationException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final ManagingLevelRepository managingLevelRepository;
    private final PermissionGroupService permissionGroupService;
    private final PasswordEncoder passwordEncoder;
    private final EmployeeRepository employeeRepository;
    private final FarmRepository farmRepository;

    @Override
    public List<User> findAll() {
        try {
            return userRepository.findAllByDeletedFalse();
        } catch (Exception e) {
            log.error("Error fetching all users: {}", e.getMessage());
            throw new RuntimeException(e);
        }
    }

    @Override
    public Optional<User> findById(Long id) {
        try {
            return userRepository.findByIdAndDeletedFalse(id);
        } catch (Exception e) {
            log.error("Error fetching user by ID {}: {}", id, e.getMessage());
            throw new RuntimeException( e);
        }
    }

    @Transactional
    @Override
    public void assignRolesToUser(Long userId, List<Long> roleIds) {
        try {
            User user = userRepository.findByIdAndDeletedFalse(userId)
                    .orElseThrow(() -> new RuntimeException("User not found "));

            Set<Role> roles = roleIds.stream()
                    .map(roleId -> roleRepository.findById(roleId)
                            .orElseThrow(() -> new RuntimeException("Invalid role ID: " + roleId)))
                    .collect(Collectors.toSet());

            user.setRoles(roles);
            userRepository.save(user);
        } catch (Exception e) {
            log.error("Error assigning roles to user: {}", e.getMessage());
            throw new RuntimeException( e);
        }
    }

    @Override
    public Optional<User> findByIdString(String id) {
        try {
            Long longId = Long.parseLong(id);
            return userRepository.findByIdAndDeletedFalse(longId);
        } catch (NumberFormatException e) {
            log.error("Invalid user ID format: {}", id);
            return Optional.empty();
        }
    }

    @Override
    public Optional<User> findByUsername(String username) {
        try {
            return userRepository.findByUsernameAndDeletedFalse(username);
        } catch (Exception e) {
            log.error("Error fetching user by username {}: {}", username, e.getMessage());
            throw new RuntimeException(e);
        }
    }

    @Transactional
    @Override
    public User createUser(UserDTO userDto, User createdBy) {
        try {
            log.info("Starting user creation for username: {}", userDto.getUsername());

            // Check if the username already exists
            if (userRepository.findByUsernameAndDeletedFalse(userDto.getUsername()).isPresent()) {
                String errorMessage = "Username " + userDto.getUsername() + " already exists";
                log.error(errorMessage);
                throw new ValidationException(errorMessage);
            }

            // Fetch managing level
            ManagingLevel userLevel = managingLevelRepository.findByLevel(userDto.getManagingLevel());
            if (userLevel == null) {
                String errorMessage = "Invalid managing level: " + userDto.getManagingLevel();
                log.error(errorMessage);
                throw new ValidationException(errorMessage);
            }

            // Check if the createdBy user's level is higher than the new user's level
            if (createdBy != null && userLevel.getLevel() > createdBy.getManagingLevel().getLevel()) {
                String errorMessage = "You cannot create a user with a managing level equal to or higher than yours. Please select a lower managing level.";
                log.error("User {} cannot create a user with the same or higher level than their own", createdBy.getUsername());
                throw new ValidationException(errorMessage);
            }

            // Fetch roles
            Set<Role> roles = userDto.getRoleIds().stream()
                    .map(roleId -> roleRepository.findById(roleId)
                            .orElseThrow(() -> {
                                String errorMessage = "Invalid role ID: " + roleId;
                                log.error(errorMessage);
                                return new ValidationException(errorMessage);
                            }))
                    .collect(Collectors.toSet());

            // Create new user entity
            User user = User.builder()
                    .username(userDto.getUsername())
                    .password(passwordEncoder.encode(userDto.getPassword()))
                    .enabled(true)
                    .tokenExpired(false)
                    .roles(roles)
                    .managingLevel(userLevel)
                    .contactNumber(userDto.getContactNumber())
                    .email(userDto.getEmail())
                    .userIsEmployee(userDto.isEmployee())
                    .build();

            // Set employee if applicable
            if (userDto.getEmployeeId() != null) {
                Employee employee = employeeRepository.findById(userDto.getEmployeeId())
                        .orElseThrow(() -> {
                            String errorMessage = "Invalid employee ID: " + userDto.getEmployeeId();
                            log.error(errorMessage);
                            return new ValidationException(errorMessage);
                        });
                user.setEmployee(employee);
                user.setUserIsEmployee(true);
            } else {
                user.setUserIsEmployee(false);
            }

            // Fetch farms
            Set<Farm> farms = userDto.getFarms().stream()
                    .map(farmDTO -> farmRepository.findById(farmDTO.getId())
                            .orElseThrow(() -> {
                                String errorMessage = "Invalid farm ID: " + farmDTO.getId();
                                log.error(errorMessage);
                                return new ValidationException(errorMessage);
                            }))
                    .collect(Collectors.toSet());
            user.setFarms(farms);
            log.info("Assigned {} farms to user: {}", farms.size(), userDto.getUsername());

            // Save user to the repository
            User savedUser = userRepository.save(user);
            log.info("User created successfully with username: {}", userDto.getUsername());
            return savedUser;

        } catch (ValidationException ex) {
            log.error("Validation error during user creation: {}", ex.getMessage(), ex);
            throw ex; // Re-throw to ensure it's handled appropriately
        } catch (Exception ex) {
            log.error("Unexpected error during user creation: {}", ex.getMessage(), ex);
            throw new RuntimeException("An unexpected error occurred while creating the user.", ex);
        }
    }

    @Transactional(readOnly = true)
    @Override
    public List<UserDetailDTO> getAllUserDetails() {
        try {
            List<User> users = userRepository.findAllByDeletedFalse();
            return users.stream().map(user -> {
                List<Farm> farms = userRepository.findFarmsByUsername(user.getUsername());
                return convertToUserDetailDTO(user, farms);
            }).collect(Collectors.toList());
        } catch (Exception e) {
            log.error("Error fetching user details: {}", e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

    private UserDetailDTO convertToUserDetailDTO(User user, List<Farm> farms) {
        return UserDetailDTO.builder()
                .id(user.getId())
                .username(user.getUsername())
                .level(user.getManagingLevel().getLevel())
                .employeeId(user.getEmployee() != null ? user.getEmployee().getId() : null)
                .email(user.getEmail())
                .contactNumber(user.getContactNumber())
                .roles(user.getRoles().stream().map(role -> RoleDetailDTO.builder()
                        .id(role.getId())
                        .name(role.getName())
                        .build()).collect(Collectors.toList()))
                .farms(farms.stream().map(farm -> FarmDetailDTO.builder()
                        .id(farm.getId())
                        .name(farm.getName())
                        .build()).collect(Collectors.toList()))

                .build();
    }

    @Transactional
    @Override
    public Optional<User> updateUser(Long id, UserDTO userDto, User updatedBy) {
        try {
            return userRepository.findByIdAndDeletedFalse(id).map(user -> {
                ManagingLevel userLevel = managingLevelRepository.findByLevel(userDto.getManagingLevel());

                if (userLevel == null) {
                    throw new RuntimeException("Invalid managing level: " + userDto.getManagingLevel());
                }

                if (updatedBy != null && userLevel.getLevel() < updatedBy.getManagingLevel().getLevel()) {
                    throw new RuntimeException("You cannot update a user to the same or higher level than yours.");
                }

                user.setUsername(userDto.getUsername());
                if (userDto.getPassword() != null && !userDto.getPassword().isEmpty()) {
                    user.setPassword(passwordEncoder.encode(userDto.getPassword()));
                }
                Set<Role> roles = userDto.getRoleIds().stream()
                        .map(roleId -> roleRepository.findById(roleId)
                                .orElseThrow(() -> new RuntimeException("Invalid role ID: " + roleId)))
                        .collect(Collectors.toSet());
                user.setRoles(roles);
                user.setManagingLevel(userLevel);
                user.setContactNumber(userDto.getContactNumber());
                user.setEmail(userDto.getEmail());
                user.setUserIsEmployee(userDto.isEmployee());

                if (userDto.isEmployee()) {
                    Optional<Employee> employeeOptional = employeeRepository.findById(userDto.getEmployeeId());
                    if (employeeOptional.isPresent()) {
                        user.setEmployee(employeeOptional.get());
                    } else {
                        throw new RuntimeException("Invalid employee ID");
                    }
                }
                // Fetch existing farms and compare with new farms
                Set<Farm> existingFarms = user.getFarms();
                Set<Farm> newFarms = userDto.getFarms().stream()
                        .map(farmDTO -> farmRepository.findById(farmDTO.getId())
                                .orElseThrow(() -> new RuntimeException("Invalid farm ID: " + farmDTO.getId())))
                        .collect(Collectors.toSet());

                // Remove old farms that are no longer in the new set
                existingFarms.removeIf(farm -> !newFarms.contains(farm));

                // Add new farms that are not in the existing set
                newFarms.forEach(farm -> {
                    if (!existingFarms.contains(farm)) {
                        existingFarms.add(farm);
                    }
                });

                user.setFarms(existingFarms);

                return userRepository.save(user);
            });
        } catch (Exception e) {
            log.error("Error updating user with ID {}: {}", id, e.getMessage());
            throw new RuntimeException(e);
        }
    }

    @Transactional
    @Override
    public void deleteUser(Long id, User deletedBy) {
        try {
            // Fetch user by ID, ensuring they are not marked as deleted already
            User user = userRepository.findByIdAndDeletedFalse(id)
                    .orElseThrow(() -> new RuntimeException("User not found with ID: " + id));

            // Check if the user can be deleted based on the managing level
            if (deletedBy != null && user.getManagingLevel().getLevel() < deletedBy.getManagingLevel().getLevel()) {
                throw new RuntimeException("You cannot delete a user with the same or higher managing level than yours.");
            }

            // Check if the user has any associated employees who are not marked as deleted
            if (user.getEmployee() != null && !user.getEmployee().isDeleted()) {
                throw new ValidationException("Cannot delete user because they are associated with an active employee.");
            }


            // Check if the user has associated farms
            if (user.getFarms().stream().allMatch(Farm::isDeleted)) {
                throw new ValidationException("Cannot delete user because they are associated with farms.");
            }

            // Check if the user is associated with roles
            if (!user.getRoles().isEmpty()) {
                throw new ValidationException("Cannot delete user because they are associated with roles.");
            }

            // Perform soft delete: mark user as deleted
            user.setDeleted(true);
            userRepository.save(user);  // Save the soft-deleted user

            log.info("User with ID {} was marked as deleted by {}", id, deletedBy != null ? deletedBy.getUsername() : "system");

        } catch (ValidationException ve) {
            log.error("Validation error during user deletion: {}", ve.getMessage());
            throw ve;  // Throw validation exception for proper handling
        } catch (Exception e) {
            log.error("Error deleting user with ID {}: {}", id, e.getMessage());
            throw new RuntimeException( e.getMessage());
        }
    }

    @Override
    public boolean isCurrentUserManagingLevelOne() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof org.springframework.security.core.userdetails.User) {
            String username = ((org.springframework.security.core.userdetails.User) principal).getUsername();
            Optional<User> userOptional = userRepository.findByUsernameAndDeletedFalse(username);
            if (userOptional.isPresent()) {
                User user = userOptional.get();
                return user.getManagingLevel().getLevel() == 1;
            }
            throw new UsernameNotFoundException("User not found: " + username);
        }
        return false;
    }

    @Override
    public List<String> findAllUserNames() {
        return userRepository.findAllByDeletedFalse().stream()
                .map(User::getUsername)
                .collect(Collectors.toList());
    }

    @Override
    public CurrentUserDTO getCurrentUserDetails(User user) {
        List<PermissionGroupDTO> allPermissionGroups = permissionGroupService.getAllPermissionGroupsWithPrivileges();
        Set<String> assignedPrivileges = user.getRoles().stream()
                .flatMap(role -> role.getPrivileges().stream())
                .map(Privilege::getName)
                .collect(Collectors.toSet());

        // Update the assigned flag for each permission
        for (PermissionGroupDTO group : allPermissionGroups) {
            boolean groupAssigned = false;
            for (PermissionDTO permission : group.getPermissions()) {
                if (assignedPrivileges.contains(permission.getName())) {
                    permission.setAssigned(true);
                    groupAssigned = true;
                }
            }
            group.setAssigned(groupAssigned);
        }

        return CurrentUserDTO.builder()
                .username(user.getUsername())
                .roles(user.getRoles().stream().map(Role::getName).collect(Collectors.toList()))
                .managingLevel(user.getManagingLevel().getLevel())
                .privileges(allPermissionGroups)
                .build();
    }


    @Override
    public User getCurrentUser() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof org.springframework.security.core.userdetails.User) {
            String username = ((org.springframework.security.core.userdetails.User) principal).getUsername();
            return userRepository.findByUsernameAndDeletedFalse(username)
                    .orElseThrow(() -> new UsernameNotFoundException("User not found: " + username));
        }
        throw new UsernameNotFoundException("User not found");
    }

    @Transactional(readOnly = true)
    @Override
    public List<FarmDetailDTO> getCurrentUserFarms() {
        String username = getCurrentUsername();
        List<Farm> farms = userRepository.findFarmsByUsername(username);
        return farms.stream()
                .map(farm -> FarmDetailDTO.builder()
                        .id(farm.getId())
                        .name(farm.getName())
                        .build())
                .collect(Collectors.toList());
    }

    private String getCurrentUsername() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof org.springframework.security.core.userdetails.User) {
            return ((org.springframework.security.core.userdetails.User) principal).getUsername();
        }
        throw new UsernameNotFoundException("User not found");
    }
}
