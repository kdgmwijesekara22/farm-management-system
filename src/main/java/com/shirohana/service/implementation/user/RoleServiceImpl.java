package com.shirohana.service.implementation.user;

import com.shirohana.entity.farm.Farm;
import com.shirohana.entity.user.RoleFarm;
import com.shirohana.repository.farm.FarmRepository;
import com.shirohana.repository.user.PrivilegeRepository;
import com.shirohana.repository.user.RoleFarmRepository;
import com.shirohana.repository.user.RoleRepository;
import com.shirohana.dto.user.PrivilegeDTO;
import com.shirohana.dto.user.RoleDTO;
import com.shirohana.entity.user.Privilege;
import com.shirohana.entity.user.Role;
import com.shirohana.service.interfaces.user.RoleService;
import jakarta.transaction.Transactional;
import jakarta.validation.ValidationException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class RoleServiceImpl implements RoleService {

    private final RoleRepository roleRepository;
    private final PrivilegeRepository privilegeRepository;
    private final RoleFarmRepository roleFarmRepository;
    private final FarmRepository farmRepository;

    public List<RoleDTO> getAllRoles() {
        try {
            List<Role> roles = roleRepository.findAll();
            return roles.stream().map(this::toRoleDTO).collect(Collectors.toList());
        } catch (Exception e) {
            log.error("Error occurred while fetching all roles: {}", e.getMessage());
            throw new RuntimeException("Failed to fetch roles", e);
        }
    }

    public Optional<RoleDTO> getRoleById(Long id) {
        try {
            Optional<Role> role = roleRepository.findById(id);
            return role.map(this::toRoleDTO);
        } catch (Exception e) {
            log.error("Error occurred while fetching role with ID {}: {}", id, e.getMessage());
            throw new RuntimeException("Failed to fetch role", e);
        }
    }

    public RoleDTO createRole(RoleDTO roleDTO) {
        try {
            Role role = toRoleEntity(roleDTO);
            // Check if the role name already exists
            if (roleRepository.existsByName(roleDTO.getName())) {
                String errorMessage = "Role name already exists. Please choose a different name.";
                log.error(errorMessage);
                throw new ValidationException(errorMessage);
            }

            Role savedRole = roleRepository.save(role);
            return toRoleDTO(savedRole);
        } catch (Exception e) {
            log.error("Error occurred while creating role: {}", e.getMessage());
            throw new RuntimeException("Failed to create role", e);
        }
    }
    public Optional<RoleDTO> updateRole(Long id, Role roleDetails) {
        try {
            return roleRepository.findById(id).map(role -> {
                role.setName(roleDetails.getName());
                Role updatedRole = roleRepository.save(role);
                return toRoleDTO(updatedRole);
            });
        } catch (Exception e) {
            log.error("Error occurred while updating role with ID {}: {}", id, e.getMessage());
            throw new RuntimeException("Failed to update role", e);
        }
    }

    /**
     * Delete a role
     * @param id role ID
     * @return true if deletion was successful, false otherwise
     */
    public boolean deleteRole(Long id) {
        try {
            return roleRepository.findById(id).map(role -> {
                if (!role.getUsers().isEmpty()) {
                    throw new RuntimeException("Role is assigned to users and cannot be deleted.");
                }
                roleRepository.delete(role);
                return true;
            }).orElse(false);
        } catch (RuntimeException e) {
            log.error("Error occurred while deleting role with ID {}: {}", id, e.getMessage());
            throw e;
        } catch (Exception e) {
            log.error("Unexpected error occurred while deleting role with ID {}: {}", id, e.getMessage());
            throw new RuntimeException("Failed to delete role", e);
        }
    }
    public Optional<RoleDTO> assignPrivilegesToRole(Long id, List<Long> privilegeIds) {
        try {
            Role role = roleRepository.findById(id)
                    .orElseThrow(() -> new RuntimeException("Role not found"));

            Set<Privilege> newPrivileges = privilegeIds.stream()
                    .map(privilegeId -> privilegeRepository.findById(privilegeId)
                            .orElseThrow(() -> new RuntimeException("Privilege not found")))
                    .collect(Collectors.toSet());

            role.setPrivileges(newPrivileges); // Set new privileges

            Role savedRole = roleRepository.save(role);
            return Optional.of(toRoleDTO(savedRole));
        } catch (RuntimeException e) {
            log.error("Error occurred while assigning privileges to role: {}", e.getMessage());
            throw e;
        } catch (Exception e) {
            log.error("Unexpected error occurred while assigning privileges to role: {}", e.getMessage());
            throw new RuntimeException("Unexpected error occurred while assigning privileges to role", e);
        }
    }

    @Transactional
    @Override
    public RoleDTO assignFarmsToRole(Long roleId, List<Long> farmIds) {
        Role role = roleRepository.findById(roleId)
                .orElseThrow(() -> new RuntimeException("Role not found"));

        List<Farm> validFarms = farmIds.stream()
                .map(farmId -> farmRepository.findById(farmId)
                        .orElseThrow(() -> new RuntimeException("Farm not found")))
                .collect(Collectors.toList());

        List<RoleFarm> existingRoleFarms = roleFarmRepository.findByRoleId(roleId);
        if (!existingRoleFarms.isEmpty()) {
            roleFarmRepository.deleteAll(existingRoleFarms);
        }

        Set<RoleFarm> newRoleFarms = validFarms.stream()
                .map(farm -> new RoleFarm(role, farm))
                .collect(Collectors.toSet());
        role.setRoleFarms(newRoleFarms);

        Role savedRole = roleRepository.save(role);
        return toRoleDTO(savedRole);
    }




    private RoleDTO toRoleDTO(Role role) {
        RoleDTO roleDTO = new RoleDTO();
        roleDTO.setId(role.getId());
        roleDTO.setName(role.getName());
        List<PrivilegeDTO> privilegeDTOS = role.getPrivileges().stream().map(privilege -> {
            PrivilegeDTO privilegeDTO = new PrivilegeDTO();
            privilegeDTO.setId(privilege.getId());
            privilegeDTO.setName(privilege.getName());
            return privilegeDTO;
        }).collect(Collectors.toList());
        roleDTO.setPrivileges(privilegeDTOS);
        return roleDTO;
    }

    private Role toRoleEntity(RoleDTO roleDTO) {
        Role role = new Role();
        role.setId(roleDTO.getId());
        role.setName(roleDTO.getName());
        Set<Privilege> privileges = roleDTO.getPrivileges().stream().map(privilegeDTO -> {
            Privilege privilege = new Privilege();
            privilege.setId(privilegeDTO.getId());
            privilege.setName(privilegeDTO.getName());
            return privilege;
        }).collect(Collectors.toSet());
        role.setPrivileges(privileges);
        return role;
    };
}
