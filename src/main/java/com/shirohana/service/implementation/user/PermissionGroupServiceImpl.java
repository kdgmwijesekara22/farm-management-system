package com.shirohana.service.implementation.user;

import com.shirohana.dto.user.PermissionDTO;
import com.shirohana.dto.user.PermissionGroupDTO;
import com.shirohana.entity.user.PermissionGroup;
import com.shirohana.entity.user.Privilege;
import com.shirohana.repository.user.PermissionGroupRepository;
import com.shirohana.service.interfaces.user.PermissionGroupService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class PermissionGroupServiceImpl implements PermissionGroupService {

    private final PermissionGroupRepository permissionGroupRepository;

    @Override
    public List<PermissionGroupDTO> getAllPermissionGroupsWithPrivileges() {
        return permissionGroupRepository.findAll().stream().map(this::toDto).collect(Collectors.toList());
    }

    private PermissionGroupDTO toDto(PermissionGroup group) {
        List<PermissionDTO> permissions = group.getPermissions().stream()
                .map(this::toDto)
                .collect(Collectors.toList());

        // Determine if any permission in the group is assigned
        boolean groupAssigned = permissions.stream().anyMatch(PermissionDTO::isAssigned);

        return new PermissionGroupDTO(group.getName(), groupAssigned, permissions);
    }

    private PermissionDTO toDto(Privilege privilege) {
        // Assuming isAssigned is false initially; it should be set based on the user's permissions
        return new PermissionDTO(privilege.getId(), privilege.getName(), privilege.getController().getName(), false);
    }
}
