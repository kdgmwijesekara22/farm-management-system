package com.shirohana.service.implementation.user;

import com.shirohana.repository.user.PrivilegeRepository;
import com.shirohana.entity.user.Privilege;
import com.shirohana.service.interfaces.user.PrivilegeService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class PrivilegeServiceImpl implements PrivilegeService {

    private final PrivilegeRepository privilegeRepository;

    /**
     * Get all privileges
     * @return list of privileges
     */
    public List<Privilege> getAllPrivileges() {
        try {
            return privilegeRepository.findAll();
        } catch (Exception e) {
            log.error("Error occurred while fetching all privileges: {}", e.getMessage());
            throw new RuntimeException("Failed to fetch privileges", e);
        }
    }

    /**
     * Get privilege by ID
     * @param id privilege ID
     * @return optional privilege
     */
    public Optional<Privilege> getPrivilegeById(Long id) {
        try {
            return privilegeRepository.findById(id);
        } catch (Exception e) {
            log.error("Error occurred while fetching privilege with ID {}: {}", id, e.getMessage());
            throw new RuntimeException("Failed to fetch privilege", e);
        }
    }

    /**
     * Create a new privilege
     * @param privilege privilege to create
     * @return created privilege
     */
    public Privilege createPrivilege(Privilege privilege) {

        try {
            Optional<Privilege> existingPrivilege = privilegeRepository.findByName(privilege.getName());
            if (existingPrivilege.isPresent()){
                throw new RuntimeException("Privilege with name " + privilege.getName() + " already exists");
            }
            return privilegeRepository.save(privilege);

        }catch (Exception e){
            log.error("Error occurred while creating privilege: {}", e.getMessage());
            throw new RuntimeException("Failed to create privilege", e);
        }
    }

    /**
     * Update an existing privilege
     * @param id privilege ID
     * @param privilegeDetails updated privilege details
     * @return optional updated privilege
     */
    public Optional<Privilege> updatePrivilege(Long id, Privilege privilegeDetails) {
        try {
            return privilegeRepository.findById(id).map(privilege -> {
                privilege.setName(privilegeDetails.getName());
                // Update other fields as necessary
                return privilegeRepository.save(privilege);
            });
        } catch (Exception e) {
            log.error("Error occurred while updating privilege with ID {}: {}", id, e.getMessage());
            throw new RuntimeException("Failed to update privilege", e);
        }
    }

    /**
     * Delete a privilege
     * @param id privilege ID
     * @return true if deletion was successful, false otherwise
     */
    public boolean deletePrivilege(Long id) {
        try {
            return privilegeRepository.findById(id).map(privilege -> {
                privilegeRepository.delete(privilege);
                return true;
            }).orElse(false);
        } catch (Exception e) {
            log.error("Error occurred while deleting privilege with ID {}: {}", id, e.getMessage());
            throw new RuntimeException("Failed to delete privilege", e);
        }
    }
}
