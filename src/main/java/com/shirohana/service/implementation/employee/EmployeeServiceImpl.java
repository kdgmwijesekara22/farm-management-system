package com.shirohana.service.implementation.employee;

import com.shirohana.entity.employee.EmployeeFarmAssociation;
import com.shirohana.entity.farm.Farm;
import com.shirohana.entity.user.Role;
import com.shirohana.repository.employee.EmployeeFarmAssociationRepository;
import com.shirohana.repository.employee.EmployeeRepository;
import com.shirohana.dto.employee.EmployeeRequestDto;
import com.shirohana.dto.employee.EmployeeResponseDto;
import com.shirohana.entity.employee.Employee;
import com.shirohana.exception.ResourceNotFoundException;
import com.shirohana.repository.farm.FarmRepository;
import com.shirohana.repository.user.RoleRepository;
import com.shirohana.service.interfaces.employee.EmployeeService;
import jakarta.transaction.Transactional;
import jakarta.validation.ValidationException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class EmployeeServiceImpl implements EmployeeService {
    private final EmployeeRepository employeeRepository;

    private final EmployeeFarmAssociationRepository employeeFarmAssociationRepository;
    private final RoleRepository roleRepository;
    private final FarmRepository farmRepository;

    @Transactional
    @Override
    public EmployeeResponseDto createEmployee(EmployeeRequestDto employeeRequestDto) {
        try {
            String sanitizedNic = sanitizeInput(employeeRequestDto.getNic());
            String sanitizedFullName = sanitizeInput(employeeRequestDto.getFullName());
            String sanitizedEmployeeGivenId = sanitizeInput(employeeRequestDto.getEmployeeGivenId());

            if (employeeRepository.existsByNicAndDeletedFalse(sanitizedNic)) {
                throw new ValidationException("NIC must be unique.");
            }
            if (employeeRepository.existsByEmployeeGivenIdAndDeletedFalse(sanitizedEmployeeGivenId)) {
                throw new ValidationException("EmployeeGivenId must be unique.");
            }

            // Fetch roles based on role IDs
            Set<Role> roles = employeeRequestDto.getRoleIds().stream()
                    .map(roleId -> roleRepository.findById(roleId)
                            .orElseThrow(() -> new ValidationException("Invalid role ID: " + roleId)))
                    .collect(Collectors.toSet());

            Employee employee = Employee.builder()
                    .fullName(sanitizedFullName)
                    .roles(roles) // Set the roles
                    .dateOfBirth(employeeRequestDto.getDateOfBirth())
                    .mobile(employeeRequestDto.getMobile())
                    .nic(sanitizedNic)
                    .address(employeeRequestDto.getAddress())
                    .employeeGivenId(sanitizedEmployeeGivenId)
                    .build();

            employee.setCreatedBy(getCurrentUsername());
            employee.setCreatedDate(LocalDateTime.now());

            Employee savedEmployee = employeeRepository.save(employee);

            // Save farm associations
            for (Long farmId : employeeRequestDto.getFarmIds()) {
                EmployeeFarmAssociation association = EmployeeFarmAssociation.builder()
                        .employeeId(savedEmployee.getId())
                        .farmId(farmId)
                        .build();
                employeeFarmAssociationRepository.save(association);
            }

            return convertToResponseDto(savedEmployee);
        } catch (ValidationException ex) {
            log.error("Validation error while creating employee: {}", employeeRequestDto, ex);
            throw ex;
        } catch (Exception ex) {
            log.error("Unexpected error while creating employee: {}", employeeRequestDto, ex);
            throw new RuntimeException("Error creating employee", ex);
        }
    }

    @Transactional
    @Override
    public EmployeeResponseDto updateEmployee(Long id, EmployeeRequestDto employeeRequestDto) {
        try {
            String sanitizedNic = sanitizeInput(employeeRequestDto.getNic());
            String sanitizedFullName = sanitizeInput(employeeRequestDto.getFullName());
            String sanitizedEmployeeGivenId = sanitizeInput(employeeRequestDto.getEmployeeGivenId());

            Employee existingEmployee = employeeRepository.findByIdAndDeletedFalse(id)
                    .orElseThrow(() -> new ResourceNotFoundException("Employee not found "));

            if (!existingEmployee.getNic().equals(sanitizedNic) &&
                    employeeRepository.existsByNicAndDeletedFalse(sanitizedNic)) {
                throw new ValidationException("NIC must be unique.");
            }
            if (!existingEmployee.getEmployeeGivenId().equals(sanitizedEmployeeGivenId) &&
                    employeeRepository.existsByEmployeeGivenIdAndDeletedFalse(sanitizedEmployeeGivenId)) {
                throw new ValidationException("EmployeeGivenId must be unique.");
            }

            existingEmployee.setFullName(sanitizedFullName);
            existingEmployee.setDateOfBirth(employeeRequestDto.getDateOfBirth());
            existingEmployee.setMobile(employeeRequestDto.getMobile());
            existingEmployee.setNic(sanitizedNic);
            existingEmployee.setAddress(employeeRequestDto.getAddress());
            existingEmployee.setEmployeeGivenId(sanitizedEmployeeGivenId);

            Set<Role> roles = employeeRequestDto.getRoleIds().stream()
                    .map(roleId -> roleRepository.findById(roleId).orElseThrow(() -> new ValidationException("Invalid role ID: " + roleId)))
                    .collect(Collectors.toSet());
            existingEmployee.setRoles(roles);
            existingEmployee.setLastModifiedBy(getCurrentUsername());
            existingEmployee.setLastModifiedDate(LocalDateTime.now());

            Employee updatedEmployee = employeeRepository.save(existingEmployee);

            // Update farm associations
            employeeFarmAssociationRepository.deleteByEmployeeId(updatedEmployee.getId());
            for (Long farmId : employeeRequestDto.getFarmIds()) {
                EmployeeFarmAssociation association = EmployeeFarmAssociation.builder()
                        .employeeId(updatedEmployee.getId())
                        .farmId(farmId)
                        .build();
                employeeFarmAssociationRepository.save(association);
            }

            return convertToResponseDto(updatedEmployee);
        } catch (ResourceNotFoundException ex) {
            log.error("Employee not found with id: {}", id, ex);
            throw ex;
        } catch (ValidationException ex) {
            log.error("Validation error while updating employee with id {}: {}", id, employeeRequestDto, ex);
            throw ex;
        } catch (Exception ex) {
            log.error("Unexpected error while updating employee with id {}: {}", id, employeeRequestDto, ex);
            throw new RuntimeException("Error updating employee", ex);
        }
    }

    @Transactional
    @Override
    public void deleteEmployee(Long id) {
        try {
            Employee existingEmployee = employeeRepository.findByIdAndDeletedFalse(id)
                    .orElseThrow(() -> new ResourceNotFoundException("Employee not found with id " + id));

            existingEmployee.setDeleted(true);
            existingEmployee.setDeletedAt(LocalDateTime.now());
            existingEmployee.setDeletedBy(getCurrentUsername());

            employeeRepository.save(existingEmployee);
        } catch (ResourceNotFoundException ex) {
            log.error("Employee not found with id: {}", id, ex);
            throw ex;
        } catch (Exception ex) {
            log.error("Unexpected error while deleting employee with id: {}", id, ex);
            throw new RuntimeException("Error deleting employee", ex);
        }
    }

    @Override
    public EmployeeResponseDto getEmployeeById(Long id) {
        try {
            Employee employee = employeeRepository.findByIdAndDeletedFalse(id)
                    .orElseThrow(() -> new ResourceNotFoundException("Employee not found with id " + id));
            return convertToResponseDto(employee);
        } catch (ResourceNotFoundException ex) {
            log.error("Employee not found with id: {}", id, ex);
            throw ex;
        } catch (Exception ex) {
            log.error("Unexpected error while retrieving employee with id: {}", id, ex);
            throw new RuntimeException("Error retrieving employee", ex);
        }
    }

    @Override
    public EmployeeResponseDto getEmployeeByEmployeeGivenId(String employeeGivenId) {
        try {
            Employee employee = employeeRepository.findByEmployeeGivenIdAndDeletedFalse(employeeGivenId)
                    .orElseThrow(() -> new ResourceNotFoundException("Employee not found with employeeGivenId " + employeeGivenId));
            return convertToResponseDto(employee);
        } catch (ResourceNotFoundException ex) {
            log.error("Employee not found with employeeGivenId: {}", employeeGivenId, ex);
            throw ex;
        } catch (Exception ex) {
            log.error("Unexpected error while retrieving employee with employeeGivenId: {}", employeeGivenId, ex);
            throw new RuntimeException("Error retrieving employee", ex);
        }
    }

    @Override
    public List<EmployeeResponseDto> getAllEmployees() {
        try {
            List<Employee> employees = employeeRepository.findAllByDeletedFalse();
            return employees.stream()
                    .map(this::convertToResponseDto)
                    .collect(Collectors.toList());
        } catch (Exception ex) {
            log.error("Unexpected error while retrieving all employees", ex);
            throw new RuntimeException("Error retrieving employees", ex);
        }
    }

    private EmployeeResponseDto convertToResponseDto(Employee employee) {

        // Fetch farm IDs associated with the employee
        List<Long> farmIds = employeeFarmAssociationRepository.findByEmployeeId(employee.getId())
                .stream()
                .map(EmployeeFarmAssociation::getFarmId)
                .collect(Collectors.toList());

        // Fetch farm names based on farm IDs
        List<String> farmNames = farmRepository.findByIdIn(farmIds).stream()
                .map(Farm::getName)
                .toList();

        // Fetch role names and IDs associated with the employee
        List<String> roleNames = employee.getRoles().stream()
                .map(Role::getName)
                .collect(Collectors.toList());

        List<Long> roleIds = employee.getRoles().stream()
                .map(role -> role.getId())
                .toList();


        return EmployeeResponseDto.builder()
                .id(employee.getId())
                .fullName(employee.getFullName())
                .roleName(roleNames)
                .roleId(roleIds) // Set the role IDs here
                .dateOfBirth(employee.getDateOfBirth())
                .mobile(employee.getMobile())
                .nic(employee.getNic())
                .address(employee.getAddress())
                .employeeGivenId(employee.getEmployeeGivenId())
                .farmName(farmNames)  // Set the farm names here
                .farmId(farmIds)
                .deleted(employee.isDeleted())
                .createdBy(employee.getCreatedBy())
                .createdDate(employee.getCreatedDate())
                .lastModifiedBy(employee.getLastModifiedBy())
                .lastModifiedDate(employee.getLastModifiedDate())
                .deletedBy(employee.getDeletedBy())
                .deletedAt(employee.getDeletedAt())
                .build();
    }

    private String getCurrentUsername() {
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (authentication != null && authentication.getPrincipal() instanceof UserDetails) {
                return ((UserDetails) authentication.getPrincipal()).getUsername();
            }
        } catch (Exception e) {
            log.error("Error retrieving current username", e);
        }
        return null;
    }

    private String sanitizeInput(String input) {
        if (input == null) {
            return "";
        }
        // Remove leading and trailing whitespace and replace multiple spaces with a single space
        return input.trim().replaceAll("\\s+", " ");
    }
}
