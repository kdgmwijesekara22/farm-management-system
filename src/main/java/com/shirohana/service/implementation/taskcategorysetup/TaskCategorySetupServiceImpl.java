package com.shirohana.service.implementation.taskcategorysetup;

import com.shirohana.dto.taskcategorysetup.TaskCategorySetupRequestDto;
import com.shirohana.dto.taskcategorysetup.TaskCategorySetupResponseDto;
import com.shirohana.entity.task.core.TaskCategorySetup;
import com.shirohana.entity.task.save.TaskProdPlanByVariety;
import com.shirohana.entity.task.setup.maintenance.TaskMaintenance;
import com.shirohana.exception.ResourceNotFoundException;
import com.shirohana.repository.task.save.TaskProdPlanByVarietyRepository;
import com.shirohana.repository.task.setup.maintenance.TaskMaintenanceRepository;
import com.shirohana.repository.task.setup.monitoring.TaskMonitoringRepository;
import com.shirohana.repository.taskcategorysetup.TaskCategorySetupRepository;
import com.shirohana.service.interfaces.taskcategorysetup.TaskCategorySetupService;
import jakarta.transaction.Transactional;
import jakarta.validation.ValidationException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;


@Service
@RequiredArgsConstructor
@Slf4j
public class TaskCategorySetupServiceImpl implements TaskCategorySetupService {
    private final TaskCategorySetupRepository taskCategorySetupRepository;
    private final TaskMaintenanceRepository taskMaintenanceRepository;
    private final TaskMonitoringRepository taskMonitoringRepository;
    private final TaskProdPlanByVarietyRepository taskProdPlanByVarietyRepository;

    @Transactional
    @Override
    public TaskCategorySetupResponseDto createTaskCategorySetup(TaskCategorySetupRequestDto requestDto) {
        try {
            validateUniqueTaskName(requestDto.getTaskName());

            TaskCategorySetup setup = buildEntityFromRequest(requestDto);
            setup.setCreatedBy(getCurrentUsername());
            setup.setCreatedDate(LocalDateTime.now());

            TaskCategorySetup savedSetup = taskCategorySetupRepository.save(setup);
            return convertToResponseDto(savedSetup);
        } catch (ValidationException ex) {
            log.error("Validation error while creating setup: {}", requestDto, ex);
            throw ex;
        } catch (Exception ex) {
            log.error("Unexpected error while creating setup: {}", requestDto, ex);
            throw new RuntimeException("Error creating setup", ex);
        }
    }

    @Transactional
    @Override
    public TaskCategorySetupResponseDto updateTaskCategorySetup(Long id, TaskCategorySetupRequestDto requestDto) {
        try {
            TaskCategorySetup existingSetup = taskCategorySetupRepository.findByIdAndDeletedFalse(id)
                    .orElseThrow(() -> new ResourceNotFoundException("Setup not found with id " + id));

            if (!existingSetup.getTaskName().equals(requestDto.getTaskName())) {
                validateUniqueTaskName(requestDto.getTaskName());
            }

            // Validate if any TaskMaintenance is associated with this TaskCategorySetup
            boolean isTaskMaintenanceAssociated = taskMaintenanceRepository.existsByTaskCategoryIdAndDeletedFalse(id);
            if (isTaskMaintenanceAssociated) {
                throw new ValidationException("Cannot Update TaskCategorySetup with id " + id + " as it is associated with existing TaskMaintenance entities.");
            }

            // Validate if any TaskMonitoring is associated with this TaskCategorySetup
            boolean isTaskMonitoringAssociated = taskMonitoringRepository.existsByTaskCategoryIdAndDeletedFalse(id);
            if (isTaskMonitoringAssociated) {
                throw new ValidationException("Cannot Update TaskCategorySetup with id " + id + " as it is associated with existing TaskMonitoring entities.");
            }

            // Validate if any TaskProdPlanByVariety is associated with this TaskCategorySetup
            boolean isTaskProdPlanByVarietyAssociated = taskProdPlanByVarietyRepository.existsByTaskCategorySetupAndTaskExpiredFalse(existingSetup);
            if (isTaskProdPlanByVarietyAssociated) {
                throw new ValidationException("Cannot Update TaskCategorySetup with id " + id + " as it is associated with existing TaskProdPlanByVariety entities.");
            }

            updateEntityFromRequest(existingSetup, requestDto);
            existingSetup.setLastModifiedBy(getCurrentUsername());
            existingSetup.setLastModifiedDate(LocalDateTime.now());

            TaskCategorySetup updatedSetup = taskCategorySetupRepository.save(existingSetup);
            return convertToResponseDto(updatedSetup);
        } catch (ResourceNotFoundException ex) {
            log.error("Setup not found with id: {}", id, ex);
            throw ex;
        } catch (ValidationException ex) {
            log.error("Validation error while updating setup with id {}: {}", id, requestDto, ex);
            throw ex;
        } catch (Exception ex) {
            log.error("Unexpected error while updating setup with id {}: {}", id, requestDto, ex);
            throw new RuntimeException(ex.getMessage());
        }
    }

    @Transactional
    @Override
    public void deleteTaskCategorySetup(Long id) {
        try {
            // Fetch the TaskCategorySetup and ensure it's not already deleted
            TaskCategorySetup existingSetup = taskCategorySetupRepository.findByIdAndDeletedFalse(id)
                    .orElseThrow(() -> new ResourceNotFoundException("Setup not found with id " + id));

            // Validate if any TaskMaintenance is associated with this TaskCategorySetup
            boolean isTaskMaintenanceAssociated = taskMaintenanceRepository.existsByTaskCategoryIdAndDeletedFalse(id);
            if (isTaskMaintenanceAssociated) {
                throw new ValidationException("Cannot delete TaskCategorySetup with id " + id + " as it is associated with existing TaskMaintenance entities.");
            }

            // Validate if any TaskMonitoring is associated with this TaskCategorySetup
            boolean isTaskMonitoringAssociated = taskMonitoringRepository.existsByTaskCategoryIdAndDeletedFalse(id);
            if (isTaskMonitoringAssociated) {
                throw new ValidationException("Cannot delete TaskCategorySetup with id " + id + " as it is associated with existing TaskMonitoring entities.");
            }

            // Validate if any TaskProdPlanByVariety is associated with this TaskCategorySetup
            boolean isTaskProdPlanByVarietyAssociated = taskProdPlanByVarietyRepository.existsByTaskCategorySetupAndTaskExpiredFalse(existingSetup);
            if (isTaskProdPlanByVarietyAssociated) {
                throw new ValidationException("Cannot delete TaskCategorySetup with id " + id + " as it is associated with existing TaskProdPlanByVariety entities.");
            }

            existingSetup.setDeleted(true);
            existingSetup.setDeletedBy(getCurrentUsername());
            existingSetup.setDeletedAt(LocalDateTime.now());

            taskCategorySetupRepository.save(existingSetup);
        } catch (ResourceNotFoundException ex) {
            log.error(ex.getMessage());
            throw ex;
        } catch (Exception ex) {
            log.error(ex.getMessage());
            throw new RuntimeException(ex.getMessage());
        }
    }

    @Override
    public TaskCategorySetupResponseDto getTaskCategorySetupById(Long id) {
        try {
            TaskCategorySetup setup = taskCategorySetupRepository.findByIdAndDeletedFalse(id)
                    .orElseThrow(() -> new ResourceNotFoundException("Setup not found with id " + id));
            return convertToResponseDto(setup);
        } catch (ResourceNotFoundException ex) {
            log.error("Setup not found with id: {}", id, ex);
            throw ex;
        } catch (Exception ex) {
            log.error("Unexpected error while retrieving setup with id: {}", id, ex);
            throw new RuntimeException("Error retrieving setup", ex);
        }
    }

    @Override
    public List<TaskCategorySetupResponseDto> getAllTaskCategorySetups() {
        try {
            List<TaskCategorySetup> setups = taskCategorySetupRepository.findAllByDeletedFalse();
            return setups.stream()
                    .map(this::convertToResponseDto)
                    .collect(Collectors.toList());
        } catch (Exception ex) {
            log.error("Unexpected error while retrieving all setups", ex);
            throw new RuntimeException("Error retrieving setups", ex);
        }
    }

    @Override
    public List<TaskCategorySetupResponseDto> getTaskCategorySetupsByTaskType(String taskType) {
        try {
            List<TaskCategorySetup> setups = taskCategorySetupRepository.findAllByTaskTypeAndDeletedFalse(taskType);
            return setups.stream()
                    .map(this::convertToResponseDto)
                    .collect(Collectors.toList());
        } catch (Exception ex) {
            log.error("Unexpected error while retrieving setups by task type", ex);
            throw new RuntimeException("Error retrieving setups by task type", ex);
        }
    }

    private void validateUniqueTaskName(String taskName) {
        if (taskCategorySetupRepository.findByTaskNameAndDeletedFalse(taskName).isPresent()) {
            throw new ValidationException("Setup with task name " + taskName + " already exists.");
        }
    }

    private TaskCategorySetup buildEntityFromRequest(TaskCategorySetupRequestDto requestDto) {
        return TaskCategorySetup.builder()
                .taskName(requestDto.getTaskName())
                .taskType(requestDto.getTaskType())
                .taskDescription(requestDto.getTaskDescription())
                .build();
    }

    private void updateEntityFromRequest(TaskCategorySetup existingSetup, TaskCategorySetupRequestDto requestDto) {
        existingSetup.setTaskName(requestDto.getTaskName());
        existingSetup.setTaskType(requestDto.getTaskType());
        existingSetup.setTaskDescription(requestDto.getTaskDescription());
    }

    private TaskCategorySetupResponseDto convertToResponseDto(TaskCategorySetup setup) {
        return TaskCategorySetupResponseDto.builder()
                .id(setup.getId())
                .taskName(setup.getTaskName())
                .taskType(setup.getTaskType())
                .taskDescription(setup.getTaskDescription())
                .createdBy(setup.getCreatedBy())
                .createdAt(setup.getCreatedDate())
                .updatedBy(setup.getLastModifiedBy())
                .updatedAt(setup.getLastModifiedDate())
                .deletedBy(setup.getDeletedBy())
                .deletedAt(setup.getDeletedAt())
                .build();
    }

    private String getCurrentUsername() {
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (authentication != null && authentication.getPrincipal() instanceof UserDetails) {
                return ((UserDetails) authentication.getPrincipal()).getUsername();
            }
        } catch (Exception e) {
            log.error("Error retrieving current username", e);
        }
        return null;
    }


}
