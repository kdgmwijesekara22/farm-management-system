package com.shirohana.service.interfaces.variety;

import com.shirohana.dto.variety.VarietyHarvestingResponseDto;
import com.shirohana.dto.variety.VarietyRequestDto;
import com.shirohana.dto.variety.VarietyResponseDto;

import java.util.List;

public interface VarietyService {
    VarietyResponseDto createVariety(VarietyRequestDto varietyRequestDto);
    VarietyResponseDto updateVariety(Long id, VarietyRequestDto varietyRequestDto);
    void deleteVariety(Long id);
    List<VarietyResponseDto> getVarietyByFamilyNameId(Long familyNameId);
    List<VarietyResponseDto> getAllVarieties();
    VarietyResponseDto getVarietyById(Long id);
    List<VarietyResponseDto> getVarietiesByPlantId(Long plantId);
    List<VarietyHarvestingResponseDto> getVarietiesByFamilyIdWithHarvesting(Long familyId);
}
