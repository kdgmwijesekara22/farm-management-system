package com.shirohana.service.interfaces.planttasksetup;

import com.shirohana.dto.planttasksetup.monitoringtasksetup.requestdtos.create.MonitoringTaskSetupCreateRequestDto;
import com.shirohana.dto.planttasksetup.monitoringtasksetup.MonitoringTaskSetupResponseDto;
import com.shirohana.dto.planttasksetup.monitoringtasksetup.requestdtos.update.MonitoringTaskSetupUpdateRequestDto;

import java.util.List;

public interface MonitoringTaskSetupService {

    MonitoringTaskSetupResponseDto createTask(MonitoringTaskSetupCreateRequestDto requestDto);
    MonitoringTaskSetupResponseDto updateTask(Long id, MonitoringTaskSetupUpdateRequestDto requestDto);
    void deleteTask(Long id);
    MonitoringTaskSetupResponseDto getTaskById(Long id);
    List<MonitoringTaskSetupResponseDto> getAllTasks();

    List<MonitoringTaskSetupResponseDto> getMonitoringTaskSetupByFamilyNameId(Long familyNameId);
}
