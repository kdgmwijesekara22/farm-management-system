package com.shirohana.service.interfaces.planttasksetup;

import com.shirohana.dto.planttasksetup.harvestingandretention.HarvestingAndRetentionSetupRequestDto;
import com.shirohana.dto.planttasksetup.harvestingandretention.HarvestingAndRetentionSetupResponseDto;

import java.util.List;

public interface HarvestingAndRetentionSetupService {

    HarvestingAndRetentionSetupResponseDto createHarvestingAndRetentionSetup(HarvestingAndRetentionSetupRequestDto requestDto);
    HarvestingAndRetentionSetupResponseDto updateHarvestingAndRetentionSetup(Long id, HarvestingAndRetentionSetupRequestDto requestDto);
    void deleteHarvestingAndRetentionSetup(Long id);
    HarvestingAndRetentionSetupResponseDto getHarvestingAndRetentionSetupById(Long id);
    List<HarvestingAndRetentionSetupResponseDto> getAllHarvestingAndRetentionSetups();
    HarvestingAndRetentionSetupResponseDto getHarvestingAndRetentionSetupByFamilyNameId(Long familyNameId);
}
