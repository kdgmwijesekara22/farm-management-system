package com.shirohana.service.interfaces.planttasksetup;


import com.shirohana.dto.planttasksetup.harvestingtasksetup.requestDtos.create.HarvestingTaskSetupCreateRequestDto;
import com.shirohana.dto.planttasksetup.harvestingtasksetup.requestDtos.update.HarvestingTaskSetupUpdateRequestDto;
import com.shirohana.dto.planttasksetup.harvestingtasksetup.responseDtos.HarvestingTaskSetupResponseDto;

import java.util.List;

public interface HarvestingTaskSetupService {

    HarvestingTaskSetupResponseDto createTask(HarvestingTaskSetupCreateRequestDto requestDto);

    HarvestingTaskSetupResponseDto updateTask(Long id, HarvestingTaskSetupUpdateRequestDto requestDto);

    void deleteTask(Long id);

    HarvestingTaskSetupResponseDto getTaskById(Long id);

    List<HarvestingTaskSetupResponseDto> getTasksByFamilyNameId(Long familyNameId);

    List<HarvestingTaskSetupResponseDto> getAllTasks();
}
