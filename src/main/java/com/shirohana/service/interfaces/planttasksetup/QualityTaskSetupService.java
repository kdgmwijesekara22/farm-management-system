package com.shirohana.service.interfaces.planttasksetup;

import com.shirohana.dto.planttasksetup.qualitytasksetup.requestdtos.create.QualityTaskSetupCreateRequestDto;
import com.shirohana.dto.planttasksetup.qualitytasksetup.requestdtos.update.QualityTaskSetupUpdateRequestDto;
import com.shirohana.dto.planttasksetup.qualitytasksetup.responsedtos.QualityTaskSetupResponseDto;

import java.util.List;

public interface QualityTaskSetupService {
    QualityTaskSetupResponseDto createTask(QualityTaskSetupCreateRequestDto requestDto);

    QualityTaskSetupResponseDto updateTask(Long id, QualityTaskSetupUpdateRequestDto requestDto);

    void deleteTask(Long id);

    QualityTaskSetupResponseDto getTaskById(Long id);

    List<QualityTaskSetupResponseDto> getTasksByFamilyNameId(Long familyNameId);

    List<QualityTaskSetupResponseDto> getAllTasks();
}
