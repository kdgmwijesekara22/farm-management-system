package com.shirohana.service.interfaces.planttasksetup;

import com.shirohana.dto.planttasksetup.maintenancetasksetup.requestDtos.create.MaintenanceTaskSetupCreateRequestDto;
import com.shirohana.dto.planttasksetup.maintenancetasksetup.MaintenanceTaskSetupResponseDto;
import com.shirohana.dto.planttasksetup.maintenancetasksetup.requestDtos.update.MaintenanceTaskSetupUpdateRequestDto;

import java.util.List;

public interface MaintenanceTaskSetupService {
    MaintenanceTaskSetupResponseDto createTask(MaintenanceTaskSetupCreateRequestDto requestDto);
    MaintenanceTaskSetupResponseDto updateTask(Long id, MaintenanceTaskSetupUpdateRequestDto requestDto);
    void deleteTask(Long id);
    MaintenanceTaskSetupResponseDto getTaskById(Long id);
    List<MaintenanceTaskSetupResponseDto> getAllTasks();

    List<MaintenanceTaskSetupResponseDto> getMaintenanceTaskSetupByFamilyNameId(Long familyNameId);
}
