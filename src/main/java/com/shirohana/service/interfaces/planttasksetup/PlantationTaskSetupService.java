package com.shirohana.service.interfaces.planttasksetup;

import com.shirohana.dto.planttasksetup.plantationtasksetup.requestdtos.create.PlantationTaskSetupCreateRequestDto;
import com.shirohana.dto.planttasksetup.plantationtasksetup.requestdtos.update.PlantationTaskSetupUpdateRequestDto;
import com.shirohana.dto.planttasksetup.plantationtasksetup.responsedtos.PlantationTaskSetupResponseDto;

import java.util.List;

public interface PlantationTaskSetupService {
    PlantationTaskSetupResponseDto createTask(PlantationTaskSetupCreateRequestDto requestDto);
    PlantationTaskSetupResponseDto updateTask(Long id, PlantationTaskSetupUpdateRequestDto requestDto);
    void deleteTask(Long id);
    PlantationTaskSetupResponseDto getTaskById(Long id);
    List<PlantationTaskSetupResponseDto> getTasksByFamilyNameId(Long familyNameId);
    List<PlantationTaskSetupResponseDto> getAllTasks();
}
