package com.shirohana.service.interfaces.prodplantaskmanagement.prodplantmonitoringtask;

import com.shirohana.dto.prodplantaskmanagement.prodplanmaintenancetask.filtered.ProdPlanMaintenanceTaskFilterByPurposeResponseDto;
import com.shirohana.dto.prodplantaskmanagement.prodplanmonitoringtask.ProdPlanMonitoringTaskRequestDto;
import com.shirohana.dto.prodplantaskmanagement.prodplanmonitoringtask.ProdPlanMonitoringTaskResponseDto;
import com.shirohana.dto.prodplantaskmanagement.prodplanmonitoringtask.filtered.ProdPlanMonitoringTaskFilterByPurposeCombinedResponseDto;
import com.shirohana.dto.prodplantaskmanagement.prodplanmonitoringtask.filtered.ProdPlanMonitoringTaskFilterByPurposeResponseDto;

import java.util.List;

public interface ProdPlanMonitoringTaskService {
    ProdPlanMonitoringTaskResponseDto createProdPlanMonitoringTask(ProdPlanMonitoringTaskRequestDto requestDto);
    ProdPlanMonitoringTaskResponseDto updateProdPlanMonitoringTask(Long id, ProdPlanMonitoringTaskRequestDto requestDto);
    ProdPlanMonitoringTaskResponseDto getProdPlanMonitoringTaskById(Long id);
    List<ProdPlanMonitoringTaskResponseDto> getAllProdPlanMonitoringTasks();
    List<ProdPlanMonitoringTaskResponseDto> getAllProdPlanMonitoringTasksByProdPlanId(Long prodPlanId, Long familyNameId);

    List<ProdPlanMonitoringTaskFilterByPurposeCombinedResponseDto> getAllProdPlanMonitoringTasksByProdPlanIdAndPurpose(
            Long prodPlanId, Long familyNameId, String purpose, Long farmId, Long plantationScheduleId, Long plantationPlanId, Long userId);
    void softDeleteProdPlanMonitoringTask(Long id);
}
