package com.shirohana.service.interfaces.prodplantaskmanagement.prodplantmaintenancetask;

import com.shirohana.dto.prodplantaskmanagement.prodplanmaintenancetask.ProdPlanMaintenanceTaskRequestDto;
import com.shirohana.dto.prodplantaskmanagement.prodplanmaintenancetask.ProdPlanMaintenanceTaskResponseDto;
import com.shirohana.dto.prodplantaskmanagement.prodplanmaintenancetask.filtered.ProdPlanMaintenanceTaskFilterByPurposeCombinedResponseDto;
import com.shirohana.dto.prodplantaskmanagement.prodplanmaintenancetask.filtered.ProdPlanMaintenanceTaskFilterByPurposeResponseDto;

import java.util.List;

public interface ProdPlanMaintenanceTaskService {
    ProdPlanMaintenanceTaskResponseDto createProdPlanMaintenanceTask(ProdPlanMaintenanceTaskRequestDto requestDto);
    ProdPlanMaintenanceTaskResponseDto updateProdPlanMaintenanceTask(Long id, ProdPlanMaintenanceTaskRequestDto requestDto);
    ProdPlanMaintenanceTaskResponseDto getProdPlanMaintenanceTaskById(Long id);
    List<ProdPlanMaintenanceTaskResponseDto> getAllProdPlanMaintenanceTasks();

    List<ProdPlanMaintenanceTaskResponseDto> getAllProdPlanMaintenanceTasksByProdPlanId(Long prodPlanId,Long familyNameId);

    List<ProdPlanMaintenanceTaskFilterByPurposeCombinedResponseDto> getAllProdPlanMaintenanceTasksByProdPlanIdAndPurpose(
            Long prodPlanId, Long familyNameId, String purpose, Long farmId, Long plantationScheduleId, Long plantationPlanId, Long userId);
    void softDeleteProdPlanMaintenanceTask(Long id);
}
