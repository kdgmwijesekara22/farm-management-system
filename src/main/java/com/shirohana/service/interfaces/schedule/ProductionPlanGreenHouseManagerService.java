package com.shirohana.service.interfaces.schedule;

import com.shirohana.dto.schedule.production_plan_green_house.ProductionPlanGHManagerRequestDto;
import com.shirohana.dto.schedule.production_plan_green_house.ProductionPlanGHManagerResponseDto;

import java.util.List;

public interface ProductionPlanGreenHouseManagerService {
    ProductionPlanGHManagerResponseDto saveProductionPlanGreenHouseManager(ProductionPlanGHManagerRequestDto requestDto);

    List<ProductionPlanGHManagerResponseDto> getProductionPlansByGreenHouseId(Long greenhouseId);
}
