package com.shirohana.service.interfaces.schedule;

import com.shirohana.dto.schedule.pshedule.FamilyResponseDto;
import com.shirohana.dto.schedule.pshedule.PlantationScheduleRequestDto;
import com.shirohana.dto.schedule.pshedule.PlantationScheduleResponseDto;
import com.shirohana.dto.schedule.psplan.PlantationPlanRequestDto;
import com.shirohana.dto.schedule.psplan.PlantationPlanResponseDto;

import java.util.List;
import java.util.Set;

public interface PlantationScheduleService {
    PlantationScheduleResponseDto saveSchedule(PlantationScheduleRequestDto plantationScheduleRequestDto);
    List<FamilyResponseDto> getAllFamilies();
    List<PlantationScheduleResponseDto> getAllPlantationSchedules(); // New method
    PlantationPlanResponseDto savePlantationPlan(PlantationPlanRequestDto plantationPlanRequestDto);
    List<PlantationPlanResponseDto> getAllPlantationPlans();  // New method to get all plantation plans
    List<PlantationPlanResponseDto> getAllPlansByPlantationScheduleId(Long plantationScheduleId);
    List<PlantationPlanResponseDto> getAllPlantationPlansByFarmIds(Set<Long> farmIds); // Add this method
    List<PlantationPlanResponseDto> getPlantationPlansByFarmId(Long farmId);
    boolean deletePlantationSchedule(Long id);
    boolean deletePlantationPlan(Long id);
}