package com.shirohana.service.interfaces.schedule;

import com.shirohana.dto.schedule.production_plan.ProductionPlanFarmManagerRequestDto;
import com.shirohana.dto.schedule.production_plan.ProductionPlanFarmManagerResponseDto;

import java.util.List;
import java.util.Set;

public interface ProductionPlanFarmManagerService {
    ProductionPlanFarmManagerResponseDto saveProductionPlanFarmManager(ProductionPlanFarmManagerRequestDto productionPlanFarmManagerRequestDto);

    List<ProductionPlanFarmManagerResponseDto> getAllProductionPlanFarmManager();

    List<ProductionPlanFarmManagerResponseDto> getAllProductionPlansByFarms(Set<Long> farmIds);
    List<ProductionPlanFarmManagerResponseDto> getAllProductionPlansByFarmId(Long farmId);
    List<ProductionPlanFarmManagerResponseDto> getAllPendingProdPlanFarmManagerByGreenHouseId(Long greenHouseId);
    boolean deleteProductionPlanFarmManager(Long id);
}
