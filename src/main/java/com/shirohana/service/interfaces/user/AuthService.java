package com.shirohana.service.interfaces.user;

public interface AuthService {
    String authenticate(String username, String password);
}
