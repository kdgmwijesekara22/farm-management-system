package com.shirohana.service.interfaces.user;

import com.shirohana.dto.user.CurrentUserDTO;
import com.shirohana.dto.user.FarmDetailDTO;
import com.shirohana.dto.user.UserDTO;
import com.shirohana.dto.user.UserDetailDTO;
import com.shirohana.entity.user.User;

import java.util.List;
import java.util.Optional;

public interface UserService {
    List<User> findAll();
    Optional<User> findById(Long id);
    Optional<User> findByIdString(String id);
    Optional<User> findByUsername(String username);
    User createUser(UserDTO userDto, User createdBy);
    Optional<User> updateUser(Long id, UserDTO userDto, User updatedBy);
    void deleteUser(Long id, User deletedBy);
    boolean isCurrentUserManagingLevelOne();
    List<String> findAllUserNames();
    CurrentUserDTO getCurrentUserDetails(User user);
    User getCurrentUser(); // Add this method
    void assignRolesToUser(Long userId, List<Long> roleIds);
    List<UserDetailDTO> getAllUserDetails();
    List<FarmDetailDTO> getCurrentUserFarms();
}
