package com.shirohana.service.interfaces.user;

import com.shirohana.dto.user.PermissionGroupDTO;
import com.shirohana.entity.user.PermissionGroup;
import java.util.List;

public interface PermissionGroupService {
    List<PermissionGroupDTO> getAllPermissionGroupsWithPrivileges();
}
