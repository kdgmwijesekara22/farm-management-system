package com.shirohana.service.interfaces.user;

import com.shirohana.entity.user.Privilege;
import java.util.List;
import java.util.Optional;

public interface PrivilegeService {
    List<Privilege> getAllPrivileges();
    Optional<Privilege> getPrivilegeById(Long id);
    Privilege createPrivilege(Privilege privilege);
    Optional<Privilege> updatePrivilege(Long id, Privilege privilegeDetails);
    boolean deletePrivilege(Long id);
}
