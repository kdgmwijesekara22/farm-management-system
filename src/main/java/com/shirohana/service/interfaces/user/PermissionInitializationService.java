package com.shirohana.service.interfaces.user;

public interface PermissionInitializationService {
    void init();
}
