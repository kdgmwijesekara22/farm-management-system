package com.shirohana.service.interfaces.user;

import com.shirohana.dto.user.RoleDTO;
import com.shirohana.entity.user.Role;

import java.util.List;
import java.util.Optional;

public interface RoleService {
    List<RoleDTO> getAllRoles();
    Optional<RoleDTO> getRoleById(Long id);
    RoleDTO createRole(RoleDTO roleDTO);
    Optional<RoleDTO> updateRole(Long id, Role roleDetails);
    boolean deleteRole(Long id);
    Optional<RoleDTO> assignPrivilegesToRole(Long id, List<Long> privilegeIds);
    RoleDTO assignFarmsToRole(Long roleId, List<Long> farmIds);  // New method

}
