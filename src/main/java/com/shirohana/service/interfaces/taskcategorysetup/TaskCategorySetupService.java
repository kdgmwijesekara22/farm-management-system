package com.shirohana.service.interfaces.taskcategorysetup;

import com.shirohana.dto.taskcategorysetup.TaskCategorySetupRequestDto;
import com.shirohana.dto.taskcategorysetup.TaskCategorySetupResponseDto;

import java.util.List;

public interface TaskCategorySetupService {

    TaskCategorySetupResponseDto createTaskCategorySetup(TaskCategorySetupRequestDto requestDto);
    TaskCategorySetupResponseDto updateTaskCategorySetup(Long id, TaskCategorySetupRequestDto requestDto);
    void deleteTaskCategorySetup(Long id);
    List<TaskCategorySetupResponseDto> getAllTaskCategorySetups();
    List<TaskCategorySetupResponseDto> getTaskCategorySetupsByTaskType(String taskType);
    TaskCategorySetupResponseDto getTaskCategorySetupById(Long id);
}
