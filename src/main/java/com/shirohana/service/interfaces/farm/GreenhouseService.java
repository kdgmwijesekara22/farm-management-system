package com.shirohana.service.interfaces.farm;

import com.shirohana.dto.farm.*;
import com.shirohana.entity.farm.Greenhouse;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface GreenhouseService {
    List<GreenhouseResponseDto> getAllGreenhousesByFarmId(Long farmId);
    GreenhouseWithBaysResponseDto getAllGreenhousesAndBaysByFarmId(Long farmId);

    GreenhouseResponseDto createGreenhouse(GreenhouseRequestDto greenhouseRequestDto);
    GreenhouseResponseDto updateGreenhouse(Long id, GreenhouseRequestDto greenhouseRequestDto);

    @Transactional
    void deleteGreenhouse(Long greenhouseId);
    List<Greenhouse> findByFarmId(Long farmId);

    List<GreenhouseResponseDto> getAllGreenhouses();

    GHBayAvailableResponseDto getAvailableGreenhousesByFarmId(GHBayBedCountRequestDto ghBayBedCountRequestDto);

}
