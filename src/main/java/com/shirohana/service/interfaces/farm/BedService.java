package com.shirohana.service.interfaces.farm;

import com.shirohana.dto.farm.BayWeekRequestDto;
import com.shirohana.dto.farm.BedAvailableResponseDto;
import com.shirohana.dto.farm.BedRequestDto;
import com.shirohana.dto.farm.BedResponseDto;
import com.shirohana.entity.farm.Bed;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface BedService {
    List<BedResponseDto> getAllBedsByBayId(Long bayId);
    BedResponseDto createBed(BedRequestDto bedRequestDto);
    BedResponseDto updateBed(Long id, BedRequestDto bedRequestDto);
    @Transactional
    void deleteBed(Long bedId);
    List<Bed> findByBayId(Long bayId);
    BedAvailableResponseDto getBedsByBayWeekAndYear(BayWeekRequestDto bayWeekRequestDto);
}
