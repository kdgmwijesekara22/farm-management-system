package com.shirohana.service.interfaces.farm;

import com.shirohana.dto.farm.FarmBayCountRequestDto;
import com.shirohana.dto.farm.FarmGreenHouseAvailableResponseDto;
import com.shirohana.dto.farm.FarmRequestDto;
import com.shirohana.dto.farm.FarmResponseDto;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface FarmService {
    List<FarmResponseDto> getAllFarms();
    FarmResponseDto createFarm(FarmRequestDto farmRequestDto);
    FarmResponseDto updateFarm(Long id, FarmRequestDto farmRequestDto);
    @Transactional
    void deleteFarm(Long farmId);
    FarmGreenHouseAvailableResponseDto getAvailableFarms(FarmBayCountRequestDto request);

}
