package com.shirohana.service.interfaces.farm;

import com.shirohana.dto.farm.*;
import com.shirohana.entity.farm.Bay;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface BayService {
    List<BayResponseDto> getAllBaysByGreenhouseId(Long greenhouseId);
    BayResponseDto createBay(BayRequestDto bayRequestDto);
    BayResponseDto updateBay(Long id, BayRequestDto bayRequestDto);
    @Transactional
    void deleteBay(Long bayId);
    List<Bay> findByGreenhouseId(Long greenhouseId);
    BayBedAvailableResponseDto getBayBedAvailability(BayBedCountRequestDto bayBedCountRequestDto);

}
