package com.shirohana.service.interfaces.plant;


import com.shirohana.dto.plant.PlantRequestDto;
import com.shirohana.dto.plant.PlantResponseDto;

import java.util.List;

public interface PlantService {
    PlantResponseDto createPlant(PlantRequestDto plantRequestDto);
    PlantResponseDto updatePlant(Long id, PlantRequestDto plantRequestDto);
    void deletePlant(Long id);
    PlantResponseDto getPlantByFamilyName(String familyName);
    List<PlantResponseDto> getAllPlants();
}
