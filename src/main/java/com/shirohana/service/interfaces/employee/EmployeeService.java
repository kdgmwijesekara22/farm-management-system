package com.shirohana.service.interfaces.employee;

import com.shirohana.dto.employee.EmployeeRequestDto;
import com.shirohana.dto.employee.EmployeeResponseDto;

import java.util.List;

public interface EmployeeService {
    EmployeeResponseDto createEmployee(EmployeeRequestDto employeeRequestDto);

    EmployeeResponseDto updateEmployee(Long id, EmployeeRequestDto employeeRequestDto);

    void deleteEmployee(Long id);

    EmployeeResponseDto getEmployeeById(Long id);

    EmployeeResponseDto getEmployeeByEmployeeGivenId(String employeeGivenId);

    List<EmployeeResponseDto> getAllEmployees();
}
