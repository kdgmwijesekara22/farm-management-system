package com.shirohana;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShirohanaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShirohanaApplication.class, args);
	}

}
